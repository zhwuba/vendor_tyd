#ifndef SPEAKER_CALIBRATION_H_
#define SPEAKER_CALIBRATION_H_

#ifdef __cplusplus
extern "C" {
#endif

extern bool  supportCalibration();
extern int   runCalibration();
extern float getCalibrationResult();

#ifdef __cplusplus
}
#endif

#endif // SPEAKER_CALIBRATION_H_