#define LOG_TAG "SpeakerCalibration_stub"
#include <android/log.h>

#include "speaker_calibration.h"

#define ALOGV(...) __android_log_print(ANDROID_LOG_VERBOSE,LOG_TAG,__VA_ARGS__)

bool supportCalibration()
{
    ALOGV("Speaker Calibration Stub !!!");
    return false;
}

int runCalibration()
{
    return 0;
}

float getCalibrationResult()
{
    return 0;
}
