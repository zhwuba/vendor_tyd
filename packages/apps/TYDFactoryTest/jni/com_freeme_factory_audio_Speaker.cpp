#define LOG_TAG "SpeakerCalibration"
#include <android/log.h>

#include <jni.h>

#include "speaker_calibration.h"

#define ALOGE(...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)


#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jboolean JNICALL
Java_com_freeme_factory_audio_Speaker_nativeSupportCalibration(JNIEnv* env, jobject thiz);

JNIEXPORT jint JNICALL
Java_com_freeme_factory_audio_Speaker_nativeRunCalibration(JNIEnv* env, jobject thiz);

JNIEXPORT jfloat JNICALL
Java_com_freeme_factory_audio_Speaker_nativeGetCalibrationResult(JNIEnv* env, jobject thiz);

#ifdef __cplusplus
}
#endif


JNIEXPORT jboolean JNICALL
Java_com_freeme_factory_audio_Speaker_nativeSupportCalibration(JNIEnv* env, jobject thiz)
{
    return supportCalibration();
}

JNIEXPORT jint JNICALL
Java_com_freeme_factory_audio_Speaker_nativeRunCalibration(JNIEnv* env, jobject thiz) {
    return runCalibration();
}

JNIEXPORT jfloat JNICALL
Java_com_freeme_factory_audio_Speaker_nativeGetCalibrationResult(JNIEnv* env, jobject thiz) {
    return getCalibrationResult();
}

