LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libjni_factory_speaker

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := \
    com_freeme_factory_audio_Speaker.cpp

LOCAL_CFLAGS := \
    -O3

LOCAL_SDK_VERSION := 21

LOCAL_LDLIBS := -llog

ifeq ($(strip $(FREEME_FACTORY_TEST_JNI_SPEAKER_EXMK)),)
  LOCAL_SRC_FILES += \
    speaker_calibration_stub.cpp
else
  $(info include $(FREEME_FACTORY_TEST_JNI_SPEAKER_EXMK))
  include $(FREEME_FACTORY_TEST_JNI_SPEAKER_EXMK)
endif

include $(BUILD_SHARED_LIBRARY)