package com.freeme.factory.storage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.StatFs;
import android.util.Log;
import android.widget.TextView;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;

public class Storage extends BaseTest {
    private static final String TAG = "Storage";

    private TextView mSdcardInfo;
    private TextView mSdcardExtInfo;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateStorageInfo();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sdcard);

        testActionCompleted(false);

        mSdcardInfo = (TextView) findViewById(R.id.sdcard_info);
        mSdcardExtInfo = (TextView) findViewById(R.id.sdcard_ext_info);

        mSdcardInfo.post(new Runnable() {
            @Override
            public void run() {
                updateStorageInfo();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        intentFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        intentFilter.addDataScheme("file");
        registerReceiver(mReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        unregisterReceiver(mReceiver);
        super.onStop();
    }

    @Override
    protected void testActionCompleted(boolean completed) {
        super.testActionCompleted(completed);
        if (completed) {
            switch (mTestMode) {
                case TestConfig.TEST_MODE_ALL:
                case TestConfig.TEST_MODE_AUTO:
                    testResultCommit(true);
                    break;
            }
        }
    }

    private void updateStorageInfo() {
        long testInternalSize = 0;
        long testExternalSize = 0;

        // Internal storage
        {
            StringBuilder intResult = new StringBuilder(getText(R.string.sdcard_label)).append(" : ");
            if (FreemeEnvironment.isInternalSdMounted()) {
                final String path = FreemeEnvironment.getInternalSdDirectory().getPath();
                final long availableSizeMB = getAvailableSize(path);
                final long totalSizeMB = getTotalSize(path);

                testInternalSize = totalSizeMB;

                intResult
                    .append(getText(R.string.sdcard_state_success)).append('\n')
                    .append("\t\t").append(getText(R.string.sdcard_totalsize)).append(totalSizeMB).append("MB").append('\n')
                    .append("\t\t").append(getText(R.string.sdcard_freesize)).append(availableSizeMB).append("MB");
            } else {
                intResult.append(getText(R.string.sdcard_state_fail));
                Log.d(TAG, "Internal sdcard fail:" + intResult.toString());
            }
            mSdcardInfo.setText(intResult.toString());
        }

        // External storage
        /*if (TestConfig.TEST_EN_STORAGE_SDCARD) */{
            StringBuilder extResult = new StringBuilder(getText(R.string.sdcard_ext_label)).append(" : ");
            if (FreemeEnvironment.isExternalSdMounted()) {
                final String path = FreemeEnvironment.getExternalSdDirectory().getPath();
                final long availableSizeMB = getAvailableSize(path);
                final long totalSizeMB = getTotalSize(path);

                testExternalSize = totalSizeMB;

                extResult
                    .append(getText(R.string.sdcard_state_success)).append('\n')
                    .append("\t\t").append(getText(R.string.sdcard_totalsize)).append(totalSizeMB).append("MB").append('\n')
                    .append("\t\t").append(getText(R.string.sdcard_freesize)).append(availableSizeMB).append("MB");

                mSdcardExtInfo.setText(extResult.toString());
            } else if (false) {
                extResult.append(getText(R.string.sdcard_state_fail));
                Log.d(TAG, "External sdcard fail:" + extResult.toString());
                mSdcardExtInfo.setText(extResult.toString());
            }
        }

        // Care about internal and/or external storage device state if exists.
        boolean passInternal = (testInternalSize > 0);
        boolean passExternal = (!TestConfig.TEST_EN_STORAGE_SDCARD)  // never exits
                || (TestConfig.TEST_EN_STORAGE_SDCARD && (testExternalSize > 0)); // exits and valid
        if (passInternal && passExternal) {
            testActionCompleted(true);
        } else if (!passExternal) {
            mSdcardExtInfo.setText(R.string.storage_sdcard_noexists_warning);
        }
    }

    public static long getAvailableSize(String path) {
        StatFs stat = new StatFs(path);
        long blockSize       = stat.getBlockSizeLong();
        long availableBlocks = stat.getAvailableBlocksLong();
        return (availableBlocks * blockSize) >> 20;
    }

    public static long getTotalSize(String path) {
        StatFs stat = new StatFs(path);
        long blockSize   = stat.getBlockSizeLong();
        long countBlocks = stat.getBlockCountLong();
        return (countBlocks * blockSize) >> 20;
    }
}
