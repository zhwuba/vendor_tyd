package com.freeme.factory.storage;

import java.io.File;
import java.util.Locale;

import android.app.ActivityThread;
import android.content.Context;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;

public class FreemeEnvironment extends Environment {
	private static final String TAG = "FreemeEnvironment";
	
	private static final File DefaultStorage = getExternalStorageDirectory();

    private static final byte[] sLock = new byte[0];
    
    private static StorageManager sStorageManager;
    
    private static String sExternalSdDir;
    private static String sInternalSdDir;
    
    private static final void ensureMountService() {
        if (sStorageManager == null) {
            synchronized (FreemeEnvironment.class) {
                if (sStorageManager == null) {
                    Context context = ActivityThread.currentApplication();
                    sStorageManager = (StorageManager) context
                            .getSystemService(Context.STORAGE_SERVICE);
                }
            }
        }
    }
    
    public static StorageVolume[] getVolumeList() {
        ensureMountService();
        if (sStorageManager == null) return new StorageVolume[0];
        
        return sStorageManager.getVolumeList();
    }
    
    public static String getVolumeState(String mountPoint) {
    	if (mountPoint == null) return MEDIA_UNKNOWN;
    	
    	ensureMountService();
        if (sStorageManager == null) return MEDIA_REMOVED;
        
    	return sStorageManager.getVolumeState(mountPoint);
    }
    
    private static void update() {
		StorageVolume[] volumes = getVolumeList();
        if (volumes != null) {
        	synchronized (sLock) {
        		sExternalSdDir = null;
            	sInternalSdDir = null;
            	
            	int count = volumes.length;
            	// Expected volumes count is 2, e.g INT-SDCard and EXT-SdCard
                if (count > 2)
                	count = 2;
                
                for (int i = 0; i < count; i++) {
                	String path = volumes[i].getPath();
                	
                	if (ignoreRemovableDevice(path)) {
                		continue;
                	}
                	
                	if (! MEDIA_MOUNTED.equals(volumes[i].getState())) {
                		continue;
                	}
                	
                    if (volumes[i].isRemovable() && (sExternalSdDir == null) ) {
                        sExternalSdDir = path;
                    } else if ((sInternalSdDir == null)) {
                        sInternalSdDir = path;
                    }
                }
			}
        }
    }
    
    private static boolean ignoreRemovableDevice(String mountPoint) {
    	String mountPoint_LC = mountPoint.toLowerCase(Locale.US);
    	// TODO: filter lists
    	return (   mountPoint_LC.contains("usb")
    			|| mountPoint_LC.contains("private")
    			);
    }
    
    // internal
    
    public static File getInternalSdDirectory() {
        update();
        
        if (sInternalSdDir != null)
            return new File(sInternalSdDir);
        
        return DefaultStorage;
    }

    public static String getInternalSdState() {
        update();
        
        return getVolumeState(sInternalSdDir);
    }

    public static boolean isInternalSdMounted() {
        return MEDIA_MOUNTED.equals(getInternalSdState());
    }
    
    // external
    
    public static File getExternalSdDirectory() {
        update();
        
        if (sExternalSdDir != null)
            return new File(sExternalSdDir);
        
        return DefaultStorage;
    }

    public static String getExternalSdState() {
        update();
        
        return getVolumeState(sExternalSdDir);
    }
    
    public static boolean isExternalSdMounted() {
        return MEDIA_MOUNTED.equals(getExternalSdState());
    }
}
