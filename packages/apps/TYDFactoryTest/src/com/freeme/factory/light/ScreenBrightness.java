package com.freeme.factory.light;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;

public class ScreenBrightness extends BaseTest implements View.OnClickListener {

    private boolean mBrightnessUp, mBrightnessDown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_backlight);

        Button display_lcd_on = (Button) findViewById(R.id.Display_lcd_on);
        display_lcd_on.setOnClickListener(this);

        Button display_lcd_off = (Button) findViewById(R.id.Display_lcd_off);
        display_lcd_off.setOnClickListener(this);

        testActionCompleted(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Display_lcd_on: {
                WindowManager.LayoutParams lp = getWindow().getAttributes();
                lp.screenBrightness = 1.0f;
                getWindow().setAttributes(lp);

                mBrightnessUp = true;
                if (mBrightnessUp && mBrightnessDown) {
                    testActionCompleted(true);
                }
            } break;
            case R.id.Display_lcd_off: {
                WindowManager.LayoutParams lp = getWindow().getAttributes();
                lp.screenBrightness = 0.2f;
                getWindow().setAttributes(lp);

                mBrightnessDown = true;
                if (mBrightnessUp && mBrightnessDown) {
                    testActionCompleted(true);
                }
            } break;
        }
    }
}
