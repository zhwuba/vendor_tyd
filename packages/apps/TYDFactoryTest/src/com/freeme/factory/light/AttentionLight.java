package com.freeme.factory.light;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;

public class AttentionLight extends BaseTest implements View.OnClickListener {
    private boolean mColorRed;
    private boolean mColorGreen;
    private boolean mColorBlue;

    private NotificationManager mNotificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attention_light);

        Button lightRed = (Button) findViewById(R.id.Light_red);
        lightRed.setOnClickListener(this);

        Button lightGreen = (Button) findViewById(R.id.Light_green);
        lightGreen.setOnClickListener(this);

        Button lightBlue = (Button) findViewById(R.id.Light_blue);
        lightBlue.setOnClickListener(this);

        testActionCompleted(false);

        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public void onClick(View v) {
        final int color;
        final int colorText;
        switch (v.getId()) {
            default:
            case R.id.Light_red:
                color = 0xFFFF0000;
                colorText = R.string.LED_red;
                mColorRed = true;
                break;
            case R.id.Light_green:
                color = 0xFF00FF00;
                colorText = R.string.LED_green;
                mColorGreen = true;
                break;
            case R.id.Light_blue:
                color = 0xFF0000FF;
                colorText = R.string.LED_blue;
                mColorBlue = true;
                break;
        }

        int id = (int) System.currentTimeMillis();

        NotificationChannel channel = new NotificationChannel(String.valueOf(id),
                "tydtest",NotificationManager.IMPORTANCE_DEFAULT);
        channel.enableLights(true);
        channel.setLightColor(color);

        mNotificationManager.createNotificationChannel(channel);

        Notification notification = new Notification.Builder(this)
                .setChannel(String.valueOf(id))
                .setContentTitle(getText(R.string.attentionlight_name))
                .setContentText(getText(colorText))
                .setSmallIcon(android.R.drawable.ic_notification_overlay)
                .setPriority(Notification.PRIORITY_HIGH)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .build();
        mNotificationManager.notify(0x111, notification);

        if (mColorRed && mColorGreen && mColorBlue) {
            testActionCompleted(true);
        }
    }
}