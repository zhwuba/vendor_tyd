package com.freeme.factory.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Debug;
import android.util.Log;

import java.util.HashMap;

public class SLog {
	private static final String TAG = "FactoryTest";
	
	public static final int VERBOSE = 2;
	public static final int DEBUG   = 3;
	public static final int INFO    = 4;
	public static final int WARN    = 5;
	public static final int ERROR   = 6;
	public static final int ASSERT  = 7;

	public static final int LOG_ID_MAIN   = 0;
	public static final int LOG_ID_RADIO  = 1;
	public static final int LOG_ID_EVENTS = 2;
	public static final int LOG_ID_SYSTEM = 3;

	private static final float ONE_MB = ((1 << 10) << 10); // 1MiByte = 1^20Byte

	public static boolean ENABLE = false;
	
	private SLog() {
	}
	
	public static void init(Context context) {
		if ((context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)
				== ApplicationInfo.FLAG_DEBUGGABLE) {
			ENABLE = true;
			Log.i(TAG, "SLog enabled by default");
		} else {
			ENABLE = false;
		}
	}

	public static void setEanbled(boolean enabled) {
		ENABLE = enabled;
	}
	
	public static HashMap<String, Object> getHashMap(String string, Object object) {
		HashMap<String, Object> logMessage = new HashMap<String, Object>();
		logMessage.put(string, object);
		return logMessage;
	}

	public static void getCaller() {
	    StackTraceElement[] stack = (new Throwable()).getStackTrace();
	    for (int i = 1; i < stack.length; ++i) {
	        StackTraceElement ste = stack[i];
	        d(ste.getClassName() + "." + ste.getMethodName() + "(...);");
	        d(i + "--" + ste.getMethodName());
	        d(i + "--" + ste.getFileName());
	        d(i + "--" + ste.getLineNumber());
	    }
	}
	
	public static void m() {
		if (ENABLE) {
			float maxMemory = Runtime.getRuntime().maxMemory() / ONE_MB;
			
			float totalMemory = Runtime.getRuntime().totalMemory() / ONE_MB;
			float freeMemory  = Runtime.getRuntime().freeMemory() / ONE_MB;
			float usedMemory  = totalMemory - freeMemory;
			
			float heap        = Debug.getNativeHeapSize() / ONE_MB;
			float allocOfHeap = Debug.getNativeHeapAllocatedSize() / ONE_MB;
			float freeOfHeap  = Debug.getNativeHeapFreeSize() / ONE_MB;
			
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			
			Log.println(INFO, TAG, "==== " + text + " ====");
			Log.println(INFO, TAG, (new StringBuilder())
							.append("VM MAX=").append(maxMemory)
							.append("MB, TOTAL=").append(totalMemory)
							.append("MB, FREE=").append(freeMemory)
							.append("MB, USED =").append(usedMemory).append("MB")
							.toString());
			Log.println(INFO, TAG, (new StringBuilder())
							.append("NATIVE HEAP=").append(heap)
							.append("MB, ALLOCATED=").append(allocOfHeap)
							.append("MB, FREE=").append(freeOfHeap).append("MB")
							.toString());
			Log.println(INFO, TAG, "====================================================");
		}
	}

	/// TODO: v
	
	public static int v(String tag, String msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(VERBOSE, tag, text + msg);
		}
		return 0;
	}
	
	public static int v() {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(VERBOSE, TAG, text);
		}
		return 0;
	}

	public static int v(String msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(VERBOSE, TAG, text + msg);
		}
		return 0;
	}

	public static int v(HashMap<String, Object> msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(VERBOSE, TAG, text + msg.toString());
		}
		return 0;
	}
	
	/// TODO: d
	public static int d(String tag, String msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(DEBUG, tag, text + msg);
		}
		return 0;
	}
	
	public static int d() {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(DEBUG, TAG, text);
		}
		return 0;
	}

	public static int d(String msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(DEBUG, TAG, text + msg);
		}
		return 0;
	}

	public static int d(HashMap<String, Object> msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(DEBUG, TAG, text + msg.toString());
		}
		return 0;
	}

	/// TODO: i
	
	public static int i(String tag, String msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(INFO, tag, text + msg);
		}
		return 0;
	}
	
	public static int i() {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(INFO, TAG, text);
		}
		return 0;
	}

	public static int i(String msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(INFO, TAG, text + msg);
		}
		return 0;
	}

	public static int i(HashMap<String, Object> msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(INFO, TAG, text + msg.toString());
		}
		return 0;
	}

	/// TODO: w
	
	public static int w(String tag, String msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(WARN, tag, text + msg);
		}
		return 0;
	}
	
	public static int w(String tag, String msg, Throwable tr) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(WARN, tag, text + msg
					+ '\n' + Log.getStackTraceString(tr));
		}
		return 0;
	}
	
	public static int w(String msg, Throwable tr) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(WARN, TAG, text + msg
					+ '\n' + Log.getStackTraceString(tr));
		}
		return 0;
	}
	
	public static int w(Throwable tr) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(WARN, TAG, text + '\n' + Log.getStackTraceString(tr));
		}
		return 0;
	}
	
	public static int w() {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(WARN, TAG, text);
		}
		return 0;
	}

	public static int w(String msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(WARN, TAG, text + msg);
		}
		return 0;
	}

	public static int w(HashMap<String, Object> msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[1].getFileName()).append(":")
					.append(ste[1].getLineNumber()).append(":")
					.append(ste[1].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(Log.WARN, TAG, text + msg.toString());
		}
		return 0;
	}
	
	/// TODO: e
	
	public static int e(String tag, String msg) {
		StackTraceElement[] ste = (new Throwable()).getStackTrace();
		String text = (new StringBuilder()).append("[")
				.append(ste[1].getFileName()).append(":")
				.append(ste[1].getLineNumber()).append(":")
				.append(ste[1].getMethodName()).append("()")
				.append("] oooooo ").toString();
		return Log.println(ERROR, tag, text + msg);
	}
	
	public static int e(String tag, String msg, Throwable tr) {
		StackTraceElement[] ste = (new Throwable()).getStackTrace();
		String text = (new StringBuilder()).append("[")
				.append(ste[1].getFileName()).append(":")
				.append(ste[1].getLineNumber()).append(":")
				.append(ste[1].getMethodName()).append("()")
				.append("] oooooo ").toString();
		return Log.println(ERROR, tag, text + msg
				+ '\n' + Log.getStackTraceString(tr));
	}
	
	public static int e(String msg, Throwable tr) {
		StackTraceElement[] ste = (new Throwable()).getStackTrace();
		String text = (new StringBuilder()).append("[")
				.append(ste[1].getFileName()).append(":")
				.append(ste[1].getLineNumber()).append(":")
				.append(ste[1].getMethodName()).append("()")
				.append("] oooooo ").toString();
		return Log.println(ERROR, TAG, text + msg
				+ '\n' + Log.getStackTraceString(tr));
	}
	
	public static int e(Throwable tr) {
		StackTraceElement[] ste = (new Throwable()).getStackTrace();
		String text = (new StringBuilder()).append("[")
				.append(ste[1].getFileName()).append(":")
				.append(ste[1].getLineNumber()).append(":")
				.append(ste[1].getMethodName()).append("()")
				.append("] oooooo ").toString();
		return Log.println(ERROR, TAG, text + '\n' + Log.getStackTraceString(tr));
	}
	
	public static int e() {
		StackTraceElement[] ste = (new Throwable()).getStackTrace();
		String text = (new StringBuilder()).append("[")
				.append(ste[1].getFileName()).append(":")
				.append(ste[1].getLineNumber()).append(":")
				.append(ste[1].getMethodName()).append("()")
				.append("] oooooo ").toString();
		return Log.println(ERROR, TAG, text);
	}

	public static int e(String msg) {
		StackTraceElement[] ste = (new Throwable()).getStackTrace();
		String text = (new StringBuilder()).append("[")
				.append(ste[1].getFileName()).append(":")
				.append(ste[1].getLineNumber()).append(":")
				.append(ste[1].getMethodName()).append("()")
				.append("] oooooo ").toString();
		return Log.println(ERROR, TAG, text + msg);
	}

	public static int e(HashMap<String, Object> msg) {
		StackTraceElement[] ste = (new Throwable()).getStackTrace();
		String text = (new StringBuilder()).append("[")
				.append(ste[1].getFileName()).append(":")
				.append(ste[1].getLineNumber()).append(":")
				.append(ste[1].getMethodName()).append("()")
				.append("] oooooo ").toString();
		return Log.println(ERROR, TAG, text + msg.toString());
	}
	
	/// TODO: wtf
	
	public static void wtf(String tag, String msg) {
		Log.wtf(tag, msg);
	}

	public static void wtf(String tag, String msg, Throwable throwable) {
		Log.wtf(tag, msg, throwable);
	}

	public static void wtf(String tag, Throwable throwable) {
		Log.wtf(tag, throwable);
	}
	
	/// TODO: Profile Time
	protected static int _time(String msg) {
		if (ENABLE) {
			StackTraceElement[] ste = (new Throwable()).getStackTrace();
			String text = (new StringBuilder()).append("[")
					.append(ste[2].getFileName()).append(":")
					.append(ste[2].getLineNumber()).append(":")
					.append(ste[2].getMethodName()).append("()")
					.append("] oooooo ").toString();
			return Log.println(DEBUG, TAG, text + msg);
		}
		return 0;
	}
	
	private static long _time = -1;
	public static void timeProf(int mode/* 0:start, 1:end, 2:restart*/) {
		switch (mode) {
		case 0:
			_time = Debug.threadCpuTimeNanos();
			break;
		case 1:
			long _dur1 = Debug.threadCpuTimeNanos() - _time;
			_time("Time-prof : " + (_dur1/1000/1000) + " ms");
			_time = -1;
			break;
		case 2:
			long _dur2 = Debug.threadCpuTimeNanos() - _time;
			_time("Time-prof : " + (_dur2/1000/1000) + " ms");
			_time = Debug.threadCpuTimeNanos();
			break;
		}
	}
}
