package com.freeme.factory.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import android.os.FileUtils;
import android.text.TextUtils;
import android.util.Log;

/**
 * DESCRIPTION: This class provides utility methods to handle files, like
 * copying and deleting directories sub-trees.
 * 
 * USAGE: See public methods
 */
public class FileUtilEx {
	private static final String TAG = FileUtilEx.class.getSimpleName();
	private static final int BUFFER_SIZE = 4096;
	
    public static void addNoMedia(String path) {
        File dir = new File(path);
        if (dir.isDirectory()) {
        	try {
        		new File(dir, ".nomedia").createNewFile();
        	} catch (IOException e) {
        		//Do nothing.
        	}
        }
    }

    /**
     * Return the File Size in Bytes.
     * 
     * @param root The root File, it can be a directory
     * @return The size of the file in bytes
     * @throws IOException
     */
    public static int getFileSize(File root) throws IOException {
        int size = 0;
        if (root.isDirectory()) {
            for (File child : root.listFiles()) {
                size += getFileSize(child);
            }
        } else if (root.isFile()) {
            FileInputStream fis = new FileInputStream(root);
            int available;
            try {
                available = fis.available();
            } finally {
                try {
                    fis.close();
                } catch (IOException e) {
                    //Do thing.
                }
            }
            size = available;
        }
        return size;
    }
    
    public static String getExtension(File file) {
        if (file == null) {
        	return "";
        }
        return getExtension(file.getName());
    }

    /**
     * getExtension(String fileName)
     *
     * @param fileName
     *           returns the extension of a given file. "extension" here means
     *           the final part of the string after the last dot.
     *
     * @return String containing the extension
     */
    public static String getExtension(String fileName) {
        if (fileName != null) {
	        int i = fileName.lastIndexOf(".") + 1;
	        return (i == 0) ? "" : fileName.substring(i);
        } else {
        	return "";
        }
    }

    public static String getFileName(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
        	return "";
        }
        int index = filePath.lastIndexOf(File.separator);
        if (index > -1) {
        	filePath = filePath.substring(index + 1);
        }
        return filePath;
    }

    public static String getFileTitle(File file) {
        if (file == null) {
            return "";
        }
        return getFileTitle(file.getName());
    }

    public static String getFileTitle(String fileName) {
        if (TextUtils.isEmpty(fileName)) {
        	return "";
        }
        int index = fileName.lastIndexOf(".");
        if (index > -1) {
        	fileName = fileName.substring(0, index);
        }
        return fileName;
    }

    public static String getParentFolderPath(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
        	return "";
        }
        int index = filePath.lastIndexOf(File.separator);
        if (index > -1) {
        	filePath = filePath.substring(0, index);
        }
        return filePath;
    }
    
    /**
     * Calculate the canonical (an absolute filename without "\.\" and "\..\")
     * that describe the file described by the absoluteFilename.
     * @param absoluteFilename a file name that describe the full path of the file to use.
     * @return the canonical File objecta
     */
    public static File getCanonicalFile(String absoluteFilename) {
        return getCanonicalFile(new File(absoluteFilename));
    }

    /**
     * Calculate the canonical (an absolute filename without "\.\" and "\..\")
     * that describe the file described by the given location and filename.
     * @param location the directory of the file to be used
     * @param filename (or a relative filename) of the file to be used
     * @return the canonical File objecta
     */
    public static File getCanonicalFile(File location, String filename) {
        return getCanonicalFile(new File(location, filename));
    }

    /**
     * Calculate the canonical (an absolute filename without "\.\" and "\..\")
     * that describe the given file.
     * @param aFile the file whose cannonical path will be calculated
     * @return the canonical File objecta
     */
    public static File getCanonicalFile(File aFile) {
        File f = null;

        try {
            f = aFile.getCanonicalFile();
        } catch (IOException e) {
            // this should never happens
            Log.e(TAG, "FileUtil.getCanonicalFile: IOException e", e); //$NON-NLS-1$

            StringBuffer sb = new StringBuffer();
            
            // since it's not possible to read from filesystem, return a File using String          
            String filename = aFile.getAbsolutePath();
            StringTokenizer st = new StringTokenizer(filename, File.separator);
            while (st.hasMoreTokens()) {
                String token = (String) st.nextElement();

                if (token.equals("..")) {
                    int lastDirIndex = sb.lastIndexOf(File.separator);

                    // do not go back currently on the root directory
                    if (lastDirIndex > 2) {
                        sb.delete(lastDirIndex, sb.length());
                    }
                } else if (!token.equals(".")) {
                    if (sb.length() > 0) {
                        sb.append(File.separator);
                    }
                    
                    sb.append(token);

                    if (token.endsWith(":")) {
                        sb.append(File.separator);
                    }
                }
            }
            
            f = new File(sb.toString());
        }
        return f;
    }
    
    /**
     * Get the list of all File objects that compose the path to the given File
     * object
     *
     * @param aFile
     *           the file whose path must be retrieved.
     * @return a List with all the File objects that compose the path to the
     *         given File object.
     */
    public static List<File> getFilesComposingPath(File aFile) {
        List<File> fileList;

        if (aFile == null) {
            fileList = new ArrayList<File>();
        } else {
            fileList = getFilesComposingPath(aFile.getParentFile());
            fileList.add(aFile);
        }

        return fileList;
    }
    
    /**
     * Retrieve the relative filename to access a targetFile from a homeFile
     * parent directory. Notice that to actualy use a relative File object you
     * must use the following new File(homeDir, relativeFilename) because using
     * only new File(relativeFilename) would give you a file whose directory is
     * the one set in the "user.dir" property.
     *
     * @param homeDir
     *           the directory from where you want to access the targetFile
     * @param targetFile
     *           the absolute file or dir that you want to access via relative
     *           filename from the homeFile
     * @return the relative filename that describes the location of the
     *         targetFile referenced from the homeFile dir
     * @throws IOException
     */
    public static String getRelativeFilename(File homeDir, File targetFile) throws IOException {
        StringBuffer relativePath = new StringBuffer();

        List<File> homeDirList = getFilesComposingPath(getCanonicalFile(homeDir));
        List<File> targetDirList = getFilesComposingPath(getCanonicalFile(targetFile));

        if (homeDirList.size() == 0) {
            Log.i(TAG, "Home Dir has no parent."); //$NON-NLS-1$
        }

        if (targetDirList.size() == 0) {
        	Log.i(TAG, "Target Dir has no parent."); //$NON-NLS-1$
        }

        // get the index of the last common directory between sourceFile and
        // targetFile
        int commonIndex = -1;

        for (int i = 0; (i < homeDirList.size()) && (i < targetDirList.size()); i++) {
            File aHomeDir = homeDirList.get(i);
            File aTargetDir = targetDirList.get(i);

            if (aHomeDir.equals(aTargetDir)) {
                commonIndex = i;
            } else {
                break;
            }
        }

        // return from all remaining directories of the homeFile
        for (int i = commonIndex + 1; i < homeDirList.size(); i++) {
            relativePath.append(".."); //$NON-NLS-1$
            relativePath.append(File.separatorChar);
        }

        // enter into all directories of the target file
        // stops when reachs the file name and extension
        for (int i = commonIndex + 1; i < targetDirList.size(); i++) {
            File targetDir = targetDirList.get(i);
            relativePath.append(targetDir.getName());

            if (i != (targetDirList.size() - 1)) {
                relativePath.append(File.separatorChar);
            }
        }

        return relativePath.toString();
    }
    
    /**
     * Return a list of file absolute paths under "baseDir" and under its subdirectories,
     * recursively.
     *
     * @param baseDirToList
     *           A string that represents the BaseDir to initial search.
     * @return A List of filepaths of files under the "baseDir".
     * @throws IOException
     *            If the "baseDir" can not be read.
     */
    public static List<String> listFilesRecursively(String baseDirToList) throws IOException {
        File baseDirToListFiles = new File(baseDirToList);
        return listFilesRecursively(baseDirToListFiles);
    }

    /**
     * Return a list of file absolute paths under "baseDir" and under its subdirectories,
     * recursively.
     *
     * @param baseDirToList
     *           A file object that represents the "baseDir".
     * @return A List of filepaths of files under the "baseDir".
     * @throws IOException
     *            If the "baseDir" can not be read.
     */
    public static List<String> listFilesRecursively(File baseDirToList) throws IOException {
        if (baseDirToList.exists() && baseDirToList.isDirectory() && baseDirToList.canRead()) {
        	List<String> listOfFiles = new ArrayList<String>();
        	
            File[] children = baseDirToList.listFiles();
            for (File child : children) {
                if (child.isFile()) {
                    listOfFiles.add(child.getAbsolutePath());
                } else {
                    List<String> temporaryList = listFilesRecursively(child);
                    listOfFiles.addAll(temporaryList);
                }
            }
            
            return listOfFiles;
        } else {
            String errorMessage = "";
            if (! baseDirToList.exists()) {
                errorMessage = "The base dir does not exist.";
            } else if (! baseDirToList.isDirectory()) {
                errorMessage = baseDirToList.getName() + "is not a directory.";
            } else if (! baseDirToList.canRead()) {
                errorMessage = "Cannot fread from " + baseDirToList.getName() + ".";
            }
            throw new IOException("Error listing files: " + errorMessage);
        }
    }
    
    /**
     * Copy full list of contents from a directory to another. The source
     * directory is not created within the target one.
     *
     * @param fromDir
     *           Source directory.
     * @param toDir
     *           Target directory.
     *           
     * @param IOException if I/O occurs         
     */
    public static void copyDir(File fromDir, File toDir) throws IOException {
        if ((fromDir != null) && fromDir.isDirectory() && fromDir.canRead()
        		&& (toDir != null) && toDir.isDirectory() && toDir.canWrite()) {
            for (File child : fromDir.listFiles()) {
                if (child.isFile()) {
                    copyFile(child, new File(toDir, child.getName()));
                } else {
                    // create directory and copy its children recursively
                    File newDir = new File(toDir.getAbsolutePath(), child.getName());
                    newDir.mkdir();
                    copyDir(child, newDir);
                }
            }

            Log.i(TAG, "The directory " + fromDir.getName() + " was successfully copied to " //$NON-NLS-1$ //$NON-NLS-2$
                    + toDir.getName() + "."); //$NON-NLS-1$

        } else {
            //error detected 
            String errorMessage = ""; //$NON-NLS-1$
            if (fromDir == null) {
                errorMessage = "Null pointer for source directory."; //$NON-NLS-1$
            } else {
                if (! fromDir.isDirectory()) {
                    errorMessage = fromDir.getName() + " is not a directory."; //$NON-NLS-1$
                } else {
                    if (! fromDir.canRead()) {
                        errorMessage = "Cannot read from " + fromDir.getName() + "."; //$NON-NLS-1$ //$NON-NLS-2$
                    } else {
                        if (toDir == null) {
                            errorMessage = "Null pointer for destination directory."; //$NON-NLS-1$
                        } else {
                            if (! toDir.isDirectory()) {
                                errorMessage = toDir.getName() + " is not a directory."; //$NON-NLS-1$
                            } else {
                                if (!toDir.canWrite()) {
                                    errorMessage = "Cannot write to" + toDir.getName() + "."; //$NON-NLS-1$ //$NON-NLS-2$
                                }
                            }
                        }
                    }
                }
            }
            Log.e(TAG, errorMessage);
            throw new IOException("Error copying directory: " + errorMessage); //$NON-NLS-1$
        }
    }

    /**
     * Copies the source file to the given target.
     *
     * @param source -
     *           the absolute path of the source file.
     * @param target -
     *           the absolute path of the target file.
     */
    public static void copyFile(File source, File target) throws IOException {
        copyFile(source.getAbsolutePath(), target.getAbsolutePath());
    }

    /**
     * Copies the source file to the given target.
     *
     * @param source -
     *           the absolute path of the source file.
     * @param target -
     *           the absolute path of the target file.
     */
    private static void copyFile(String source, String target) throws IOException {
        FileChannel sourceFileChannel = null;
        FileChannel targetFileChannel = null;
        FileInputStream sourceFileInStream = null;
        FileOutputStream targetFileOutStream = null;
        try {
            sourceFileInStream = new FileInputStream(source);
            sourceFileChannel = sourceFileInStream.getChannel();
            targetFileOutStream = new FileOutputStream(target);
            targetFileChannel = targetFileOutStream.getChannel();
            targetFileChannel.transferFrom(sourceFileChannel, 0, sourceFileChannel.size());
            Log.i(TAG, "The file " + source + " was successfully copied to " + target + "."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        } catch (IOException e) {
        	Log.e(TAG, "Error copying file" + source + "to " + target + "."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            throw e;
        } finally {
            try {
                if (sourceFileChannel != null) {
                    sourceFileChannel.close();
                }
            } catch (IOException e) {
            	Log.e(TAG, "Error closing file " + source + "."); //$NON-NLS-1$ //$NON-NLS-2$
                throw e;
            }

            try {
                if (targetFileChannel != null) {
                    targetFileChannel.close();
                }
            } catch (IOException e) {
            	Log.e(TAG, "Error closing file" + target + "."); //$NON-NLS-1$ //$NON-NLS-2$
                throw e;
            }

            try {
                if (sourceFileInStream != null) {
                    sourceFileInStream.close();
                }
            } catch (IOException e) {
                Log.e(TAG, "Error closing file" + source + "."); //$NON-NLS-1$ //$NON-NLS-2$
                throw e;
            }

            try {
                if (targetFileOutStream != null) {
                    targetFileOutStream.close();
                }
            } catch (IOException e) {
            	Log.e(TAG, "Error closing file" + target + "."); //$NON-NLS-1$ //$NON-NLS-2$
                throw e;
            }
        }
    }

    /**
     * Copy the input stream to the output stream
     * @param inputStream
     * @param outputStream
     * @throws IOException
     */
    public static void copyStreams(InputStream inputStream, OutputStream outputStream)
            throws IOException {
    	int length;
        byte[] buffer = new byte[BUFFER_SIZE];
        while ((length = inputStream.read(buffer)) >= 0) {
            outputStream.write(buffer, 0, length);
        }
    }
    
    /**
     * This method is responsible to copy informed source file to informed
     * target.
     * 
     * @param sourceFile
     * @param targetFile
     * @throws IOException
     */
    public static void copy(File sourceFile, File targetFile) throws IOException {
        OutputStream outputStream = new FileOutputStream(targetFile);
        InputStream inputStream = new FileInputStream(sourceFile);
        try {
            int length;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((length = inputStream.read(buffer)) >= 0) {
                outputStream.write(buffer, 0, length);
            }
        } catch (IOException e) {
            throw new IOException("Error copying file:" + sourceFile.getAbsolutePath() + //$NON-NLS-1$
                    " to " + targetFile.getAbsolutePath()); //$NON-NLS-1$
        } finally {
            outputStream.close();
            inputStream.close();
        }
    }

    public static boolean deleteDir(File fileOrFolder) {
        boolean isSuccess = true;
        if (fileOrFolder.isDirectory()) {
        	String[] children = fileOrFolder.list();
            if (children != null) {
            	for (int i = 0; i < children.length; i++)
                    if (!deleteDir(new File(fileOrFolder, children[i])))
                    	isSuccess = false;
            } else {
            	return false;
            }
        }
        if (!fileOrFolder.delete()) {
        	isSuccess = false;
        }
        return isSuccess;
    }
    
    /**
     * Delete the specified file, recursively as necessary.
     * 
     * @param file The file to delete
     * @throws IOException 
     */
    public static void delete(File file) throws IOException {
        if (file.exists()) {
            if (file.isDirectory()) {
            	deleteDirRecursively(file);
            } else {
            	deleteFile(file);
            }
        }
    }

    /**
     * Delete the specified file, recursively as necessary.
     * 
     * @param fileName The file to delete
     * @throws IOException 
     */
    public static void delete(String fileName) throws IOException {
        delete(new File(fileName));
    }
    
    /**
     * This method deletes the directory, all files and all subdirectories under
     * it. If a deletion fails, the method stops attempting to delete and
     * returns false.
     *
     * @param directory
     *           The directory to be deleted
     * @return Returns true if all deletions were successful. If the directory
     *         doesn't exist returns false.
     * @throws IOException
     *            When the parameter isn't a directory
     */
    public static boolean deleteDirRecursively(File directory) throws IOException {
        String dirName = "";

        boolean success = true;

        if (directory.exists()) {
            if (directory.isDirectory()) {
                dirName = directory.getName();
                File[] children = directory.listFiles();

                for (File element : children) {
                    if (element.isFile()) {
                        success &= element.delete();
                    } else {
                        success &= deleteDirRecursively(element);
                    }
                }

                success &= directory.delete();
            } else {
                throw new IOException(directory.getName() + " is not a diretory.");
            }
        }
        //else {
        //	directory is not exist, so is successful.
        //}

        if ((success) && (! dirName.equals(""))) {
            Log.i(TAG, "The directory " + dirName + "was successfully deleted.");
        }

        return success;
    }

    /**
     * Delete a single file from the filesystem.
     *
     * @param fileToDelete
     *           A <code>File</code> object representing the file to be
     *           deleted.
     * @throws IOException
     *            if any problem occurs deleting the file.
     */
    public static void deleteFile(File fileToDelete) throws IOException {
        if ((fileToDelete != null) && fileToDelete.exists() && fileToDelete.isFile()
                && fileToDelete.canWrite()) {
            fileToDelete.delete();
            Log.i(TAG, "The file " + fileToDelete.getName() + "was successfully deleted.");
        } else {
            String errorMessage = "";
            if (fileToDelete == null) {
                errorMessage = "Null pointer for file to delete.";
            } else if (! fileToDelete.exists()) {
                errorMessage = "The file " + fileToDelete.getName() + " does not exist.";
            } else if (! fileToDelete.isFile()) {
                errorMessage = fileToDelete.getName() + " is not a file.";
            } else if (! fileToDelete.canWrite()) {
                errorMessage = "Cannot write to " + fileToDelete.getName();
            }
            Log.e(TAG, errorMessage);
            throw new IOException("Cannot delete file: " + errorMessage);
        }
    }

    /**
     * Delete a list of files from the filesystem.
     *
     * @param filesToDelete
     *           A list of <code>File</code> objects representing the files
     *           to be deleted.
     * @throws IOException
     *            if any problem occurs deleting the files.
     */
    public static void deleteFilesOnList(List<File> filesToDelete) throws IOException {
        for (File element : filesToDelete) {
            if (element.exists()) {
                deleteFile((element));
            }
        }
    }

    /**
     * This method creates the specified directory.
     * 
     * @param directory The directory to create.
     * @throws IOException
     */
    public static void mkdir(String directory) throws IOException {
        File f = new File(directory);
        if (f.exists()) {
            if (f.isFile()) {
                throw new IOException("Error creating directory:" + directory); //$NON-NLS-1$
            }
        } else {
            if (! f.mkdirs()) {
                throw new IOException("Error creating directory:" + directory); //$NON-NLS-1$
            }
        }
    }
    
    public static boolean mkdirs(File directory, int mode, int uid, int gid) {
    	try {
    		mkdir(directory.getPath());
    		FileUtils.setPermissions(directory.getPath(), mode, uid, gid);
    		return true;
    	} catch (IOException e) {
    		return false;
    	}
    }

    /**
     * This method normalize a directory path.
     * 
     * @param folder Full path to a directory
     * @return The normalized path.
     */
    public static String normalizePath(String folder) {
        return folder.endsWith(File.separator) ? folder : folder + File.separator;
    }
    
    /**
     * Checks if a File object can be read
     * 
     * @param file the File object
     * @return true if the File object can be read or false otherwise
     */
    public static boolean canRead(File file) {
        boolean canRead = false;

        if ((file != null) && file.exists()) {
            FileInputStream fis = null;
            try {
                if (file.isFile()) {
                    fis = new FileInputStream(file);
                    fis.read();
                    canRead = true;
                } else {
                    String[] children = file.list();

                    if (children != null) {
                        canRead = true;
                    }
                }
            } catch (Exception e) {
                // Do nothing. canRead is false already
            } finally {
                try {
                    if (fis != null) {
                        fis.close();
                    }
                } catch (IOException e) {
                    // Do nothing
                }
            }
        }
        return canRead;
    }

    /**
     * Checks if a File object can be written
     * 
     * @param file the File object
     * @return true if the File object can be written or false otherwise
     */
    public static boolean canWrite(File file) {
        boolean canWrite = false;

        if (file != null) {
            FileOutputStream fos = null;
            try {
                if (! file.exists()) {
                    canWrite = file.createNewFile();

                    if (canWrite)
                    {
                        file.delete();
                    }
                } else if (file.isDirectory()) {
                    File tempFile = File.createTempFile("StudioForAndroidFSChecking", null, file); //$NON-NLS-1$
                    if (tempFile.exists()) {
                        canWrite = true;
                        tempFile.delete();
                    }
                } else if (file.isFile()) {
                    fos = new FileOutputStream(file);
                    fos.getFD();
                    canWrite = true;
                }
            } catch (Exception e) {
                // Do nothing. canWrite is false already
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        // Do nothing
                    }
                }
            }
        }
        return canWrite;
    }
    
    /**
     * Given a directory descriptor represented by a {@link File}, creates it
     * only if it does not exist. In case it does, try to create another
     * one with its name plus "-1". If it does exists, try to create it with
     * its name plus "+2", and so on... 
     * <br>
     * Note that the directory is not fisically created. To do so, on must
     * use the method {@link File#mkdir()}.
     * 
     * @param directory Directory to be created.
     * 
     * @return Returns the created directory as a {@link File}.
     */
    public static File createUniqueDirectoryDescriptor(File directory) {
        if (directory.exists()) {
            boolean exists = true;
            int counter = 1;
            String rootPath = directory.getAbsolutePath();
            while (exists) {
                directory = new File(rootPath + "-" + counter); //$NON-NLS-1$
                exists = directory.exists();
                counter++;
            }
        }
        return directory;
    }
    
    /**
     * Opens the stream;
     *
     * @param stream File Stream
     *
     * @return StringBuffer with the file content
     *
     * @throws IOException
     */
    public static StringBuffer openFile(InputStream stream) throws IOException {
        InputStreamReader streamReader = null;
        StringBuffer fileBuffer = new StringBuffer();
        BufferedReader reader = null;
        try {
            streamReader = new InputStreamReader(stream);
            reader = new BufferedReader(streamReader);
            char[] buffer = new char[1024];
            
            int line;
            while ((line = reader.read(buffer)) > 0) {
                fileBuffer.append(buffer, 0, line);
            }
        } finally {
            if (streamReader != null) {
                try {
                    streamReader.close();
                } catch (Exception e) {
                    //Do nothing.
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    //Do nothing.
                }
            }
        }

        return fileBuffer;
    }

    /**
     * Reads a file into a string array
     * 
     * @param filename The file name
     * @return The file contents as a string array
     * @throws IOException 
     */
    public static String[] readFileAsArray(String filename) throws IOException {
        LinkedList<String> file = new LinkedList<String>();
        String[] lines = new String[0];
        String line;
        FileReader reader = null;
        LineNumberReader lineReader = null;

        try {
            reader = new FileReader(filename);
            lineReader = new LineNumberReader(reader);

            while ((line = lineReader.readLine()) != null) {
                file.add(line);
            }

            lines = new String[file.size()];
            lines = file.toArray(lines);
        } finally {
            try {
                lineReader.close();
                reader.close();
            } catch (Exception e) {
                // Do nothing
            }
        }

        return lines;
    }
}