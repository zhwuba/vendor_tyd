package com.freeme.factory.nfc;

import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.widget.TextView;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;

public class Nfc extends BaseTest {
    public static final String TAG = "Nfc";

    private TextView mTextNfc;

    private NfcAdapter mNfcAdapter;
    private PendingIntent mPendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nfc);

        mTextNfc = (TextView) findViewById(R.id.nfc_info);
        testActionCompleted(false);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        mPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, getClass())
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),
                0);

        if (mNfcAdapter == null) {
            mTextNfc.setText(R.string.nfc_failed);
        } else if (!mNfcAdapter.isEnabled() && !mNfcAdapter.enable()) {
            mTextNfc.setText(R.string.nfc_failed);
        } else {
            mTextNfc.setText(R.string.nfc_hint);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mNfcAdapter != null) {
            mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mNfcAdapter != null) {
            mNfcAdapter.disableForegroundDispatch(this);
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        final String action = intent.getAction();
        if (action != null) {
            switch (action) {
                case NfcAdapter.ACTION_TAG_DISCOVERED:
                case NfcAdapter.ACTION_TECH_DISCOVERED:
                case NfcAdapter.ACTION_NDEF_DISCOVERED: {
                    String id = getNfcId(intent);
                    if (id != null) {
                        testActionCompleted(true);
                        mTextNfc.setText(getString(R.string.nfc_ok) + id);
                    } else {
                        mTextNfc.setText(R.string.nfc_failed);
                    }
                } break;
            }
        }
    }

    private static String getNfcId(Intent intent) {
        Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        byte[] id = tag.getId();

        StringBuilder builder = new StringBuilder();
        final int N = id.length;
        for (int i = 0; i < N; i++) {
            builder.append(Byte.toHexString(id[i], true));
        }
        return builder.toString();
    }
}
