package com.freeme.factory;

import android.app.Application;

import com.freeme.factory.config.TestConfig;
import com.freeme.factory.config.TestResult;
import com.freeme.factory.utils.SLog;

public class FactoryApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SLog.init(this);
        TestConfig.getInstance().init(this);
        TestResult.getInstance().create(this);
    }
}
