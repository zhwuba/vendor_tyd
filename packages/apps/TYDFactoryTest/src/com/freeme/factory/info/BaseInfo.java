package com.freeme.factory.info;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.freeme.factory.R;

public class BaseInfo extends PreferenceActivity {
    private PreferenceScreen mPreferenceScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPreferenceScreen = getPreferenceManager().createPreferenceScreen(this);
        setPreferenceScreen(mPreferenceScreen);
    }

    protected void addInfoItem(InfoItemWrapper item) {
        Preference preference=new Preference(this);
        preference.setTitle(item.getTitle());
        preference.setSummary(item.getSummary());
        mPreferenceScreen.addPreference(preference);
    }

    public static class InfoItemWrapper {
        private String mTitle;
        private String mSummary;

        public InfoItemWrapper(String title, String summary){
            mTitle = title;
            mSummary = summary;
        }

        public String getTitle(){
            return mTitle;
        }

        public String getSummary() {
            return mSummary;
        }
    }
}
