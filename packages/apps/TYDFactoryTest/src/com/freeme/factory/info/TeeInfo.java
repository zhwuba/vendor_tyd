package com.freeme.factory.info;

import android.util.Log;
import android.os.Bundle;
import android.os.SystemProperties;
import com.freeme.factory.config.FeatureOption;

/*
    show TEE info
*/

public class TeeInfo extends BaseInfo {

    private static final String TAG    = "TeeInfo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateInfoItem();
    }

    private void updateInfoItem() {
        if (FeatureOption.FREEME_BEANPOD_TEE) {
            addInfoItem(new InfoItemWrapper(
                    "[TEE OS Staus]:",
                    getTeeOsStaus()));
        }
        addInfoItem(new InfoItemWrapper(
                "[TEE Active Staus]:",
                getActiveStaus()));

        addInfoItem(new InfoItemWrapper(
                "[GoogleKey Active Staus]:",
                getGoogleKeyStaus()));
    }

    private String getActiveStaus(){
        String activeStaus = "";
        String activeStausEx = "";
        if (FeatureOption.FREEME_BEANPOD_TEE) {
            activeStaus = SystemProperties.get("soter.teei.vold.decrypt");//for 80O1
            activeStausEx = SystemProperties.get("vendor.soter.teei.thh.init");
        } else if (FeatureOption.FREEME_TRUSTKERNEL_TEE) {
            activeStausEx = SystemProperties.get("trustkernel.state");
        }
        Log.i(TAG, "activeStaus == " + activeStaus + ",activeStausEx =="+activeStausEx);
        if (activeStausEx.equals("ACTIVE") || activeStausEx.equals("good")) {
            return activeStausEx;
        } else if (activeStausEx.equals("UNACTIVE")) {
            return activeStausEx;
        }
        if (activeStaus.equals("OK")) {
            return activeStaus;
        }
        return "UNACTIVE";
    }

    private String getTeeOsStaus(){
        String teeOsStaus = SystemProperties.get("vendor.soter.teei.init");
        Log.i(TAG, "teeOsStaus == " + teeOsStaus);
        if(teeOsStaus.equals("")){
            teeOsStaus = "NOT_SUPPORT";
        }
        return teeOsStaus;
    }

    private String getGoogleKeyStaus(){
        String activeStaus = "";
        if (FeatureOption.FREEME_BEANPOD_TEE) {
            activeStaus = SystemProperties.get("vendor.soter.teei.googlekey.status");
        } else if (FeatureOption.FREEME_TRUSTKERNEL_TEE) {
            activeStaus = SystemProperties.get("trustkernel.state");
        }
        Log.i(TAG, "activeStaus == " + activeStaus);
        if (activeStaus.equals("")) {
            activeStaus = "UNACTIVE";
        }
        return activeStaus;
    }

}

