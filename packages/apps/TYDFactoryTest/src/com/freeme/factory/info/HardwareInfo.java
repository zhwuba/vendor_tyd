package com.freeme.factory.info;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

public class HardwareInfo extends BaseInfo {
    private static final String HW_INFO_INTERFACE = "/sys/hwm_info/hw_module_info";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateHwInfo(getHwInfos());
    }

    private void updateHwInfo(String infos) {
        String[] items = infos.split("\\[");
        for (String item : items) {
            if (!TextUtils.isEmpty(item)) {
                String title = "";
                String summary = "";
                String[] info = item.split("\\n");
                for (int i = 0; i < info.length; i++) {
                    if(i == 0) {
                        title = "[" + info[i];
                    } else if(i < info.length - 1) {
                        summary += info[i] + "\n";
                    } else {
                        summary += info[i];
                    }
                }

                addInfoItem(new InfoItemWrapper(title, summary));
            }
        }
    }

    private final String getHwInfos() {
        StringBuilder infos = new StringBuilder();

        File file = new File(HW_INFO_INTERFACE);

        InputStream inputStream = null;
        BufferedReader buffer = null;
        InputStreamReader in = null;
        try {
            inputStream = new FileInputStream(file);

            in = new InputStreamReader(inputStream, "GBK");
            buffer = new BufferedReader(in);

            // read line by line.
            String line = null;
            while ((line = buffer.readLine()) != null) {
                if (!TextUtils.isEmpty(line)) {
                    infos.append(line).append("\n");
                }
            }
        } catch (FileNotFoundException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (UnsupportedEncodingException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        } finally {
            try {
                if (buffer != null) {
                	buffer.close();
                }
                if (inputStream != null) {
                	inputStream.close();
                }
                if (in != null) {
                	in.close();
                }
            } catch (IOException e) {
                // ignore
            }
        }

        return infos.toString();
    }

    public static boolean exists() {
        return (new File(HW_INFO_INTERFACE)).exists();
    }
}
