package com.freeme.factory.info;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.res.Resources;
import android.os.SystemProperties;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.widget.TextView;

import com.freeme.factory.R;

public class UpdateSelfInfo extends Activity {

    private static final String PRODUCT_BRAND        = "ro.product.brand";
    private static final String BUILD_HARDWARE       = "ro.board.platform";
    private static final String FREEMEOS_LABEL       = "ro.build.freeme.label";
    private static final String FREEMEOS_VERSION     = "ro.build.freeme.version";
    private static final String FREEMEOS_CHANNEL_ID  = "ro.build.freeme.channel";
    private static final String FREEMEOS_HW_VERSION  = "ro.build.freeme.hardware";
    private static final String FREEMEOS_CUSTOMER_NO = "ro.build.freeme.customer";

    private TextView mChannelID;
    private TextView mCpuInfo;
    private TextView mResolutionInfo;
    private TextView mAndroidVersion;
    private TextView mFreemeOsVersion;
    private TextView mProjectID;
    private TextView mCustomerID;
    private TextView mProductBrand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.updateselflabel);

        mChannelID       = (TextView) findViewById(R.id.text_channel);
        mCpuInfo         = (TextView) findViewById(R.id.text_cpu);
        mResolutionInfo  = (TextView) findViewById(R.id.text_resolution);
        mAndroidVersion  = (TextView) findViewById(R.id.text_androidVersion);
        mFreemeOsVersion = (TextView) findViewById(R.id.text_osversion);
        mProjectID       = (TextView) findViewById(R.id.text_project);
        mCustomerID      = (TextView) findViewById(R.id.text_customer);
        mProductBrand    = (TextView) findViewById(R.id.text_brand);

        mChannelID.setText(formatString(FREEMEOS_CHANNEL_ID, getChannelData(this, R.raw.updateself_cp)));

        mCpuInfo.setText(formatString(BUILD_HARDWARE, SystemProperties.get(BUILD_HARDWARE)));

        DisplayMetrics outMetrics = getResources().getDisplayMetrics();
        mResolutionInfo.setText(outMetrics.widthPixels + "*" + outMetrics.heightPixels);

        mAndroidVersion.setText("" + android.os.Build.VERSION.SDK_INT);

        mFreemeOsVersion.setText(SystemProperties.get(FREEMEOS_LABEL).toLowerCase()
            + SystemProperties.get(FREEMEOS_VERSION).toLowerCase());

        mProjectID.setText(formatString(FREEMEOS_HW_VERSION, SystemProperties.get(FREEMEOS_HW_VERSION)));

        mCustomerID.setText(formatString(FREEMEOS_CUSTOMER_NO, SystemProperties.get(FREEMEOS_CUSTOMER_NO)));

        mProductBrand.setText(formatString(PRODUCT_BRAND, SystemProperties.get(PRODUCT_BRAND)));
    }

    private String formatString(String propertyName, String propertyValue) {
        return propertyName + ": " + propertyValue;
    }

    public static String getChannelData(Context context, int rawId) {
        String channel = SystemProperties.get(FREEMEOS_CHANNEL_ID, "");
        if (TextUtils.isEmpty(channel)) {
            InputStream is = null;
            try {
                is = context.getResources().openRawResource(rawId);
                byte[] buffer = new byte[is.available()];
                is.read(buffer);

                channel = new String(buffer).trim();
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        // ignore
                    }
                }
            }
        }
        return channel;
    }
}

