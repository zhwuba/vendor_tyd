package com.freeme.factory.info;

import android.annotation.SuppressLint;
import android.os.AsyncResult;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.Handler;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;

import com.android.internal.util.HexDump;

//import vendor.mediatek.hardware.nvram.V1_0.INvram;

public class SoftwareInfo extends BaseInfo {
    private static final String TAG    = "SoftwareInfo";
    private static final String TAG_NV = "NVRAM-j";

    private static final int MSG_UPDATE_ITEM  = 0;
    private static final int MSG_UPDATE_SN    = 1;

    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_UPDATE_ITEM: {
                    addInfoItem((InfoItemWrapper)msg.obj);
                    break;
                }
                case MSG_UPDATE_SN: {
                    updateSerialNumber(msg);
                    break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateInfoItem();
        updateMainBoardNumber();
    }

    @Override
    protected void onDestroy() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    private void updateInfoItem() {
        final String eVer = SystemProperties.get("ro.build.display.id");        // Externel Version
        final String iVer = SystemProperties.get("ro.build.tyd.display.id"); // Internel Version
        addInfoItem(new InfoItemWrapper(
                "[Version]:",
                (TextUtils.equals(eVer, iVer)) ? eVer : eVer + "\n" + iVer));

        addInfoItem(new InfoItemWrapper(
                "[Build time]:",
                SystemProperties.get("ro.build.date")));

        addInfoItem(new InfoItemWrapper(
                "[Baseband Version]:",
                SystemProperties.get("gsm.version.baseband")));

        addInfoItem(new InfoItemWrapper(
                "[Version type]:",
                SystemProperties.get("ro.build.type")));

        /*
        addInfoItem(new InfoItemWrapper(
                "[MTK ver]:",
                SystemProperties.get("ro.mediatek.version.release")));
         */

        addInfoItem(new InfoItemWrapper(
                "[Calibration]:",
                getCalibrateResult()));

        /*
        addInfoItem(new InfoItemWrapper(
                "[FreemeOS Customer]:",
                SystemProperties.get("ro.build.freeme.customer")));

        addInfoItem(new InfoItemWrapper(
                "[FreemeOS Customer Branch]:",
                SystemProperties.get("ro.build.freeme.customer.branch")));
         */
    }

    private void updateMainBoardNumber() {
        new Thread("Init Phone") {
            @Override
            public void run() {
                try {
                    getMainBoardNumber();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private static String getCalibrateResult() {
        String SN = "";
        /*
        final ITelephonyEx phoneEx=ITelephonyEx.Stub.asInterface(ServiceManager.getService("phoneEx"));
        Log.i("SoftwareInfoActivity","phoneEx = "+phoneEx);
        if(phoneEx!=null){
            try{
                SN = phoneEx.getSerialNumber();
                Log.i("SoftwareInfoActivity","getSerialNumber = "+SN);
            } catch (Exception e){

            }
        }*/
        return SN.indexOf("10") >= 0 ? "ok" : "no";
    }

    private void updateSerialNumber(Message msg) {
        AsyncResult ar = (AsyncResult) msg.obj;
        String[] result = (String[]) ar.result;
        String rawDate = "";
        String mainBoard = "";
        if (result != null && result.length > 0) {
            rawDate = result[0];
        }
        if (rawDate.length() > 0) {
            Log.d(TAG_NV, "rawDate = " + rawDate);
            int start = rawDate.indexOf("\"");
            int end = rawDate.lastIndexOf("\"");
            if (end > start + 1) {
                mainBoard = rawDate.substring(start + 1, end);
                if (mainBoard.contains(" ")) {
                    mainBoard = mainBoard.substring(0, mainBoard.indexOf(" "));
                }
            }
            Log.d(TAG_NV, "main board: " + mainBoard);
        }

        String[] productInfo = getProductInfo();
        String serial = "";
        if (productInfo[1] != null && !productInfo[1].equals("")) {
            serial = productInfo[1];
        } else if (!TextUtils.isEmpty(mainBoard)) {
            serial = mainBoard;
        } else if (productInfo[0] != null && !productInfo[0].equals("")) {
            serial = productInfo[0];
        }
        if (!TextUtils.isEmpty(serial)) {
            mHandler.obtainMessage(MSG_UPDATE_ITEM,
                    new InfoItemWrapper("[Serial Number]:", serial)).sendToTarget();
            mHandler.obtainMessage(MSG_UPDATE_ITEM,
                    new InfoItemWrapper("[Main Board]:", mainBoard)).sendToTarget();
        }
    }

    private static String[] getProductInfo() {
        int PRODUCT_INFO_SIZE = 1024;
        int BARCODE_DIGITS = 16;
        int SERIALNOMER_DIGITS = 16;
        int SERIALNOMER_OFFSET = 64 + 4 * 10 + 12;

        byte[] buffArr = null;
        /*try {
            INvram nvram = INvram.getService();
            String buff = nvram.readFileByName("/vendor/nvdata/APCFG/APRDEB/PRODUCT_INFO", PRODUCT_INFO_SIZE);

            // Remove \0 in the end
            buffArr = HexDump.hexStringToByteArray(
                    buff.substring(0, buff.length() - 1));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
	*/
        String[] productInfo = new String[2];
        if (buffArr != null) {
            if (PRODUCT_INFO_SIZE == buffArr.length) {
                byte[] bBarcode = new byte[BARCODE_DIGITS];
                byte[] bSerialno = new byte[SERIALNOMER_DIGITS];
                System.arraycopy(buffArr, 0, bBarcode, 0, BARCODE_DIGITS);
                System.arraycopy(buffArr, SERIALNOMER_OFFSET, bSerialno, 0, SERIALNOMER_DIGITS);
                if (isNumberOrLetter(bBarcode)) {
                    productInfo[0] = new String(bBarcode);
                } else {
                    productInfo[0] = Build.getSerial();
                }
                if (isNumberOrLetter(bSerialno)) {
                    productInfo[1] = new String(bSerialno);
                } else {
                    productInfo[1] = Build.getSerial();
                }
                StringBuilder sb = new StringBuilder();
                for (int i=0;i<BARCODE_DIGITS;i++) {
                    sb.append(bBarcode[i]).append("-");
                }
                sb.append("\n");
                for (int i=0;i<SERIALNOMER_DIGITS;i++) {
                    sb.append(bSerialno[i]).append("-");
                }
                Log.d(TAG_NV, "bBarcode + bSerialno: " + sb.toString() + ", " +
                        "sbarcode = " + productInfo[0] + ", " +
                        "sserialnom = " + productInfo[1]);
            } else {
                Log.e(TAG_NV, "PRODUCT_INFO size: " + buffArr.length);
            }
        } else {
            Log.e(TAG_NV, "buff is bull");
        }
        return productInfo;
    }

    private static boolean isNumberOrLetter(byte[] src) {
        boolean ret = false;
        int size = src.length;
        for (int i = 0; i < size; i++) {
            if ((src[i] >= 48 && src[i] <= 57) || (src[i] >= 65 && src[i] <= 70)
                    || (src[i] >= 97 && src[i] <= 102)) {
                ret = true;
                break;
            }
        }
        return ret;
    }

    private void getMainBoardNumber() {
        final String FK_MTK_C2K_SUPPORT = "ro.vendor.mtk_c2k_support";
        final String FK_EVDO_DT_SUPPORT = "ro.evdo_dt_support";
        Phone mPhone = PhoneFactory.getDefaultPhone();

        if (isSupported(FK_MTK_C2K_SUPPORT)) {
            if (isSupported(FK_EVDO_DT_SUPPORT)
                    && mPhone.getPhoneType() == PhoneConstants.PHONE_TYPE_CDMA) {
                mPhone = PhoneFactory.getPhone(PhoneConstants.SIM_ID_2);
            }
        }

        mPhone.invokeOemRilRequestStrings(new String[]{"AT+EGMR=0,5", "+EGMR"},
                mHandler.obtainMessage(MSG_UPDATE_SN));
    }

    private static boolean isSupported(String featureKey) {
        return "1".equals(SystemProperties.get(featureKey));
    }
}
