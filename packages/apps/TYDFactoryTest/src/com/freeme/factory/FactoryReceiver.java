package com.freeme.factory;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

public class FactoryReceiver extends BroadcastReceiver {
    private static final String TAG = "Factory/SECRET_CODE";
    private static final String SECRET_CODE_ACTION = "android.provider.Telephony.SECRET_CODE";
    private final Uri mFactoryUri = Uri.parse("android_secret_code://6804");

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == null) {
            Log.i("@M_" + TAG, "Null action");
            return;
        }
        if (intent.getAction().equals(SECRET_CODE_ACTION)) {
            Uri uri = intent.getData();
            Log.i("@M_" + TAG, "getIntent success in if");
            if (uri.equals(mFactoryUri)) {
                Intent intentfacoty = new Intent(context, FactoryTest.class);
                intentfacoty.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Log.i("@M_" + TAG, "Before start Factory activity");
                context.startActivity(intentfacoty);
            }
        } else {
            Log.i("@M_" + TAG, "Not SECRET_CODE_ACTION!");
        }
    }
}
