package com.freeme.factory.wifi;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.collect.Maps;
import com.google.android.collect.Sets;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.ProjectConfig;
import com.freeme.factory.config.TestConfig;
import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;

public class WiFiTest extends BaseTest {
    private TextView wifistate;
    private TextView wifiresult;

    private WifiManager mWifiManager;
    private final WifiStateReceiver mWifiStateReceiver = new WifiStateReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wifi_test);

        wifistate  = (TextView) findViewById(R.id.wifi_state_id);
        wifiresult = (TextView) findViewById(R.id.wifi_result_id);

        testActionCompleted(false);
    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(mWifiStateReceiver, intentFilter);

        mWifiManager= (WifiManager) getSystemService(WIFI_SERVICE);
        if (mWifiManager.isWifiEnabled()) {
            wifistate.setText(getString(R.string.WiFi_info_open));
            //*/dz.wangqi, 20181218. wifi
            mWifiManager.startScan();
            wifiresult.setText(getString(R.string.WiFi_scaning));
            //*/
        } else {
            mWifiManager.setWifiEnabled(true);
            wifistate.setText(getString(R.string.WiFi_info_opening));
        }

        /*/dz.wangqi,20181218. remove
        mWifiManager.startScan();
        wifiresult.setText(getString(R.string.WiFi_scaning));
        /*/
    }

    @Override
    protected void onStop() {
        if (mWifiStateReceiver != null) {
            unregisterReceiver(mWifiStateReceiver);
        }
        mWifiManager.setWifiEnabled(false);

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onTestSuccess(View v) {
        Intent intent = new Intent(WiFiTest.this, FactoryTest.class);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onTestFailed(View v) {
        Intent intent = new Intent(WiFiTest.this, FactoryTest.class);
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    private class WifiStateReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            final String action = intent.getAction();
            Log.d("wifi>>>>>>>>>>>>>action", action);

            if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
                final Bundle extras = intent.getExtras();
                //final int previousWifiState = bundle.getInt("previous_wifi_state");
                final int wifiState = extras.getInt("wifi_state");

                switch (wifiState) {
                case WifiManager.WIFI_STATE_DISABLED:
                    //onWifiStateChange();
                    wifistate.setText(getString(R.string.WiFi_info_close));
                    break;
                case WifiManager.WIFI_STATE_ENABLED:
                    wifistate.setText(getString(R.string.WiFi_info_open));
                    break;
                case WifiManager.WIFI_STATE_ENABLING:
                    wifistate.setText(getString(R.string.WiFi_info_opening));
                    //*/dz.wangqi,20181218. wifi
                    mWifiManager.startScan();
                    wifiresult.setText(getString(R.string.WiFi_scaning));
                    //*/
                    break;
                case WifiManager.WIFI_STATE_DISABLING:
                    wifistate.setText(R.string.WiFi_info_closeing);
                    break;
                }

            } else if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action)) {
                //       <SSID,   securities>
                final Map<String, Set<Integer>> filtedSSIDs = Maps.newHashMap();
                final StringBuilder scanList = new StringBuilder();

                final List<ScanResult> results = mWifiManager.getScanResults();
                if (results != null) {
                    for (ScanResult result : results) {
                        // TODO: ssid([ ]+) is still valid
                        final String SSID = result.SSID;

                        if (! TextUtils.isEmpty(SSID/*.trim()*/))
                        if (! SSID.toUpperCase(Locale.US).contains("NVRAM WARNING"))
                        {
                            Set<Integer> securities = filtedSSIDs.get(SSID);
                            if (securities == null) {
                                securities = Sets.newHashSet();
                                filtedSSIDs.put(SSID, securities);
                            }

                            final int security = getSecurity(result);
                            if (! securities.contains(security)) {
                                securities.add(security);

                                scanList.append(SSID)
                                    .append("\t\t\t")
                                    .append(getString(R.string.Wifi_strength, result.level))
                                    .append('\n');
                            }
                        }
                    }
                }
                wifiresult.setText(scanList.toString());

                if (! filtedSSIDs.isEmpty()) {
                    testActionCompleted(true);

                    switch (FactoryTest.getTestMode()) {
                    case TestConfig.TEST_MODE_ALL:
                        testResultCommit(true);
                        break;
                    case TestConfig.TEST_MODE_AUTO:
                        if (ProjectConfig.EN_PREDEF_WIFI_OK) {
                            if (mWifiConnectReq == null) {
                                mWifiConnectReq = new WifiConnectReq(new Runnable() {
                                    @Override
                                    public void run() {
                                        testResultCommit(true);
                                    }
                                });
                            }
                            mWifiConnectReq.execute(filtedSSIDs);
                        } else {
                            testResultCommit(true);
                        }
                        break;
                    default:
                        break;
                    }
                }
            }
        }
    }

    private WifiConnectReq mWifiConnectReq;
    private class WifiConnectReq implements WifiManager.ActionListener {
        private Runnable postJob;

        WifiConnectReq(Runnable post) {
            postJob = post;
        }

        public void execute(Map<String, Set<Integer>> ssids) {
            if (ssids.containsKey(ProjectConfig.CO_PREDEF_WIFI_OK_SSID)) {
                submit(ProjectConfig.CO_PREDEF_WIFI_OK_SSID);
            }
        }

        public void submit(String ssid) {
            WifiConfiguration config = new WifiConfiguration();

            config.SSID = convertToQuotedString(ssid);
            config.allowedKeyManagement.set(KeyMgmt.NONE);

            mWifiManager.connect(config, this);
        }

        @Override
        public void onFailure(int reason) {
            //Toast.makeText(getApplicationContext(), "Wifi connected failed!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess() {
            //Toast.makeText(getApplicationContext(), "Wifi connected OK!", Toast.LENGTH_SHORT).show();
            if (postJob != null) {
                WiFiTest.this.runOnUiThread(postJob);
            }
        }
    }


    /** These values are matched in string arrays -- changes must be kept in sync */
    static final int SECURITY_NONE = 0;
    static final int SECURITY_WEP = 1;
    static final int SECURITY_PSK = 2;
    static final int SECURITY_EAP = 3;
    private static int getSecurity(ScanResult result) {
        if (result.capabilities.contains("WEP")) {
            return SECURITY_WEP;
        } else if (result.capabilities.contains("PSK")) {
            return SECURITY_PSK;
        } else if (result.capabilities.contains("EAP")) {
            return SECURITY_EAP;
        }
        return SECURITY_NONE;
    }

    static String convertToQuotedString(String string) {
        return "\"" + string + "\"";
    }
}
