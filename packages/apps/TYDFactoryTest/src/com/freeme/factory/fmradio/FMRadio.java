package com.freeme.factory.fmradio;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;

import android.content.ComponentName;
import android.widget.Toast;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;

public class FMRadio extends BaseTest {

    private AudioManager mAudioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signal);

        maximizeVolume();

        Intent intent = new Intent("com.freeme.intent.action.HARDWARE_TEST_FMRADIO");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName(
                    "com.caf.fmradio", "com.caf.fmradio.FMRadio"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException ee) {
                Toast.makeText(this, "Found no FmRadio app !", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void maximizeVolume() {
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                AudioManager.FLAG_ALLOW_RINGER_MODES);
        if (mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
                > mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC)) {
            //*/dz.wangqi, 20181214.hide UI.
            mAudioManager.disableSafeMediaVolume();
            //*/
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                    mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                    /*/dz.wangqi, 20181226. remove Volume UI.
                    AudioManager.FLAG_SHOW_UI|AudioManager.FLAG_ALLOW_RINGER_MODES);
                    /*/
                    AudioManager.FLAG_ALLOW_RINGER_MODES);
                    //*/
        }
    }
}
