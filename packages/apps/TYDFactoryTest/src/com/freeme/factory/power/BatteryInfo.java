package com.freeme.factory.power;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.freeme.factory.base.BaseTest;
import com.freeme.factory.R;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BatteryInfo extends BaseTest {
    private static final String TAG = "Battery";

    private TextView mTextStatus;
    private TextView mTextLevel;
    private TextView mTextScale;
    private TextView mTextHealth;
    private TextView mTextVoltage;
    private TextView mTextUptime;
    private TextView mTextChgrVol;
    private TextView mTextChgrCurr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.battery_info);

        mTextStatus = (TextView) findViewById(R.id.status);
        mTextLevel = (TextView) findViewById(R.id.level);
        mTextScale = (TextView) findViewById(R.id.scale);
        mTextHealth = (TextView) findViewById(R.id.health);
        mTextVoltage = (TextView) findViewById(R.id.voltage);

        mTextChgrVol= (TextView) findViewById(R.id.chgrvol);
        mTextChgrCurr = (TextView) findViewById(R.id.chgrcurr);

        mTextUptime = (TextView) findViewById(R.id.uptime);
        long elapsed = SystemClock.elapsedRealtime() / 1000; // s
        long hour = elapsed / 3600;
        long minute = (elapsed % 3600) / 60;
        long second = (elapsed % 3600) % 60;
        mTextUptime.setText(hour + getString(R.string.hour)
                + minute + getString(R.string.minute)
                + second + getString(R.string.second));

        testActionCompleted(false);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        intentFilter.addAction(UsbManager.ACTION_USB_STATE);
        final Intent intent = registerReceiver(mReceiver, intentFilter);
        if (isCharing(intent)) {
            testActionCompleted(true);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (UsbManager.ACTION_USB_STATE.equals(action)){
                boolean connected = intent.getExtras().getBoolean("connected");
                if (!connected){
                    mTextChgrVol.setText("0"+" "+getString(R.string.battery_info_voltage_units));
                    mTextChgrCurr.setText("0"+" "+getString(R.string.battery_info_chgrcurr_units));
                }
                Log.d(TAG, "USB_CONNECT_STATE:"+connected);
            } else if (Intent.ACTION_BATTERY_CHANGED.equals(action)) {
                int status = intent.getIntExtra("status", 0);
                int health = intent.getIntExtra("health", 0);
                int level = intent.getIntExtra("level", 0);
                int scale = intent.getIntExtra("scale", 0);
                int plugged = intent.getIntExtra("plugged", 0);
                int voltage = intent.getIntExtra("voltage", 0);

                int chgrvol = -999;
                String chargerVol = getFileContent("/sys/devices/platform/charger/ADC_Charger_Voltage");
                if (!TextUtils.isEmpty(chargerVol)) {
                    chgrvol = Integer.parseInt(chargerVol);
                }
                int chgrcurr = -999;//FIXME: intent.getIntExtra(BatteryManager.EXTRA_CHARGER_CURRENT,0);

                String statusString = "";
                switch (status) {
                    case BatteryManager.BATTERY_STATUS_UNKNOWN:
                        statusString = getString(R.string.battery_info_status_unknown);//"unknown";
                        break;
                    case BatteryManager.BATTERY_STATUS_CHARGING:
                        statusString = getString(R.string.battery_info_status_charging);//"charging";
                        break;
                    case BatteryManager.BATTERY_STATUS_DISCHARGING:
                        statusString = getString(R.string.battery_info_status_discharging);//"discharging";
                        break;
                    case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                        statusString = getString(R.string.battery_info_status_not_charging);//"not charging";
                        break;
                    case BatteryManager.BATTERY_STATUS_FULL:
                        statusString = getString(R.string.battery_info_status_full);//"full";
                        break;
                }

                String healthString = "";
                switch (health) {
                    case BatteryManager.BATTERY_HEALTH_UNKNOWN:
                        healthString = getString(R.string.battery_info_health_unknown);//"unknown";
                        break;
                    case BatteryManager.BATTERY_HEALTH_GOOD:
                        healthString = getString(R.string.battery_info_health_good);//"good";
                        break;
                    case BatteryManager.BATTERY_HEALTH_OVERHEAT:
                        healthString = getString(R.string.battery_info_health_overheat);//"overheat";
                        break;
                    case BatteryManager.BATTERY_HEALTH_DEAD:
                        healthString = getString(R.string.battery_info_health_dead);//"dead";
                        break;
                    case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
                        healthString = getString(R.string.battery_info_health_over_voltage);//"voltage";
                        break;
                    case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
                        healthString = getString(R.string.battery_info_health_unspecified_failure);//"unspecified failure";
                        break;
                }

                String acString = "";
                switch (plugged) {
                    case BatteryManager.BATTERY_PLUGGED_AC:
                        acString = getString(R.string.battery_info_status_charging_ac);//"plugged ac";
                        break;
                    case BatteryManager.BATTERY_PLUGGED_USB:
                        acString = getString(R.string.battery_info_status_charging_usb);//"plugged usb";
                        break;
                }

                mTextStatus.setText(statusString+" "+acString);
                mTextHealth.setText(healthString);
                mTextLevel.setText(""+level+"%");
                mTextScale.setText(""+scale+"%");
                mTextVoltage.setText(""+voltage+" "+getString(R.string.battery_info_voltage_units));
                mTextChgrVol.setText(""+chgrvol+" "+getString(R.string.battery_info_voltage_units));
                mTextChgrCurr.setText(""+chgrcurr+" "+getString(R.string.battery_info_chgrcurr_units));

                if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
                    Log.d(TAG, "level: " + intent.getIntExtra("level", 0));
                    Log.d(TAG, "scale: " + intent.getIntExtra("scale", 0));
                    Log.d(TAG, "voltage: " + intent.getIntExtra("voltage", 0));
                    Log.d(TAG, "charger-voltage: " + intent.getIntExtra("charger-voltage", 0));
                    Log.d(TAG, "charger-current: " + intent.getIntExtra("charger-current", 0));
                    Log.d(TAG, "temperature: " + intent.getIntExtra("temperature", 0));
                    Log.d(TAG,"status: "+ intent.getIntExtra("status",BatteryManager.BATTERY_STATUS_CHARGING));
                    Log.d(TAG, "plugged: " + intent.getIntExtra("plugged", 0));
                    Log.d(TAG,"health: "+ intent.getIntExtra("health",BatteryManager.BATTERY_HEALTH_UNKNOWN));
                }

                if (isCharing(intent)) {
                    testActionCompleted(true);
                }
            }
        }
    };

    private static boolean isCharing(Intent intent) {
        final int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        switch (plugged) {
            case BatteryManager.BATTERY_PLUGGED_AC:
            case BatteryManager.BATTERY_PLUGGED_USB:
            case BatteryManager.BATTERY_PLUGGED_WIRELESS:
                return true;
            default:
                return false;
        }
    }

    private static String getFileContent(String filePath) {
        if (filePath == null) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(filePath));
            char[] buffer = new char[500];
            int ret;
            while ((ret = reader.read(buffer)) != -1) {
                builder.append(buffer, 0, ret);
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException:" + e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e(TAG, "IOException:" + e.getMessage());
                }
            }
        }
        String result = builder.toString();
        if (result != null) {
            result = result.trim();
        }
        return result;
    }
}
