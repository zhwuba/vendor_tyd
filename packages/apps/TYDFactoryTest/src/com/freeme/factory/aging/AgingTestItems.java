package com.freeme.factory.aging;

import java.io.Serializable;

public class AgingTestItems implements Serializable {
	private boolean backCamera;
	private boolean frontCamera;
	private boolean micLoop;
	private boolean receiver;
	private boolean speaker;
	private boolean vibrate;
	private boolean video;
	private boolean frontLigth;
	private boolean backLigth;
  
	public static final String[] TEST_ITEM = {
		"video_speaker_duration",
		"video_receiver_duration",
		"vibrate_duration",
		"mic_loop_duration",
		"front_camera_duration",
		"back_camera_duration",
		"front_light_duration",
		"back_light_duration",
		//*/tyd.sunyuepeng, 20190507. restart test.
		"reboot"
		//*/
	};

	public boolean getVideo() {
		return this.video;
	}
	
	public boolean getSpeaker() {
		return this.speaker;
	}

	public boolean getReceiver() {
		return this.receiver;
	}

	public boolean getVibrate() {
		return this.vibrate;
	}

	public boolean getmicLoop() {
		return this.micLoop;
	}

	public boolean getFrontCamera() {
		return this.frontCamera;
	}

	public boolean getBackCamera() {
		return this.backCamera;
	}
  	
	public boolean getFrontLight() {
		return this.frontLigth;
	}
  
	public boolean getBackLight() {
		return this.backLigth;
	}

	public void setVideo(boolean paramBoolean) {
		this.video = paramBoolean;
	}

	public void setSpeaker(boolean paramBoolean) {
		this.speaker = paramBoolean;
	}

	public void setReceiver(boolean paramBoolean) {
		this.receiver = paramBoolean;
	}

	public void setVibrate(boolean paramBoolean) {
		this.vibrate = paramBoolean;
	}

	public void setMicLoop(boolean paramBoolean) {
		this.micLoop = paramBoolean;
	}

	public void setFrontCamera(boolean paramBoolean) {
		this.frontCamera = paramBoolean;
	}

	public void setBackCamera(boolean paramBoolean)	{
		this.backCamera = paramBoolean;
	}
  
	public void setFrontLight(boolean paramBoolean)	{
		this.frontLigth = paramBoolean;
	}
  
	public void setBackLight(boolean paramBoolean){
		this.backLigth = paramBoolean;
	}
}
