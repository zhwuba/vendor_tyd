package com.freeme.factory.aging;

import android.content.Context;
import android.content.SharedPreferences;

public class AgingSharedPreference
{
  Context mContext;
  SharedPreferences mSharedPreferences;
  SharedPreferences.Editor mSharedPreferencesEditor;
  static final String SHARE_NAME = "AgingTestShare";

  public AgingSharedPreference(Context paramContext)
  {
	mContext = paramContext;
    mSharedPreferences = mContext.getSharedPreferences(SHARE_NAME, 0);
    mSharedPreferencesEditor = mSharedPreferences.edit();
  }

  public void setValue(String paramString1, String paramString2)
  {
    mSharedPreferencesEditor = mSharedPreferences.edit();
    mSharedPreferencesEditor.putString(paramString1, paramString2);
    mSharedPreferencesEditor.commit();
  }

  public String getValue(String paramString)
  {
    return mSharedPreferences.getString(paramString, null);
  }

  //*/ tyd.sunyuepeng, 20190507. restart test.
  public void setIntValue(String paramString1, int paramString2)
  {
    mSharedPreferencesEditor = mSharedPreferences.edit();
    mSharedPreferencesEditor.putInt(paramString1, paramString2);
    mSharedPreferencesEditor.commit();
  }

  public int getIntValue(String paramString)
  {
    return mSharedPreferences.getInt(paramString, 0);
  }
  //*/
}
