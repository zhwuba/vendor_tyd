package com.freeme.factory.aging;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

public class AgingTestRebootReceiver extends BroadcastReceiver {
	private final static String TAG = "RebootTestReceiver";
	private int mRebootNumber = 0;
	private AgingSharedPreference mAgingSharedPreference;

	@Override
	public void onReceive(Context context, Intent intent) {
		mAgingSharedPreference = new AgingSharedPreference(context);
		mRebootNumber = mAgingSharedPreference.getIntValue(AgingTestActivity.REBOOT_NUMBER);
            	if (mRebootNumber > 0) {
                    mRebootNumber--;
		    mAgingSharedPreference.setIntValue(AgingTestActivity.REBOOT_NUMBER, mRebootNumber);
                    PowerManager mPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                    mPowerManager.reboot("");
		}
	}
}

