package com.freeme.factory.aging;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.freeme.factory.R;
import com.freeme.factory.camera.CameraManager;
import com.freeme.factory.camera.CameraUtil;

public class AgingTestActivityImpl extends Activity {

    private static String TAG = "AgingTestActivityImpl";

    private AgingSharedPreference mAgingSharedPreference;
    private AgingTestItems mAgingTestItems;

    private TextView mElapsedPlaytime;
    private SurfaceView mVideoSurface;
    private TextureView mCameraSurface;
    private SurfaceHolder mSurfaceHolder;

    private Handler mHandler = new Handler();

    private boolean mVideoTestable             = false;
    private boolean mSpeakerTestable             = false;
    private boolean mReceiverTestable         = false;
    private boolean mVibrateTestable             = false;
    private boolean mMicLoopTestable             = false;
    private boolean mFrontCameraTestable         = false;
    private boolean mBackCameraTestable         = false;
    private boolean mFrontFlashCameraTestable = false;
    private boolean mBackFlashCameraTestable  = false;

    /* Test Speaker and Receiver Same Time */
    private boolean mTestSpeakerAndReceiver = false;

    /* Test Front and Back Camera Same Time */
    private boolean mTestSwitchCamera         = false;

    /* Reset Orientation */
    private boolean mNeedUpdateOrientation     = false;

    /* Close Front Camera Prview */
    private boolean mCloseFCameraPreview     = false;

    /* Close BackCamera Prview */
    private boolean mCloseBCameraPreview     = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aging_impl_activity);

        init();
        doAgingTest();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        //getWindow().addFlagsEx(WindowManager.LayoutParams.FLAG_EX_POWERKEY_DISPATCHED);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_POWER) {
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        doCheckTestTime();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "onDestroy");

        finishVideoTest();
        finishAudioTest();
        finishVibratorTest();
        finishMicLoopTest();
        finishCameraTest();
        finishFlashLightTest();

        releaseLock();
        stopTimer();
    }

    ///-------------------------------------Init--------------------------------------------

    private void init() {
        initTestItems();
        initViews();
        initCountDownTimer();

        acquireLock();
    }

    private void initViews() {
        initOrientation();
        mVideoSurface = ((SurfaceView) findViewById(R.id.video_surface));
        mCameraSurface = ((TextureView) findViewById(R.id.camera_surface));
        mElapsedPlaytime = ((TextView) findViewById(R.id.elapsetimeshow_tv));
    }

    private void initOrientation() {
        int orient = getRequestedOrientation();

        if(mNeedUpdateOrientation) {
            if (orient == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        } else {
            if (orient == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }
    }

    private void initTestItems() {
        mAgingTestItems = (AgingTestItems)getIntent().getSerializableExtra(AgingTestActivity.EXTRA_AGINGTEST_ITEMS);

        mVideoTestable            = mAgingTestItems.getVideo();
        mSpeakerTestable            = mAgingTestItems.getSpeaker();
        mReceiverTestable            = mAgingTestItems.getReceiver();
        mVibrateTestable            = mAgingTestItems.getVibrate();
        mMicLoopTestable            = mAgingTestItems.getmicLoop();
        mFrontCameraTestable        = mAgingTestItems.getFrontCamera();
        mBackCameraTestable        = mAgingTestItems.getBackCamera();
        mFrontFlashCameraTestable = mAgingTestItems.getFrontLight();
        mBackFlashCameraTestable  = mAgingTestItems.getBackLight();

        mTestSpeakerAndReceiver = mSpeakerTestable  && mReceiverTestable ;
        mTestSwitchCamera         = (mBackFlashCameraTestable  || mBackCameraTestable )
                                        && (mFrontCameraTestable  || mFrontFlashCameraTestable );

        mCloseBCameraPreview = mBackFlashCameraTestable  && !mBackCameraTestable ;
        mCloseFCameraPreview = mFrontFlashCameraTestable  && !mFrontCameraTestable ;

        mNeedUpdateOrientation     = mBackFlashCameraTestable  || mBackCameraTestable
                                        || mFrontCameraTestable  || mFrontFlashCameraTestable ;

        Log.d(TAG, "testReceiver" + mAgingTestItems.getVideo() + mReceiverTestable );
    }

    private PowerManager.WakeLock mWakeLock = null;
    private PowerManager mPowerManager;

    private void acquireLock() {
        mPowerManager = ((PowerManager) getSystemService("power"));
        mWakeLock = mPowerManager.newWakeLock(26, TAG);
        mWakeLock.acquire();
    }

    private void releaseLock() {
        if(mWakeLock != null) {
            mWakeLock.release();
        }
    }

    ///-------------------------------------Init CountDownTimer-------------------------------------

    private long mStartTime     = 0;
    private long mTotalTestTime = 0;

    private CountDownTimer mCountDownTimer;

    private void initTotalTime() {
        mTotalTestTime = getIntent().getLongExtra(AgingTestActivity.EXTRA_AGINGTEST_DURATION, 0L);
        Log.d(TAG, "Total Test Time" + mTotalTestTime);
    }

    private void initCountDownTimer() {
        initTotalTime();

        mStartTime = SystemClock.elapsedRealtime();
        mCountDownTimer = new CountDownTimer(mTotalTestTime * 1000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                mElapsedPlaytime.setText(formatTime(SystemClock
                        .elapsedRealtime() - mStartTime));
            }

            @Override
            public void onFinish() {
                finish();
            }
        }.start();
    }

    private void stopTimer() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }

    ///-------------------------------------Do Test-------------------------------------------------

    private void doAgingTest() {
        initTestItems();

        if(mVideoTestable) {
            playVideo();

            if(mTestSpeakerAndReceiver) {
                switchSpeakerAndReceiver();
            } else if (mSpeakerTestable) {
                mLastVideoSpeakerTime = SystemClock.elapsedRealtime();
                setAudioNormal();
            } else if (mReceiverTestable) {
                mLastVideoReceiverTime = SystemClock.elapsedRealtime();
                setAudioReceiver();
            }
        }

        if (mMicLoopTestable) {
            testSpeakerAndRecord();
        }

        if (mVibrateTestable) {
            testVibrator();
        }

        if (mTestSwitchCamera) {
            testCameraSwitch();
        } else if (mFrontCameraTestable || mFrontFlashCameraTestable) {
            testFrontCamera();
        } else if (mBackCameraTestable || mBackFlashCameraTestable) {
            testBackCamera();
        }
    }

    ///-------------------------------------Video Test----------------------------------------------
    private MediaPlayer mMediaPlayer;

    private String mVideoFilePath;

    private static final int DEFAULT_VDIEO_HEIGHT = 480;

    private static final String DEFAULT_VDIEO_FIEL_NAME = "AgingVideo.mp4";

    private void playVideo() {
        mVideoSurface.setVisibility(View.VISIBLE);
        mSurfaceHolder = mVideoSurface.getHolder();
        mSurfaceHolder.setFixedSize(getWidth(), DEFAULT_VDIEO_HEIGHT);
        mSurfaceHolder.addCallback( new Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                Log.d(TAG, "SurfaceHolder surfaceCreated");
                doPlay(0);
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width,
                    int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });
    }

    protected void doPlay(int position) {
        File localFile = getVideoFile();
        if (localFile != null) {
            mVideoFilePath = localFile.getAbsolutePath().toString();
            while (true) {
                localFile = new File(mVideoFilePath);
                if (localFile.exists())
                    break;
                showToast(getString(R.string.agingtest_no_vieofile_notice));
                finish();
                return;
            }
        } else {
            showToast(getString(R.string.agingtest_no_vieofile_notice));
            finish();
            return;
        }
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setAudioStreamType(3);
            mMediaPlayer.setDataSource(mVideoFilePath);
            mMediaPlayer.setLooping(true);
            mMediaPlayer.setDisplay(mSurfaceHolder);
            mMediaPlayer.prepare();
            mMediaPlayer.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    Log.d(TAG, "mMediaPlayer onPrepared");
                    mMediaPlayer.start();
                    mMediaPlayer.seekTo(0);
                }
            });
            mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    finish();
                }
            });
            mMediaPlayer.setOnErrorListener(new OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    return false;
                }
            });
            return;
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    private File getVideoFile() {
        File localFile = new File(this.getFilesDir(), DEFAULT_VDIEO_FIEL_NAME);
        FileOutputStream localFileOutputStream = null;
        if (!localFile.exists()) {
            try {
                showToast(getString(R.string.agingtest_copy_video_notice));
                InputStream localInputStream = this.getAssets().open(DEFAULT_VDIEO_FIEL_NAME);
                localFileOutputStream = new FileOutputStream(localFile);
                byte[] arrayOfByte = new byte[1024];
                while (true) {
                    int i = localInputStream.read(arrayOfByte);
                    if (i == -1)
                        break;
                    localFileOutputStream.write(arrayOfByte, 0, i);
                }

                if (localInputStream != null) {
                    localInputStream.close();
                }
            } catch (Exception localException) {
                localException.printStackTrace();
            } finally {
                try {
                    if (localFileOutputStream != null) {
                        localFileOutputStream.close();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            return localFile;
        }
        return localFile;
    }

    private int getWidth() {
        return getWindowManager().getDefaultDisplay().getWidth();
    }

    private void finishVideoTest() {
        if(mVideoTestable) {
            if (mMediaPlayer != null) {
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.stop();
                }
                mMediaPlayer.release();
            }
        }
    }

   ///-------------------------------------Vibrator Test--------------------------------------------

    private Vibrator mVibrator;
    private Runnable mVibratorRunnable;
    private long[] mVibratorPattern = { 500L, 1000L };
    private long mVibratorTime = 0;

    private void testVibrator() {
        mVibratorTime = SystemClock.elapsedRealtime();
        mVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        mVibratorRunnable = new Thread(new Runnable() {

            @Override
            public void run() {
                mVibrator.vibrate(mVibratorPattern, 0);
            }
        });
        mHandler.postDelayed(mVibratorRunnable, 0);
    }

    private void finishVibratorTest() {
        if(mVibrateTestable ) {
            if(mVibrator != null && mHandler != null) {
                mHandler.removeCallbacks(mVibratorRunnable);
                mVibrator.cancel();
                mVibrator =null;
            }
        }
    }

    ///-------------------------------------Audio Test----------------------------------------------

    private AudioManager mAudioManager;

    private void setAudioNormal() {
        mAudioManager = getAudioManager();
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) - 5 , 0);
        mAudioManager.setMode(AudioManager.MODE_NORMAL);
    }

    private void setAudioReceiver() {
        mAudioManager = getAudioManager();
        mAudioManager.setStreamVolume(AudioManager.MODE_IN_COMMUNICATION,
                mAudioManager.getStreamMaxVolume(AudioManager.MODE_IN_COMMUNICATION), 0);
        mAudioManager.setSpeakerphoneOn(false);
        setVolumeControlStream(3);
        mAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
    }

    ///-------------------------------------Audio Test: Mic ----------------------------------------
    private AudioRecord mAudioRecord;
    private AudioTrack mAudioTrack;

    private Thread mMicLoopThread;

    private byte[] buffer;
    private int bufferSize;
    private volatile boolean canRecord = false;
    private long micLoopTime = 0;

    private final static int SAMPLE_RATE  = 8 * 1000;
    private final static int DEFAULT_SIZE = 1024; // byte

    private void testSpeakerAndRecord() {
        mAudioManager = getAudioManager();
        mAudioManager.setSpeakerphoneOn(false);
        setVolumeControlStream(0);
        mAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        mMicLoopThread = new Thread(new Runnable() {

            @Override
            public void run() {
                canRecord = true;
                micLoopTime = SystemClock.elapsedRealtime();
                startRecord();
            }
        });
        mMicLoopThread.start();
    }

    private void startRecord() {
        canRecord = true;
        bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, 16, 2);
        bufferSize = Math.max(bufferSize,
                AudioTrack.getMinBufferSize(SAMPLE_RATE, 16, 2));
        bufferSize = Math.max(bufferSize, DEFAULT_SIZE);

        mAudioRecord = findAudioRecord();
        if (mAudioRecord == null) {
            Log.e(TAG, "Audio is not init ready!");
            return;
        }

        buffer = new byte[bufferSize];
        mAudioTrack = new AudioTrack(0, SAMPLE_RATE, 4, 2, bufferSize, 1);
        mAudioTrack.setPlaybackRate(SAMPLE_RATE);
        mAudioRecord.startRecording();
        mAudioTrack.play();

        while (canRecord) {
            int i = mAudioRecord.read(buffer, 0, bufferSize);
            if (i > 0)
                mAudioTrack.write(buffer, 0, i);
        }
    }

   private AudioRecord findAudioRecord() {
       int i = 0;
       while (i <= 5) { // 5 is MAX_TRY
           AudioRecord ar = new AudioRecord(1, SAMPLE_RATE, 16, 2, bufferSize);
           if (ar.getState() == AudioRecord.STATE_INITIALIZED)
               return ar;

           try {
               Thread.sleep(1000);
           } catch (Exception e) {
               return null;
           }
       }
       return null;
   }


    public void finishMicLoopTest() {
        if (mMicLoopTestable) {
            canRecord = false;
            if ((mAudioTrack != null) && (mAudioTrack.getState() != 0)
                    && (mAudioTrack.getPlayState() != 1)) {
                mAudioTrack.stop();
                mAudioTrack.release();
            }
            if (mAudioRecord != null) {
                mAudioRecord.release();
                mAudioRecord = null;
            }

            if (mMicLoopThread != null && mMicLoopThread.isAlive()) {
                try {
                    mMicLoopThread.stop();
                    mMicLoopThread.join();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    ///-------------------------------------Audio Test: Speaker Receiver ---------------------------

    private Timer mSpeakReceiverTimer = null;
    private TimerTask mSpeakReceiverTimerTask = null;

    private void switchSpeakerAndReceiver() {
        setAudioNormal();
        mLastSwitchToSpeakerTime = SystemClock.elapsedRealtime();
        if (mSpeakReceiverTimer == null)
            mSpeakReceiverTimer = new Timer();
        if (mSpeakReceiverTimerTask == null)
            mSpeakReceiverTimerTask = new TimerTask() {

                @Override
                public void run() {
                    Log.d(TAG, "doSwitchAudioMode");
                    doSwitchAudioMode();
                }
            };
        if ((mSpeakReceiverTimer != null) && (mSpeakReceiverTimerTask != null))
            mSpeakReceiverTimer.schedule(mSpeakReceiverTimerTask, 5000L, 5000L);
    }

    private boolean useSpeaker = false;
    private long mLastSwitchToSpeakerTime     = 0;
    private long mLastSwitchToReceiverTime = 0;
    private long mLastVideoReceiverTime     = 0;
    private long mLastVideoSpeakerTime     = 0;

    private void doSwitchAudioMode() {
        if (useSpeaker) {
            setAudioNormal();
            mLastVideoReceiverTime += SystemClock.elapsedRealtime()
                    - mLastSwitchToReceiverTime;
            mLastSwitchToSpeakerTime = SystemClock.elapsedRealtime();
            useSpeaker = false;
            return;
        }
        setAudioReceiver();
        mLastVideoSpeakerTime += SystemClock.elapsedRealtime()
                - mLastSwitchToSpeakerTime;
        mLastSwitchToReceiverTime = SystemClock.elapsedRealtime();
        useSpeaker = true;
    }

    private void stopSwitchSpeakerAndReceiver() {
        if (mSpeakReceiverTimer != null) {
            mSpeakReceiverTimer.cancel();
            mSpeakReceiverTimer = null;
        }
        if (mSpeakReceiverTimerTask != null) {
            mSpeakReceiverTimerTask.cancel();
            mSpeakReceiverTimerTask = null;
        }
    }

    private void finishAudioTest() {
        if (mTestSpeakerAndReceiver) {
            stopSwitchSpeakerAndReceiver();
        }

        if(mSpeakerTestable || mReceiverTestable ) {
            mHandler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    getAudioManager().setMode(AudioManager.MODE_NORMAL);
                }
            }, 0);
        }
    }

    ///-------------------------------------Camera Test--------------------------------------------

    private CameraManager mCameraManager;
    private int mCameraId = -1;
    private long mFontCameraTime = 0;
    private long mBackCameraTime = 0;
    private long mLastSwitchToFontCameraTime = 0;
    private long mLastSwitchToBackCameraTime = 0;

    private static final int START_CAMERA = 0;
    private static final int COLSE_CAMERA = 1;

    private boolean isCloseCameraPreview = false;

    private Handler mCameraHandler = new Handler() {
        public void handleMessage(Message msg) {
            Log.d(TAG, "handleMessage　" + msg.what);
            switch (msg.what) {
            case START_CAMERA:
                if (mCameraId == -1) {
                    startSwitchCamera();
                } else {
                    startCamera(mCameraId);
                }
                break;
            case COLSE_CAMERA:
                mCameraManager.closeCamera();
                break;
            default:
                break;
            }
        }
    };

    private void setupCamera() {
        mCameraManager = CameraManager.instance();
        mCameraSurface.setSurfaceTextureListener(new SurfaceTextureListener() {

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                Log.d(TAG, "onSurfaceTextureDestroyed");
                mCameraHandler.obtainMessage(COLSE_CAMERA).sendToTarget();
                return false;
            }

            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                Log.d(TAG, "onSurfaceTextureAvailable");
                mCameraHandler.obtainMessage(START_CAMERA).sendToTarget();
            }
        });
        mCameraSurface.setVisibility(View.VISIBLE);
    }

    private void testFrontCamera() {
        mCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
        setupCamera();
        mFontCameraTime = SystemClock.elapsedRealtime();
    }

    private void testBackCamera() {
        mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        setupCamera();
        mBackCameraTime = SystemClock.elapsedRealtime();
    }

    private void testCameraSwitch() {
        mCameraId = -1;
        setupCamera();
    }

    private void startCamera(int cameraId) {
        if (mCameraManager.getCameraState() == CameraManager.PREVIEW_STOPPED) {

            // update display orientation
            if (cameraId != -1) {
                setCameraDisplayOrientation(cameraId);
            }

            if (!mCameraManager.openCamera(cameraId)) {
                showToast(R.string.agingtest_camera_in_use);
                return;
            }

            CameraUtil.setCameraPreviewRatio_4_3(mCameraManager, this);
            CameraUtil.fitPreviewSize(this,
                    mCameraManager.getParameters(),
                    mCameraSurface);
            mCameraManager.startPreview(mCameraSurface.getSurfaceTexture());
            doFlashLightTest();
        } else {
            Log.d(TAG, "onClick mCameraManager.getCameraState() = " + mCameraManager.getCameraState());
        }
    }

    private void switchCamera() {
        setupCamera();

        final int cameraId = mCameraId;
        if (mCameraId != -1) {
            mCameraManager.closeCamera();
            mCameraId = -1;
        }

        if (cameraId == -1) {
            mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        } else if (cameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
            mCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
        } else if (cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        }

        SystemClock.sleep(100);
        startCamera(mCameraId);
    }

    private Timer mSwitchCameraTimer = null;
    private TimerTask mSwitchCameraTimerTask = null;

    private void startSwitchCamera() {
        if (mSwitchCameraTimer == null)
            mSwitchCameraTimer = new Timer();
        if (mSwitchCameraTimerTask == null)
            mSwitchCameraTimerTask = new TimerTask() {

                @Override
                public void run() {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            checkSwitchTime();
                            switchCamera();
                        }});
                }
            };
        if ((mSwitchCameraTimer != null) && (mSwitchCameraTimerTask != null))
            mSwitchCameraTimer.schedule(mSwitchCameraTimerTask, 0, 5000L);
    }

    private void checkSwitchTime() {
        if (mCameraId == -1) {
            mLastSwitchToBackCameraTime = SystemClock.elapsedRealtime();
        } else if (mCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
            mBackCameraTime += SystemClock.elapsedRealtime() - mLastSwitchToBackCameraTime;
            mLastSwitchToFontCameraTime = SystemClock.elapsedRealtime();
        } else if (mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            mFontCameraTime += SystemClock.elapsedRealtime() - mLastSwitchToFontCameraTime;
            mLastSwitchToBackCameraTime = SystemClock.elapsedRealtime();
        }
    }

    private void doFlashLightTest() {
        finishFlashLightTest();

        do {
            if(mFrontFlashCameraTestable) {
                if (checkClosePreview()) {
                    mCameraSurface.setVisibility(View.INVISIBLE);
                }
                if (mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    startFlashLightTest();
                    break;
                }
            }

            if (mBackFlashCameraTestable) {
                if (checkClosePreview()) {
                    mCameraSurface.setVisibility(View.INVISIBLE);
                }
                if (mCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    startFlashLightTest();
                    break;
                }
            }
        } while (false);
    }

    private boolean checkClosePreview() {
        return mCloseFCameraPreview && (mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT )
                || mCloseBCameraPreview && (mCameraId == Camera.CameraInfo.CAMERA_FACING_BACK);
    }

    public void setCameraDisplayOrientation(int cameraId) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        int rotation = getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else {     // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        Log.d(TAG,"" + result);

        if (mCameraManager != null) {
            mCameraManager.chechCameraDisplayOrientation(result);
        }
    }

    private void stopSwitchCamera() {
        if (mSwitchCameraTimer != null) {
            mSwitchCameraTimer.cancel();
            mSwitchCameraTimer = null;
        }
        if (mSwitchCameraTimerTask != null) {
            mSwitchCameraTimerTask.cancel();
            mSwitchCameraTimerTask = null;
        }
    }

    private void finishCameraTest() {
        if(mBackCameraTestable
                || mFrontCameraTestable
                || mBackFlashCameraTestable
                || mFrontFlashCameraTestable ) {
            stopSwitchCamera();

            if(mCameraManager!= null) {
                if(mCameraManager.getCamera() != null) {
                    mCameraManager.closeCamera();
                }
            }
        }
    }

///-----------------------------------------Light Test----------------------------------------------
    private Timer mFlashLightTimer = null;
    private TimerTask mFlashLightTimerTask = null;
    private Parameters mParameters = null;
    private List<String> mFlashModes = null;

    private final static int FLASH_OPEN_DELAY = 500; //ms
    private final static int FLASH_FREQUENCY = 500; //ms

    private void startFlashLightTest() {
        mParameters = mCameraManager.getParameters();
        if(mParameters == null) {
            return;
        }

        mFlashModes = mParameters.getSupportedFlashModes();
        if (mFlashModes == null) {
            return;
        }

        if (mFlashLightTimer == null) {
            mFlashLightTimer = new Timer();
        }
        if (mFlashLightTimerTask == null) {
            mFlashLightTimerTask = new TimerTask() {

                @Override
                public void run() {
                    setFlashMode();
                }
            };
        }

        if(mFlashLightTimer != null && mFlashLightTimerTask != null) {
            mFlashLightTimer.schedule(mFlashLightTimerTask, FLASH_OPEN_DELAY, FLASH_FREQUENCY);
        }
    }

    private void setFlashMode() {
        if (!mCameraManager.cameraIsOpened()) {
            return;
        }

        Log.d(TAG, "cameraIsOpened light Camera!!!");

        String flashMode = mParameters.getFlashMode();
        if (!Parameters.FLASH_MODE_TORCH.equals(flashMode)) {
            // Turn on the flash
            if (mFlashModes.contains(Parameters.FLASH_MODE_TORCH)) {
                mParameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
                mCameraManager.setParameters(mParameters);
            }
        } else {
            // Turn off the flash
            if (mFlashModes.contains(Parameters.FLASH_MODE_OFF)) {
                mParameters.setFlashMode(Parameters.FLASH_MODE_OFF);
                mCameraManager.setParameters(mParameters);
            }
        }
    }

    private void finishFlashLightTest() {
        if (mFlashLightTimer != null) {
            mFlashLightTimer.cancel();
            mFlashLightTimer = null;
        }
        if (mFlashLightTimerTask != null) {
            mFlashLightTimerTask.cancel();
            mFlashLightTimerTask = null;
        }
    }

    ///-------------------------------------Save time-----------------------------------------------

    private void doCheckTestTime() {
        mAgingSharedPreference = new AgingSharedPreference(getApplicationContext());

        if(mLastVideoSpeakerTime > 0) {
            mAgingSharedPreference.setValue(AgingTestItems.TEST_ITEM[0],
                    formatTime(mTestSpeakerAndReceiver ?
                                    mLastVideoSpeakerTime :
                                    SystemClock.elapsedRealtime() - mLastVideoSpeakerTime ));
            mLastVideoSpeakerTime = 0;
        }

        if(mLastVideoReceiverTime > 0) {
            mAgingSharedPreference.setValue(AgingTestItems.TEST_ITEM[1],
                    formatTime(mTestSpeakerAndReceiver ?
                            mLastVideoReceiverTime : SystemClock.elapsedRealtime() - mLastVideoReceiverTime ));
            mLastVideoReceiverTime = 0;
        }

        if(mVibratorTime > 0) {
            mAgingSharedPreference.setValue(AgingTestItems.TEST_ITEM[2],
                    formatTime(SystemClock.elapsedRealtime() - mVibratorTime));
            mVibratorTime = 0;
        }

        if(micLoopTime > 0) {
            mAgingSharedPreference.setValue(AgingTestItems.TEST_ITEM[3],
                    formatTime(SystemClock.elapsedRealtime() - micLoopTime));
            micLoopTime = 0;
        }

        if(mFontCameraTime > 0) {
            if (mFrontCameraTestable) {
                mAgingSharedPreference.setValue(AgingTestItems.TEST_ITEM[4],
                        formatTime(mTestSwitchCamera?
                                mFontCameraTime : SystemClock.elapsedRealtime() - mFontCameraTime));
            }

            if (mFrontFlashCameraTestable) {
                mAgingSharedPreference.setValue(AgingTestItems.TEST_ITEM[6],
                        formatTime(mTestSwitchCamera ?
                                mFontCameraTime : SystemClock.elapsedRealtime() - mFontCameraTime));
            }

            mFontCameraTime = 0;
        }

        if(mBackCameraTime > 0) {
            if (mBackCameraTestable) {
                mAgingSharedPreference.setValue(AgingTestItems.TEST_ITEM[5],
                        formatTime(mTestSwitchCamera ?
                                mBackCameraTime : SystemClock.elapsedRealtime() - mBackCameraTime));
            }

            if (mBackFlashCameraTestable) {
                mAgingSharedPreference.setValue(AgingTestItems.TEST_ITEM[7],
                        formatTime(mTestSwitchCamera ?
                                mBackCameraTime : SystemClock.elapsedRealtime() - mBackCameraTime));
            }

            mBackCameraTime = 0;
        }
    }

    ///-----------------------------------------Util------------------------------------------------

    public void showToast(Object paramObject) {
        if (paramObject == null)
            return;
        Toast.makeText(this, paramObject + "", Toast.LENGTH_LONG).show();
    }

    private static final String STORAGE_TIME_FORMAT = "%02d:%02d:%02d";

    private static final int SECOND_IN_HOUR     = 3600; //second
    private static final int SECOND_IN_MINUTE  = 60; //second

    private static String formatTime(long time) {
        time /= 1000;
        long hour = time / SECOND_IN_HOUR;
        long minute = (time - hour * SECOND_IN_HOUR) / SECOND_IN_MINUTE;
        return String.format(STORAGE_TIME_FORMAT, hour, minute,
                (time - SECOND_IN_HOUR * hour - minute * SECOND_IN_MINUTE));
    }

    private AudioManager getAudioManager() {
        return ((AudioManager) getSystemService("audio"));
    }
}
