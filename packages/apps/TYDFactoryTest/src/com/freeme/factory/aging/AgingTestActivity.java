package com.freeme.factory.aging;

import java.util.ArrayList;
import java.util.HashMap;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;

import com.freeme.factory.R;
import com.freeme.factory.config.TestConfig;

public class AgingTestActivity extends Activity implements View.OnClickListener {
	private static final String TAG = "AgingTestActivity";

	public static final String EXTRA_AGINGTEST_ITEMS    = "aging_test_items";
	public static final String EXTRA_AGINGTEST_DURATION = "aging_test_duration";
	//*/ tyd.sunyuepeng, 20190507. restart test.
	public static final String REBOOT_NUMBER = "reboot_number";
	private int mRebootNumber = 0;
	private AlertDialog mAlertDialog;
	private EditText mInputRebootNumber;
	//*/

	private GridView mGridView;
	private AgingTestAdapter mAgingTestItem;
	
	private Button   mStartBtn;
	private TextView mResultText;
	private EditText mHourText;
	private EditText mMinuteText;

	private LayoutInflater mInflater;

	private HashMap<String, Boolean> mTestItemMap = new HashMap<String, Boolean>();
	private String[] mAllTestItem = {};
	private String[] mTestItem = {};
	
	private String mDefaultUseTime = "00:00:00";

	private AgingSharedPreference mAgingSharedPreference;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.aging_main_activity);
		mInflater = LayoutInflater.from(this);
		mAllTestItem = getResources().getStringArray(R.array.aging_test_items);
		mTestItem = filerOutItem(mAllTestItem);
		
		mAgingTestItem = new AgingTestAdapter(this, mTestItem);
		mGridView = (GridView) findViewById(R.id.agingtest_gv);
		mGridView.setAdapter(mAgingTestItem);

		mStartBtn = (Button) findViewById(R.id.agingteststart_bt);
		mStartBtn.setOnClickListener(this);

		mResultText = (TextView) findViewById(R.id.duration_detail_tv);
		mHourText	= (EditText) findViewById(R.id.hour_edt);
		mMinuteText = (EditText) findViewById(R.id.minute_edt);

		initSharePreference();
		initTestMap();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mResultText.setText(initShowTime());
	}

	@Override
	public void onClick(View v) {
		if (!checkNeedTest()) {
			Toast.makeText(this, getString(R.string.agingtest_notice), Toast.LENGTH_LONG).show();
			return;
		}

		if (initTotalTestTime() <= 0) {
			Toast.makeText(this, getString(R.string.agingtest_duration_none_notice), Toast.LENGTH_LONG).show();
			return;
		}

		//*/ tyd.sunyuepeng, 20190507. restart test.
		if(TestConfig.TEST_EN_REBOOT){
			if((boolean)mTestItemMap.get(mAllTestItem[8])){
			    confirmRebootOperation();
			    return;
			}
		}
		//*/
		doAgingTest();
	}

	private void doAgingTest() {
		Intent intent = new Intent().setClass(this, AgingTestActivityImpl.class);
		intent.putExtra(EXTRA_AGINGTEST_ITEMS, getTestItem());
		intent.putExtra(EXTRA_AGINGTEST_DURATION, initTotalTestTime());
		this.startActivity(intent);
	}

	//*/ tyd.sunyuepeng, 20190507. restart test.
	private void confirmRebootOperation() {
		mInputRebootNumber = new EditText(this);
		mInputRebootNumber.setHint(R.string.agingtest_input_reboot_num);
		mInputRebootNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
		mAlertDialog = new AlertDialog.Builder(this)
				.setTitle(R.string.agingtest_alertdialog_reboot_title)
				.setView(mInputRebootNumber)
				.setMessage(R.string.agingtest_alertdialog_reboot_message)
				.setNegativeButton(R.string.agingtest_alertdialog_cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					    dialog.dismiss();
					}
				})
				.setPositiveButton(R.string.agingtest_alertdialog_start, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					    String userInputRebootNumber = mInputRebootNumber.getText().toString();
					    if (userInputRebootNumber.isEmpty()) {
					        Toast.makeText(getApplicationContext(), getString(R.string.number_empty_toast), Toast.LENGTH_SHORT).show();
						return;
					    }
					    mRebootNumber = Integer.parseInt(userInputRebootNumber);
					    if(mRebootNumber > 500 || mRebootNumber < 1){
					        Toast.makeText(getApplicationContext(), getString(R.string.number_exceed_toast), Toast.LENGTH_SHORT).show();
						return;
					    }
					    mRebootNumber--;
					    mAgingSharedPreference.setIntValue(REBOOT_NUMBER, mRebootNumber);
					    PowerManager mPowerManager = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
					    mPowerManager.reboot("");
					}
				}).create();
		mAlertDialog.show();
	}
	//*/

	private void initSharePreference() {
		mAgingSharedPreference = new AgingSharedPreference(
				getApplicationContext());
		
		for (String item : AgingTestItems.TEST_ITEM) {
			if (mAgingSharedPreference.getValue(item) == null) {
				mAgingSharedPreference.setValue(item, mDefaultUseTime);
			}
		}
		//*/ tyd.sunyuepeng, 20190507. restart test.
		mAgingSharedPreference.setIntValue(REBOOT_NUMBER, mRebootNumber);
		//*/
	}

	private String initShowTime() {
		String[] saveItems = filerOutItem(AgingTestItems.TEST_ITEM);
		StringBuilder localStringBuilder = new StringBuilder();
		
		localStringBuilder.append(getString(R.string.agingtest_last_duration_detail) + "\n");
		for (int i = 0; i < mTestItem.length; i++) {
			localStringBuilder.append(mTestItem[i] + ":" + 
					mAgingSharedPreference.getValue(saveItems[i]) + "\n");
		}
		return localStringBuilder.toString();
	}

	private long initTotalTestTime() {
		String minute = mMinuteText.getText().toString();
		String hour = mHourText.getText().toString();

		if (mMinuteText.getText().toString().length() < 1) {
			minute = "0";
		}
		if (mHourText.getText().toString().length() < 1) {
			hour = "0";
		}

		return Integer.parseInt(hour) * 3600 + Integer.parseInt(minute) * 60;
	}

	private void initTestMap() {
		for (String item : mTestItem) {
			mTestItemMap.put(item, false);
		}
	}

	private boolean checkNeedTest() {
		boolean test = false;
		for (String item : mTestItem) {
			test |= (Boolean) mTestItemMap.get(item);
		}
		
		return test;
	}

	private AgingTestItems getTestItem() {
		AgingTestItems localAgingTestItems = new AgingTestItems();
		
		localAgingTestItems.setVideo(		getValue(mAllTestItem[0]) || getValue(mAllTestItem[1]));
		localAgingTestItems.setSpeaker(	getValue(mAllTestItem[0]));
		localAgingTestItems.setReceiver(	getValue(mAllTestItem[1]));
		localAgingTestItems.setVibrate(	getValue(mAllTestItem[2]));
		localAgingTestItems.setMicLoop(	getValue(mAllTestItem[3]));
		localAgingTestItems.setFrontCamera(getValue(mAllTestItem[4]));
		localAgingTestItems.setBackCamera( getValue(mAllTestItem[5]));
		localAgingTestItems.setFrontLight(	getValue(mAllTestItem[6]));
		localAgingTestItems.setBackLight(	getValue(mAllTestItem[7]));
		
		return localAgingTestItems;
	}
	
	private boolean getValue(String item) {
		return mTestItemMap.get(item) == null ? false : (Boolean) mTestItemMap.get(item);
	}
	
	
	private String[] filerOutItem(String[] items) {
		ArrayList<String> tempList = new ArrayList<String>();
		for (String item : items) {
			tempList.add(item);
		}
		
		if(!TestConfig.TEST_EN_CAMERA_FORE) {
			tempList.remove(items[4]);
		}
		
		if(!TestConfig.TEST_EN_CAMERA_BACK) {
			tempList.remove(items[5]);
		}
		
		if (!TestConfig.TEST_EN_STROBE_FORE) {
			tempList.remove(items[6]);
		}
		
		if (!TestConfig.TEST_EN_STROBE_BACK) {
			tempList.remove(items[7]);
		}

		//*/ tyd.sunyuepeng, 20190507. restart test.
		if (!TestConfig.TEST_EN_REBOOT) {
			tempList.remove(items[8]);
		}
		//*/

		String[] tempStr = {};
		
		return tempList.toArray(tempStr);
	}
	
	/// ----------------------------------------AgingTestAdapter-----------------------------------

	public class AgingTestAdapter extends BaseAdapter {
		private String[] mItem;

		public AgingTestAdapter(Context context, String[] paraString) {
			mItem = paraString;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = mInflater.inflate(R.layout.aging_check_item, null);
				holder.checkbox = (CheckBox) convertView.findViewById(R.id.agingtest_item);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.checkbox.setId(position);
			holder.checkbox.setText(mItem[position]);
			holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							mTestItemMap.put(buttonView.getText().toString(), isChecked);
						}
					});
			holder.checkbox.setChecked(false);
			holder.id = position;
			return convertView;
		}

		class ViewHolder {
			CheckBox checkbox;
			int id;
		}

		@Override
		public int getCount() {
			return mItem.length;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
	}
}
