package com.freeme.factory.config;

public class ProjectConfig {
	// gps
	public static final boolean 	EN_PREDEF_GPS_SNR_THRESHOLD = true;
	public static final int 		CO_PREDEF_GPS_SNR_THRESHOLD = 37;
	public static final int 		CO_PREDEF_GPS_SNR_NUM = 5;
	
	// wifi
	public static final boolean 	EN_PREDEF_WIFI_OK = true;
	public static final String  	CO_PREDEF_WIFI_OK_SSID = "mtkguest";
	
	// strobe
	public static final boolean 	CO_PREDEF_CAPTURE_STROB_ON = false;
	
	// software info
	public static final String  	CO_PREDEF_SW_INFO_SECRET_CODE = "*#6810#";
	
	// reset device
	public static final boolean 	EN_PREDEF_RESET_DEVICE_SHUTDOWN = true;
	public static final boolean 	EN_PREDEF_RESET_DEVICE_STORAGE  = true;
	public static final String[] 	CO_PREDEF_RESET_DEVICE_STORAGE_FILES = {
		"DCIM", "Pictures",
	};
	
	// infrared
	public static final int   CO_PREDEF_CONSUMER_IR_FREQS = 38000;
	public static final int[] CO_PREDEF_CONSUMER_IR_PATTERN = { // Samsung TV Power // 9012
		4496, 4496, 560, 1680, 560, 1680, 560, 1680, 560, 560, 560, 560, 560,
        560, 560, 560, 560, 560, 560, 1680, 560, 1680, 560, 1680, 560, 560, 560,
        560, 560, 560, 560, 560, 560, 560, 560, 560, 560, 1680, 560, 560, 560,
        560, 560, 560, 560, 560, 560, 560, 560, 560, 560, 1680, 560, 560, 560,
        1680, 560, 1680, 560, 1680, 560, 1680, 560, 1680, 560, 1680, 560, 40000
	};
}
