package com.freeme.factory.config;

import java.util.Arrays;
import java.util.Set;

import android.content.Context;
import android.util.ArraySet;

import com.freeme.factory.fingerprint.FingerprintTest;
import com.google.android.collect.Sets;

import com.freeme.factory.R;

public class TestConfig {

    /** <DEFINE> Test mode */
    public static final int TEST_MODE_MANUAL = 0;
    public static final int TEST_MODE_ALL    = 1;
    public static final int TEST_MODE_AUTO   = 2;

    /** <DEFINE> Test enableds */
    public static final boolean TEST_EN_SENSOR_MAGN = FeatureOption.FREEME_FACTORYMODE_MSENSOR;
    public static final boolean TEST_EN_SENSOR_ACCE = FeatureOption.FREEME_FACTORYMODE_GSENSOR;
    public static final boolean TEST_EN_SENSOR_LIGH = FeatureOption.FREEME_FACTORYMODE_LSENSOR;
    public static final boolean TEST_EN_SENSOR_PROX = FeatureOption.FREEME_FACTORYMODE_DSENSOR;
    public static final boolean TEST_EN_SENSOR_GYRO = FeatureOption.FREEME_FACTORYMODE_GYROSCOPE;
    public static final boolean TEST_EN_SENSOR_STEP = FeatureOption.FREEME_FACTORYMODE_STEPSENSOR;
    public static final boolean TEST_EN_SENSOR_HALL = FeatureOption.FREEME_FACTORYMODE_HSENSOR;
    public static final boolean TEST_EN_CAMERA_FORE = FeatureOption.FREEME_FACTORYMODE_FCAMERA;
    public static final boolean TEST_EN_CAMERA_BACK = FeatureOption.FREEME_FACTORYMODE_BCAMERA;
    public static final boolean TEST_EN_CAMERA_DUAL = FeatureOption.FREEME_FACTORYMODE_CAMERA_DUAL;
    public static final boolean TEST_EN_WIFI        = FeatureOption.FREEME_FACTORYMODE_WIFI;
    public static final boolean TEST_EN_GPS         = FeatureOption.FREEME_FACTORYMODE_GPS;
    public static final boolean TEST_EN_MHL         = FeatureOption.FREEME_FACTORYMODE_MHL;
    public static final boolean TEST_EN_LIGHT_KEYBOARD  = FeatureOption.FREEME_FACTORYMODE_LED;
    public static final boolean TEST_EN_LIGHT_ATTENTION = FeatureOption.FREEME_FACTORYMODE_ATLIGHT;
    public static final boolean TEST_EN_AUDIO_HIFI      = FeatureOption.FREEME_FACTORYMODE_HIFI;
    public static final boolean TEST_EN_AUDIO_DUALMIC   = FeatureOption.FREEME_FACTORYMODE_DUALMIC;
    public static final boolean TEST_EN_INFRARED        = FeatureOption.FREEME_FACTORYMODE_INFRARED;
    public static final boolean TEST_EN_AUDIO_DUALSPEAKER = FeatureOption.FREEME_FACTORYMODE_AUDIO_DUALSPEAKER;
    public static final boolean TEST_EN_STROBE_BACK     = FeatureOption.FREEME_FACTORYMODE_STROBE_BACK;
    public static final boolean TEST_EN_STROBE_FORE     = FeatureOption.FREEME_FACTORYMODE_STROBE_FRONT;
    public static final boolean TEST_EN_GEMINI_SUPPORT  = FeatureOption.FREEME_FACTORYMODE_GEMINI_SUPPORT;
    public static final boolean TEST_EN_USB_OTG         = FeatureOption.FREEME_FACTORYMODE_USB_OTG;
    public static final boolean TEST_EN_STORAGE_SDCARD  = FeatureOption.FREEME_FACTORYMODE_STORAGE_SDCARD;
    public static final boolean TEST_EN_CAMERA_BACK_VICE = FeatureOption.FREEME_FACTORYMODE_CAMERA_BACK_VICE;
    public static final boolean TEST_EN_NFC             = FeatureOption.FREEME_FACTORYMODE_NFC;
    public static final boolean TEST_EN_HEADSET         = FeatureOption.FREEME_FACTORYMODE_HEADSET;
    public static final boolean TEST_EN_SHAKE           = FeatureOption.FREEME_FACTORYMODE_SHAKE;
    //*/ tyd.sunyuepeng, 20190507. restart test.
    public static final boolean TEST_EN_REBOOT          = FeatureOption.FREEME_FACTORYMODE_AGING_TEST_REBOOT;
    //*/

    // CTA
    public static final boolean TEST_EN_FM          = FeatureOption.FREEME_FACTORYMODE_FM;
    public static final boolean TEST_EN_BT          = FeatureOption.FREEME_FACTORYMODE_BT;
    //droi.fanshunzhong, 20180312.show TEE status.
    public static final boolean TEST_EN_TEE         = FeatureOption.FREEME_TEE_TEST;
    public static final boolean TEST_EN_PRESSURE_SENSOR         = FeatureOption.FREEME_FACTORYMODE_PRESSURE_SENSOR;

    /** <DEFINE> Test item id */
    public static final int TEST_ITEM_START             = 0;
    public static final int TEST_ITEM_CAMERA_BACK       = TEST_ITEM_START;
    public static final int TEST_ITEM_CAMERA_FRONT      = 1;
    public static final int TEST_ITEM_GPS               = 2;
    public static final int TEST_ITEM_POWER             = 3;
    public static final int TEST_ITEM_KEY               = 4;
    public static final int TEST_ITEM_AUDIO_SPEAKER     = 5;
    public static final int TEST_ITEM_HEADSET           = 6;
    public static final int TEST_ITEM_MIC               = 7;
    public static final int TEST_ITEM_RECEIVER          = 8;
    public static final int TEST_ITEM_WIFI              = 9;
    public static final int TEST_ITEM_BT                = 10;
    public static final int TEST_ITEM_SHAKE             = 11;
    public static final int TEST_ITEM_CALL              = 12;
    public static final int TEST_ITEM_BL                = 13;
    public static final int TEST_ITEM_MEMORY            = 14;
    public static final int TEST_ITEM_GSENSOR           = 15;
    public static final int TEST_ITEM_MSENSOR           = 16;
    public static final int TEST_ITEM_LSENSOR           = 17;
    public static final int TEST_ITEM_DSENSOR           = 18;
    public static final int TEST_ITEM_STORAGE           = 19;
    public static final int TEST_ITEM_TOUCH             = 20;
    public static final int TEST_ITEM_LCD               = 21;
    public static final int TEST_ITEM_FM                = 22;
    public static final int TEST_ITEM_SIM               = 23;
    public static final int TEST_ITEM_LED               = 24;
    public static final int TEST_ITEM_GYROSCOPE         = 25;
    public static final int TEST_ITEM_MHL               = 26;
    public static final int TEST_ITEM_LIGHT_ATTENTION     = 27;
    public static final int TEST_ITEM_HSENSOR             = 28;
    public static final int TEST_ITEM_HIFI                = 29;
    public static final int TEST_ITEM_SUBMIC              = 30;
    public static final int TEST_ITEM_INFRARED            = 31;
    public static final int TEST_ITEM_AUDIO_SPEAKER_LEFT  = 32;
    public static final int TEST_ITEM_AUDIO_SPEAKER_RIGHT = 33;
    public static final int TEST_ITEM_STROBE_BACK         = 34;
    public static final int TEST_ITEM_STROBE_FORE         = 35;
    public static final int TEST_ITEM_FINGERPRINT         = 36;
    public static final int TEST_ITEM_USB_OTG             = 37;
    public static final int TEST_ITEM_CAMERA_BACK_VICE    = 38;
    public static final int TEST_ITEM_NFC                 = 39;
    public static final int TEST_ITEM_STEPSENSOR          = 40;
    //droi.fanshunzhong, 20180312.show TEE status.
    public static final int TEST_ITEM_TEE                 = 41;
    public static final int TEST_ITEM_PRESSURE_SENSOR     = 42;
    public static final int TEST_ITEM_TOTAL               = 43;

    /** All test items */
    private static final int[] kTestItems_Full = {
		/* -- TODO: Custom */
		TEST_ITEM_LCD,
		TEST_ITEM_CAMERA_BACK,
		TEST_ITEM_CAMERA_FRONT,
		TEST_ITEM_CAMERA_BACK_VICE,
		TEST_ITEM_TOUCH,
		TEST_ITEM_POWER,
		TEST_ITEM_KEY,
		TEST_ITEM_AUDIO_SPEAKER,
		TEST_ITEM_AUDIO_SPEAKER_LEFT,
		TEST_ITEM_AUDIO_SPEAKER_RIGHT,
		TEST_ITEM_MIC,
		TEST_ITEM_SUBMIC,
		TEST_ITEM_HEADSET,
		TEST_ITEM_HIFI,
		TEST_ITEM_FM,
		TEST_ITEM_RECEIVER,
		TEST_ITEM_BT,
		TEST_ITEM_SHAKE,
		TEST_ITEM_DSENSOR,
		TEST_ITEM_LSENSOR,
		TEST_ITEM_CALL,
		TEST_ITEM_BL,
		TEST_ITEM_LED,
		TEST_ITEM_LIGHT_ATTENTION,
		TEST_ITEM_MEMORY,
		TEST_ITEM_SIM,
		TEST_ITEM_STORAGE,
		TEST_ITEM_HSENSOR,
		TEST_ITEM_WIFI,
		TEST_ITEM_GSENSOR,
		TEST_ITEM_MSENSOR,
		TEST_ITEM_GYROSCOPE,
		TEST_ITEM_MHL,
		TEST_ITEM_INFRARED,
		TEST_ITEM_GPS,
		TEST_ITEM_STROBE_BACK,
		TEST_ITEM_STROBE_FORE,
		TEST_ITEM_FINGERPRINT,
		TEST_ITEM_USB_OTG,
		TEST_ITEM_NFC,
		TEST_ITEM_STEPSENSOR,
        TEST_ITEM_TEE,
        TEST_ITEM_PRESSURE_SENSOR,
    };

    /** Filter */

    private static final Set<Integer> kFiltedItems_Platform_Filter = Sets.newHashSet();
    private static final void __disable_feature(int feature, boolean enabled) {
        if ( ! enabled) {
            kFiltedItems_Platform_Filter.add(feature);
        }
    }
    static {
        /* -- TODO: Custom */
        __disable_feature(TEST_ITEM_MSENSOR, TEST_EN_SENSOR_MAGN);
        __disable_feature(TEST_ITEM_GSENSOR, TEST_EN_SENSOR_ACCE);
        __disable_feature(TEST_ITEM_LSENSOR, TEST_EN_SENSOR_LIGH);
        __disable_feature(TEST_ITEM_DSENSOR, TEST_EN_SENSOR_PROX);
        __disable_feature(TEST_ITEM_GYROSCOPE, TEST_EN_SENSOR_GYRO);
        __disable_feature(TEST_ITEM_STEPSENSOR, TEST_EN_SENSOR_STEP);
        __disable_feature(TEST_ITEM_HSENSOR, TEST_EN_SENSOR_HALL);
        __disable_feature(TEST_ITEM_CAMERA_FRONT, TEST_EN_CAMERA_FORE);
        __disable_feature(TEST_ITEM_CAMERA_BACK, TEST_EN_CAMERA_BACK);
        __disable_feature(TEST_ITEM_WIFI, TEST_EN_WIFI);
        __disable_feature(TEST_ITEM_GPS, TEST_EN_GPS);
        __disable_feature(TEST_ITEM_MHL, TEST_EN_MHL);
        __disable_feature(TEST_ITEM_LED, TEST_EN_LIGHT_KEYBOARD);
        __disable_feature(TEST_ITEM_LIGHT_ATTENTION, TEST_EN_LIGHT_ATTENTION);
        __disable_feature(TEST_ITEM_HIFI, TEST_EN_AUDIO_HIFI);
        __disable_feature(TEST_ITEM_SUBMIC, TEST_EN_AUDIO_DUALMIC);
        __disable_feature(TEST_ITEM_INFRARED, TEST_EN_INFRARED);

        __disable_feature(TEST_ITEM_AUDIO_SPEAKER, !TEST_EN_AUDIO_DUALSPEAKER);
        __disable_feature(TEST_ITEM_AUDIO_SPEAKER_LEFT, TEST_EN_AUDIO_DUALSPEAKER);
        __disable_feature(TEST_ITEM_AUDIO_SPEAKER_RIGHT, TEST_EN_AUDIO_DUALSPEAKER);
        __disable_feature(TEST_ITEM_STROBE_BACK, TEST_EN_STROBE_BACK);
        __disable_feature(TEST_ITEM_STROBE_FORE, TEST_EN_STROBE_FORE);

        __disable_feature(TEST_ITEM_USB_OTG, TEST_EN_USB_OTG);
        __disable_feature(TEST_ITEM_CAMERA_BACK_VICE, TEST_EN_CAMERA_BACK_VICE);
        __disable_feature(TEST_ITEM_NFC, TEST_EN_NFC);
        __disable_feature(TEST_ITEM_HEADSET, TEST_EN_HEADSET);
        __disable_feature(TEST_ITEM_SHAKE, TEST_EN_SHAKE);

        // CTA
        __disable_feature(TEST_ITEM_FM, TEST_EN_FM);
        __disable_feature(TEST_ITEM_BT, TEST_EN_BT);
        //droi.fanshunzhong, 20180312.show TEE status.
        __disable_feature(TEST_ITEM_TEE, TEST_EN_TEE);
        __disable_feature(TEST_ITEM_PRESSURE_SENSOR, TEST_EN_PRESSURE_SENSOR);
    }

    private static final Set<Integer> kFiltedItems_AutoTest_Filter = Sets.newHashSet(
            /* -- TODO: Custom */
            TEST_ITEM_LED
    );

    private static final Set<Integer> kFiltedItems_AllTest_Filter = Sets.newHashSet(
            /* -- TODO: Custom */
    );

    public static String getNameByItemId(Context c, int id) {
        final int resid;
        switch (id) {
        case TestConfig.TEST_ITEM_TOUCH:
            resid = R.string.touchscreen_name;
            break;
        case TestConfig.TEST_ITEM_LCD:
            resid = R.string.lcd_name;
            break;
        case TestConfig.TEST_ITEM_GPS:
            resid = R.string.gps_name;
            break;
        case TestConfig.TEST_ITEM_POWER:
            resid = R.string.battery_name;
            break;
        case TestConfig.TEST_ITEM_KEY:
            resid = R.string.KeyCode_name;
            break;
        case TestConfig.TEST_ITEM_AUDIO_SPEAKER:
            resid = R.string.speaker_name;
            break;
        case TestConfig.TEST_ITEM_HEADSET:
            resid = R.string.headset_name;
            break;
        case TestConfig.TEST_ITEM_MIC:
            resid = R.string.microphone_name;
            break;
        case TestConfig.TEST_ITEM_RECEIVER:
            resid = R.string.earphone_name;
            break;
        case TestConfig.TEST_ITEM_WIFI:
            resid = R.string.wifi_name;
            break;
        case TestConfig.TEST_ITEM_BT:
            resid = R.string.bluetooth_name;
            break;
        case TestConfig.TEST_ITEM_SHAKE:
            resid = R.string.vibrator_name;
            break;
        case TestConfig.TEST_ITEM_CALL:
            resid = R.string.telephone_name;
            break;
        case TestConfig.TEST_ITEM_BL:
            resid = R.string.backlight_name;
            break;
        case TestConfig.TEST_ITEM_MEMORY:
            resid = R.string.memory_name;
            break;
        case TestConfig.TEST_ITEM_GSENSOR:
            resid = R.string.gsensor_name;
            break;
        case TestConfig.TEST_ITEM_MSENSOR:
            resid = R.string.msensor_name;
            break;
        case TestConfig.TEST_ITEM_LSENSOR:
            resid = R.string.lsensor_name;
            break;
        case TestConfig.TEST_ITEM_DSENSOR:
            resid = R.string.psensor_name;
            break;
        case TestConfig.TEST_ITEM_STORAGE:
            resid = R.string.sdcard_name;
            break;
        case TestConfig.TEST_ITEM_CAMERA_BACK:
            resid = R.string.camera_name;
            break;
        case TestConfig.TEST_ITEM_CAMERA_FRONT:
            resid = R.string.subcamera_name;
            break;
        case TestConfig.TEST_ITEM_FM:
            resid = R.string.FMRadio;
            break;
        case TestConfig.TEST_ITEM_SIM:
            resid = R.string.sim_name;
            break;
        case TestConfig.TEST_ITEM_LED:
            resid = R.string.led_name;
            break;
        case TestConfig.TEST_ITEM_GYROSCOPE:
            resid = R.string.gyroscopesensor_name;
            break;
        case TestConfig.TEST_ITEM_STEPSENSOR:
            resid = R.string.step_sensor_name;
            break;
        case TestConfig.TEST_ITEM_MHL:
            resid = R.string.mhl_name;
            break;
        case TestConfig.TEST_ITEM_LIGHT_ATTENTION:
            resid = R.string.attentionlight_name;
            break;
        case TestConfig.TEST_ITEM_HSENSOR:
            resid = R.string.hsensor_name;
            break;
        case TestConfig.TEST_ITEM_HIFI:
            resid = R.string.hifi_name;
            break;
        case TestConfig.TEST_ITEM_SUBMIC:
            resid = R.string.submicrophone_name;
            break;
        case TestConfig.TEST_ITEM_INFRARED:
            resid = R.string.infrared;
            break;
        case TestConfig.TEST_ITEM_AUDIO_SPEAKER_LEFT:
            resid = R.string.speaker_name_left;
            break;
        case TestConfig.TEST_ITEM_AUDIO_SPEAKER_RIGHT:
            resid = R.string.speaker_name_right;
            break;
        case TEST_ITEM_STROBE_BACK:
            resid = R.string.label_strobe_back;
            break;
        case TEST_ITEM_STROBE_FORE:
            resid = R.string.label_strobe_fore;
            break;
        case TEST_ITEM_FINGERPRINT:
            resid = R.string.label_fingerprint;
            break;
        case TEST_ITEM_USB_OTG:
            resid = R.string.otg_label;
            break;
        case TEST_ITEM_CAMERA_BACK_VICE:
            resid = R.string.camera_back_vice_label;
            break;
        case TEST_ITEM_NFC:
            resid = R.string.nfc_label;
            break;
        //*/droi.fanshunzhong, 20180312. show tee status.
        case TEST_ITEM_TEE:
            resid = R.string.tee_label;
            break;
        //*/
        case TEST_ITEM_PRESSURE_SENSOR:
            resid = R.string.pressure_sensor;
            break;
        default:
            resid = 0;
            break;
        }

        if (resid != 0) {
            return c.getString(resid);
        }
        return "";
    }


    private static TestConfig sInstance;
    public static TestConfig getInstance() {
        if (sInstance == null) {
            sInstance = new TestConfig();
        }
        return sInstance;
    }

    ///////////////////////////////////////////////////////////////////////////
    private Context mContext;

    private int[] mTestItemsAll  = new int[0];
    private int[] mTestItemsAuto = new int[0];

    private TestConfig() {
        // ignore
    }

    public void init(Context context) {
        mContext = context.getApplicationContext();

        // generate complete items except platform limit
        final int[] validItems = except(kTestItems_Full, kFiltedItems_Platform_Filter);

        // generate items of ALL
        mTestItemsAll  = except(validItems, kFiltedItems_AllTest_Filter);

        // generate items of Auto
        mTestItemsAuto = except(mTestItemsAll, kFiltedItems_AutoTest_Filter);
    }

    public int[] getTestItems(int mode) {
        switch (mode) {
        case TEST_MODE_ALL:
            return except(mTestItemsAll, getPlatformFilterRuntime(mContext));
        case TEST_MODE_AUTO:
            return except(mTestItemsAuto, getPlatformFilterRuntime(mContext));
        default:
            return new int[0];
        }
    }

    private static int[] except(int[] source, Set<Integer> except) {
        if (except.isEmpty()) {
            return source;
        }

        int[] result       = new int[source.length];
        int   result_count = 0;

        final int N = source.length;
        for (int i = 0; i < N; i++) {
            int item = source[i];
            if (except.contains(item))
                continue;
            result[result_count++] = item;
        }

        return Arrays.copyOf(result, result_count);
    }

    private static Set<Integer> getPlatformFilterRuntime(Context context) {
        Set<Integer> filters = new ArraySet<>();

        { // fingerprint
            if (!FingerprintTest.hasFingerprintHardware(context)) {
                filters.add(TEST_ITEM_FINGERPRINT);
            }
        }

        return filters;
    }
}
