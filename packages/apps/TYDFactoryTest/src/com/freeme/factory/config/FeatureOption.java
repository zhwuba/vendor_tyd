package com.freeme.factory.config;

import android.os.SystemProperties;
import android.util.Log;

public class FeatureOption {

    public static final boolean FREEME_FACTORYMODE_MSENSOR             = getBoolean("ro.vendor.tyd.hw_sensor_magnetic");
    public static final boolean FREEME_FACTORYMODE_GSENSOR             = getBoolean("ro.vendor.tyd.hw_sensor_acce");
    public static final boolean FREEME_FACTORYMODE_LSENSOR             = getBoolean("ro.vendor.tyd.hw_sensor_light");
    public static final boolean FREEME_FACTORYMODE_DSENSOR             = getBoolean("ro.vendor.tyd.hw_sensor_proximity");
    public static final boolean FREEME_FACTORYMODE_GYROSCOPE           = getBoolean("ro.vendor.tyd.hw_sensor_gyroscope");
    public static final boolean FREEME_FACTORYMODE_STEPSENSOR          = getBoolean("ro.vendor.tyd.hw_sensor_step");
    public static final boolean FREEME_FACTORYMODE_HSENSOR             = getBoolean("ro.vendor.tyd.hw_sensor_hall");
    public static final boolean FREEME_FACTORYMODE_FCAMERA             = getBoolean("ro.vendor.tyd.hw_camera_front");
    public static final boolean FREEME_FACTORYMODE_BCAMERA             = getInt("ro.vendor.tyd.hw_camera_back", 0) > 0;
    public static final boolean FREEME_FACTORYMODE_CAMERA_DUAL         = getInt("ro.vendor.tyd.hw_camera_back", 0) == 2;
    public static final boolean FREEME_FACTORYMODE_WIFI                = getBoolean("ro.vendor.tyd.hw_wifi");
    public static final boolean FREEME_FACTORYMODE_GPS                 = getBoolean("ro.vendor.tyd.hw_gps");
    public static final boolean FREEME_FACTORYMODE_MHL                 = getBoolean("ro.vendor.tyd.hw_mhl");
    public static final boolean FREEME_FACTORYMODE_INFRARED            = getBoolean("ro.vendor.tyd.hw_infrared");
    public static final boolean FREEME_FACTORYMODE_LED                 = getBoolean("ro.vendor.tyd.hw.light_keyboard");
    public static final boolean FREEME_FACTORYMODE_ATLIGHT             = getBoolean("ro.vendor.tyd.hw_light_attention");
    public static final boolean FREEME_FACTORYMODE_HIFI                = getBoolean("ro.vendor.tyd.hw_audio_hifi");
    public static final boolean FREEME_FACTORYMODE_DUALMIC             = getBoolean("ro.vendor.tyd.hw_audio_dualmic");
    public static final boolean FREEME_FACTORYMODE_AUDIO_DUALSPEAKER   = getBoolean("ro.vendor.tyd.hw_audio_dualspeaker");
    public static final boolean FREEME_FACTORYMODE_STROBE_BACK         = getBoolean("ro.vendor.tyd.hw_strobe_back");
    public static final boolean FREEME_FACTORYMODE_STROBE_FRONT        = getBoolean("ro.vendor.tyd.hw_strobe_front");
    public static final boolean FREEME_FACTORYMODE_GEMINI_SUPPORT      = android.telephony.TelephonyManager.getDefault().getPhoneCount() == 2;
    public static final boolean FREEME_FACTORYMODE_USB_OTG             = getBoolean("ro.vendor.tyd.hw_usb_otg");
    public static final boolean FREEME_FACTORYMODE_STORAGE_SDCARD      = getBoolean("ro.vendor.tyd.hw_storage_sdcard");
    public static final boolean FREEME_FACTORYMODE_CAMERA_BACK_VICE    = getBoolean("ro.vendor.tyd.hw_camera_back_vice");
    public static final boolean FREEME_FACTORYMODE_NFC                 = getBoolean("ro.vendor.tyd.hw_nfc");
    public static final boolean FREEME_FACTORYMODE_HEADSET             = getBoolean("ro.vendor.tyd.hw_headset");
    public static final boolean FREEME_FACTORYMODE_SHAKE               = getBoolean("ro.vendor.tyd.hw_shake");
    //*/ tyd.sunyuepeng, 20190507. restart test.
    public static final boolean FREEME_FACTORYMODE_AGING_TEST_REBOOT   = getBoolean("ro.vendor.tyd.aging_test_reboot");
    //*/

    // CTA
    public static final boolean FREEME_FACTORYMODE_FM                  = getBoolean("ro.vendor.tyd.hw_fm");
    public static final boolean FREEME_FACTORYMODE_BT                  = getBoolean("ro.vendor.tyd.hw_bt");

    //*/droi.fanshunzhong, 20180312. show TEE info
    public static final boolean FREEME_TRUSTKERNEL_TEE                 =  getInt("ro.mtk.trustkernel_tee_support", 0) == 1;
    public static final boolean FREEME_BEANPOD_TEE                     =  getInt("ro.vendor.mtk_microtrust_tee_support", 0) == 1;
    public static final boolean FREEME_TEE_TEST    =  FREEME_BEANPOD_TEE || FREEME_TRUSTKERNEL_TEE;
    //*/

    public static final boolean FREEME_FACTORYMODE_PRESSURE_SENSOR   =   getBoolean("ro.vendor.tyd.hw_pressure_sensor");

    private static boolean getBoolean(String key) {
        return SystemProperties.getBoolean(key, false);
    }

    private static int getInt(String key, int defaultVal) {
        Log.i("zwb", " key = "+ key + " value = " + SystemProperties.getInt(key, defaultVal));
        return SystemProperties.getInt(key, defaultVal);
    }
}
