package com.freeme.factory.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;

//import vendor.freeme.hardware.soul.V1_0.ISoul;

public final class TestResult {
    private static final String TAG = "TestResult";

    public static final int RESULT_NONE = 0;
    public static final int RESULT_OK   = 1;
    public static final int RESULT_FAIL = 2;

    private static TestResult sInstance;
    public static TestResult getInstance() {
        if (sInstance == null) {
            sInstance = new TestResult();
        }
        return sInstance;
    }

    interface Backend {
        String KEY_DETAIL = "DETAIL";

        void create(Context context);
        void put(String key, String value);
        String get(String key);
        void destory();
    }

    private Backend mBackend;
    private StringBuilder mValues;

    private class AsyncHandler extends Handler {
        static final int MSG_PUT_RESULTS = 1;

        AsyncHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_PUT_RESULTS:
                    mBackend.put(Backend.KEY_DETAIL, (String) msg.obj);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }
    private Handler mHandler;

    private TestResult() {
        // Do not initialize
    }

    public void create(Context context) {
        HandlerThread thread = new HandlerThread(TAG);
        thread.start();
        mHandler = new AsyncHandler(thread.getLooper());

        mBackend = new NmBackend();
        mBackend.create(context);
        mValues = new StringBuilder(mBackend.get(Backend.KEY_DETAIL));
    }

    public void destory() {
        if (mHandler != null) {
            mHandler.getLooper().quitSafely();
        }
        if (mBackend != null) {
            mBackend.destory();
            mBackend = null;
        }
    }

    public void put(int id, int resultCode) {
        String latestResult = encode(id + 1, resultCode);
        mHandler.removeMessages(AsyncHandler.MSG_PUT_RESULTS);
        mHandler.obtainMessage(AsyncHandler.MSG_PUT_RESULTS, 0, 0, latestResult)
                .sendToTarget();
    }
    public int get(int id) {
        return decode(id + 1);
    }
    public void put(int resultCode) {
        put(-1/*master*/, resultCode);
    }
    public int get() {
        return get(-1/*master*/);
    }

    private String encode(int id, int resultCode) {
        if (mValues == null) {
            mValues = new StringBuilder();
        }
        if (id >= mValues.length()) {
            for (int i = mValues.length(); i < id + 1; i++) {
                mValues.append(RESULT_NONE);
            }
        }
        mValues.setCharAt(id, Integer.toString(resultCode).charAt(0));
        return mValues.toString();
    }
    private int decode(int id) {
        if (mValues == null || id >= mValues.length()) {
            return RESULT_NONE;
        }
        return validate(Character.toString(mValues.charAt(id)));
    }
    private int validate(String character) {
        try {
            int value = Integer.parseInt(character);
            switch (value) {
                case RESULT_OK:
                case RESULT_FAIL:
                case RESULT_NONE:
                    return value;
            }
        } catch (NumberFormatException ignored) {
        }
        return RESULT_NONE;
    }
}

final class SpBackend implements TestResult.Backend {
    private SharedPreferences mSp;

    @Override
    public void create(Context context) {
        mSp = context.getSharedPreferences("testResult", Context.MODE_PRIVATE);
    }

    @Override
    public void put(String key, String value) {
        switch (key) {
            case KEY_DETAIL:
                mSp.edit().putString("keyResult.detail", value).apply();
                break;
        }
    }

    @Override
    public String get(String key) {
        switch (key) {
            case KEY_DETAIL:
                return mSp.getString("keyResult.detail", "");
        }
        return "";
    }

    @Override
    public void destory() {
        mSp = null;
    }
}

final class NmBackend implements TestResult.Backend {
    //private ISoul mService;

    @Override
    public void create(Context context) {
        /*        
	try {
            mService = ISoul.getService();
        } catch (RemoteException e) {
            e.rethrowAsRuntimeException();
        }
        */
    }

    @Override
    public void put(String key, String value) {
        /*      
        try {
            switch (key) {
                case KEY_DETAIL:
                    mService.setNvramPropertyString("device.factory.result", value);
                    break;
            }
        } catch (RemoteException e) {
            e.rethrowAsRuntimeException();
        }
       */
    }

    @Override
    public String get(String key) {
        /*
        try {
            switch (key) {
                case KEY_DETAIL:
                    return mService.getNvramPropertyString("device.factory.result");
            }
        } catch (RemoteException e) {
            e.rethrowAsRuntimeException();
        }
        */
        return "";
    }

    @Override
    public void destory() {
        //mService = null;
    }
}
