package com.freeme.factory.gps;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Chronometer;
import android.widget.TextView;

import com.google.android.collect.Lists;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.ProjectConfig;
import com.freeme.factory.config.TestConfig;
import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;

public class GPS extends BaseTest {
    private TextView gps_state_id;
    private TextView gps_satellite_id;
    private TextView gps_signal_id;
    private TextView gps_result_id;
    private TextView gps_signal_snr_id;
    private Chronometer gps_time_id;

    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gps);

        // ui
        gps_state_id = (TextView) this.findViewById(R.id.gps_state_id);
        gps_satellite_id = (TextView) this.findViewById(R.id.gps_satellite_id);
        gps_signal_id = (TextView) this.findViewById(R.id.gps_signal_id);
        gps_result_id = (TextView) this.findViewById(R.id.gps_result_id);
        gps_signal_snr_id = (TextView) this.findViewById(R.id.gps_signal_snr_id);

        testActionCompleted(false);

        gps_time_id = (Chronometer) this.findViewById(R.id.gps_time_id);
        gps_time_id.setFormat(getString(R.string.GPS_time));
        gps_time_id.start();

        gps_satellite_id.setText(getString(R.string.GPS_satelliteNum)+0);
        gps_signal_id.setText(getString(R.string.GPS_Signal)+getString(R.string.nosignal));


        // manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        openGPSSettings();
        getLocation();

    }

    @Override
    protected void onStop() {
        locationManager.removeUpdates(locationListener);

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Intent gpsIntent = new Intent();
        gpsIntent.setClassName("com.android.settings","com.android.settings.widget.SettingsAppWidgetProvider");
        gpsIntent.addCategory("android.intent.category.ALTERNATIVE");
        gpsIntent.setData(Uri.parse("custom:3"));
        try {
            PendingIntent.getBroadcast(this, 0, gpsIntent, 0).send();
        } catch (CanceledException e) {
            e.printStackTrace();
        }
        //*/DZ.zhangzhao, 20190102. GPS
        ContentResolver resolver = this.getContentResolver();
        Settings.Secure.setLocationProviderEnabled(resolver, LocationManager.GPS_PROVIDER,false);
        //*/
        super.onDestroy();
    }

    @Override
    protected void onTestSuccess(View v) {
        Intent intent = new Intent(GPS.this, FactoryTest.class);
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    protected void onTestFailed(View v) {
        Intent intent = new Intent(GPS.this, FactoryTest.class);
        setResult(RESULT_CANCELED,intent);
        finish();
    }

    private void openGPSSettings() {
        gps_state_id.setText(R.string.GPS_connect);
        if ( ! locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
            Intent gpsIntent = new Intent();
            gpsIntent.setClassName("com.android.settings","com.android.settings.widget.SettingsAppWidgetProvider");
            gpsIntent.addCategory("android.intent.category.ALTERNATIVE");
            gpsIntent.setData(Uri.parse("custom:3"));
            try {
                PendingIntent.getBroadcast(this, 0, gpsIntent, 0).send();
            } catch (CanceledException e) {
                e.printStackTrace();
            }
            //*/DZ.zhangzhao, 20190102. GPS
            ContentResolver resolver = this.getContentResolver();
            Settings.Secure.setLocationProviderEnabled(resolver, LocationManager.GPS_PROVIDER,true);
            //*/
        }
    }

    private static boolean getGpsState(Context context) {
        ContentResolver resolver = context.getContentResolver();
        boolean open = Settings.Secure.isLocationProviderEnabled(resolver, LocationManager.GPS_PROVIDER);
        System.out.println("getGpsState:" + open);
        return open;
    }

    private void getLocation() {
        // Criteria criteria = new Criteria();
        // criteria.setAccuracy(Criteria.ACCURACY_FINE);

        // criteria.setAltitudeRequired(false);
        // criteria.setBearingRequired(false);
        // criteria.setCostAllowed(true);
        // criteria.setPowerRequirement(Criteria.POWER_LOW);

        // String provider = locationManager.getBestProvider(criteria, true);

        String provider = LocationManager.GPS_PROVIDER;
        Location location = locationManager.getLastKnownLocation(provider);
        ///if (location == null)
        ///    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        updateToNewLocation(location);

        locationManager.requestLocationUpdates(provider, 1000, 0, locationListener);
        locationManager.addGpsStatusListener(statusListener);
    }

    //private List<GpsSatellite> numSatelliteList = new ArrayList<GpsSatellite>();

    private final GpsStatus.Listener statusListener = new GpsStatus.Listener() {
        public void onGpsStatusChanged(int event) {
            GpsStatus status = locationManager.getGpsStatus(null);
            updateGpsStatus(event, status);
        }
    };

    private List<Float> listSnr;
    private void updateGpsStatus(int event, GpsStatus status) {
        if (event == GpsStatus.GPS_EVENT_SATELLITE_STATUS) {
            if (listSnr == null) {
                listSnr = Lists.newArrayList();
            } else {
                listSnr.clear();
            }

            final int maxSatellites = status.getMaxSatellites();

            Iterator<GpsSatellite> it = status.getSatellites().iterator();

            int countSatellite = 0;
            while (it.hasNext() && countSatellite <= maxSatellites) {
                GpsSatellite s = it.next();

                listSnr.add(s.getSnr());

                countSatellite++;
            }

            // ---
            // count
            gps_satellite_id.setText(getString(R.string.GPS_satelliteNum) + countSatellite);
            // state
            if (countSatellite == 0) {
                gps_signal_id.setText(getString(R.string.GPS_Signal) + countSatellite);
            } else if (countSatellite > 2) {
                gps_signal_id.setText(getString(R.string.GPS_Signal) + getString(R.string.GPS_normal));
            } else {
                gps_signal_id.setText(getString(R.string.GPS_Signal) + countSatellite);
            }
            // snr
            float maxSnr = 0;
            gps_signal_snr_id.setText(null);
            if (countSatellite > 0) {
                Collections.sort(listSnr);

                StringBuilder text = new StringBuilder();
                text.append("C/N : ")
                    .append(String.format("min(%.1f) ~ max(%.1f)\n",
                            listSnr.get(0), (maxSnr = listSnr.get(countSatellite-1))))
                    .append("\t\t - - - - - - - - \n");
                for (int i = 0; i < countSatellite; ++i) {
                    text.append(String.format("\t\t[%02d] -> %.1f", i, listSnr.get(i)));
                    if (((i + 1) % 3) == 0) {
                        text.append('\n');
                    }
                }

                gps_signal_snr_id.setText(text.toString());
            }

            if (ProjectConfig.EN_PREDEF_GPS_SNR_THRESHOLD) {
                if (countSatellite >= (ProjectConfig.CO_PREDEF_GPS_SNR_NUM)) {
                    int value = 0;
                    for (int i = 1; i <= ProjectConfig.CO_PREDEF_GPS_SNR_NUM; i++) {
                        if(listSnr.get(countSatellite-i) >= ProjectConfig.CO_PREDEF_GPS_SNR_THRESHOLD ){
                            value = value + 1;
                        }
                    }
                    if (value == ProjectConfig.CO_PREDEF_GPS_SNR_NUM) {
                        testActionCompleted(true);
                        if (FactoryTest.getTestMode() == TestConfig.TEST_MODE_AUTO) {
                            testResultCommit(true);
                        }
                    }

                }
            } else {
                if (countSatellite > 0) {
                    testActionCompleted(true);
                    if (FactoryTest.getTestMode() == TestConfig.TEST_MODE_AUTO) {
                        testResultCommit(true);
                    }
                }
            }
        }
    }

    private void updateToNewLocation(Location location) {
        //TextView tv1 = (TextView) this.findViewById(R.id.tv1);
        if (location != null) {
        }
    }

    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                updateToNewLocation(location);
            }
        }

        public void onProviderDisabled(String provider) {
            updateToNewLocation(null);
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
}
