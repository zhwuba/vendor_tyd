package com.freeme.factory.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;

public abstract class BaseTest extends Activity {

    public static final String EXTRA_TEST_MODE = "EXTRA_TEST_MODE";

    protected int mTestMode;

    // ---

	public static final String KEY_PURPOSE = "cause_purpose";
	
	protected int mCausePurpose;
	
	private Button mBtnSuccess, mBtnFailed;
	private final View.OnClickListener mLisBtnResult = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.ResultOK:
				onTestSuccess(v);
				break;
			case R.id.ResultFail:
				onTestFailed(v);
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		final Intent intent = getIntent();
		if (intent != null) {
            mTestMode = intent.getIntExtra(EXTRA_TEST_MODE, 0);

			mCausePurpose = intent.getIntExtra(KEY_PURPOSE, -1);
			if (mCausePurpose != -1) {
                onTestTitle(mCausePurpose);
            }
		}
	}

	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		
		mBtnSuccess = (Button) findViewById(R.id.ResultOK);
		if (mBtnSuccess != null) {
			mBtnSuccess.setOnClickListener(mLisBtnResult);
		}
		mBtnFailed  = (Button) findViewById(R.id.ResultFail);
		if (mBtnFailed != null) {
			mBtnFailed.setOnClickListener(mLisBtnResult);
		}
	}
	
	protected void onTestTitle(int purpose) {
		// ignore
	}
	
	protected void onTestSuccess(View v) {
		Intent intent = new Intent(this, FactoryTest.class);
		setResult(RESULT_OK, intent);
		finish();
	}

	protected void onTestFailed(View v) {
		Intent intent = new Intent(this, FactoryTest.class);
		setResult(RESULT_CANCELED, intent);
        finish();
	}

	protected void testActionCompleted(boolean completed) {
		if (mBtnSuccess != null) {
			mBtnSuccess.setEnabled(completed);
		}
	}

	protected void testResultCommit(boolean success) {
		if (success) {
			if (mBtnSuccess != null) {
				mBtnSuccess.performClick();
			}
		} else {
			if (mBtnFailed != null) {
				mBtnFailed.performClick();
			}
		}
	}
}
