package com.freeme.factory.usb;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.storage.StorageManager;
import android.os.storage.VolumeInfo;
import android.widget.TextView;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
//import com.freeme.os.storage.FreemeVolumeInfo;

public class Otg extends BaseTest {

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            switch (action) {
                case Intent.ACTION_MEDIA_MOUNTED: {
                    otgDeviceChanged(hasUSBOTG());
                } break;
                case UsbManager.ACTION_USB_DEVICE_ATTACHED: {
                    UsbDevice device = intent.getParcelableExtra(
                            UsbManager.EXTRA_DEVICE);
                    if (device != null) {
                        otgDeviceChanged(true);
                    }
                } break;
                case UsbManager.ACTION_USB_DEVICE_DETACHED: {
                    UsbDevice device = intent.getParcelableExtra(
                            UsbManager.EXTRA_DEVICE);
                    if (device != null) {
                        otgDeviceChanged(false);
                    }
                } break;
            }
        }
    };

    private StorageManager mStorageManager;

    private TextView mText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otg);
        mText = (TextView) findViewById(R.id.text);

        mStorageManager = getSystemService(StorageManager.class);

        testActionCompleted(false);

        if (hasUSBOTG()) {
            otgDeviceChanged(true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(mReceiver, filter);

        filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addDataScheme("file");
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onStop() {
        unregisterReceiver(mReceiver);
        super.onStop();
    }

    private void otgDeviceChanged(boolean found) {
        if (found) {
            mText.setText(R.string.otg_found);
            testActionCompleted(true);
            if (FactoryTest.getTestMode() != 0) {
                testResultCommit(true);
            }
        } else {
            mText.setText(R.string.otg_hint);
        }
    }

    // check if there is a volume is usb-otg
    private boolean hasUSBOTG() {
        final List<VolumeInfo> vols = mStorageManager.getVolumes();
        for (VolumeInfo vol : vols) {
            /*if (FreemeVolumeInfo.isUsbOtg(vol)) {
                return true;
            }*/
        }
        return false;
    }
}
