package com.freeme.factory.telephony;

import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import android.telecom.PhoneAccount;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;

public class Phone extends BaseTest {

    private AudioManager mAudioManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signal);

        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL,
                mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL),
                AudioManager.FLAG_ALLOW_RINGER_MODES);

        // FIXME: freeme.gejun, mark factory mode.
        Settings.System.putInt(getContentResolver(), "factory_test_call", 1);

        startActivity(new Intent(Intent.ACTION_CALL_EMERGENCY)
                .setData(Uri.fromParts(PhoneAccount.SCHEME_TEL, "112", null))
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
	}
}