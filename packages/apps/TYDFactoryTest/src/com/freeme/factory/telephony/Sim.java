package com.freeme.factory.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;

public class Sim extends BaseTest {
    public static final String TAG = "SimCard";

    private TextView mTextSim1;
    private TextView mTextSim2;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive: " + intent);
            String stateExtra = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
            if (stateExtra != null) {
                switch (stateExtra) {
                    case IccCardConstants.INTENT_VALUE_ICC_ABSENT:
                    case IccCardConstants.INTENT_VALUE_ICC_LOADED:
                        updateSimStates();
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simcard);

        mTextSim1 = (TextView) findViewById(R.id.simcard_sim1_info);
        mTextSim2 = (TextView) findViewById(R.id.simcard_sim2_info);

        mTextSim1.post(new Runnable() {
            @Override
            public void run() {
                updateSimStates();
            }
        });
    }

    @Override
    protected void onStart() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        registerReceiver(mReceiver, filter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(mReceiver);
        super.onStop();
    }

    @Override
    protected void testActionCompleted(boolean completed) {
        super.testActionCompleted(completed);
        if (completed) {
            switch (mTestMode) {
                case TestConfig.TEST_MODE_ALL:
                case TestConfig.TEST_MODE_AUTO:
                    testResultCommit(true);
                    break;
            }
        }
    }

    private void updateSimStates() {
        if (TestConfig.TEST_EN_GEMINI_SUPPORT) {
            TelephonyManager telephony = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);

            boolean existSim1 = telephony.getSimState(PhoneConstants.SIM_ID_1) == TelephonyManager.SIM_STATE_READY;
            boolean existSim2 = telephony.getSimState(PhoneConstants.SIM_ID_2) == TelephonyManager.SIM_STATE_READY;

            if (mTextSim1 != null) {
                mTextSim1.setText(existSim1 ? R.string.sim1_info_ok : R.string.sim1_info_failed);
            }
            if (mTextSim2 != null) {
                mTextSim2.setText(existSim2 ? R.string.sim2_info_ok : R.string.sim2_info_failed);
            }

            testActionCompleted(existSim1 && existSim2);
        } else {
            ITelephony telephony = ITelephony.Stub.asInterface(ServiceManager.getService(TELEPHONY_SERVICE));

            boolean existSim1;
            try {
                existSim1 = telephony.hasIccCard();
            } catch (RemoteException e) {
                existSim1 = false;
            }
            if (mTextSim1 != null) {
                mTextSim1.setText(existSim1 ? R.string.sim1_info_ok : R.string.sim1_info_failed);
            }
            if (mTextSim2 != null) {
                mTextSim2.setVisibility(View.GONE);
            }

            testActionCompleted(existSim1);
        }
    }
}