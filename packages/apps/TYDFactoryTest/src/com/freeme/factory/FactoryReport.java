package com.freeme.factory;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.graphics.Color;

import com.freeme.factory.config.TestConfig;
import com.freeme.factory.config.TestResult;

public class FactoryReport extends Activity {

	private TextView mReportResult, mReportSuccess, mReportFail, mReportDefault;

	private TestResult mTestResult;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report);
		
		mReportResult  = (TextView) findViewById(R.id.report_result);
		mReportSuccess = (TextView) findViewById(R.id.report_success);
		mReportFail    = (TextView) findViewById(R.id.report_failed);
		mReportDefault = (TextView) findViewById(R.id.report_default);

        mTestResult = TestResult.getInstance();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		StringBuilder testSuccess = new StringBuilder();
		StringBuilder testFail    = new StringBuilder();
		StringBuilder testDefault = new StringBuilder();
		
		boolean testAllOk = true;
		
		final int testMode = FactoryTest.getTestMode();
		final int[] testItems = TestConfig.getInstance().getTestItems(testMode);
		for (int i = 0; i < testItems.length; ++i) {
			final int testItemId = testItems[i];
			
			String itemName = TestConfig.getNameByItemId(this, testItemId);
			switch (mTestResult.get(testItemId)) {
                case TestResult.RESULT_OK:
                    if (testSuccess.length() != 0) {
                        testSuccess.append('|');
                    }
                    testSuccess.append(itemName);
                    break;
                case TestResult.RESULT_FAIL:
                    if (testFail.length() != 0) {
                        testFail.append('|');
                    }
                    testFail.append(itemName);
                    testAllOk = false;
                    break;
                default:
                    if (testDefault.length() != 0) {
                        testDefault.append('|');
                    }
                    testDefault.append(itemName);
                    break;
			}
		}

		if (testAllOk) {
			mReportResult.setTextColor(Color.GREEN);
			mReportResult.setText(getString(R.string.report_test_result) + getString(R.string.report_test_ok) + "\n");
		} else {
			mReportResult.setTextColor(Color.RED);
			mReportResult.setText(getString(R.string.report_test_result) + getString(R.string.report_test_fail) + "\n");
		}

		mReportSuccess.setText(getString(R.string.report_ok)+testSuccess);
		mReportFail.setText(getString(R.string.report_failed)+testFail);
		mReportDefault.setText(getString(R.string.report_notest)+testDefault);

		TestResult.getInstance().put(testAllOk ? TestResult.RESULT_OK : TestResult.RESULT_FAIL);
	}

	@Override
	protected void onPause() {
		FactoryTest.setTestMode(0);
		FactoryTest.setTestItem(0);
		super.onPause();
	}

	@Override
	protected void onStop() {
		FactoryTest.setTestMode(0);
		FactoryTest.setTestItem(0);
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		FactoryTest.setTestMode(0);
		FactoryTest.setTestItem(0);
		super.onDestroy();
	}
}
