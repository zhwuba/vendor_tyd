package com.freeme.factory.camera;

import android.graphics.SurfaceTexture;
import android.os.Handler;
import android.view.TextureView;

import java.util.ArrayList;
import java.util.List;

import com.freeme.factory.utils.SLog;

public class CameraStateController {

    public interface OnStateChangeListener {
        public void onStartCamera();

        public void onStartPreview();

        public void onStopPreview();

        public void onStopCamera();
    }

    private static final String TAG = "CameraStateController";

    private TextureView mPreviewView;
    private SurfaceTexture mPreviewTexture;

    private static final int STATE_OFF     = 0;
    private static final int STATE_PREVIEW = 1;
    private int mState = STATE_OFF;

    private List<OnStateChangeListener> mListeners = new ArrayList<>();

    public CameraStateController () {
        // do nothing
    }

    public void onResume() {
        setUpCamera();
    }

    public void onPause() {
        shutdownCamera();
    }

    public void onDestroy() {
        unRegisterStateListener();
        mPreviewView = null;
        mPreviewTexture = null;
    }

    public void registerStateListener(OnStateChangeListener listener) {
        mListeners.add(listener);
    }

    private void unRegisterStateListener() {
        mListeners.clear();
    }

    protected void setSurfaceTexture(TextureView texture) {
        mPreviewView = texture;
        mPreviewView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                mPreviewTexture = surface;
                startPreview();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }
        });
    }

    private void setUpCamera() {
        SLog.i(TAG, "setUpCamera");
        shutdownCamera();

        for (OnStateChangeListener listener : mListeners) {
            listener.onStartCamera();
        }

        if (mPreviewTexture != null) {
            startPreview();
        }
    }

    private void shutdownCamera() {
        SLog.i(TAG, "shutdownCamera, state = " + mState);
        if (mState != STATE_OFF) {
            for (OnStateChangeListener listener : mListeners) {
                listener.onStopPreview();
                listener.onStopCamera();
            }
            mState = STATE_OFF;
        }
    }

    private void startPreview() {
        SLog.i(TAG, "startPreview, state = " + mState);
        if (mState != STATE_OFF) {
            for (OnStateChangeListener listener : mListeners) {
                listener.onStopPreview();
            }
            mState = STATE_OFF;
            Handler h = new Handler();
            Runnable mDelayedPreview = new Runnable() {
                public void run() {
                    startPreview();
                }
            };
            h.postDelayed(mDelayedPreview, 300);
            return;
        }

        mState = STATE_PREVIEW;

        for (OnStateChangeListener listener : mListeners) {
            listener.onStartPreview();
        }
    }
}
