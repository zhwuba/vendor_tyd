package com.freeme.factory.camera;

import android.app.Activity;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Handler;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;

import com.freeme.factory.R;
import com.freeme.factory.utils.SLog;

public class DualCameraTest implements CameraStateController.OnStateChangeListener {

    private TextureView mDualCameraPreview;
    private CameraManager mCameraManager;

    private Activity mActivity;

    private CameraStateController mCameraStateController = new CameraStateController();

    public DualCameraTest(Activity activty) {
        mActivity = activty;

        mDualCameraPreview = (TextureView) mActivity.findViewById(R.id.dual_camera_view);
        mDualCameraPreview.setVisibility(View.VISIBLE);

        mCameraManager = CameraManager.getDualInstance();

        mCameraStateController.setSurfaceTexture(mDualCameraPreview);
        mCameraStateController.registerStateListener(this);
    }

    @Override
    public void onStartCamera() {
        startDualCamera();
    }

    @Override
    public void onStartPreview() {
        if (mCameraManager.cameraIsOpened()) {
            CameraUtil.setCameraPreviewRatio_4_3(mCameraManager, mActivity, true);
            CameraUtil.fitPreviewSize(mActivity,
                    mCameraManager.getParameters(),
                    mDualCameraPreview);
            mCameraManager.startPreview(mDualCameraPreview.getSurfaceTexture());
        }
    }

    @Override
    public void onStopPreview() {
        if (mCameraManager.cameraIsOpened()) {
            mCameraManager.stopPreview();
        }
    }

    @Override
    public void onStopCamera() {
        if (mCameraManager.cameraIsOpened()) {
            closeCamera();
        }
    }

    public void onResume() {
        mCameraStateController.onResume();
    }

    public void onPause() {
        mCameraStateController.onPause();
    }

    public void onDestroy() {
        mCameraStateController.onDestroy();
    }

    private boolean isRealDualCamera() {
        boolean isAddMain2 = false;
        int m = android.hardware.Camera.getNumberOfCameras();
        for (int i = 0; i < m; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            android.hardware.Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                isAddMain2 = i != 0;
            }
        }

        return isAddMain2 ;
    }

    private void startDualCamera() {
        if (mCameraManager.getCameraState() == CameraManager.PREVIEW_STOPPED) {

            final int targetCameraId = isRealDualCamera() ?
                    2 : Camera.CameraInfo.CAMERA_FACING_FRONT; // instead of front-camera-id
            // update display orientation
            if (targetCameraId != -1) {
                setCameraDisplayOrientation(targetCameraId);
            }

            if (!mCameraManager.openCamera(targetCameraId)) {
                //showToast(R.string.camera_in_use);
            }
        } else {
            SLog.i("onClick mCameraManager.getCameraState() = " + mCameraManager.getCameraState());
        }
    }

    private void setCameraDisplayOrientation(int cameraId) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        int rotation = mActivity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else {    // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        if (mCameraManager != null) {
            mCameraManager.chechCameraDisplayOrientation(result);
        }
    }

    private void closeCamera() {
        if (mCameraManager != null) {
            mCameraManager.closeCamera();
        }
    }
}

