package com.freeme.factory.camera;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;

public class StrobeTest extends BaseTest implements View.OnClickListener {

    private Handler mHandler = new MyHandler();
    private Button mSwitcher;

    private FlashlightController mFlashlightController;

    private boolean mStrobeActive = false;

    private FlashlightController.FlashlightListener mFlashlightListener
            = new FlashlightController.FlashlightListener() {
        @Override
        public void onFlashlightChanged(boolean enabled) {
            mHandler.sendEmptyMessage(MSG_UPDATE_STATE);
        }

        @Override
        public void onFlashlightError() {
        }

        @Override
        public void onFlashlightAvailabilityChanged(boolean available) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.strobe);

        mSwitcher = (Button) findViewById(R.id.strobe_switcher);
        mSwitcher.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHandler.obtainMessage(MSG_INIT,
                1 /* FIXME: 1, strobe on; 0, off */,
                0).sendToTarget();
    }

    @Override
    protected void onPause() {
        mHandler.sendEmptyMessage(MSG_UNINIT);
        super.onPause();
    }

    @Override
    protected void onTestSuccess(View v) {
        Intent intent = new Intent(this, FactoryTest.class);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onTestFailed(View v) {
        Intent intent = new Intent(this, FactoryTest.class);
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    @Override
    protected void onTestTitle(int purpose) {
        setTitle(TestConfig.getNameByItemId(this, purpose));
    }

    private static final int MSG_INIT   = 0;
    private static final int MSG_UNINIT = 1;
    private static final int MSG_SWITCH = 2;
    private static final int MSG_UPDATE_STATE = 3;

    @SuppressLint("HandlerLeak")
    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_INIT: {
                switch (mCausePurpose) {
                    case TestConfig.TEST_ITEM_STROBE_FORE:
                        mFlashlightController = new FlashlightController(getApplicationContext(), true);
                        break;
                    default:
                        mFlashlightController = new FlashlightController(getApplicationContext(), false);
                        break;
                    }
                    mFlashlightController.addListener(mFlashlightListener);
                    if (msg.arg1 == 1) {
                        setFlashlight(mStrobeActive = !mStrobeActive);
                    }
                }
                break;
            case MSG_UNINIT: {
                    if (mStrobeActive) {
                        setFlashlight(false);
                    }

                    mFlashlightController.removeListener(mFlashlightListener);
                    closeCamera();
                }
                break;
            case MSG_SWITCH: {
                    setFlashlight(mStrobeActive = !mStrobeActive);
                }
                break;
            case MSG_UPDATE_STATE : {
                mSwitcher.setText(getString(R.string.strobe_switcher_arg,
                        getText(mStrobeActive ? R.string.strobe_state_on : R.string.strobe_state_off)));
                }
                break;
            default:
                super.handleMessage(msg);
                break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        mHandler.sendEmptyMessage(MSG_SWITCH);
    }

    private void setFlashlight(boolean enabled) {
        try {
            mFlashlightController.setFlashlight(enabled);
        } catch (IllegalArgumentException e) {
            doFlash(enabled);
        }
    }

    /// ----------------------------------- adapt to fake camera strobe--------------------------

    private CameraManager mCameraMgr;
    private TextureView mCameraSurface;

    private int mCameraId;

    private static final int MSG_START_CAMERA = 0;

    private Handler mCameraHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_START_CAMERA:
                    startCamera(mCameraId);
                    break;
                default:
                    break;
            }
        }
    };

    private void doFlash(boolean enabled ) {
        if(mCameraMgr == null) {
            mCameraMgr = CameraManager.instance();
            switch (mCausePurpose) {
                case TestConfig.TEST_ITEM_STROBE_FORE:
                    mCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                    break;
                default:
                    mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                    break;
            }

            mCameraSurface = (TextureView) findViewById(R.id.camera_surface);
            mCameraSurface.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {

                @Override
                public void onSurfaceTextureUpdated(SurfaceTexture surface) {
                }

                @Override
                public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                }

                @Override
                public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                    return false;
                }

                @Override
                public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                    mCameraHandler.obtainMessage(MSG_START_CAMERA).sendToTarget();
                }
            });
            mCameraSurface.setVisibility(View.VISIBLE);
        }
       flashSwitcher(enabled);
    }

    private void startCamera(int cameraId) {
        if (mCameraMgr.getCameraState() == CameraManager.PREVIEW_STOPPED) {
            if (!mCameraMgr.openCamera(cameraId)) {
                return;
            }

            mCameraMgr.startPreview(mCameraSurface.getSurfaceTexture());
            flashSwitcher(true);
        }
    }

    private void flashSwitcher(boolean active) {
        if (mCameraMgr != null) {
            Camera.Parameters parameters = mCameraMgr.getParameters();
            if (parameters != null) {
                parameters.setFlashMode(active ? Camera.Parameters.FLASH_MODE_TORCH :
                        Camera.Parameters.FLASH_MODE_OFF);
                mCameraMgr.setParameters(parameters);
            }
            mSwitcher.setText(getString(R.string.strobe_switcher_arg,
                    getText(mStrobeActive ? R.string.strobe_state_on : R.string.strobe_state_off)));
        }
    }

    private void closeCamera() {
        if (mCameraMgr != null) {
            if (mStrobeActive) {
                flashSwitcher(false);
            }

            mCameraMgr.closeCamera();
        }
    }
}
