package com.freeme.factory.camera;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.ProjectConfig;
import com.freeme.factory.config.TestConfig;
import com.freeme.factory.utils.SLog;

public class CameraTest extends BaseTest implements
        CameraStateController.OnStateChangeListener, View.OnClickListener {

    private TextureView mCameraPreview;
    private Button mBtnTakePic;
    private CheckBox mChkStrobe;
	
	private final AutoFocusCallback mAutoFocusCallback = new AutoFocusCallback();
    private ShutterCallback mShutterCallback;
    private JpegPictureCallback mJpegPictureCallback;
    
	private CameraManager mCameraManager;
    private boolean mCameraTaking = false;
	private boolean mCanTakePicture = true;

    private DualCameraTest mDualCameraTest;

    private CameraStateController mCameraStateController = new CameraStateController();
	
    @Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        		WindowManager.LayoutParams.FLAG_FULLSCREEN); 
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.camera);
     
        mBtnTakePic = (Button) findViewById(R.id.camera_take);
        mBtnTakePic.setOnClickListener(this);
        
        mCameraPreview = (TextureView) findViewById(R.id.camera_view);

        mChkStrobe = (CheckBox) findViewById(R.id.camera_check_strobe);
        mChkStrobe.setChecked(/* default true */ ProjectConfig.CO_PREDEF_CAPTURE_STROB_ON);
        mChkStrobe.setVisibility(TestConfig.TEST_EN_STROBE_BACK ? View.VISIBLE : View.GONE);
        
		testActionCompleted(false);

        mCameraManager = CameraManager.instance();

        if(TestConfig.TEST_EN_CAMERA_DUAL) {
            mDualCameraTest = new DualCameraTest(this);
        }

        mCameraStateController.setSurfaceTexture(mCameraPreview);
        mCameraStateController.registerStateListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCameraStateController.onResume();

        if (mDualCameraTest != null) {
            mDualCameraTest.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCameraStateController.onPause();

        if (mDualCameraTest != null) {
            mDualCameraTest.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCameraStateController.onDestroy();

        if (mDualCameraTest != null) {
            mDualCameraTest.onDestroy();
        }
    }

    @Override
    public void onStartCamera() {
        startCamera();
    }

    @Override
    public void onStartPreview() {
        if (mCameraManager.cameraIsOpened()) {
            CameraUtil.setCameraPreviewRatio_4_3(mCameraManager, CameraTest.this);
            CameraUtil.fitPreviewSize(CameraTest.this,
                    mCameraManager.getParameters(),
                    mCameraPreview);
            mCameraManager.startPreview(mCameraPreview.getSurfaceTexture());
        }
    }

    @Override
    public void onStopPreview() {
        if (mCameraManager.cameraIsOpened()) {
            mCameraManager.stopPreview();
        }
    }

    @Override
    public void onStopCamera() {
        if (mCameraManager.cameraIsOpened()) {
            closeCamera();
        }
    }

	@Override
	protected void onTestSuccess(View v) {
		Intent intent = new Intent(this, FactoryTest.class);
		setResult(RESULT_OK,intent);
		finish();
	}

	@Override
	protected void onTestFailed(View v) {
		Intent intent = new Intent(this, FactoryTest.class);
		setResult(RESULT_CANCELED,intent);
		finish();
	}
	
	@Override
	public void onClick(View v) {
		if (! mCameraTaking) {
            if (mCanTakePicture) {
                if (CameraUtil.isSupported(Camera.Parameters.FOCUS_MODE_AUTO,
                            mCameraManager.getParameters().getSupportedFocusModes(),
                            mCameraManager.getParameters().getMaxNumFocusAreas())) {
                    autoFocus(true);
                } else {
                    capture();
                }
                mBtnTakePic.setEnabled(false);
                mCameraTaking = true;
            }
	    }
	}

	private final class AutoFocusCallback implements Camera.AutoFocusCallback {
		@Override
		public void onAutoFocus(boolean focused, Camera camera) {
			if (focused && mCameraManager != null) { 
				mCanTakePicture = true;
				switch (mCameraManager.getCameraState()) {
					case CameraManager.FOCUSING_SNAP_ON_FINISH: {
						mCameraManager.setCameraState(CameraManager.IDLE);
						capture();
						break;
					}
					default: {
						mCameraManager.setCameraState(CameraManager.IDLE);
						break;
					}
				}
			} else if (! focused && mCameraManager != null){  
            	onAutoFocusFail();

            	mCanTakePicture = true;
                mBtnTakePic.setEnabled(true);
                mCameraTaking = false;  
            }
		}
	}
	
	private final class JpegPictureCallback implements PictureCallback {
		@Override
        public void onPictureTaken(byte[] jpegData, Camera camera) {
            try {
                Bitmap bitmap = BitmapFactory.decodeByteArray(jpegData, 0, jpegData.length);
                
                File file = new File(getCacheDir(), "CachedCameraTest.jpg");
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));  
                bitmap.compress(Bitmap.CompressFormat.JPEG, 60, bos);
                bos.flush();
                bos.close();
                
                mBtnTakePic.setEnabled(false);
                testActionCompleted(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            mCameraManager.setCameraState(CameraManager.IDLE);
            //mCameraManager.startPreview(mCameraPreview.getSurfaceTexture());
        }
    }
	
    private void onAutoFocusFail() {
        Toast.makeText(this, R.string.camera_autofocus_fail, Toast.LENGTH_SHORT).show();
    }

	private final class ShutterCallback implements Camera.ShutterCallback {
		@Override
		public void onShutter() {
			SLog.i("onShutter ... ");
		}
	}
      
    private void startCamera() {
        if (mCameraManager.getCameraState() == CameraManager.PREVIEW_STOPPED) {
            
            final int targetCameraId = mCameraManager.getDefaultCameraId();
            // update display orientation
            if (targetCameraId != -1) {
            	setCameraDisplayOrientation(targetCameraId);
            }
            
            if (!mCameraManager.openCamera(targetCameraId)) {
                //showToast(R.string.camera_in_use);
            }
        } else {
            SLog.i("onClick mCameraManager.getCameraState() = " + mCameraManager.getCameraState());
        }
    }
    
    public void autoFocus(boolean focusSnapOnFinish) {
        if (mCameraManager != null) {
        	try {
        		mCameraManager.autoFocus(mAutoFocusCallback);
        		mCanTakePicture = focusSnapOnFinish ? false : true;
        		mCameraManager.setCameraState(focusSnapOnFinish
        				? CameraManager.FOCUSING_SNAP_ON_FINISH : CameraManager.FOCUSING);
        	} catch (RuntimeException e) {
        		SLog.e("autofocus exception occurs, autofocus failed!");
        		mCanTakePicture = true;
        		mCameraManager.setCameraState(CameraManager.IDLE);
        	}
            
        }
    }
    
    public boolean capture() {
        if (mCameraManager == null || 
        		mCameraManager.getCameraState() == CameraManager.PREVIEW_STOPPED || 
        		mCameraManager.getCameraState() == CameraManager.FOCUSING_SNAP_ON_FINISH || 
        		mCameraManager.getCameraState() == CameraManager.SNAPSHOT_IN_PROGRESS) {
            SLog.i("capture mCameraManager.getCameraState() = " + mCameraManager.getCameraState());
            return false;
        }
        
        Camera.Parameters parameters = mCameraManager.getParameters();
        {
	        // flash light
        	if (TestConfig.TEST_EN_STROBE_BACK && mChkStrobe.isChecked()) {
		        List<String> supportedFlash = parameters.getSupportedFlashModes();
		    	if (CameraUtil.isSupported(Camera.Parameters.FLASH_MODE_ON, supportedFlash)) {
		    		parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
		    	}
        	} else {
        		parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        	}
        }
        mCameraManager.setParameters(parameters);
        
        if (mShutterCallback == null) {
            mShutterCallback = new ShutterCallback();
        }
        if (mJpegPictureCallback == null) {
            mJpegPictureCallback = new JpegPictureCallback();
        }
        mCameraManager.takePicture(mShutterCallback, null, null, mJpegPictureCallback);
        
        mCameraManager.setCameraState(CameraManager.SNAPSHOT_IN_PROGRESS);
        
        return false;
    }

    public void cancelAutoFocus() {
        if (mCameraManager != null) {
            mCameraManager.cancelAutoFocus();
            mCameraManager.setCameraState(CameraManager.IDLE);
        }
    }
    
    private void closeCamera() {
        if (mCameraManager != null) {
            cancelAutoFocus();
            mCameraManager.closeCamera();
        }
    }
    
    public void setCameraDisplayOrientation(int cameraId) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        
        int rotation = getWindowManager().getDefaultDisplay()
        		.getRotation();
        int degrees = 0;
        switch (rotation) {
	        case Surface.ROTATION_0: degrees = 0; break;
	        case Surface.ROTATION_90: degrees = 90; break;
	        case Surface.ROTATION_180: degrees = 180; break;
	        case Surface.ROTATION_270: degrees = 270; break;
        }
        
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
        	result = (info.orientation + degrees) % 360;
        	result = (360 - result) % 360; // compensate the mirror
        } else { 	// back-facing
        	result = (info.orientation - degrees + 360) % 360;
        }
        if (mCameraManager != null) {
            mCameraManager.chechCameraDisplayOrientation(result);
        }
    }
}

