package com.freeme.factory.sensor;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.freeme.factory.base.BaseTest;
import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;

public class GSensor extends BaseTest implements SensorEventListener{
    private SensorManager mSensorManager;
    private Sensor mSensor;

    private TextView mResult;
    private TextView mResultAccuracy;

	private boolean mRegistered;
	private float mLastValues;
	private int mRoundCount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gsensor);
	
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

		mResult = (TextView) findViewById(R.id.gsensor_xyz);
		mResult.setText("X:\nY:\nZ");

		mResultAccuracy = (TextView) findViewById(R.id.gsensor_tv_info);
		mResultAccuracy.setText(R.string.GSensor_tips);

        Button calibration = (Button) findViewById(R.id.gsensor_calibration);
		calibration.setOnClickListener(new View.OnClickListener(){
            @Override
			public void onClick(View v) {
				Intent intent = new Intent(GSensor.this, GSensorSettings.class);
				startActivity(intent);
			}
		});

        testActionCompleted(false);
	}

    @Override
    protected void onResume() {
        super.onResume();
        mRegistered = mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

	@Override
	protected void onPause() {
		if (mRegistered) {
			mRegistered = false;
			mSensorManager.unregisterListener(this);
		}
		super.onPause();
	}

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
			float[] values = event.values;
			mResult.setText("X:"+values[0]+"\nY:"+values[1]+"\nZ:"+values[2]);

			if (mLastValues != values[1]) {
				mRoundCount++;
				mLastValues = values[1];
			}
			if (mRoundCount >= 6) {
                testActionCompleted(true);
				if (FactoryTest.getTestMode() == 2 || FactoryTest.getTestMode() == 1) {
                    testResultCommit(true);
				}
			}
		}
	}
}
