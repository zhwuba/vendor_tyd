package com.freeme.factory.sensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;

public class MSensor extends BaseTest implements SensorEventListener {

	private SensorManager mSensorManager;
	private Sensor mSensor;

	private TextView mAccuracy;
	private TextView mValue0;
    private TextView mValue1;
    private TextView mValue2;

	private float mLastValue;
	private int mCount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.msensor);

		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

		mAccuracy = (TextView) findViewById(R.id.accuracy);
		mValue0 = (TextView) findViewById(R.id.value0);
        mValue1 = (TextView) findViewById(R.id.value1);
        mValue2 = (TextView) findViewById(R.id.value2);

        testActionCompleted(false);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mSensor != null) {
            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_GAME);
		}
	}

    @Override
    protected void onPause() {
        super.onPause();
        if (mSensor != null) {
            mSensorManager.unregisterListener(this, mSensor);
        }
    }

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
			float[] values = event.values;
			mValue0.setText(String.valueOf(values[0]));
            mValue1.setText(String.valueOf(values[1]));
            mValue2.setText(String.valueOf(values[2]));

			if (mLastValue != values[0]) {
				mCount++;
				mLastValue = values[0];
			}
			if (mCount >= 5) {
			    testActionCompleted(true);
			}
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
			mAccuracy.setText(getString(R.string.LSensor_accuracy) + accuracy);
		}
	}

    @Override
    protected void testActionCompleted(boolean completed) {
        super.testActionCompleted(completed);
        if (completed) {
            switch (FactoryTest.getTestMode()) {
                case TestConfig.TEST_MODE_ALL:
                case TestConfig.TEST_MODE_AUTO:
                    testResultCommit(true);
                    break;
            }
        }
    }
}
