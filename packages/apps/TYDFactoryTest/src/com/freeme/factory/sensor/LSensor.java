package com.freeme.factory.sensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;

public class LSensor extends BaseTest implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mLSensor;

    private TextView mTvAccuracy;
    private TextView mTvValue;

    private int mThresholdValue;
    private float mLastValue;
    private int mCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lsensor);
        mThresholdValue = getResources().getInteger(R.integer.config_light_sensor_threshold);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mLSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        mTvAccuracy = (TextView) findViewById(R.id.lsensor_accuracy);
        mTvValue = (TextView) findViewById(R.id.lsensor_value);

        testActionCompleted(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mLSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(this, mLSensor);
        super.onPause();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            float value = event.values[0];
            mTvValue.setText(String.valueOf(value));

            if (mLastValue != value &&
                    (value == 0 || value > 0 && mLastValue > mThresholdValue)) {
                mCount++;
                mLastValue = value;
            }
            if (mCount >= 3) {
                testActionCompleted(true);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if (sensor.getType() == Sensor.TYPE_LIGHT){
            mTvAccuracy.setText(getString(R.string.LSensor_accuracy) + accuracy);
        }
    }

    @Override
    protected void testActionCompleted(boolean completed) {
        super.testActionCompleted(completed);
        if (completed) {
            switch (FactoryTest.getTestMode()) {
                case TestConfig.TEST_MODE_ALL:
                case TestConfig.TEST_MODE_AUTO:
                    testResultCommit(true);
                    break;
            }
        }
    }
}
