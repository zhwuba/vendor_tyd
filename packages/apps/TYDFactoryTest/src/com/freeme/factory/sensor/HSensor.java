package com.freeme.factory.sensor;

import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

//import com.freeme.hardware.FreemeSensorManager;
import com.freeme.factory.base.BaseTest;

import com.freeme.factory.R;

public class HSensor extends BaseTest {
	private static final String TAG = "HSensor";

	private static final int CHECK_PERIOD = 200; //ms

	private TextView mResult;

	private int mLastValue = 1;
	private int mRoundCount = 0;

	private Timer mTimer;
	private TimerTask mTimerTask;

	private static final int MSG_UPDATE_UI = 0;
    private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case MSG_UPDATE_UI:
					updateState();
					break;
				default:
					break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hsensor);
	
		mResult = (TextView) findViewById(R.id.proximity_hall);
		
        testActionCompleted(false);

		/* [MERGE]
		Settings.System.putInt(getContentResolver(), Settings.System.FREEME_LEATHER_CASE_SETTING, 0);
		*/
	}

    @Override
    protected void onResume() {
        super.onResume();
        initTimeTask();
    }

	@Override
	protected void onPause() {
		super.onPause();
		/* [MERGE]
		Settings.System.putInt(getContentResolver(), Settings.System.FREEME_LEATHER_CASE_SETTING, 1);
		*/
		cancelTimeTask();
	}

	private void initTimeTask() {
		if (mTimer == null) {
			mTimer = new Timer();
		}

		if (mTimerTask == null) {
			mTimerTask = new TimerTask() {
				@Override
				public void run() {
					mHandler.sendEmptyMessage(MSG_UPDATE_UI);
				}
			};
		}

		if (mTimer != null && mTimerTask != null) {
			mTimer.schedule(mTimerTask, 0, CHECK_PERIOD);
		}
	}

	private void cancelTimeTask() {
		if (mTimer != null) {
			mTimer.cancel();
			mTimer = null;
		}

		if (mTimerTask != null) {
			mTimerTask.cancel();
			mTimerTask = null;
		}
	}

	private void updateState() {
		int state = 0;//FreemeSensorManager.SensorHall.isClose() ? 0 : 1;
		Log.d(TAG, "updateState = " + state);

		mResult.setText(getString(R.string.proximity) + state);

		if (mLastValue != state) {
			mRoundCount++;
			mLastValue =state;
		}

		if (mRoundCount >= 2) {
            testResultCommit(true);
		}
	}
}
