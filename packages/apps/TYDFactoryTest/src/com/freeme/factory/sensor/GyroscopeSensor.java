package com.freeme.factory.sensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

//import vendor.freeme.hardware.soul.V1_0.ISoul;
//import vendor.freeme.hardware.soul.V1_0.Status;
//import vendor.freeme.hardware.soul.V1_0.SensorType;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;

public class GyroscopeSensor extends BaseTest implements SensorEventListener{

    private SensorManager mSensorManager;
    private Sensor mSensor;
    private boolean mRegistered;

    private TextView mAccuracy;
    private TextView mValues;

    private float mLastValue;
    private int mCount;
    private boolean mCalibrated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gyroscopesensor);
    
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        mAccuracy = (TextView) findViewById(R.id.accuracy);
        mValues = (TextView) findViewById(R.id.values);
        mValues.setText("X:\nY:\nZ");

        Button btnCalibration = (Button) findViewById(R.id.gyroscope_calibration);
        btnCalibration.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)  {
                mCalibrated = runCalibration();
                Toast.makeText(getApplicationContext(),
                        mCalibrated ? "陀螺仪校准：成功!" : "陀螺仪校准：失败！",
                        Toast.LENGTH_SHORT).show();
                mCount = 0;
            }
        });

        testActionCompleted(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mRegistered) {
            mRegistered = mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_GAME);
        }
    }

    @Override
    protected void onPause() {
        if (mRegistered) {
            mSensorManager.unregisterListener(this);
            mRegistered = false;
        }
        super.onPause();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // do nothing.
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE){
            final float[] values = event.values;

            mValues.setText("X:"+values[0]+"\nY:"+values[1]+"\nZ:"+values[2]);

            if (mLastValue != values[1]) {
                mCount++;
                mLastValue = values[1];
            }
            if (mCount >= 6 && mCalibrated) {
                testActionCompleted(true);
            }
        }
    }

    @Override
    protected void testActionCompleted(boolean completed) {
        super.testActionCompleted(completed);
        if (completed) {
            switch (FactoryTest.getTestMode()) {
                case TestConfig.TEST_MODE_ALL:
                case TestConfig.TEST_MODE_AUTO:
                    testResultCommit(true);
                    break;
            }
        }
    }

    private boolean runCalibration() {
        /*try {
            ISoul soul = ISoul.getService();
            return soul.calibrateSensor(SensorType.GYROSCOPE) == Status.OK;
        } catch (RemoteException e) {
            return false;
        }
	    */
	    return true;
    }
}
