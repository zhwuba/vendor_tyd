package com.freeme.factory.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;

import java.text.DecimalFormat;

public class PressureSensor extends BaseTest implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mPresSensor ;

    private TextView mValue;

    private DecimalFormat mFormat;

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_PRESSURE) {
            float pressure = event.values[0];
            mValue.setText(mFormat.format(pressure));

            if (pressure > 0f) {
                testActionCompleted(true);
            }
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pressure_sensor);

        mValue = (TextView) findViewById(R.id.pressure_sensor);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mPresSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);

        mFormat = new DecimalFormat("0.00");
        mFormat.getRoundingMode();

        testActionCompleted(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mPresSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(this, mPresSensor);
        super.onPause();
    }

    @Override
    protected void testActionCompleted(boolean completed) {
        super.testActionCompleted(completed);
        if (completed) {
            switch (FactoryTest.getTestMode()) {
                case TestConfig.TEST_MODE_ALL:
                case TestConfig.TEST_MODE_AUTO:
                    testResultCommit(true);
                    break;
            }
        }
    }
}
