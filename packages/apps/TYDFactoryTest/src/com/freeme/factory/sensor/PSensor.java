package com.freeme.factory.sensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

//import vendor.freeme.hardware.soul.V1_0.ISoul;
//import vendor.freeme.hardware.soul.V1_0.Status;
//import vendor.freeme.hardware.soul.V1_0.SensorType;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;

public class PSensor extends BaseTest implements SensorEventListener{
    private static final String TAG = "PSensor";

    private SensorManager mSensorManager;
    private Sensor mPSensor ;

    private TextView value_0 = null;
    private Button mBtnCali ;

    private float mValueLast = 0;
    private int mValueCount = 0;

    private boolean mCondCalibrated;
    private boolean mCondValueChangedPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.psensor);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mPSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        value_0 = (TextView) findViewById(R.id.proximity);

        mBtnCali = (Button) findViewById(R.id.psensor_calibration);
        mBtnCali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int msgResid;
                if (runCalibrationProximity()){
                    mCondCalibrated = true;
                    testActionCompleted(mCondCalibrated && mCondValueChangedPass);
                    msgResid = R.string.psensor_calibration_success;
                } else {
                    msgResid = R.string.psensor_calibration_fail;
                }
                Toast.makeText(PSensor.this, msgResid, Toast.LENGTH_LONG).show();
            }
        });

        testActionCompleted(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mPSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(this, mPSensor);
        super.onPause();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do nothing
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
            final float value = event.values[0] > 0 ? 1 : 0;
            value_0.setText(getString(R.string.proximity) + value);

            if (mValueLast != value) {
                mValueCount++;
                mValueLast = value;
            }

            if (mValueCount >= 3) {
                Toast.makeText(PSensor.this, R.string.psensor_calibration_note, Toast.LENGTH_SHORT).show();
                mValueCount = 0;
                mCondValueChangedPass = true;
            }

            testActionCompleted(mCondCalibrated && mCondValueChangedPass);
        }
    }

    @Override
    protected void testActionCompleted(boolean completed) {
        super.testActionCompleted(completed);
        if (completed) {
            switch (FactoryTest.getTestMode()) {
                case TestConfig.TEST_MODE_ALL:
                case TestConfig.TEST_MODE_AUTO:
                    testResultCommit(true);
                    break;
            }
        }
    }

    public boolean runCalibrationProximity() {
        /*try {
            ISoul soul = ISoul.getService();
            return soul.calibrateSensor(SensorType.PROXIMITY) == Status.OK;
        } catch (Exception e) {
            return false;
        }
	    */
	    return true;
    }
}
