package com.freeme.factory.sensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;
import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;

public class StepSensor extends BaseTest implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mSensor;

    private TextView mResult;

    private boolean mRegistered;
    private float mLastValues;
    private int mRoundCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step_sensor);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        mResult = (TextView) findViewById(R.id.step_sensor_info);
        mResult.setText(getResources().getString(R.string.step_sensor_total));

        testActionCompleted(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRegistered = mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        if (mRegistered) {
            mRegistered = false;
            mSensorManager.unregisterListener(this);
        }
        super.onPause();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER){
            float[] values = event.values;
            mResult.setText(getResources().getString(R.string.step_sensor_total)
                    + values[0]
                    + getResources().getString(R.string.step_sensor_number));

            if (mLastValues != values[0]) {
                mRoundCount++;
                mLastValues = values[0];
            }
            if (mRoundCount >= 3) {
                testActionCompleted(true);
            }
        }
    }

    @Override
    protected void testActionCompleted(boolean completed) {
        super.testActionCompleted(completed);
        if (completed) {
            switch (FactoryTest.getTestMode()) {
                case TestConfig.TEST_MODE_ALL:
                case TestConfig.TEST_MODE_AUTO:
                    testResultCommit(true);
                    break;
            }
        }
    }
}
