package com.freeme.factory.imei;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;

//import vendor.freeme.hardware.soul.V1_0.ISoul;

import com.freeme.factory.config.TestConfig;
import com.freeme.factory.R;
//import com.freeme.provider.FreemeSettings;

public class IMEI extends Activity implements View.OnClickListener {
    private static final String TAG = "IMEI";

    //private ISoul mISoul;

    private Button mBtnSim1;
    private Button mBtnSim2;
    private Button mBtnImei;
    private EditText mEditImeiValue;
    private Button mBtnMeid;
    private EditText mEditMeidValue;

    private AlertDialog mDialog;

    private Phone mPhone;
    private int[] mPhoneSlotIds;
    private int mFocusedPhoneSlotId;

    private static final int EVENT_WRITE_IMEI = 7;
    private static final int EVENT_WRITE_MEID = 8;
    private static final int EVENT_SET_ESUO9 = 9;
    private static final int EVENT_RFSSYNC = 10;
    @SuppressLint("HandlerLeak")
    private final Handler mResponseHander = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (isDestroyed()) {
                return;
            }

            if (mDialog.isShowing()) {
                mDialog.dismiss();
            }

            switch (msg.what) {
                case EVENT_WRITE_IMEI: {
                    AsyncResult ar = (AsyncResult) msg.obj;
                    Log.e(TAG, "EVENT_WRITE_IMEI ar.exception: " + ar.exception);
                    if (ar.exception == null) {
                        mDialog.setTitle("IMEI WRITE");
                        mDialog.setMessage("The IMEI is writen successfully.");
                        mDialog.show();
                        backupImeis();
                    } else {
                        mDialog.setTitle("IMEI WRITE");
                        mDialog.setMessage("Fail to write IMEI due to radio unavailable or something else.");
                        mDialog.show();
                    }
                } break;

                case EVENT_WRITE_MEID: {
                    AsyncResult ar = (AsyncResult) msg.obj;
                    Log.e(TAG, "EVENT_WRITE_MEID ar.exception: " + ar.exception);
                    if (ar.exception == null) {
                        mDialog.setTitle("MEID WRITE");
                        mDialog.setMessage("The MEID is writen successfully.");
                        mDialog.show();
                        backupImeis();
                    }
                } break;
                case EVENT_RFSSYNC: {
                    AsyncResult ar = (AsyncResult) msg.obj;
                    Log.e(TAG, "EVENT_RFSSYNC ar.exception: " + ar.exception);
                    if (ar.exception == null) {
                        mDialog.setTitle("MEID WRITE");
                        mDialog.setMessage("The MEID is writen successfully.");
                        mDialog.show();
                        backupImeis();
                    }
                } break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imei);

        /*try {
            mISoul = ISoul.getService();
        } catch (RemoteException e) {
        }*/

        mPhone = PhoneFactory.getDefaultPhone();
        mPhoneSlotIds = getSlotIds();
        mFocusedPhoneSlotId = mPhoneSlotIds[0];

        // IMEI
        mBtnSim1 = (Button) findViewById(R.id.Sim1);
        mBtnSim2 = (Button) findViewById(R.id.Sim2);
        mBtnSim1.setOnClickListener(this);
        mBtnSim2.setOnClickListener(this);
        mEditImeiValue = (EditText) findViewById(R.id.IMEI_VALUE);
        mBtnImei = (Button) findViewById(R.id.IMEI);
        mBtnImei.setOnClickListener(this);
        if (!TestConfig.TEST_EN_GEMINI_SUPPORT) {
            mBtnSim2.setVisibility(View.GONE);
        }
        // MEID
        mEditMeidValue = (EditText) findViewById(R.id.meid_value);
        mBtnMeid = (Button) findViewById(R.id.meid);
        mBtnMeid.setOnClickListener(this);
        if (isCdmaSupport()) {
            mEditMeidValue.setVisibility(View.VISIBLE);
            mBtnMeid.setVisibility(View.VISIBLE);
            setSimMEID();
        }

        setFocusedSim();

        mDialog = new AlertDialog.Builder(this)
                .setPositiveButton(android.R.string.ok, null)
                .create();
    }

    @Override
    public void onDestroy() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == mBtnSim1) {
            mFocusedPhoneSlotId = mPhoneSlotIds[0];
            setFocusedSim();
        } else if (v == mBtnSim2) {
            mFocusedPhoneSlotId = mPhoneSlotIds[1];
            setFocusedSim();
        } else if (v == mBtnImei) {
            String imei = mEditImeiValue.getText().toString();
            Log.v(TAG, "ditImeiValue:" + imei);

            String[] imeiString = {"AT+EGMR=1,", ""};
            if (TestConfig.TEST_EN_GEMINI_SUPPORT) {
                if (mFocusedPhoneSlotId == mPhoneSlotIds[0]) {
                    imeiString[0] = "AT+EGMR=1,7,\"" + imei + "\"";
                } else {
                    imeiString[0] = "AT+EGMR=1,10,\"" + imei + "\"";
                }
            } else {
                imeiString[0] = "AT+EGMR=1,7,\"" + imei + "\"";
            }
            Log.v(TAG, "IMEI String:" + imeiString[0]);
            mPhone.invokeOemRilRequestStrings(imeiString,
                    mResponseHander.obtainMessage(EVENT_WRITE_IMEI));
        } else if (v == mBtnMeid) {
            String meid = mEditMeidValue.getText().toString();
            if (!validMeid(meid)){
                Toast.makeText(this, "MEID is wrong!", Toast.LENGTH_SHORT).show();
                return;
            }

            writeMeid(PhoneFactory.getPhone(0), meid);
            writeMeid(PhoneFactory.getPhone(1), meid);
        }
    }

    private void setFocusedSim() {
        if (TestConfig.TEST_EN_GEMINI_SUPPORT) {
            if (mFocusedPhoneSlotId == mPhoneSlotIds[0]) {
                mBtnSim1.setEnabled(false);
                mBtnSim2.setEnabled(true);
            } else {
                mBtnSim1.setEnabled(true);
                mBtnSim2.setEnabled(false);
            }
        }

	/*
        String imei = SystemProperties.get(
                FreemeSettings.System.FREEME_SIM_GSM_IMEI_ARR[mFocusedPhoneSlotId]);
        if (!TextUtils.isEmpty(imei)) {
            mEditImeiValue.setText(imei);
        }
	*/
    }

    private void setSimMEID() {
        /*String meid = SystemProperties.get(FreemeSettings.System.FREEME_SIM_CDMA_MEID);
        if (!TextUtils.isEmpty(meid)) {
            mEditMeidValue.setText(meid);
        }
	*/
    }

    public static boolean isCdmaSupport() {
        return false;//"1".equals(SystemProperties.get("ro.mtk_c2k_support"));
    }

    private void writeMeid(Phone phone, String meid) {
        if (phone == null) {
            return;
        }

        phone.invokeOemRilRequestStrings(
                new String[] {"AT+vmobid=0,\"7268324842763108\",2,\"" + meid + "\"", ""},
                mResponseHander.obtainMessage(EVENT_WRITE_MEID));
    }

    private static boolean validMeid(String meid){
        int count = meid.length();
        for (int i = 0; i < count; i++) {
            char c = meid.charAt(i);
            if ((47 < c && c < 58) || (64 < c && c < 71) || (96 < c && c < 103)) {
                continue;
            }
            return false;
        }
        return true;
    }

    private void backupImeis() {
        Log.v(TAG, "backupImeis");
        /*if (mISoul != null) {
            try {
                mISoul.backupNvram();
            } catch (RemoteException e) {
                Log.e(TAG, "RemoteException in ITelephony.Stub.asInterface backupImeis");
            }
        }*/
    }

    private static int[] getSlotIds() {
        int slotCount = TelephonyManager.getDefault().getPhoneCount();
        int[] slotIds = new int[slotCount];
        for (int i = 0; i < slotCount; i++) {
            slotIds[i] = i;
        }
        return slotIds;
    }
}
