package com.freeme.factory.audio;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.widget.TextView;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;

public class Earphone extends BaseTest {

    private TextView mTxtStats;

    private AudioManager mAudioManager;
    private MediaPlayer mPlayer;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (Intent.ACTION_HEADSET_PLUG.equals(action)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0/*unplugged*/:
                        maximizeVolume();
                        mTxtStats.setText("");
                        testActionCompleted(true);
                        break;
                    case 1/*plugged*/:
                        mTxtStats.setText(R.string.earphone_tips);
                        testActionCompleted(false);
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.earphone);

        mTxtStats = (TextView) findViewById(R.id.tv_status);

        maximizeVolume();

    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(mReceiver, intentFilter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        playonce();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(mReceiver);

        if (mPlayer != null) {
            mPlayer.pause();
            mPlayer.release();
            mPlayer = null;
        }
        mAudioManager.setMode(AudioManager.MODE_NORMAL);
        super.onStop();
    }

    protected void playonce() {
        mPlayer = MediaPlayer.create(this, R.raw.audio_person);
        mPlayer.setLooping(true);
        mPlayer.start();
    }

    private void maximizeVolume() {
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        mAudioManager.setSpeakerphoneOn(false);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                AudioManager.FLAG_ALLOW_RINGER_MODES);
        if (mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
                > mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC)) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                    mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                    AudioManager.FLAG_SHOW_UI|AudioManager.FLAG_ALLOW_RINGER_MODES);
        }
        mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL,
                mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL),
                AudioManager.FLAG_ALLOW_RINGER_MODES);
    }
}
