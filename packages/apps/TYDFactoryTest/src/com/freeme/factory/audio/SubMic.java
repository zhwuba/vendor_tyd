package com.freeme.factory.audio;

import java.io.File;
import java.io.IOException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.freeme.factory.R;
import com.freeme.factory.widget.VUMeter;
import com.freeme.factory.base.BaseTest;

public class SubMic extends BaseTest {

    private AudioManager mAudioManager;
    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;

    private Button  mBtnTest;
    private VUMeter mVUMeter;

    private static final int TEST_IDLE        = 0;
    private static final int TEST_RECORDERING = 1;
    private static final int TEST_PLAYYING    = 2;

    private int  mStatus = TEST_IDLE;
    private File mSoundFile;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (Intent.ACTION_HEADSET_PLUG.equals(action)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0/*unplugged*/: {
                        mBtnTest.setEnabled(true);

                        switch (mStatus) {
                            case TEST_RECORDERING:
                                mBtnTest.setText(R.string.Mic_stop);
                                break;
                            case TEST_PLAYYING:
                                mBtnTest.setText(R.string.stopplayer);

                                testActionCompleted(true);
                                break;
                            case TEST_IDLE:
                                mBtnTest.setText(R.string.Mic_start);
                                break;
                        }
                    } break;
                    case 1/*plugged*/: {
                        testActionCompleted(false);

                        mBtnTest.setText(R.string.earphone_tips);
                        mBtnTest.setEnabled(false);

                        switch (mStatus) {
                            case TEST_RECORDERING:
                                stopRecording();
                                mStatus = TEST_IDLE;
                                break;
                            case TEST_PLAYYING:
                                stopPlayback();
                                mStatus = TEST_IDLE;
                                break;
                            case TEST_IDLE:
                                break;
                        }
                    } break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.submicrecorder);

        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        mVUMeter = (VUMeter) findViewById(R.id.uvMeter);

        mBtnTest = (Button) findViewById(R.id.mic_bt_start);
        mBtnTest.setText(R.string.Mic_start);
        mBtnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mStatus) {
                    case TEST_IDLE:
                        mStatus = TEST_RECORDERING;
                        startRecording();

                        mBtnTest.setText(R.string.Mic_stop);
                        break;

                    case TEST_RECORDERING:
                        mStatus = TEST_PLAYYING;
                        stopRecording();
                        startPlayback();

                        mBtnTest.setText(R.string.stopplayer);
                        testActionCompleted(true);
                        break;

                    case TEST_PLAYYING:
                        mStatus = TEST_IDLE;
                        stopPlayback();

                        mBtnTest.setText(R.string.Mic_start);
                        break;
                }
            }
        });

        mSoundFile = Environment.buildPath(getExternalCacheDir(), "CachedRecorder.amr");

        testActionCompleted(false);
    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(mReceiver, intentFilter);

        mAudioManager.setParameters("which_mic=2");
    }

    @Override
    protected void onStop() {
        mStatus = TEST_IDLE;

        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        }

        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;

            mBtnTest.setText(R.string.Mic_start);
        }

        mAudioManager.setParameters("which_mic=0");

        unregisterReceiver(mReceiver);

        if (mSoundFile != null) mSoundFile.delete();

        super.onStop();
    }

    // record

    void startRecording() {
        // create file
        try {
            if (mSoundFile.exists()) {
                mSoundFile.delete();
            }
            mSoundFile.createNewFile();
        } catch (IOException e) {
            Log.e("Recorder", "Create file fail", e);
            return;
        }

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(
                MediaRecorder.AudioSource.MIC
                );
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);

        mRecorder.setOutputFile(mSoundFile.getAbsolutePath());

        // Handle IOException
        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e("Recorder", "Recoder prepare fail", e);
            mRecorder.reset();
            mRecorder.release();
            if (mSoundFile != null) mSoundFile.delete();
            mRecorder = null;
            return;
        }
        mVUMeter.startRecorder();
        // Handle RuntimeException if the recording couldn't start
        try {
            mRecorder.start();
        } catch (IllegalStateException e) {
            Log.e("Recorder", "Recoder start fail", e);
            mRecorder.reset();
            mRecorder.release();
            if (mSoundFile != null) mSoundFile.delete();
            mRecorder = null;
            return;
        }

        mVUMeter.setRecorder(mRecorder);
    }

    private void stopRecording() {
        if (mRecorder == null) {
            return;
        }
        mVUMeter.stopRecorder();
        try {
            mRecorder.stop();
        } catch (RuntimeException exception){
            Log.e("Recorder", "Stop Failed");
        }
        mRecorder.reset();
        mRecorder.release();
        mRecorder = null;
    }

    // playback

    private void startPlayback() {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mSoundFile.getAbsolutePath());
            mPlayer.setLooping(true);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            mPlayer = null;
        } catch (IOException e) {
            e.printStackTrace();
            mPlayer = null;
        }

        mBtnTest.setText(R.string.stopplayer);
    }

    private void stopPlayback() {
        if (mPlayer == null) {
            return;
        }
        mPlayer.stop();
        mPlayer.release();
        mPlayer = null;

        mBtnTest.setText(R.string.Mic_start);
    }
}
