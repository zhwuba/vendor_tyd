package com.freeme.factory.audio;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;

public class Speaker extends BaseTest {

    private AudioManager mAudioManager;
    private MediaPlayer mPlayer;

    private Button mBtnCali;
    private TextView mTextShowResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speaker);

        maximizeVolume();

        //initCalibrateItem();

    }

    @Override
    protected void onResume() {
        super.onResume();

        mAudioManager.setSpeakerphoneOn(true);

        /**
         * for left or right audio channel
         */
        switch (mCausePurpose) {
            case TestConfig.TEST_ITEM_AUDIO_SPEAKER_LEFT:
            case TestConfig.TEST_ITEM_AUDIO_SPEAKER_RIGHT:
                break;
            default:
                mAudioManager.setMode(AudioManager.MODE_NORMAL);
                break;
        }

        playOnce();
    }

    @Override
    protected void onPause() {
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }

        /**
         * for left or right audio channel
         */
        switch (mCausePurpose) {
            case TestConfig.TEST_ITEM_AUDIO_SPEAKER_LEFT:
            case TestConfig.TEST_ITEM_AUDIO_SPEAKER_RIGHT:
                break;
            default:
                mAudioManager.setMode(AudioManager.MODE_NORMAL);
                break;
        }

        mAudioManager.setSpeakerphoneOn(false);

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }

        super.onDestroy();
    }

    @Override
    protected void onTestTitle(int purpose) {
        setTitle(TestConfig.getNameByItemId(this, purpose));
    }

    private void initCalibrateItem() {
        /*
        if (nativeSupportCalibration()) {
            mTextShowResult = (TextView) findViewById(R.id.calibrate_result);
            mTextShowResult.setVisibility(View.VISIBLE);

            mBtnCali = (Button) findViewById(R.id.speaker_calibration);
            mBtnCali.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new CalibrateTask().execute();
                }
            });
            mBtnCali.setVisibility(View.VISIBLE);
            testActionCompleted(false);
        }
        */
    }

    private void playOnce() {
        if (mPlayer == null) {
            final int mediaId;
            switch (mCausePurpose) {
                case TestConfig.TEST_ITEM_AUDIO_SPEAKER_LEFT:
                    mediaId = R.raw.audio_channel_left;
                    break;
                case TestConfig.TEST_ITEM_AUDIO_SPEAKER_RIGHT:
                    mediaId = R.raw.audio_channel_right;
                    break;
                default:
                    mediaId = R.raw.audio_person;
                    break;
            }
            mPlayer = MediaPlayer.create(this, mediaId);
            mPlayer.setLooping(true);
        }
        mPlayer.start();
    }

    private void maximizeVolume() {
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                AudioManager.FLAG_ALLOW_RINGER_MODES);
        if (mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
                > mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC)) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                    mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                    AudioManager.FLAG_SHOW_UI|AudioManager.FLAG_ALLOW_RINGER_MODES);
        }
    }

    /*
    private class CalibrateTask extends AsyncTask<Void, Void, Float> {
        private int mMsgResid = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mBtnCali.setClickable(false);

            mMsgResid = R.string.speaker_calibration_start;
            Toast.makeText(Speaker.this, mMsgResid, Toast.LENGTH_LONG).show();
            testActionCompleted(false);
        }

        @Override
        protected Float doInBackground(Void... objectses) {
            if (nativeRunCalibration() != 0) {
                // handle error
                return -1f;
            }
            return nativeGetCalibrationResult();
        }

        @Override
        protected void onPostExecute(Float result) {
            mMsgResid = R.string.speaker_calibration_complete;
            Toast.makeText(Speaker.this, mMsgResid, Toast.LENGTH_LONG).show();
            mTextShowResult.setText(getString(R.string.speaker_calibration_value) + result);

            testActionCompleted(true);
            mBtnCali.setClickable(true);
        }
    }
    */

    /// ---------------------------- Calibrate ---------------------------------
    /*
    static {
        System.loadLibrary("jni_factory_speaker");
    }

    private native boolean nativeSupportCalibration();
    private native int nativeRunCalibration();
    private native float nativeGetCalibrationResult();
    */
}
