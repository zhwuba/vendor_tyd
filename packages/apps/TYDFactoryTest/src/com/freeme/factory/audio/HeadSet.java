package com.freeme.factory.audio;

import java.io.File;
import java.io.IOException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;
import com.freeme.factory.widget.VUMeter;

public class HeadSet extends BaseTest {

    private AudioManager mAudioManager;
    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;

    private Button  mBtnTest;
    private VUMeter mVUMeter;
    private TextView mTxtGuide;

    private static final int TEST_IDLE        = 0;
    private static final int TEST_RECORDERING = 1;
    private static final int TEST_PLAYYING    = 2;

    private int  mStatus = TEST_IDLE;
    private File mSoundFile;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (Intent.ACTION_HEADSET_PLUG.equals(action)) {
                int state = intent.getIntExtra("state", 0);
                switch (state) {
                    case 0/*unplugged*/: {
                        if (mStatus == TEST_RECORDERING) {
                            mStatus = TEST_IDLE;
                            stopRecording();
                        } else if (mStatus == TEST_PLAYYING) {
                            mStatus = TEST_IDLE;
                            stopPlayback();
                        }
                        mBtnTest.setEnabled(false);
                        mBtnTest.setText(R.string.HeadSet_tips);
                    } break;

                    case 1/*plugged*/: {
                        maximizeVolume();
                        mBtnTest.setEnabled(true);
                        mBtnTest.setText(R.string.Mic_start);
                    } break;
                }
            }
        }
    };

    private boolean mHeadsetRecorded;
    private boolean mHeadsetHookKeyPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.headset);

        maximizeVolume();

        mVUMeter = (VUMeter) findViewById(R.id.uvMeter);
        mTxtGuide = (TextView) findViewById(R.id.guide);
        mBtnTest = (Button) findViewById(R.id.mic_bt_start);
        //*/DZ.zhangzhao, 20181229. button
        if (!mAudioManager.isWiredHeadsetOn()){
            mBtnTest.setEnabled(false);
        }
        //*/
        mBtnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mStatus) {
                    case TEST_IDLE:
                        mStatus = TEST_RECORDERING;
                        startRecording();
                        break;

                    case TEST_RECORDERING:
                        mStatus = TEST_PLAYYING;
                        stopRecording();
                        startPlayback();

                        mHeadsetRecorded = true;
                        checkConditions();
                        break;

                    case TEST_PLAYYING:
                        mStatus = TEST_IDLE;
                        stopPlayback();
                        break;
                }
            }
        });

        mSoundFile = Environment.buildPath(getExternalCacheDir(), "CachedRecorder.amr");

        testActionCompleted(false);
        checkConditions();
    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(mReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        mStatus = TEST_IDLE;

        if (mRecorder != null) {
            stopRecording();
        }

        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
        mBtnTest.setText(R.string.Mic_start);

        unregisterReceiver(mReceiver);

        if (mSoundFile != null) mSoundFile.delete();

        super.onStop();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_HEADSETHOOK:
                mHeadsetHookKeyPressed = true;
                checkConditions();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    @Override
    protected void testActionCompleted(boolean completed) {
        super.testActionCompleted(completed);
        if (completed) {
            switch (FactoryTest.getTestMode()) {
                case TestConfig.TEST_MODE_ALL:
                    testResultCommit(true);
                    break;
            }
        }
    }

    private void checkConditions() {
        if (mHeadsetRecorded && mHeadsetHookKeyPressed) {
            testActionCompleted(true);
        } else if (!mHeadsetRecorded) {
            mTxtGuide.setText(R.string.HeadSet_recorder_tip);
        } else if (!mHeadsetHookKeyPressed) {
            mTxtGuide.setText(R.string.HeadSet_hookkey_tip);
        }
    }

    // record

    private void startRecording() {
        // create file
        try {
            if (mSoundFile.exists()) {
                mSoundFile.delete();
            }
            mSoundFile.createNewFile();
        } catch (IOException e) {
            Log.e("Recorder", "Create file fail", e);
            return;
        }

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);

        mRecorder.setOutputFile(mSoundFile.getAbsolutePath());

        // Handle IOException
        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e("Recorder", "Recoder prepare fail", e);
            mRecorder.reset();
            mRecorder.release();
            if (mSoundFile != null) mSoundFile.delete();
            mRecorder = null;
            return;
        }
        // Handle RuntimeException if the recording couldn't start
        mVUMeter.startRecorder();
        try {
            mRecorder.start();
        } catch (IllegalStateException e) {
            Log.e("Recorder", "Recoder start fail", e);
            mRecorder.reset();
            mRecorder.release();
            if (mSoundFile != null) mSoundFile.delete();
            mRecorder = null;
            return;
        }

        mBtnTest.setText(R.string.Mic_stop);
        mVUMeter.setRecorder(mRecorder);
    }

    private void stopRecording() {
        if (mRecorder == null) {
            return;
        }
         mVUMeter.stopRecorder();
        try {
            mRecorder.stop();
        } catch (RuntimeException exception){
            Log.e("Recorder", "Stop Failed");
        }
        mRecorder.reset();
        mRecorder.release();
        mRecorder = null;
    }

    // playback

    private void startPlayback() {
        mPlayer = new MediaPlayer();
        try{
            mPlayer.setDataSource(mSoundFile.getAbsolutePath());
            mPlayer.setLooping(true);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            mPlayer = null;
        } catch (IOException e) {
            e.printStackTrace();
            mPlayer = null;
        }

        mBtnTest.setText(R.string.stopplayer);
    }

    private void stopPlayback() {
        if (mPlayer == null) {
            return;
        }
        mPlayer.stop();
        mPlayer.release();
        mPlayer = null;

        mBtnTest.setText(R.string.Mic_start);
    }

    private void maximizeVolume() {
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                AudioManager.FLAG_ALLOW_RINGER_MODES);
        if (mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
                > mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC)) {
            //*/dz.wangqi, 20181214.hide UI.
            mAudioManager.disableSafeMediaVolume();
            //*/
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                    mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                    /*/dz.wangqi, 20181226. remove Volume UI.
                    AudioManager.FLAG_SHOW_UI|AudioManager.FLAG_ALLOW_RINGER_MODES);
                    /*/
                    AudioManager.FLAG_ALLOW_RINGER_MODES);
                    //*/
        }
    }
}