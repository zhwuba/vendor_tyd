package com.freeme.factory.audio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.TextView;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;

public class HiFi extends BaseTest {

    private AudioManager mAudioManager;
    private MediaPlayer mPlayer;

    private TextView mTxtTip;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Intent.ACTION_HEADSET_PLUG.equals(action)) {
                int state = intent.getIntExtra("state", 0);
                switch (state) {
                    case 0/*unplugged*/: {
                        if (mPlayer != null) {
                            mPlayer.pause();
                            mPlayer.release();
                            mPlayer = null;
                        }

                        mTxtTip.setText(getString(R.string.HeadSet_tips));
                    } break;

                    case 1/*plugged*/: {
                        playonce();

                        testActionCompleted(true);
                        mTxtTip.setText(getString(R.string.HeadSet_plugged));
                    } break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hifi);

        mTxtTip = (TextView) findViewById(R.id.hifi_tips);

        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        //mAudioManager.SetAudioHiFiState(true);

        testActionCompleted(false);
    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(mReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        if (mPlayer != null) {
            mPlayer.pause();
            mPlayer.release();
            mPlayer = null;
        }
        //mAudioManager.SetAudioHiFiState(false);

        unregisterReceiver(mReceiver);

        super.onStop();
    }

    protected void playonce() {
        if (mPlayer != null) {
            mPlayer.pause();
            mPlayer.release();
            mPlayer = null;
        }

        //mAudioManager.SetAudioHiFiState(true);

        mPlayer = MediaPlayer.create(this, R.raw.audio_person);
        mPlayer.setLooping(true);
        mPlayer.start();
    }
}