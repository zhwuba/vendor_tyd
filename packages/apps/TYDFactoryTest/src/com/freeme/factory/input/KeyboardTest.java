package com.freeme.factory.input;

import android.os.Bundle;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.SimpleAdapter;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class KeyboardTest extends BaseTest {

    private static       boolean TESTING_KEY_MENU           = false;
    private static       boolean TESTING_KEY_HOME           = false;
    private static       boolean TESTING_KEY_BACK           = false;
    private static final boolean TESTING_KEY_SEARCH         = false;
    private static final boolean TESTING_KEY_VOLUME_DOWN    = true;
    private static final boolean TESTING_KEY_VOLUME_UP      = true;
    private static final boolean TESTING_KEY_CAMERA         = false;
    private static       boolean TESTING_KEY_SIDEKEY        = false;

    private boolean mKeyTestedMenu;
    private boolean mKeyTestedHome;
    private boolean mKeyTestedBack;
    private boolean mKeyTestedSearch;
    private boolean mKeyTestedVolumeDown;
    private boolean mKeyTestedVolumeUp;
    private boolean mKeyTestedCamera;
    private boolean mKeyTestedSideKey;

    private GridView mTestingKeysView;
    private BaseAdapter mTestingKeysAdapter;
    private List<Map<String, Integer>> mTestingKeys = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.keycode);

        testActionCompleted(false);

        handleVirtualKeys();
        {
            if (TESTING_KEY_MENU) {
                handleTestingKey(true, R.drawable.menu);
            }

            if (TESTING_KEY_HOME) {
                handleTestingKey(true, R.drawable.home);
            }

            if (TESTING_KEY_BACK) {
                handleTestingKey(true, R.drawable.back);
            }

            if (TESTING_KEY_SEARCH) {
                handleTestingKey(true, R.drawable.search);
            }

            if (TESTING_KEY_VOLUME_DOWN) {
                handleTestingKey(true, R.drawable.vldown);
            }

            if (TESTING_KEY_VOLUME_UP) {
                handleTestingKey(true, R.drawable.vlup);
            }

            if (TESTING_KEY_CAMERA) {
                handleTestingKey(true, R.drawable.camera);
            }

            if (TESTING_KEY_SIDEKEY) {
                handleTestingKey(true, R.drawable.camera);
            }
        }

        mTestingKeysAdapter = new SimpleAdapter(this, mTestingKeys, R.layout.keycode_grid,
                new String[] { "imageResId" }, new int[] { R.id.imgview });
        mTestingKeysView = (GridView) findViewById(R.id.keycode_grid);
        mTestingKeysView.setAdapter(mTestingKeysAdapter);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        //getWindow().addFlagsEx(WindowManager.LayoutParams.FLAG_EX_HOMEKEY_DISPATCHED
        //        | WindowManager.LayoutParams.FLAG_EX_RECENTKEY_DISPATCHED);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_VOLUME_UP:
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_MENU:
            case KeyEvent.KEYCODE_APP_SWITCH:
                if (!mKeyTestedMenu) {
                    handleTestingKey(false, R.drawable.menu);
                    mKeyTestedMenu = true;
                }
                break;
            case KeyEvent.KEYCODE_HOME:
                if (!mKeyTestedHome) {
                    handleTestingKey(false, R.drawable.home);
                    mKeyTestedHome = true;
                }
                break;
            case KeyEvent.KEYCODE_BACK:
                if (!mKeyTestedBack) {
                    handleTestingKey(false, R.drawable.back);
                    mKeyTestedBack = true;
                }
                break;
            case KeyEvent.KEYCODE_SEARCH:
                if (!mKeyTestedSearch) {
                    handleTestingKey(false, R.drawable.search);
                    mKeyTestedSearch = true;
                }
                break;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (!mKeyTestedVolumeDown) {
                    handleTestingKey(false, R.drawable.vldown);
                    mKeyTestedVolumeDown = true;
                }
                break;
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (!mKeyTestedVolumeUp) {
                    handleTestingKey(false, R.drawable.vlup);
                    mKeyTestedVolumeUp = true;
                }
                break;
            case KeyEvent.KEYCODE_CAMERA:
                if (!mKeyTestedCamera) {
                    handleTestingKey(false, R.drawable.camera);
                    mKeyTestedCamera = true;
                }
                break;
            case KeyEvent.KEYCODE_F12:
                if (!mKeyTestedSideKey){
                    handleTestingKey(false, R.drawable.camera);
                    mKeyTestedSideKey = true;
                }
            default:
                break;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_MENU:
            case KeyEvent.KEYCODE_APP_SWITCH:
            case KeyEvent.KEYCODE_HOME:
            case KeyEvent.KEYCODE_BACK:
            case KeyEvent.KEYCODE_SEARCH:
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_CAMERA:
            case KeyEvent.KEYCODE_F12:
                mTestingKeysAdapter.notifyDataSetChanged();
                if (mTestingKeysAdapter.isEmpty()) {
                    testActionCompleted(true);
                }
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    @Override
    protected void testActionCompleted(boolean completed) {
        super.testActionCompleted(completed);
        if (completed) {
            switch (FactoryTest.getTestMode()) {
                case TestConfig.TEST_MODE_ALL:
                case TestConfig.TEST_MODE_AUTO:
                    testResultCommit(true);
                    break;
            }
        }
    }

    private void handleVirtualKeys() {
        final String keyboard = SystemProperties.get("ro.freeme.hw_keyboard").toLowerCase();
        if (!TextUtils.isEmpty(keyboard)) {
            TESTING_KEY_HOME = keyboard.contains("home");
            TESTING_KEY_MENU = keyboard.contains("menu");
            TESTING_KEY_BACK = keyboard.contains("back");
            TESTING_KEY_SIDEKEY = keyboard.contains("side");
        }
    }

    private void handleTestingKey(boolean all, int imageResId) {
        Map<String, Integer> resMap = new ArrayMap<>();
        resMap.put("imageResId", imageResId);
        if (all)
            mTestingKeys.add(resMap);
        else
            mTestingKeys.remove(resMap);
    }
}
