package com.freeme.factory.input;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.app.StatusBarManager;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.R;
import com.freeme.factory.config.TestConfig;
//import com.freeme.util.FreemeFeature;

public class TouchTest extends BaseTest {
    private static final String TAG = "TouchTest";

    // HEIGHT_BASIS & WIDTH_BASIS init in FreemeFeature.ini
    private static final int HEIGHT_BASIS = 27;//FreemeFeature.getLocalInt("height.basis", 27);
    private static final int WIDTH_BASIS  = 16;//FreemeFeature.getLocalInt("width.basis", 16);

    private static final int HEIGHT_BASIS_CROSS  = (HEIGHT_BASIS - 2) * 2;

    private static final boolean[][] click = new boolean[HEIGHT_BASIS][WIDTH_BASIS];
    private static final boolean[][] draw = new boolean[HEIGHT_BASIS][WIDTH_BASIS];
    private static final boolean[][] isDrawArea = new boolean[HEIGHT_BASIS][WIDTH_BASIS];
    private static final boolean[] drawCross = new boolean[HEIGHT_BASIS_CROSS * 2];

    private int mBottommostOfMatrix = HEIGHT_BASIS - 1;
    private int mRightmostOfMatrix = WIDTH_BASIS - 1;
    private int mLeftmostOfMatrix = 0;
    private int mTopmostOfMatrix = 0;

    private StatusBarManager mSBM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(new MyView(this));

        fillUpMatrix();

        mSBM = (StatusBarManager) getSystemService(Context.STATUS_BAR_SERVICE);
    }

    @Override
    protected void onStart() {
        mSBM.disable(StatusBarManager.DISABLE_EXPAND
                | StatusBarManager.DISABLE_NOTIFICATION_ICONS
                | StatusBarManager.DISABLE_NOTIFICATION_ALERTS
                | StatusBarManager.DISABLE_SYSTEM_INFO
                | StatusBarManager.DISABLE_HOME
                | StatusBarManager.DISABLE_SEARCH
                | StatusBarManager.DISABLE_RECENT);
        super.onStart();
    }

    @Override
    protected void onResume() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_GESTURE_ISOLATED);
        super.onResume();
    }

    @Override
    protected void onStop() {
        mSBM.disable(StatusBarManager.DISABLE_NONE);
        super.onStop();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_BACK:
                startResultActivity(false);
                break;
        }
        return true;
    }

    private void startResultActivity(boolean finished) {
        if (!finished && FactoryTest.getTestMode() == TestConfig.TEST_MODE_MANUAL) {
            setContentView(R.layout.touch);
            testActionCompleted(false);
        } else {
            if (isPass() && isPassCross()) {
                onTestSuccess(null);
            } else {
                onTestFailed(null);
            }
        }
    }

    private void fillUpMatrix() {
        int row = 0;
        while (row < HEIGHT_BASIS) {
            int column = 0;
            while (column < WIDTH_BASIS) {
                if (!isNeededCheck(row, column)) {
                    isDrawArea[row][column] = false;
                } else {
                    isDrawArea[row][column] = true;
                }
                column++;
            }
            row++;
        }
    }

    private boolean isNeededCheck(int row, int column) {
        return row == mTopmostOfMatrix
                || row == mBottommostOfMatrix
                || column == mLeftmostOfMatrix
                || column == mRightmostOfMatrix;
    }

    private boolean isNeededCheck_Hovering(int row, int column) {
        if (row == mTopmostOfMatrix && column != mLeftmostOfMatrix && column != mRightmostOfMatrix) {
            return true;
        }
        if ((row == mBottommostOfMatrix && column != mLeftmostOfMatrix && column != mRightmostOfMatrix)
                || column == mLeftmostOfMatrix + 1
                || column == mRightmostOfMatrix - 1) {
            return true;
        }
        return false;
    }

    private boolean isPass() {
        boolean isPass = true;
        for (int i = 0; i < HEIGHT_BASIS; i++) {
            for (int j = 0; j < WIDTH_BASIS; j++) {
                if (isDrawArea[i][j]) {
                    isPass &= draw[i][j];
                }
            }
        }
        return isPass;
    }

    private boolean isPassCross() {
        boolean isPassCross = true;
        for (int i = 0; i < HEIGHT_BASIS_CROSS * 2; i++) {
            isPassCross &= drawCross[i];
        }
        return isPassCross;
    }

    public class MyView extends View {
        private float mPreTouchedX;
        private float mPreTouchedY;
        private float mTouchedX;
        private float mTouchedY;
        private float mWidthCross;
        private float mHeightCross;
        private float mColHeight;
        private float mColWidth;

        private int mScreenHeight;
        private int mScreenWidth;

        private boolean isTouchDown;

        private Paint mClickPaint;
        private Paint mEmptyPaint;
        private Paint mLinePaint;
        private Paint mNonClickPaint;

        private Canvas mMatrixCanvas;

        private Bitmap mMatrixBitmap;

        public MyView(Context context) {
            super(context);
            setKeepScreenOn(true);
            Display mDisplay = ((WindowManager) context.getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
            Point outpoint = new Point();
            mDisplay.getRealSize(outpoint);

            mScreenWidth = outpoint.x;
            mScreenHeight = outpoint.y;

            Log.i(TAG, "onCreate" + "Screen size: " + mScreenWidth + " x " + mScreenHeight);

            mMatrixBitmap = Bitmap.createScaledBitmap(Bitmap.createBitmap(mScreenWidth, mScreenHeight, Config.RGB_565), mScreenWidth, mScreenHeight, false);
            mMatrixCanvas = new Canvas(mMatrixBitmap);
            mMatrixCanvas.drawColor(-1);

            mColHeight = ((float) mScreenHeight) / ((float) HEIGHT_BASIS);
            mColWidth = ((float) mScreenWidth) / ((float) WIDTH_BASIS);

            mWidthCross = (((float) mScreenWidth) - (mColWidth * 1.0f)) / ((float) ((HEIGHT_BASIS_CROSS - 1) + 4));
            mHeightCross = mColHeight / 2.0f;

            setPaint();
            initRect();
            isTouchDown = false;
        }

        private void setPaint() {
            mLinePaint = new Paint();
            mLinePaint.setAntiAlias(true);
            mLinePaint.setDither(true);
            mLinePaint.setColor(Color.BLACK );
            mLinePaint.setStyle(Style.STROKE);
            mLinePaint.setStrokeJoin(Join.ROUND);
            mLinePaint.setStrokeCap(Cap.SQUARE);
            mLinePaint.setStrokeWidth(5.0f);
            mLinePaint.setPathEffect(new DashPathEffect(new float[]{5.0f, 5.0f}, 1.0f));

            mClickPaint = new Paint();
            mClickPaint.setAntiAlias(false);
            mClickPaint.setStyle(Style.FILL);
            mClickPaint.setColor(Color.GREEN);
            mClickPaint.setXfermode(new PorterDuffXfermode(Mode.MULTIPLY));

            mNonClickPaint = new Paint();
            mNonClickPaint.setAntiAlias(false);
            mNonClickPaint.setColor(-1);

            mEmptyPaint = new Paint();
            mEmptyPaint.setAntiAlias(false);
            mEmptyPaint.setColor(-1);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawBitmap(mMatrixBitmap, 0, 0, null);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            int action = event.getAction();
            if (event.getToolType(0) == MotionEvent.TOOL_TYPE_STYLUS) {
                return true;
            }

            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    drawDown(event);
                    break;
                case MotionEvent.ACTION_UP:
                    drawUp(event);
                    break;
                case MotionEvent.ACTION_MOVE:
                    drawMove(event);
                    break;
            }
            return true;
        }

        private void drawDown(MotionEvent event) {
            mTouchedX = event.getX();
            mTouchedY = event.getY();
            drawRect(mTouchedX, mTouchedY, mClickPaint);
            isTouchDown = true;
        }

        private void drawMove(MotionEvent event) {
            if (isTouchDown) {
                for (int i = 0; i < event.getHistorySize(); i++) {
                    mPreTouchedX = mTouchedX;
                    mPreTouchedY = mTouchedY;
                    mTouchedX = event.getHistoricalX(i);
                    mTouchedY = event.getHistoricalY(i);
                    drawRect(mTouchedX, mTouchedY, mClickPaint);
                    drawLine(mPreTouchedX, mPreTouchedY, mTouchedX, mTouchedY);
                }
            }
        }

        private void drawUp(MotionEvent event) {
            if (isTouchDown) {
                mPreTouchedX = mTouchedX;
                mPreTouchedY = mTouchedY;
                mTouchedX = event.getX();
                mTouchedY = event.getY();
                if (mPreTouchedX == mTouchedX && mPreTouchedY == mTouchedY) {
                    drawPoint(mTouchedX, mTouchedY);
                }
                isTouchDown = false;
            }
        }

        private void drawLine(float preX, float preY, float x, float y) {
            int highX;
            int lowX;
            int highY;
            int lowY;
            mMatrixCanvas.drawLine(preX, preY, x, y, mLinePaint);
            if (preX >= x) {
                highX = (int) preX;
                lowX = (int) x;
            } else {
                highX = (int) x;
                lowX = (int) preX;
            }
            if (preY >= y) {
                highY = (int) preY;
                lowY = (int) y;
            } else {
                highY = (int) y;
                lowY = (int) preY;
            }
            invalidate(new Rect(lowX - 6, lowY - 6, highX + 6, highY + 6));
        }

        private void drawPoint(float x, float y) {
            mMatrixCanvas.drawPoint(x, y, mLinePaint);
            invalidate(new Rect(((int) x) - 6, ((int) y) - 6,
                    ((int) x) + 6, ((int) y) + 6));
        }

        private void initRect() {
            int ColX;
            int ColY;
            Paint mRectPaint = new Paint();
            Paint mRectPaintCross = new Paint();
            mRectPaint.setColor(Color.BLACK );
            mRectPaintCross.setColor(Color.BLACK );
            mRectPaintCross.setStyle(Style.STROKE);
            for (int i = 0; i <= HEIGHT_BASIS; i++) {
                ColY = (int) (mColHeight * i);
                mMatrixCanvas.drawLine(0, ColY, mScreenWidth, ColY, mRectPaint);
            }
            for (int j = 0; j <= WIDTH_BASIS; j++) {
                ColX = (int) (mColWidth * j);
                mMatrixCanvas.drawLine(ColX, 0, ColX, mScreenHeight, mRectPaint);
            }
            for (int i = 0; i < HEIGHT_BASIS; i++) {
                for (int j = 0; j < WIDTH_BASIS; j++) {
                    draw[i][j] = false;
                    click[i][j] = false;
                }
            }
            mMatrixCanvas.drawRect(
                    mColWidth + 1.0f,
                    mColHeight + 1.0f,
                    (mColWidth * (WIDTH_BASIS - 1)) - 1.0f,
                    (mColHeight * (HEIGHT_BASIS - 1)) - 1.0f, mEmptyPaint);

            for (int i = 0; i < HEIGHT_BASIS_CROSS; i++) {
                ColX = (int) (mWidthCross * (i + 2));
                ColY = (int) (mColHeight + (mHeightCross * i));
                mMatrixCanvas.drawRect(ColX, ColY, ColX + mColWidth,
                        ColY + (mColHeight / 2.0f), mRectPaintCross);
                drawCross[i] = false;
            }
            for (int i = 0; i < HEIGHT_BASIS_CROSS; i++) {
                ColX = (int) (mWidthCross * (i + 2));
                ColY = (int) (mScreenHeight - (mColHeight + (mHeightCross * (i + 1))));
                mMatrixCanvas.drawRect(ColX, ColY, ColX + mColWidth,
                        ColY + (mColHeight / 2.0f), mRectPaintCross);
                drawCross[HEIGHT_BASIS_CROSS + i] = false;
            }

            Paint helpPaint= new Paint();
            helpPaint.setColor(Color.RED );
            helpPaint.setTextAlign(Paint.Align.CENTER);
            helpPaint.setTextSize(50);
            mMatrixCanvas.drawText(getString(R.string.tp_test_exit),
                    (mColWidth * (WIDTH_BASIS / 2)) -1.0f,
                    (mColHeight * (HEIGHT_BASIS / 4)) - 1.0f, helpPaint);
        }

        private void drawRect(float x, float y, Paint paint) {
            float col_height = mScreenHeight / (float) HEIGHT_BASIS;
            float col_width = mScreenWidth / (float) WIDTH_BASIS;
            int countX = (int) (x / col_width);
            int countY = (int) (y / col_height);
            float ColX = col_width * countX;
            float ColY = col_height * countY;
            if (countY > HEIGHT_BASIS - 1 || countX > WIDTH_BASIS - 1 || countX < 0 || countY < 0) {
                Log.e(TAG, "drawRect" + "You are out of bounds!");
                return;
            }
            if (!draw[countY][countX]) {
                draw[countY][countX] = true;
                if (draw[countY][countX] && isDrawArea[countY][countX]) {
                    mMatrixCanvas.drawRect(
                            (float) (((int) ColX) + 1),
                            (float) (((int) ColY) + 1),
                            (float) ((int) (ColX + col_width)),
                            (float) ((int) (ColY + col_height)), paint);
                    invalidate(new Rect(
                            (int) (ColX - 1.0f),
                            (int) (ColY - 1.0f),
                            (int) ((ColX + col_width) + 1.0f),
                            (int) ((ColY + col_height) + 1.0f)));
                }
            }
            checkCrossRectRegion(x, y, countX, countY, paint);
            if (isPass() && isPassCross()) {
                startResultActivity(true);
            }
        }

        private void checkCrossRectRegion(float x, float y, int countX, int countY, Paint paint) {
            int countCrossY;
            int startRectTopX;
            int startRectBottomX;
            if (countY > 0 && countY < HEIGHT_BASIS - 1) {
                countCrossY = (int) ((y - mColHeight) / mHeightCross);
                int startRectTopY = (int) (mColHeight + (mHeightCross * countCrossY));
                startRectTopX = (int) (mWidthCross * (countCrossY + 2));
                startRectBottomX = (int) ((mScreenWidth - (mColWidth * 1.0f)) - (mWidthCross * (countCrossY + 2)));

                if (x > startRectTopX && x < startRectTopX + mColWidth) {
                    if (!drawCross[countCrossY]) {
                        mMatrixCanvas.drawRect(
                                startRectTopX + 1,
                                startRectTopY + 1,
                                (int) (startRectTopX + mColWidth),
                                (int) (startRectTopY + (mColHeight / 2.0f)), paint);
                        invalidate(new Rect(
                                startRectTopX - 1,
                                startRectTopY - 1,
                                (int) ((startRectTopX + mColWidth) + 1.0f),
                                (int) ((startRectTopY + mColHeight) + 1.0f)));
                    }
                    if (countCrossY >= 0) {
                        drawCross[countCrossY] = true;
                    }
                }
                if (x > startRectBottomX && x < startRectBottomX + mColWidth) {
                    if (!drawCross[((HEIGHT_BASIS_CROSS * 2) - 1) - countCrossY]) {
                        mMatrixCanvas.drawRect(
                                startRectBottomX + 1,
                                startRectTopY + 1,
                                (int) (startRectBottomX + mColWidth),
                                (int) (startRectTopY + (mColHeight / 2.0f)), paint);
                        invalidate(new Rect(
                                startRectBottomX - 1,
                                startRectTopY - 1,
                                (int) ((startRectBottomX + mColWidth) + 1.0f),
                                (int) ((startRectTopY + mColHeight) + 1.0f)));
                    }
                    if (countCrossY >= 0) {
                        drawCross[((HEIGHT_BASIS_CROSS * 2) - 1) - countCrossY] = true;
                    }
                }
            }
        }
    }
}
