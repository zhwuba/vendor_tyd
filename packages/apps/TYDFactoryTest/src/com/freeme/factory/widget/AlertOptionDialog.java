package com.freeme.factory.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.freeme.factory.R;

public class AlertOptionDialog {

    protected AlertOptionDialog() {
    }

    public static class Builder {
        private final AlertDialog.Builder B;

        private CharSequence mMessage;

        private CharSequence[] mItems;
        private boolean[] mCheckedItems;
        private OnMultiChoiceClickListener mOnCheckboxClickListener;

        public Builder(Context context) {
            B = new AlertDialog.Builder(context);
        }

        public Builder setTitle(int titleId) {
            B.setTitle(titleId);
            return this;
        }

        public Builder setTitle(CharSequence title) {
            B.setTitle(title);
            return this;
        }

        public Builder setMessage(int messageId) {
            mMessage = B.getContext().getText(messageId);
            return this;
        }

        public Builder setMessage(CharSequence message) {
            mMessage = message;
            return this;
        }

        public Builder setIcon(int iconId) {
            B.setIcon(iconId);
            return this;
        }

        public Builder setIcon(Drawable icon) {
            B.setIcon(icon);
            return this;
        }

        public Builder setIconAttribute(int attrId) {
            B.setIconAttribute(attrId);
            return this;
        }

        public Builder setPositiveButton(int textId, final OnClickListener listener) {
            B.setPositiveButton(textId, listener);
            return this;
        }

        public Builder setPositiveButton(CharSequence text, final OnClickListener listener) {
            B.setPositiveButton(text, listener);
            return this;
        }

        public Builder setNegativeButton(int textId, final OnClickListener listener) {
            B.setNegativeButton(textId, listener);
            return this;
        }

        public Builder setNegativeButton(CharSequence text, final OnClickListener listener) {
            B.setNegativeButton(text, listener);
            return this;
        }

        public Builder setNeutralButton(int textId, final OnClickListener listener) {
            B.setNeutralButton(textId, listener);
            return this;
        }

        public Builder setNeutralButton(CharSequence text, final OnClickListener listener) {
            B.setNeutralButton(text, listener);
            return this;
        }

        public Builder setCancelable(boolean cancelable) {
            B.setCancelable(cancelable);
            return this;
        }

        public Builder setOnCancelListener(OnCancelListener onCancelListener) {
            B.setOnCancelListener(onCancelListener);
            return this;
        }

        public Builder setOnDismissListener(OnDismissListener onDismissListener) {
            B.setOnDismissListener(onDismissListener);
            return this;
        }

        public Builder setOnKeyListener(OnKeyListener onKeyListener) {
            B.setOnKeyListener(onKeyListener);
            return this;
        }


        public Builder setMultiChoiceItems(int itemsId, boolean[] checkedItems,
                                           final OnMultiChoiceClickListener listener) {
            mItems = B.getContext().getResources().getTextArray(itemsId);
            mOnCheckboxClickListener = listener;
            mCheckedItems = checkedItems;
            //mIsMultiChoice = true;
            return this;
        }

        public Builder setMultiChoiceItems(CharSequence[] items, boolean[] checkedItems,
                                           final OnMultiChoiceClickListener listener) {
            mItems = items;
            mOnCheckboxClickListener = listener;
            mCheckedItems = checkedItems;
            //mIsMultiChoice = true;
            return this;
        }

        public AlertDialog create() {
            Context context = B.getContext();
            View custom = View.inflate(context, R.layout.alert_option_dialog, null);
            B.setView(custom);
            final AlertDialog dialog = B.create();

            TextView msgView = (TextView) custom.findViewById(android.R.id.message);
            msgView.setText(mMessage);

            final ListView listView = (ListView) custom.findViewById(android.R.id.list);
            ListAdapter adapter = new ArrayAdapter<CharSequence>(
                    context,
                    R.layout.alert_option_dialog_select_multichoice,
                    android.R.id.text1,
                    mItems) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);
                    if (mCheckedItems != null) {
                        boolean isItemChecked = mCheckedItems[position];
                        if (isItemChecked) {
                            listView.setItemChecked(position, true);
                        }
                    }
                    return view;
                }
            };
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    if (mCheckedItems != null) {
                        mCheckedItems[position] = listView.isItemChecked(position);
                    }
                    mOnCheckboxClickListener.onClick(
                            dialog, position, listView.isItemChecked(position));
                }
            });
            listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            listView.setAdapter(adapter);

            return dialog;
        }

        public AlertDialog show() {
            final AlertDialog dialog = create();
            dialog.show();
            return dialog;
        }
    }
}
