package com.freeme.factory.fingerprint;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Animatable2;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.internal.os.BackgroundThread;
import com.android.internal.widget.LockPatternUtils;
import com.freeme.factory.R;

/**
 * Enroll progress >= 50% -> Success
 * Enroll timeout
 */
public class FingerprintEnrollEnrolling extends Activity implements FingerprintEnrollSidecar.Listener {

    private static final String TAG_SIDECAR = "sidecar";

    private static final int PROGRESS_BAR_MAX = 10000;
    private static final int FINISH_DELAY = 250;

    /**
     * If we don't see progress during this time, we show an error message to remind the user that
     * he needs to lift the finger and touch again.
     */
    private static final int HINT_TIMEOUT_DURATION = 2500;

    private ProgressBar mProgressBar;
    private ImageView mFingerprintAnimator;
    private ObjectAnimator mProgressAnim;
    private TextView mStartMessage;
    private TextView mRepeatMessage;
    private TextView mErrorText;
    private Interpolator mFastOutSlowInInterpolator;
    private Interpolator mLinearOutSlowInInterpolator;
    private Interpolator mFastOutLinearInInterpolator;
    private FingerprintEnrollSidecar mSidecar;
    private boolean mAnimationCancelled;
    private AnimatedVectorDrawable mIconAnimationDrawable;
    private int mIndicatorBackgroundRestingColor;
    private int mIndicatorBackgroundActivatedColor;

    protected int mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fingerprint);

        mStartMessage = (TextView) findViewById(R.id.start_message);
        mRepeatMessage = (TextView) findViewById(R.id.repeat_message);
        mErrorText = (TextView) findViewById(R.id.error_text);
        mProgressBar = (ProgressBar) findViewById(R.id.fingerprint_progress_bar);
        mFingerprintAnimator = (ImageView) findViewById(R.id.fingerprint_animator);
        mIconAnimationDrawable = (AnimatedVectorDrawable) mFingerprintAnimator.getDrawable();
        mIconAnimationDrawable.registerAnimationCallback(mIconAnimationCallback);
        mFastOutSlowInInterpolator = AnimationUtils.loadInterpolator(
                this, android.R.interpolator.fast_out_slow_in);
        mLinearOutSlowInInterpolator = AnimationUtils.loadInterpolator(
                this, android.R.interpolator.linear_out_slow_in);
        mFastOutLinearInInterpolator = AnimationUtils.loadInterpolator(
                this, android.R.interpolator.fast_out_linear_in);
        mIndicatorBackgroundRestingColor
                = getColor(R.color.fingerprint_indicator_background_resting);
        mIndicatorBackgroundActivatedColor
                = getColor(R.color.fingerprint_indicator_background_activated);

        mUserId = getIntent().getIntExtra(Intent.EXTRA_USER_ID, UserHandle.myUserId());

        preEnroll();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSidecar = (FingerprintEnrollSidecar) getFragmentManager().findFragmentByTag(TAG_SIDECAR);
        if (mSidecar == null) {
            mSidecar = new FingerprintEnrollSidecar();
            getFragmentManager().beginTransaction().add(mSidecar, TAG_SIDECAR).commit();
        }
        mSidecar.setListener(this);
        updateProgress(false /* animate */);
        updateDescription();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSidecar.setListener(null);
        stopIconAnimation();

        postEnroll();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        //getWindow().addFlagsEx(WindowManager.LayoutParams.FLAG_EX_HOMEKEY_DISPATCHED);
    }

    @Override
    public void onEnterAnimationComplete() {
        super.onEnterAnimationComplete();
        mAnimationCancelled = false;
        startIconAnimation();
    }

    private void startIconAnimation() {
        mIconAnimationDrawable.start();
    }

    private void stopIconAnimation() {
        mAnimationCancelled = true;
        mIconAnimationDrawable.stop();
    }

    private final Animatable2.AnimationCallback mIconAnimationCallback =
            new Animatable2.AnimationCallback() {
                @Override
                public void onAnimationEnd(Drawable d) {
                    if (mAnimationCancelled) {
                        return;
                    }

                    // Start animation after it has ended.
                    mFingerprintAnimator.post(new Runnable() {
                        @Override
                        public void run() {
                            startIconAnimation();
                        }
                    });
                }
            };

    private void updateProgress(boolean animate) {
        int progress = getProgress(
                mSidecar.getEnrollmentSteps(), mSidecar.getEnrollmentRemaining());
        if (animate) {
            animateProgress(progress);
        } else {
            mProgressBar.setProgress(progress);
        }
    }

    private int getProgress(int steps, int remaining) {
        if (steps == -1) {
            return 0;
        }
        int progress = Math.max(0, steps + 1 - remaining);
        return PROGRESS_BAR_MAX * progress / (steps + 1);
    }

    private void animateProgress(int progress) {
        if (mProgressAnim != null) {
            mProgressAnim.cancel();
        }
        ObjectAnimator anim = ObjectAnimator.ofInt(mProgressBar, "progress",
                mProgressBar.getProgress(), progress);
        anim.addListener(mProgressAnimationListener);
        anim.setInterpolator(mFastOutSlowInInterpolator);
        anim.setDuration(250);
        anim.start();
        mProgressAnim = anim;
    }

    private void animateFlash() {
        ValueAnimator anim = ValueAnimator.ofArgb(mIndicatorBackgroundRestingColor,
                mIndicatorBackgroundActivatedColor);
        final ValueAnimator.AnimatorUpdateListener listener =
                new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        mFingerprintAnimator.setBackgroundTintList(ColorStateList.valueOf(
                                (Integer) animation.getAnimatedValue()));
                    }
                };
        anim.addUpdateListener(listener);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                ValueAnimator anim = ValueAnimator.ofArgb(mIndicatorBackgroundActivatedColor,
                        mIndicatorBackgroundRestingColor);
                anim.addUpdateListener(listener);
                anim.setDuration(300);
                anim.setInterpolator(mLinearOutSlowInInterpolator);
                anim.start();
            }
        });
        anim.setInterpolator(mFastOutSlowInInterpolator);
        anim.setDuration(300);
        anim.start();
    }

    // Give the user a chance to see progress completed before jumping to the next stage.
    private final Runnable mDelayedFinishRunnable = new Runnable() {
        @Override
        public void run() {
            finishWithResult(FingerprintTest.RESULT_FINISHED);
        }
    };

    private final Animator.AnimatorListener mProgressAnimationListener
            = new Animator.AnimatorListener() {

        @Override
        public void onAnimationStart(Animator animation) { }

        @Override
        public void onAnimationRepeat(Animator animation) { }

        @Override
        public void onAnimationEnd(Animator animation) {
            if (mProgressBar.getProgress() >= PROGRESS_BAR_PROGRESS_FOR_SUCCESS) {
                mProgressBar.postDelayed(mDelayedFinishRunnable, FINISH_DELAY);
            }
        }

        @Override
        public void onAnimationCancel(Animator animation) { }
    };

    private final Runnable mTouchAgainRunnable = new Runnable() {
        @Override
        public void run() {
            showError(getString(R.string.fingerprint_enroll_lift_touch_again));
        }
    };

    private void showError(CharSequence error) {
        mErrorText.setText(error);
        if (mErrorText.getVisibility() == View.INVISIBLE) {
            mErrorText.setVisibility(View.VISIBLE);
            mErrorText.setTranslationY(getResources().getDimensionPixelSize(
                    R.dimen.fingerprint_error_text_appear_distance));
            mErrorText.setAlpha(0f);
            mErrorText.animate()
                    .alpha(1f)
                    .translationY(0f)
                    .setDuration(200)
                    .setInterpolator(mLinearOutSlowInInterpolator)
                    .start();
        } else {
            mErrorText.animate().cancel();
            mErrorText.setAlpha(1f);
            mErrorText.setTranslationY(0f);
        }
    }

    private void clearError() {
        if (mErrorText.getVisibility() == View.VISIBLE) {
            mErrorText.animate()
                    .alpha(0f)
                    .translationY(getResources().getDimensionPixelSize(
                            R.dimen.fingerprint_error_text_disappear_distance))
                    .setDuration(100)
                    .setInterpolator(mFastOutLinearInInterpolator)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            mErrorText.setVisibility(View.INVISIBLE);
                        }
                    })
                    .start();
        }
    }

    private void updateDescription() {
        if (mSidecar.getEnrollmentSteps() == -1) {
            mStartMessage.setVisibility(View.VISIBLE);
            mRepeatMessage.setVisibility(View.INVISIBLE);
        } else {
            mStartMessage.setVisibility(View.INVISIBLE);
            mRepeatMessage.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onEnrollmentHelp(CharSequence helpString) {
        mErrorText.setText(helpString);
    }

    @Override
    public void onEnrollmentError(int errMsgId, CharSequence errString) {
        stopIconAnimation();
        mErrorText.removeCallbacks(mTouchAgainRunnable);
        mErrorText.removeCallbacks(mEnrollingNextTimeoutRunnable);

        finishWithResult(FingerprintTest.RESULT_SKIP);
    }

    @Override
    public void onEnrollmentProgressChange(int steps, int remaining) {
        updateProgress(true /* animate */);
        updateDescription();
        clearError();
        animateFlash();
        mErrorText.removeCallbacks(mTouchAgainRunnable);
        mErrorText.postDelayed(mTouchAgainRunnable, HINT_TIMEOUT_DURATION);

        if (mProgressBar.getProgress() > PROGRESS_BAR_PROGRESS_FOR_SUCCESS) {
            mErrorText.removeCallbacks(mEnrollingNextTimeoutRunnable);
            mErrorText.postDelayed(mEnrollingNextTimeoutRunnable, HINT_ENROLL_NEXT_TIMEOUT_DURATION);
        }
    }

    protected void finishWithResult(int result) {
        setResult(result);
        finish();
    }

    private static final int PROGRESS_BAR_PROGRESS_FOR_SUCCESS = PROGRESS_BAR_MAX / 10;
    private static final int HINT_ENROLL_NEXT_TIMEOUT_DURATION = 7000;
    private final Runnable mEnrollingNextTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            finishWithResult(FingerprintTest.RESULT_TIMEOUT);
        }
    };

    ////////////////////////////////////////////////////////////////////////////
    public static final String EXTRA_KEY_CHALLENGE_TOKEN = "hw_auth_token";
    private FingerprintManager mFingerprintManager;
    private byte[] mToken;

    private static final String DefaultPassword = "1111";
    private static final boolean FeatureLockPassword = true;//SystemProperties.getBoolean("ro.freeme.fingerprint_test_pwd", false);
    private LockPatternUtils mLockPatternUtils;

    private void preEnroll() {
        mFingerprintManager = getSystemService(FingerprintManager.class);
        if (mUserId != UserHandle.USER_NULL) {
            mFingerprintManager.setActiveUser(mUserId);
        }

        if (FeatureLockPassword) {
            mLockPatternUtils = new LockPatternUtils(this);
            mLockPatternUtils.setCredentialRequiredToDecrypt(false);
            mLockPatternUtils.saveLockPassword(DefaultPassword, null, DevicePolicyManager.PASSWORD_QUALITY_NUMERIC,
                    UserHandle.myUserId());
        }

        final long challenge = mFingerprintManager.preEnroll();

        if (FeatureLockPassword) {
            BackgroundThread.getHandler().post(new Runnable() {
                @Override
                public void run() {
                    if (mToken == null) {
                        try {
                            mToken = mLockPatternUtils.verifyPassword(DefaultPassword.getBytes(),
                                    challenge, UserHandle.myUserId());
                        } catch (LockPatternUtils.RequestThrottledException e) {
                            // do nothing
                        }
                    }
                    synchronized (DefaultPassword) {
                        DefaultPassword.notifyAll();
                    }
                }
            });
            synchronized (DefaultPassword) {
                try {
                    DefaultPassword.wait();
                } catch (InterruptedException e) {
                    // ignore
                }
            }
        }

        if (mToken == null) mToken = new byte[69];
        getIntent().putExtra(EXTRA_KEY_CHALLENGE_TOKEN, mToken);
        if (mUserId != UserHandle.USER_NULL) {
            getIntent().putExtra(Intent.EXTRA_USER_ID, mUserId);
        }
    }

    private void postEnroll() {
        if (FeatureLockPassword) {
            mLockPatternUtils.clearLock(DefaultPassword.getBytes()/* FIXME: issue with Android O ?*/, mUserId);
        }
    }
}
