package com.freeme.factory.fingerprint;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.Fingerprint;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.UserHandle;
import android.util.Log;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.R;

public class FingerprintTest extends BaseTest {
    private static final String TAG = "FingerprintTest";


    public static FingerprintManager getFingerprintManagerOrNull(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
            return context.getSystemService(FingerprintManager.class);
        } else {
            return null;
        }
    }

    public static boolean hasFingerprintHardware(Context context) {
        FingerprintManager fingerprintManager = getFingerprintManagerOrNull(context);
        return fingerprintManager != null && fingerprintManager.isHardwareDetected();
    }


    private static final int REQUEST_CODE_FINGERPRINT_ENROLL = 1;
    protected static final int RESULT_FINISHED  = RESULT_FIRST_USER;
    protected static final int RESULT_SKIP      = RESULT_FIRST_USER + 1;
    protected static final int RESULT_TIMEOUT   = RESULT_FIRST_USER + 2;

    private FingerprintManager mFingerprintManager;

    private static final int MSG_ENROLL_PREPARE = 1;
    private static final int MSG_ENROLL_START   = 2;
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_ENROLL_PREPARE: {
                    prepareFingerprint();
                    break;
                }
                case MSG_ENROLL_START: {
                    Intent intent = new Intent(FingerprintTest.this, FingerprintEnrollEnrolling.class);
                    startActivityForResult(intent, REQUEST_CODE_FINGERPRINT_ENROLL);
                    break;
                }
                default:
                    super.handleMessage(msg);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signal);

        testActionCompleted(false);

        mHandler.sendEmptyMessage(MSG_ENROLL_PREPARE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_FINGERPRINT_ENROLL:
                switch (resultCode) {
                    case RESULT_TIMEOUT:
                    case RESULT_FINISHED:
                        testActionCompleted(true);
                        if (FactoryTest.getTestMode() == 2 || FactoryTest.getTestMode() == 1) {
                            testResultCommit(true);
                        }
                        break;
                    case RESULT_CANCELED:
                    case RESULT_SKIP:
                    default:
                        testActionCompleted(false);
                        if (FactoryTest.getTestMode() == 2 || FactoryTest.getTestMode() == 1) {
                            testResultCommit(false);
                        }
                        break;
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void prepareFingerprint() {
        final int userId = UserHandle.myUserId();

        mFingerprintManager = getSystemService(FingerprintManager.class);
        mFingerprintManager.setActiveUser(userId);

        final int max = getResources().getInteger(
                com.android.internal.R.integer.config_fingerprintMaxTemplatesPerUser);

        if (mFingerprintManager.getEnrolledFingerprints(userId).size() >= max) {
            // Fuck Android N, Fuck Google !!!

            // For the purposes of M and N, groupId is the same as userId.
            final int groupId = userId;
            Fingerprint finger = new Fingerprint(null, groupId, 0, 0);
            mFingerprintManager.remove(finger, groupId,
                    new FingerprintManager.RemovalCallback() {
                        @Override
                        public void onRemovalSucceeded(Fingerprint fp, int remaining) {
                            //Log.v(TAG, "onRemovalSucceeded: "+ fp.getFingerId() + ", " + remaining);
                        }

                        @Override
                        public void onRemovalError(Fingerprint fp, int errMsgId, CharSequence errString) {
                            //Log.v(TAG, "onRemovalError: "+ fp.getFingerId() + ", " + errString);
                        }
                    });
            mHandler.sendEmptyMessageDelayed(MSG_ENROLL_START, 600);
        } else {
            mHandler.sendEmptyMessage(MSG_ENROLL_START);
        }
    }
}
