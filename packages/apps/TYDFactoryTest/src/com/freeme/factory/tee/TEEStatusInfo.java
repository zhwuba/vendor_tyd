package com.freeme.factory.tee;

import android.util.Log;
import android.os.Bundle;
import android.os.SystemProperties;
import com.freeme.factory.config.FeatureOption;
import android.content.Intent;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.FactoryTest;
import com.freeme.factory.config.TestConfig;

/*
    show TEE status info
*/

public class TEEStatusInfo extends BaseTest {

    private static final String TAG    = "TEEStatusInfo";

    private TextView teeState;
    private Button succesButton;
    private Button failButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tee);
        teeState = (TextView) this.findViewById(R.id.tee_state_id);

        testActionCompleted(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkTeeGoogleKeyStaus();
    }

    @Override
    protected void testActionCompleted(boolean completed) {
        super.testActionCompleted(completed);
        if (completed) {
            switch (FactoryTest.getTestMode()) {
                case TestConfig.TEST_MODE_ALL:
                case TestConfig.TEST_MODE_AUTO:
                    testResultCommit(true);
                    break;
            }
        }
    }

    private void checkTeeGoogleKeyStaus() {
        SetTEEStatusInfo();
        if (getTEEActiveStatus() && getTEEInitStatus() && getGoogleKeyStaus()) {
            testActionCompleted(true);
        } else {
            testActionCompleted(false);
        }
    }

    private boolean getTEEActiveStatus(){
        String activeInfo = "";
        String activeInfoEx = "";
        if (FeatureOption.FREEME_BEANPOD_TEE) {
            activeInfo = SystemProperties.get("soter.teei.vold.decrypt");//for 80O1
            activeInfoEx = SystemProperties.get("vendor.soter.teei.thh.init");
        } else if (FeatureOption.FREEME_TRUSTKERNEL_TEE) {
            activeInfoEx = SystemProperties.get("trustkernel.state");
        }
        Log.i(TAG, "activeInfo == " + activeInfo + ",activeInfoEx=="+activeInfoEx);

        if (activeInfoEx.equals("ACTIVE") || activeInfoEx.equals("good")) {
            return true;
        } else if (activeInfoEx.equals("UNACTIVE")) {
            return false;
        }

        if (activeInfo.equals("OK")) {
            return true;
        }
        return false;
    }

    private boolean getTEEInitStatus(){
        String teeOsInfo = "";
        if (FeatureOption.FREEME_BEANPOD_TEE) {
            teeOsInfo = SystemProperties.get("vendor.soter.teei.init");
        }
        Log.i(TAG, "teeOsInfo == " + teeOsInfo);
        if (teeOsInfo.equals("INIT_OK")) {
            return true;
        }
        return false;
    }

    private boolean getGoogleKeyStaus(){
        String activeStaus = "";
        if (FeatureOption.FREEME_BEANPOD_TEE) {
            activeStaus = SystemProperties.get("vendor.soter.teei.googlekey.status");
        } else if (FeatureOption.FREEME_TRUSTKERNEL_TEE) {
            activeStaus = SystemProperties.get("trustkernel.state");
        }
        Log.i(TAG, "activeStaus == " + activeStaus);
        if (activeStaus.equals("OK")) {
            return true;
        }
        return false;
    }

    private void SetTEEStatusInfo(){
        StringBuilder strings = new StringBuilder()
                .append(getText(R.string.tee_os_status))
                .append(" ")
                .append(SystemProperties.get("vendor.soter.teei.init"))
                .append("\n")
                .append(getText(R.string.tee_active_status))
                .append(" ")
                .append(SystemProperties.get("vendor.soter.teei.thh.init"))
                .append("\n")
                .append(getText(R.string.google_key_active_status))
                .append(" ")
                .append(SystemProperties.get("vendor.soter.teei.googlekey.status"))
                ;
        teeState.setText(strings.toString());
    }

}
