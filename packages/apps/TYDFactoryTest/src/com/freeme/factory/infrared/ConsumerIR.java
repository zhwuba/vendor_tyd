package com.freeme.factory.infrared;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.ConsumerIrManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.ProjectConfig;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class ConsumerIR extends BaseTest {
    private static final String TAG = "ConsumerIrTest";

    private TextView mFreqsText;

    private ConsumerIrManager mCIR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.consumer_ir);

        mCIR = (ConsumerIrManager) getSystemService(Context.CONSUMER_IR_SERVICE);

        findViewById(R.id.send_button).setOnClickListener(mSendClickListener);
        findViewById(R.id.get_freqs_button).setOnClickListener(mGetFreqsClickListener);
        mFreqsText = (TextView) findViewById(R.id.freqs_text);

        testActionCompleted(false);
    }

    private final View.OnClickListener mSendClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!mCIR.hasIrEmitter()) {
                Log.e(TAG, "No IR Emitter found\n");
                return;
            }

            // A pattern of alternating series of carrier on and off periods measured in
            // microseconds.
            int[] pattern = ProjectConfig.CO_PREDEF_CONSUMER_IR_PATTERN;

            // transmit the pattern at 38.4KHz
            mCIR.transmit(ProjectConfig.CO_PREDEF_CONSUMER_IR_FREQS, pattern);

            testActionCompleted(true);
        }
    };

    private final View.OnClickListener mGetFreqsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            StringBuilder b = new StringBuilder();

            if (!mCIR.hasIrEmitter()) {
                mFreqsText.setText("No IR Emitter found!");
                Log.e(TAG, "No IR Emitter found!\n");
                return;
            }

            // Get the available carrier frequency ranges
            ConsumerIrManager.CarrierFrequencyRange[] freqs = mCIR.getCarrierFrequencies();
            b.append("IR Carrier Frequencies:\n");
            for (ConsumerIrManager.CarrierFrequencyRange range : freqs) {
                b.append(String.format("    %d - %d\n", range.getMinFrequency(),
                            range.getMaxFrequency()));
            }
            mFreqsText.setText(b.toString());
        }
    };
}
