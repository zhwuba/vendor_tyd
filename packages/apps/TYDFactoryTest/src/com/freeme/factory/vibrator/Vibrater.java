package com.freeme.factory.vibrator;

import android.os.Bundle;
import android.os.Vibrator;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;

public class Vibrater extends BaseTest {

    private static final long[] VIBRATE_PATTERN = { 400, 800, 400, 800 }; // OFF/ON/OFF/ON...
    private Vibrator mVibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vibrator);

        mVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mVibrator != null) {
            mVibrator.vibrate(VIBRATE_PATTERN, 2);
        }
    }

    @Override
    protected void onPause() {
        if (mVibrator != null) {
            mVibrator.cancel();
        }

        super.onPause();
    }
}
