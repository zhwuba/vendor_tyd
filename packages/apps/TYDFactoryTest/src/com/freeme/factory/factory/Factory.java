package com.freeme.factory.factory;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class Factory {

    public static void resetPhone(Context context, boolean finallyShutdown, boolean eraseSdCard) {
        Intent intent = new Intent(Intent.ACTION_FACTORY_RESET);
        intent.setPackage("android");
        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        intent.putExtra(Intent.EXTRA_REASON, "FactoryMode");
        intent.putExtra("shutdown", finallyShutdown);
        intent.putExtra(Intent.EXTRA_WIPE_EXTERNAL_STORAGE, eraseSdCard);
        context.sendBroadcast(intent);
    }

    public static void broadcastMasterClearWithShutdown(Context context, boolean finallyShutdown) {
        Intent intent = new Intent(Intent.ACTION_MASTER_CLEAR);
        intent.setPackage("android");
        intent.putExtra("shutdown", finallyShutdown);
        context.sendBroadcast(intent);
    }
}
