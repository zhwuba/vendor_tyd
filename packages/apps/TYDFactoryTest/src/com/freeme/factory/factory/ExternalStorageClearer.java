package com.freeme.factory.factory;

import java.io.File;
import java.io.IOException;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.view.WindowManager;
import android.widget.Toast;

import com.freeme.factory.config.ProjectConfig;
import com.freeme.factory.utils.FileUtilEx;
import com.freeme.factory.utils.SLog;

import com.freeme.factory.R;

public class ExternalStorageClearer extends Service {
    static final String TAG = "ExternalStorageClearer";

    public static final String PARTIAL_CLEAR_ONLY
    				= "com.freeme.internal.storage.PARTIAL_CLEAR";
    public static final String PARTIAL_CLEAR_AND_FACTORY_RESET
    				= "com.freeme.internal.storage.PARTIAL_CLEAR_AND_FACTORY_RESET";

    public static final String EXTRA_ALWAYS_RESET = "always_reset";

	public static final ComponentName COMPONENT_NAME = new ComponentName(
			"com.freeme.factory",
			ExternalStorageClearer.class.getName());

    private PowerManager.WakeLock mWakeLock;

    private ProgressDialog mProgressDialog = null;

    private boolean mFactoryReset = false;
    private boolean mAlwaysReset = false;

    @Override
    public void onCreate() {
        super.onCreate();

        mWakeLock = ((PowerManager)getSystemService(Context.POWER_SERVICE))
                .newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "ExternalStorageClearer");
        mWakeLock.acquire();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (PARTIAL_CLEAR_AND_FACTORY_RESET.equals(intent.getAction())) {
            mFactoryReset = true;
        }
        if (intent.getBooleanExtra(EXTRA_ALWAYS_RESET, false)) {
            mAlwaysReset = true;
        }

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            updateProgressState();
            mProgressDialog.show();
        }

        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        mWakeLock.release();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    void fail(int msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        if (mAlwaysReset) {
        	Factory.broadcastMasterClearWithShutdown(this,
        			ProjectConfig.EN_PREDEF_RESET_DEVICE_SHUTDOWN);
        }
        stopSelf();
    }

    void updateProgressState() {
        String status = Environment.getExternalStorageState();
        //SLog.d("FactoryReset : " + mFactoryReset + ", AlwaysReset : " + mAlwaysReset);
        //SLog.d("State of external storage is " + status);
        if (Environment.MEDIA_MOUNTED.equals(status)) {
            updateProgressDialog(R.string.progress_erasing);
            final String extStoragePath = Environment.getExternalStorageDirectory().toString();
            //SLog.d("Path of external storage is " + extStoragePath);
            {
	            new Thread() {
	                @Override
	                public void run() {
	                    boolean success = false;
	                    try {
	                    	clearDirsAndFiles(extStoragePath);
	                        success = true;
	                    } catch (Exception e) {
	                        Toast.makeText(ExternalStorageClearer.this,
	                        		R.string.format_error, Toast.LENGTH_LONG).show();
	                    }
	                    if (success) {
	                        if (mFactoryReset) {
	                        	Factory.broadcastMasterClearWithShutdown(ExternalStorageClearer.this,
	                        			ProjectConfig.EN_PREDEF_RESET_DEVICE_SHUTDOWN);
	                            // Intent handling is asynchronous -- assume it will happen soon.
	                            stopSelf();
	                            return;
	                        }
	                    }
	                    // If we didn't succeed, or aren't doing a full factory
	                    // reset, then it is time to remount the storage.
	                    if (!success && mAlwaysReset) {
	                    	Factory.broadcastMasterClearWithShutdown(ExternalStorageClearer.this,
	                    			ProjectConfig.EN_PREDEF_RESET_DEVICE_SHUTDOWN);
	                    } else {
	                        // Stop current task of clearing and resetting.
	                    	SLog.w("Fail to clear external storage and exit.");
	                    }
	                    stopSelf();
	                    return;
	                }
	            }.start();
            }
        } else if (Environment.MEDIA_NOFS.equals(status)
                || Environment.MEDIA_UNMOUNTED.equals(status)
                || Environment.MEDIA_UNMOUNTABLE.equals(status)) {
        	fail(R.string.progress_unmounting);
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(status)) {
        	fail(R.string.media_bad_removal);
        } else if (Environment.MEDIA_BAD_REMOVAL.equals(status)) {
            fail(R.string.media_bad_removal);
        } else if (Environment.MEDIA_CHECKING.equals(status)) {
            fail(R.string.media_checking);
        } else if (Environment.MEDIA_REMOVED.equals(status)) {
            fail(R.string.media_removed);
        } else if (Environment.MEDIA_SHARED.equals(status)) {
            fail(R.string.media_shared);
        } else {
            fail(R.string.media_unknown_state);
            SLog.w("Unknown storage state: " + status);
            stopSelf();
        }
    }

    public void updateProgressDialog(int msg) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            mProgressDialog.show();
        }

        mProgressDialog.setMessage(getText(msg));
    }
    
    private void clearDirsAndFiles(final String extStoragePath) {
    	final String[] files = ProjectConfig.CO_PREDEF_RESET_DEVICE_STORAGE_FILES;
    	final int N = files.length;
    	for (int i = 0; i < N; i++) {
    		String file = files[i];
    		try {
				FileUtilEx.delete(new File(extStoragePath, file));
			} catch (IOException e) {
				SLog.w("Clear current item failed, try next...", e);
			}
    	}
    }
}
