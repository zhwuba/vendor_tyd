package com.freeme.factory.memory;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.os.Bundle;
import android.text.format.Formatter;
import android.widget.TextView;

import com.freeme.factory.FactoryTest;
import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.config.TestConfig;

public class Memory extends BaseTest {

    private TextView mTxtInfo;

    private ActivityManager mActivityManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.memory);

        mActivityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        mTxtInfo = (TextView) findViewById(R.id.comm_info);
        testActionCompleted(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkDeviceMemory();
    }

    @Override
    protected void testActionCompleted(boolean completed) {
        super.testActionCompleted(completed);
        if (completed) {
            switch (FactoryTest.getTestMode()) {
                case TestConfig.TEST_MODE_ALL:
                case TestConfig.TEST_MODE_AUTO:
                    testResultCommit(true);
                    break;
            }
        }
    }

    private void checkDeviceMemory() {
        MemoryInfo memInfo = new MemoryInfo();
        mActivityManager.getMemoryInfo(memInfo);

        final long availMem = memInfo.availMem;
        final long totalMem = memInfo.totalMem;
        final Formatter.BytesResult totalMemRes
                = Formatter.formatBytes(getResources(), totalMem, Formatter.FLAG_IEC_UNITS);
        final Formatter.BytesResult availMemRes
                = Formatter.formatBytes(getResources(), availMem, Formatter.FLAG_IEC_UNITS);

        StringBuilder strings = new StringBuilder()
                .append(getText(R.string.memorytotal))
                .append(getString(com.android.internal.R.string.fileSizeSuffix,
                        totalMemRes.value, totalMemRes.units))
                .append("\n")
                .append(getText(R.string.memoryfree))
                .append(getString(com.android.internal.R.string.fileSizeSuffix,
                        availMemRes.value, availMemRes.units))
                ;
        mTxtInfo.setText(strings.toString());

        if (totalMem > 0 && (totalMem - availMem > 0)) {
            testActionCompleted(true);
        }
    }
}