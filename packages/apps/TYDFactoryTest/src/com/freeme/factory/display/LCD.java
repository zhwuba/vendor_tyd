package com.freeme.factory.display;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.freeme.factory.base.BaseTest;
import com.freeme.factory.R;

public class LCD extends BaseTest {

    private View mContent;
    private View mBottomBar;

    private static class Cases {
        private static final int[] COLOR_CASES = {
                Color.RED,
                Color.GREEN,
                Color.BLUE,
                Color.WHITE,
                Color.BLACK,
        };
        private static final int INTERVAL = 0; // 0s

        private int mIndex;
        private long mLastTimestamp;

        private boolean hasNext() {
            long now = System.currentTimeMillis();
            return (mIndex < COLOR_CASES.length)
                || (now - mLastTimestamp <= INTERVAL); // Last case need enough time
        }

        private void next(View holder) {
            long now = System.currentTimeMillis();
            if (now - mLastTimestamp > INTERVAL) {
                holder.setBackgroundColor(COLOR_CASES[mIndex++]);
                mLastTimestamp = now;
            }
        }
    }
    private final Cases mCases = new Cases();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lcd);

        mContent = findViewById(R.id.main_content);
        mBottomBar = findViewById(R.id.bottom_bar);
        testActionCompleted(false);

        nextCase();
    }

    @Override
    protected void testActionCompleted(boolean completed) {
        if (completed) {
            getActionBar().show();
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            mBottomBar.setVisibility(View.VISIBLE);
        } else {
            getActionBar().hide();
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            mBottomBar.setVisibility(View.GONE);
        }
        super.testActionCompleted(completed);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                nextCase();
                break;
        }
        return super.onTouchEvent(event);
    }

    private void nextCase() {
        if (mCases.hasNext()) {
            mCases.next(mContent);
        } else {
            testActionCompleted(true);
        }
    }
}
