package com.freeme.factory.display;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.freeme.factory.R;
import com.freeme.factory.base.BaseTest;

public class MHL extends BaseTest implements View.OnClickListener {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mhl);

        Button MHLPlayButtun = (Button) findViewById(R.id.MHL_play);
        MHLPlayButtun.setOnClickListener(this);

        testActionCompleted(false);
    }

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		Uri data = Uri.parse("/system/vendor/media/video/AgingVideo.mp4");
		intent.setDataAndType(data, "video/mp4");
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK
				| Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

		try {
			startActivityForResult(intent, 1);

			testActionCompleted(true);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(getApplicationContext(), "Can't find media player to play!", Toast.LENGTH_SHORT).show();
		}
	}
}
