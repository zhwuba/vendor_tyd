package com.freeme.factory;

import java.util.Arrays;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.freeme.factory.audio.Earphone;
import com.freeme.factory.audio.HeadSet;
import com.freeme.factory.audio.HiFi;
import com.freeme.factory.audio.Mic;
import com.freeme.factory.audio.Speaker;
import com.freeme.factory.audio.SubMic;
import com.freeme.factory.base.BaseTest;
import com.freeme.factory.bluetooth.Bluetooth;
import com.freeme.factory.camera.CameraTest;
import com.freeme.factory.camera.StrobeTest;
import com.freeme.factory.camera.SubCamera;
import com.freeme.factory.camera.BackViceCamera;
import com.freeme.factory.config.ProjectConfig;
import com.freeme.factory.config.TestConfig;
import com.freeme.factory.config.TestResult;
import com.freeme.factory.display.LCD;
import com.freeme.factory.display.MHL;
import com.freeme.factory.factory.Factory;
import com.freeme.factory.fingerprint.FingerprintTest;
import com.freeme.factory.fmradio.FMRadio;
import com.freeme.factory.gps.GPS;
import com.freeme.factory.info.HardwareInfo;
import com.freeme.factory.info.SoftwareInfo;
import com.freeme.factory.infrared.ConsumerIR;
import com.freeme.factory.input.KeyboardTest;
import com.freeme.factory.input.TouchTest;
import com.freeme.factory.light.AttentionLight;
import com.freeme.factory.light.KeyboardLight;
import com.freeme.factory.light.ScreenBrightness;
import com.freeme.factory.memory.Memory;
import com.freeme.factory.power.BatteryInfo;
import com.freeme.factory.sensor.GSensor;
import com.freeme.factory.sensor.PressureSensor;
import com.freeme.factory.sensor.StepSensor;
import com.freeme.factory.sensor.GyroscopeSensor;
import com.freeme.factory.sensor.HSensor;
import com.freeme.factory.sensor.LSensor;
import com.freeme.factory.sensor.MSensor;
import com.freeme.factory.sensor.PSensor;
import com.freeme.factory.storage.Storage;
import com.freeme.factory.telephony.Phone;
import com.freeme.factory.telephony.Sim;
import com.freeme.factory.usb.Otg;
import com.freeme.factory.nfc.Nfc;
import com.freeme.factory.vibrator.Vibrater;
import com.freeme.factory.widget.AlertOptionDialog;
import com.freeme.factory.wifi.WiFiTest;
import com.freeme.factory.tee.TEEStatusInfo;

public class FactoryTest extends Activity implements View.OnClickListener,
        AdapterView.OnItemClickListener {
    private static final String TAG = "FactoryMode";

    private static int sTestMode = TestConfig.TEST_MODE_MANUAL;
    private static int sTestItem = TestConfig.TEST_ITEM_START;

    public static int getTestMode() {
        return sTestMode;
    }
    public static void setTestMode(int mode) {
        sTestMode = mode;
    }
    public static void setTestItem(int item) {
        sTestItem = item;
    }


    ///////////////////////////////////////////////////////////////////////////

    private PowerManager.WakeLock mWakeLock;

    private Button   mBtnTestALL;
    private Button   mBtnTestAUTO;
    private GridView mTestGridView;
    private ArrayAdapter<String> mTestAdapter;

    private Button   mBtnExtHwInfo;
    private Button   mBtnExtSwInfo;
    private Button   mBtnExtDevReset;

    private int[] mTestItemsALL;
    private int[] mTestItemsAUTO;

    private TestResult mTestResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.main);

        mTestResult = TestResult.getInstance();
        mTestItemsALL  = TestConfig.getInstance().getTestItems(TestConfig.TEST_MODE_ALL);
        mTestItemsAUTO = TestConfig.getInstance().getTestItems(TestConfig.TEST_MODE_AUTO);

        mBtnTestALL  = (Button) findViewById(R.id.main_bt_alltest);
        mBtnTestALL.setOnClickListener(this);

        mBtnTestAUTO = (Button) findViewById(R.id.main_bt_autotest);
        mBtnTestAUTO.setOnClickListener(this);

        mBtnExtHwInfo = (Button) findViewById(R.id.main_foot_hw_info);
        if (HardwareInfo.exists()) {
            mBtnExtHwInfo.setOnClickListener(this);
        } else {
            mBtnExtHwInfo.setVisibility(View.GONE);
        }

        mBtnExtSwInfo = (Button) findViewById(R.id.main_foot_info);
        mBtnExtSwInfo.setOnClickListener(this);

        mBtnExtDevReset = (Button) findViewById(R.id.main_foot_reset);
        mBtnExtDevReset.setOnClickListener(this);

        final String[] testItems = new String[mTestItemsALL.length];
        for (int i = 0; i < mTestItemsALL.length; i++) {
            testItems[i] = TestConfig.getNameByItemId(this, mTestItemsALL[i]);
        }
        mTestAdapter = new TestArrayAdapter<>(this, R.layout.main_grid_item,
                testItems, mTestItemsALL, mTestResult);

        mTestGridView = (GridView) findViewById(R.id.main_grid);
        mTestGridView.setOnItemClickListener(this);
        mTestGridView.setAdapter(mTestAdapter);

        initAteTestResult();
        initCoupleTestResult();

        sendBroadcast(new Intent("com.android.music.musicservicecommand.stop"));
        sendBroadcast(new Intent("com.android.soundRecorder.command.stop"));
    }

    @Override
    protected void onStart() {
        super.onStart();
        acquireWakeLock();
    }

    @Override
    protected void onResume() {
        super.onResume();

        initHwTestResult();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        sTestMode = 0;
        sTestItem = 0;
        // FIXME relase lock in onDestroy will keep Screen ON even if app in background!
        releaseWakeLock();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_bt_alltest: {
                new AlertDialog.Builder(this)
                    .setTitle(R.string.AllTest)
                    .setMessage(R.string.ISAllTest)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FactoryTest.setTestMode(TestConfig.TEST_MODE_ALL);
                            sTestItem = 0;
                            testItem(mTestItemsALL[sTestItem]);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .show();
            } break;

            case R.id.main_bt_autotest: {
                new AlertDialog.Builder(this)
                    .setTitle(R.string.AutoTest)
                    .setMessage(R.string.ISAutoTest)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FactoryTest.setTestMode(TestConfig.TEST_MODE_AUTO);
                            sTestItem = 0;
                            testItem(mTestItemsAUTO[sTestItem]);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .show();
            } break;

            case R.id.main_foot_hw_info: {
                Intent intent = new Intent();
                intent.setClass(this, HardwareInfo.class);
                startActivity(intent);
            } break;

            case R.id.main_foot_info: {
                Intent intent = new Intent();
                intent.setClass(this, SoftwareInfo.class);
                startActivity(intent);
            } break;

            case R.id.main_foot_reset: {
                final boolean[] checkedItems = {
                        ProjectConfig.EN_PREDEF_RESET_DEVICE_STORAGE,  // 0
                };
                new AlertOptionDialog.Builder(this)
                    .setTitle(R.string.label_device_reset)
                    .setMessage(R.string.msg_device_reset_confirm)
                    .setMultiChoiceItems(R.array.phone_reset_options, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                            // nothing
                        }
                    })
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Factory.resetPhone(getApplicationContext(),
                                    ProjectConfig.EN_PREDEF_RESET_DEVICE_SHUTDOWN,
                                    checkedItems[0]);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .show();
            } break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (FactoryTest.getTestMode()) {
            case TestConfig.TEST_MODE_AUTO:
            case TestConfig.TEST_MODE_ALL:
                break;
            default:
                testItem(mTestItemsALL[position]);
                break;
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            final int testMode = FactoryTest.getTestMode();

            switch (testMode) {
            case TestConfig.TEST_MODE_AUTO: {
                sTestItem++;
                if (sTestItem >= mTestItemsAUTO.length) {
                    Intent intent = new Intent();
                    intent.setClass(FactoryTest.this, FactoryReport.class);
                    startActivity(intent);

                    mTestAdapter.notifyDataSetChanged();
                } else {
                    testItem(mTestItemsAUTO[sTestItem]);
                }
            } break;

            case TestConfig.TEST_MODE_ALL: {
                sTestItem++;
                if (sTestItem >= mTestItemsALL.length) {
                    Intent intent = new Intent();
                    intent.setClass(FactoryTest.this, FactoryReport.class);
                    startActivity(intent);

                    mTestAdapter.notifyDataSetChanged();
                } else {
                    testItem(mTestItemsALL[sTestItem]);
                }
            } break;

            default:
                break;
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        final int testMode = FactoryTest.getTestMode();
        final int testItemId = requestCode;

        // Record result
        Log.v(TAG, "TestItemId -> " + testItemId);
        mTestResult.put(testItemId, resultCode == RESULT_OK ?
                TestResult.RESULT_OK : TestResult.RESULT_FAIL);
        mTestAdapter.notifyDataSetChanged();

        // Do next
        Log.v(TAG, "TestMode -> " + testMode);
        switch (testMode) {
            case  TestConfig.TEST_MODE_ALL:
            case  TestConfig.TEST_MODE_AUTO:
                mHandler.sendEmptyMessageDelayed(-1/*not care*/, 100);
                break;
            default:
                // do nothing
                break;
        }
    }

    public void testItem(int item) {
        Log.d(TAG, "Testing item : " + item + "<-> sItem : " + sTestItem);

        Intent intent = new Intent();
        intent.putExtra(BaseTest.EXTRA_TEST_MODE, sTestMode);

        switch (item) {
        case TestConfig.TEST_ITEM_TOUCH:
            intent.setClass(this, TouchTest.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_LCD:
            intent.setClass(this, LCD.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_GPS:
            intent.setClass(this, GPS.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_POWER:
            intent.setClass(this, BatteryInfo.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_KEY:
            intent.setClass(this, KeyboardTest.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_AUDIO_SPEAKER:
        case TestConfig.TEST_ITEM_AUDIO_SPEAKER_LEFT:
        case TestConfig.TEST_ITEM_AUDIO_SPEAKER_RIGHT:
            intent.setClass(this, Speaker.class);
            intent.putExtra(BaseTest.KEY_PURPOSE, item);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_HEADSET:
            intent.setClass(this, HeadSet.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_MIC:
            intent.setClass(this, Mic.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_RECEIVER:
            intent.setClass(this, Earphone.class);
            startActivityForResult(intent, item);
            break;

        case TestConfig.TEST_ITEM_WIFI:
            intent.setClass(this, WiFiTest.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_BT:
            intent.setClass(this, Bluetooth.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_SHAKE:
            intent.setClass(this, Vibrater.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_CALL:
            intent.setClass(this, Phone.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_BL:
            intent.setClass(this, ScreenBrightness.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_MEMORY:
            intent.setClass(this, Memory.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_GSENSOR:
            intent.setClass(this, GSensor.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_MSENSOR:
            intent.setClass(this, MSensor.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_LSENSOR:
            intent.setClass(this, LSensor.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_DSENSOR:
            intent.setClass(this, PSensor.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_STORAGE:
            intent.setClass(this, Storage.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_CAMERA_BACK:
            intent.setClass(this, CameraTest.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_CAMERA_FRONT:
            intent.setClass(this, SubCamera.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_FM:
            intent.setClass(this, FMRadio.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_SIM:
            intent.setClass(this, Sim.class);

            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_LED:
            intent.setClass(this, KeyboardLight.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_GYROSCOPE:
            intent.setClass(this, GyroscopeSensor.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_STEPSENSOR:
            intent.setClass(this, StepSensor.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_MHL:
            intent.setClass(this, MHL.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_LIGHT_ATTENTION:
            intent.setClass(this, AttentionLight.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_HSENSOR:
            intent.setClass(this, HSensor.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_HIFI:
            intent.setClass(this, HiFi.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_SUBMIC:
            intent.setClass(this, SubMic.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_INFRARED:
            intent.setClass(this, ConsumerIR.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_STROBE_BACK:
        case TestConfig.TEST_ITEM_STROBE_FORE:
            intent.setClass(this, StrobeTest.class);
            intent.putExtra(BaseTest.KEY_PURPOSE, item);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_FINGERPRINT:
            intent.setClass(this, FingerprintTest.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_USB_OTG:
            intent.setClass(this, Otg.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_CAMERA_BACK_VICE:
            intent.setClass(this, BackViceCamera.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_NFC:
            intent.setClass(this, Nfc.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_TEE:
            intent.setClass(this, TEEStatusInfo.class);
            startActivityForResult(intent, item);
            break;
        case TestConfig.TEST_ITEM_PRESSURE_SENSOR:
            intent.setClass(this, PressureSensor.class);
            startActivityForResult(intent, item);
        default:
            break;
        }
    }

    @SuppressWarnings("deprecation")
    private void acquireWakeLock() {
        if (mWakeLock == null) {
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, getClass().getSimpleName());
            mWakeLock.acquire();
        }
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void releaseWakeLock() {
        if (mWakeLock != null && mWakeLock.isHeld()) {
            mWakeLock.release();
            mWakeLock = null;
        }
    }

    private static class TestArrayAdapter<T> extends ArrayAdapter<T> {
        private final int[] mAll;
        private final TestResult mResults;

        TestArrayAdapter(Context context, int resource,
                T[] objects, int[] all, TestResult results) {
            super(context, resource, 0, Arrays.asList(objects));
            mAll = all;
            mResults = results;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView text;
            if (view instanceof TextView) {
                text = (TextView) view;
            } else {
                text = (TextView) view.findViewById(android.R.id.text1);
            }
            switch (mResults.get(mAll[position])) {
                case TestResult.RESULT_OK:
                    text.setBackgroundResource(R.drawable.btn_default_sucess);
                    break;
                case TestResult.RESULT_FAIL:
                    text.setBackgroundResource(R.drawable.btn_default_fail);
                    break;
            }
            return view;
        }
    }

    private static final String PROP_CALIBRATE_RESULT = SystemProperties.get("vendor.gsm.serial");

    private static final int MAX_LENGTH = 62;

    private void initAteTestResult() {
        int resultStr = R.string.ATE_no_test;
        if (!TextUtils.isEmpty(PROP_CALIBRATE_RESULT)) {
            String res = "";
            if (PROP_CALIBRATE_RESULT.length() >= MAX_LENGTH){
                res = PROP_CALIBRATE_RESULT.substring(60,MAX_LENGTH); // 60,61 is calibrate test result
            }

            resultStr = res.equals("10") ? R.string.ATE_Pass : R.string.ATE_Fail;
        }
        TextView ateInfo = (TextView) findViewById(R.id.ate_test_info);
        ateInfo.setText(resultStr);
    }

    private void initCoupleTestResult() {
        int resultStr = R.string.factory_couple_test_result_NoTest;
        if (!TextUtils.isEmpty(PROP_CALIBRATE_RESULT)) {
            char res = 'N';
            if (PROP_CALIBRATE_RESULT.length() >= MAX_LENGTH){
                res = PROP_CALIBRATE_RESULT.charAt(59);// 59 is couple test result
            }

            switch (res) {
                case 'P' :
                    resultStr = R.string.factory_couple_test_result_Pass;
                    break;
                case 'F':
                    resultStr = R.string.factory_couple_test_result_Fail;
                    break;
                default:
                    break;
            }
        }

        TextView coupleInfo = (TextView) findViewById(R.id.couple_test_info);
        coupleInfo.setText(resultStr);
    }

    private void initHwTestResult() {
        final int textId;
        switch (TestResult.getInstance().get()) {
            case TestResult.RESULT_OK:
                textId = R.string.factory_test_result_Pass;
                break;
            case TestResult.RESULT_FAIL:
                textId = R.string.factory_test_result_Fail;
                break;
            default:
                textId = R.string.factory_test_result_NoTest;
                break;
        }
        TextView testResult = (TextView) findViewById(R.id.factory_test_result);
        testResult.setText(textId);
    }
}
