LOCAL_PATH:= $(call my-dir)

#ifeq ($(FREEME_PRODUCT_CHIPVENDOR),mtk)

########################
include $(CLEAR_VARS)

LOCAL_PACKAGE_NAME := TYDFactoryTest

LOCAL_MODULE_TAGS := optional

LOCAL_JAVA_LIBRARIES := telephony-common

#LOCAL_STATIC_JAVA_LIBRARIES := \
#    vendor.freeme.hardware.soul-V1.0-java \
#    vendor.mediatek.hardware.nvram-V1.0-java

LOCAL_SRC_FILES := $(call all-subdir-java-files)

LOCAL_PRIVATE_PLATFORM_APIS := true

LOCAL_CERTIFICATE := platform

LOCAL_REQUIRED_MODULES := AgingVideo.mp4

# If this is an unbundled build (to install seprately) then include
# the libraries in the APK, otherwise just put them in /system/lib and
# leave them out of the APK
ifneq (,$(TARGET_BUILD_APPS))
  LOCAL_JNI_SHARED_LIBRARIES += libjni_factory_speaker
else
  LOCAL_REQUIRED_MODULES += libjni_factory_speaker
endif

include $(BUILD_PACKAGE)

########################
include $(CLEAR_VARS)
LOCAL_MODULE := AgingVideo.mp4
LOCAL_SRC_FILES := assets/$(LOCAL_MODULE)
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_PATH := $(TARGET_OUT)/media/video
include $(BUILD_PREBUILT)

########################
include $(call all-makefiles-under, $(LOCAL_PATH))

########################
#endif
