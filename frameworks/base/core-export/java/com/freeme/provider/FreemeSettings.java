package com.freeme.provider;

import android.content.ContentResolver;
import android.os.UserHandle;
import android.provider.Settings;
import android.net.Uri;

import com.freeme.util.FreemeOption;

/** @hide */
public class FreemeSettings {

    public static final class System {
        //*/ freeme.zhiwei.zhang, 20170531. common util.
        public static final int FREEME_BOOLBIT_MASTER_ENABLE = 1 << 24;
        public static final int FREEME_BOOLBIT_BLANK_BIT_0 = 1 << 25;
        public static final boolean getBoolbit(
                ContentResolver cr, String name, int nick, boolean def) {
            try {
                final int value = Settings.System.getIntForUser(cr, name, UserHandle.USER_CURRENT_OR_SELF);
                if (nick == FREEME_BOOLBIT_MASTER_ENABLE || nick >= FREEME_BOOLBIT_BLANK_BIT_0) {
                    return (value & nick) != 0;
                } else if ((nick & FREEME_BOOLBIT_MASTER_ENABLE) != 0) {
                    return (value & (nick & ~FREEME_BOOLBIT_MASTER_ENABLE)) != 0;
                } else {
                    return (value & (nick | FREEME_BOOLBIT_MASTER_ENABLE)) ==
                            (nick | FREEME_BOOLBIT_MASTER_ENABLE);
                }
            } catch (Settings.SettingNotFoundException e) {
                return def;
            }
        }
        public static final synchronized boolean putBoolbit(
                ContentResolver cr, String name, int nick, boolean value) {
            int stored = Settings.System.getIntForUser(cr, name, 0, UserHandle.USER_CURRENT_OR_SELF);
            stored = value ? (stored | nick) : (stored & ~nick);
            return Settings.System.putIntForUser(cr, name, stored, UserHandle.USER_CURRENT_OR_SELF);
        }
        //*/

        //*/ freeme.huzhongtao, 20170324. for default date format
        public static final String FREEME_DEFAULT_DATE_FORMAT = "freeme_def_date_format";
        //*/

        //*/ freeme.chenxinsi, 20180131. HongbaoAssistant
        public static final String FREEME_HONGBAO_ENABLED = "hongbao_setting_enabled";
        public static final String FREEME_HONGBAO_LAUNCH_APP = "hongbao_setting_launch_app";
        public static final String FREEME_HONGBAO_NOTIFICATION_SOUND = "hongbao_setting_notification_sound";
        public static final String FREEME_HONGBAO_NOTIFICATION_SOUND_INDEX = "hongbao_setting_notification_sound_index";
        //*/

        //*/ freeme.zhiwei.zhang, 20180117. Navigation - All.
        public static final String FREEME_NAVIGATION_BAR_MODE = "navigationbar_mode";
        public static final String FREEME_NAVIGATION_BAR_COLLAPSABLE = "navigationbar_collapsable";
        public static final String FREEME_NAVIGATION_BAR_COLLAPSED = "navigationbar_collapsed";
        public static final String FREEME_NAVIGATION_BAR_STYLE = "navigationbar_style";
        public static final String FREEME_NAVIGATION_GESTURE_GUIDE_BAR = "navigation_gesture_guide_bar";
        //*/

        //*/ freeme.biantao, 20161230. Fingerprint Custom 1.
        public static final String FREEME_FP_CAPTURE_ENABLE = "freeme_fp_capture_enable";
        public static final String FREEME_FP_HOME_CONTROL = "freeme_fp_home_control";
        public static final String FREEME_FP_VIDEO_CONTROL = "freeme_fp_video_control";
        public static final String FREEME_FP_MUSIC_CONTROL = "freeme_fp_music_control";
        public static final String FREEME_FP_DIAL_CONTROL = "freeme_fp_dial_control";
        public static final String FREEME_FP_ALARM_CONTROL = "freeme_fp_alarm_control";
        //*/
        //*/ freeme.chenming, 20170301. Fingerprint Custom 2.
        public static final String FREEME_FP_SELECT_APP = "freeme_fp_select_app";

        //*/ freeme.chenming, 20170113. dual sim ringtone
        public static final String FREEME_RINGTONE_MESSAGE            = "message";
        public static final String FREEME_RINGTONE_MESSAGE_SIM2       = "message2";
        public static final String FREEME_RINGTONE_PHONE_SIM2         = "ringtone2";

        public static final String FREEME_RINGTONE_MESSAGE_CACHE      = "message_cache";
        public static final String FREEME_RINGTONE_MESSAGE_SIM2_CACHE = "message2_cache";
        public static final String FREEME_RINGTONE_PHONE_SIM2_CACHE   = "ringtone2_cache";

        public static final Uri FREEME_DEFAULT_RINGTONE_MESSAGE_URI =
                Settings.System.getUriFor(FREEME_RINGTONE_MESSAGE);
        public static final Uri FREEME_DEFAULT_RINGTONE_PHONE_SIM2_URI =
                Settings.System.getUriFor(FREEME_RINGTONE_PHONE_SIM2);
        public static final Uri FREEME_DEFAULT_RINGTONE_MESSAGE_SIM2_URI =
                Settings.System.getUriFor(FREEME_RINGTONE_MESSAGE_SIM2);

        public static final Uri FREEME_DEFAULT_RINGTONE_MESSAGE_CACHE_URI =
                Settings.System.getUriFor(FREEME_RINGTONE_MESSAGE_CACHE);
        public static final Uri FREEME_DEFAULT_RINGTONE_PHONE_SIM2_CACHE_URI =
                Settings.System.getUriFor(FREEME_RINGTONE_PHONE_SIM2_CACHE);
        public static final Uri FREEME_DEFAULT_RINGTONE_MESSAGE_SIM2_CACHE_URI =
                Settings.System.getUriFor(FREEME_RINGTONE_MESSAGE_SIM2_CACHE);
        //*/

        //*/ freeme.zhiwei.zhang, 20180120. OneHand.
        public static final String FREEME_ONEHAND_ENABLED = "onehand_enabled";
        public static final String FREEME_ONEHAND_SHOW_HARD_KEYS = "onehand_show_hard_keys";
        public static final String FREEME_ONEHAND_DIRECTION = "onehand_direction";
        //*/

        //*/ freeme.zhiwei.zhang, 20170120. Supershot.
        public static final String FREEME_SUPERSHOT_MODE_DEFAULT = "freeme_supershot_mode_default";
        //*/

        //*/ freeme.lincheng.gu, 20180320. Fooview.
        public static final String FREEME_GESTURE_TOUCH_TWO_POINT_LONGPRESS = "freeme_gesture_touch_two_point_longpress";

        //*/ freeme.zhiwei.zhang, 20180120. Three finger snapshot.
        public static final String FREEME_GESTURE_TOUCH_THREE_POINT = "freeme_gesture_touch_three_point";
        //*/

        //*/ freeme.chenming, 20180120. Five finger back home.
        public static final String FREEME_GESTURE_TOUCH_FIVE_POINTE = "freeme_gesture_touch_five_point";
        //*/

        //*/ freeme.wanglei, 20180122. TouchProtect.
        public static final String FREEME_TOUCH_PROTECT_KEY = "freeme_touch_protect_key";
        //*/

        //*/ freeme.zhiwei.zhang, 20180123. SmartWake.
        public static final String FREEME_HOME_DOUBLETAP_POWEROFF_ENABLED = "freeme_home_doubletap_poweroff_enabled";
        public static final String FREEME_HOME_DOUBLETAP_WAKEUP_ENABLED = "freeme_home_doubletap_wakeup_enabled";
        public static final String FREEME_SCREEN_DOUBLETAP_WAKEUP_ENABLED = "freeme_screen_doubletap_wakeup_enabled";

        public static final String FREEME_SCREEN_WAKEUP_SETS = "freeme_screen_wakeup_sets";
        public static final int FREEME_SCREEN_WAKEUP_SETS_ENABLE = FREEME_BOOLBIT_MASTER_ENABLE;
        public static final int FREEME_SCREEN_WAKEUP_UP_SUPPORTED = 1 << 0; // 1
        public static final int FREEME_SCREEN_WAKEUP_DOWN_SUPPORTED = 1 << 1; // 2
        public static final int FREEME_SCREEN_WAKEUP_LEFT_SUPPORTED = 1 << 2; // 4
        public static final int FREEME_SCREEN_WAKEUP_RIGHT_SUPPORTED = 1 << 3; // 8
        public static final int FREEME_SCREEN_WAKEUP_W_SUPPORTED = 1 << 4; // 16
        public static final int FREEME_SCREEN_WAKEUP_O_SUPPORTED  = 1 << 5; // 32
        public static final int FREEME_SCREEN_WAKEUP_E_SUPPORTED = 1 << 6; // 64
        public static final int FREEME_SCREEN_WAKEUP_C_SUPPORTED = 1 << 7; // 128
        public static final int FREEME_SCREEN_WAKEUP_M_SUPPORTED = 1 << 8; // 256
        public static final int FREEME_SCREEN_WAKEUP_V_SUPPORTED = 1 << 9; // 512
        public static final int FREEME_SCREEN_WAKEUP_S_SUPPORTED = 1 << 10; // 1024
        public static final int FREEME_SCREEN_WAKEUP_Z_SUPPORTED = 1 << 11; // 2048
        public static final int FREEME_SCREEN_WAKEUP_RARROW_SUPPORTED = 1 << 12; // 4096
        public static final int FREEME_SCREEN_WAKEUP_TARROW_SUPPORTED = 1 << 13; // 8192

        public static final String FREEME_SCREEN_ACTION_W_SETTING = "freeme_screen_action_w_setting";
        public static final String FREEME_SCREEN_ACTION_O_SETTING = "freeme_screen_action_o_setting";
        public static final String FREEME_SCREEN_ACTION_E_SETTING = "freeme_screen_action_e_setting";
        public static final String FREEME_SCREEN_ACTION_C_SETTING = "freeme_screen_action_c_setting";
        public static final String FREEME_SCREEN_ACTION_M_SETTING = "freeme_screen_action_m_setting";
        public static final String FREEME_SCREEN_ACTION_V_SETTING = "freeme_screen_action_v_setting";
        public static final String FREEME_SCREEN_ACTION_S_SETTING = "freeme_screen_action_s_setting";
        public static final String FREEME_SCREEN_ACTION_Z_SETTING = "freeme_screen_action_z_setting";
        public static final String FREEME_SCREEN_ACTION_UP_SETTING = "freeme_screen_action_up_setting";
        public static final String FREEME_SCREEN_ACTION_DOWN_SETTING = "freeme_screen_action_down_setting";
        public static final String FREEME_SCREEN_ACTION_LEFT_SETTING = "freeme_screen_action_left_setting";
        public static final String FREEME_SCREEN_ACTION_RIGHT_SETTING = "freeme_screen_action_right_setting";
        public static final String FREEME_SCREEN_ACTION_RARROW_SETTING = "freeme_screen_action_rarrow_setting";
        public static final String FREEME_SCREEN_ACTION_TARROW_SETTING = "freeme_screen_action_tarrow_setting";
        //*/

        //*/ freeme.zhaozehong, 20180127. for freemeOS, dialer other settins
        public static final String FREEME_REVERSE_SILENT_SETTING = "freeme_reverse_silent_key";
        public static final String FREEME_GRADIENT_RING_KEY = "freeme_gradient_ring";
        public static final String FREEME_PHONE_VIBRAT_KEY = "freeme_phone_vibrate";
        public static final String FREEME_POCKET_MODE_KEY = "freeme_pocket_mode_ring_key";
        public static final String FREEME_SMART_DIAL_KEY = "freeme_smart_dial_key";
        public static final String FREEME_SMART_ANSWER_KEY = "freeme_smart_answer_key";
        public static final String FREEME_NO_TOUCH_ANSWER_SPEAKER_KEY = "freeme_no_touch_answer_speaker_key";
        //*/

        //*/ freeme.zhiwei.zhang, 20170106. non-touch operation.
        public static final String FREEME_GESTURE_SETS = "freeme_gesture_sets";
        public static final int FREEME_GESTURE_SETS_ENABLE = FREEME_BOOLBIT_MASTER_ENABLE;
        public static final int FREEME_GESTURE_GALLERY_SLIDE            = 1 << 0; // 1
        public static final int FREEME_GESTURE_LAUNCHER_SLIDE           = 1 << 1; // 2
        public static final int FREEME_GESTURE_LOCKSCR_UNLOCK           = 1 << 2; // 4
        public static final int FREEME_GESTURE_VIDEO_CONTROL            = 1 << 3; // 8
        public static final int FREEME_GESTURE_MUSIC_CONTROL            = 1 << 4; // 16
        public static final int FREEME_GESTURE_PHONE_CONTROL            = 1 << 5; // 32
        public static final int FREEME_GESTURE_PHONE_HandFree_CONTROL   = 1 << 6; // 64
        //*/

        //*/ freeme.gouzhouping, 20170725. for show notification icon
        public static final String FREEME_SHOW_NOTI_ICON = "freeme_show_noti_switch";
        //*/

        //*/ freeme.wanglei, 20180307. vibrate when messaging
        public static final String FREEME_VIBRATE_WHEN_MESSAGING = "freeme_vibrate_when_messaging";
        //*/

        //*/ freeme.zhaozehong, 20170604. for mms ZenMode
        public static final String FREEME_MMS_ZENMODE_PRIORITY_ALLOW = "freeme_mms_zenmode_priority_allow";
        public static final String FREEME_ZENMODE_SCREEN_ON_BLOCK = "freeme_zenmode_screen_on_block";
        public static final String FREEME_ZENMODE_SCREEN_OFF_BLOCK = "freeme_zenmode_screen_off_block";
        //*/

        //*/ freeme.liming, 20180307. clock silent when reverse
        public static final String FREEME_REVERSE_CLOCK_SILENT_SETTING =
                "freeme_reverse_clock_silent_key";
        //*/

        //*/ freeme.zhaozehong, 20180320. for custom IMEI & MEID
        public static final String[] FREEME_SIM_GSM_IMEI_ARR = {
                "gsm.imei.sim1",
                "gsm.imei.sim2"
        };
        public static final String FREEME_SIM_CDMA_MEID = "cdma.meid.sim";
        //*/

        //*/ freeme.biantao, 20180331. Recent locked-tasks.
        public static final String FREEME_SYSUI_RECENT_LOCKED_TASKS = "freeme_sysui_recent_locked_tasks";
        //*/

        //*/ freeme.chenming, 20180305. Permission-PreAuth.
        public static final String FREEME_PERMISSION_PREAUTH = "freeme_permission_pre_auth";
        //*/

        //*/ freeme.liming, 20180423. Super-PowerSaver
        public static final String FREEME_SUPER_POWER_SAVER_ENABLE =
                "freeme_super_power_saver_enable";
        //*/

        //*/ freeme.gouzhouping, 20180507. for show network speed switch.
        public static final String FREEME_SHOW_NETWORK_SPEED = "show_network_speed";
        //*/

        //*/ freeme.xiaohui, 20180521. FreemeLockscreen.
        public static final String FREEME_LOCKSCREEN_COMPONENT = "freeme_lockscreen_component";

        //*/ freeme.zhiwei.zhang, 20181113. AppCompat.
        public static final String FREEME_DISPLAY_COMPAT_NONIMMERSIVE_APPS = "freeme_display_compat_nonimmersive_apps";
        public static final String FREEME_DISPLAY_COMPAT_SHOULD_FULLSCREEN_APPS =
                "freeme_display_compat_should_fullscreen_apps";
        public static final String FREEME_DISPLAY_COMPAT_ALREADY_FULLSCREEN_APPS =
                "freeme_display_compat_already_fullscreen_apps";
        public static final String FREEME_DISPLAY_NOTCH_STATUS = "freeme_display_notch_status";
        //*/

        //*/ freeme.zhiwei.zhang, 20180802. OneStep.
        public static final String FREEME_ONESTEP_MODE = "freeme_onestep_mode";
        public static final String FREEME_ONESTEP_SWITCHAPP_ENABLE = "freeme_switchapp_enable";
        //*/

        //*/ freeme.zhaozehong, 20180913. GameMode
        public static final String FREEME_GAMEMODE = "freeme_gamemode";
        public static final String FREEME_GAMEMODE_TOOLS = "freeme_gamemode_tools";
        public static final String FREEME_GAMEMODE_SETS_ANSWER_CALL = "freeme_gm_sets_answer_call";
        public static final String FREEME_GAMEMODE_SETS_BLOCK_NOTIFICATIONS = "freeme_gm_sets_block_notifications";
        public static final String FREEME_GAMEMODE_SETS_BLOCK_AUTO_BRIGHTNESS = "freeme_gm_sets_block_auto_brightness";
        public static final String FREEME_GAMEMODE_SETS_LOCK_KEYS = "freeme_gm_settings_lock_keys";
        //*/

        //*/ freeme.songyan, 20190103. simple launcher.
        public static final String FREEME_SIMPLE_LAUNCHER_ENABLE = "freeme_simple_launcher_enable";
        //*/

        //*/ ZY.zhouzhe, 20180711. Hotkey.
        public static final String FREEME_HOT_KEY_ENABLED = "freeme_hot_key_enabled";
        public static final String FREEME_PRESS_PACKAGENAME= "freeme_press_packageName";
        public static final String FREEME_LONG_PRESS_PACKAGENAME= "freeme_press_className";
        public static final String FREEME_PRESS_CLASSNAME= "freeme_longpress_packageName";
        public static final String FREEME_LONG_PRESS_CLASSNAME= "freeme_longpress_className";
        //*/

        //*/ ZY.weiyunsheng, 20190225. glove mode.
        public static final String FREEME_GLOVE_MODE_ENABLED= "freeme_glove_mode_enabled";
        //*/

        //*/ ZY.guoyong, 20190422. Tface.
        public static final String FREEME_HAS_ENROLLED_FACE= "com.freeme.intent.extra.face.HAS_ENROLLED_FACE";
        //*/

        //*/ DZ.zhangzhao, 20190428. volume wakeup
        public static final String VOLUME_WAKEUP_MODE_ENABLE = "volume_wakeup_mode_enable";
        //*/

        //*/ DZ.zhangling, 20190516. screen record
        public static final String FREEME_SCREEEN_RECORD_SOUND = "freeme_screen_record_sound";
        public static final String FREEME_SCREEEN_RECORD_QUALITY = "freeme_screen_record_quality";
        public static final String FREEME_SCREEEN_RECORD_TIME = "freeme_screen_record_time";
        //*/
    }

    public static final class Secure {
        //*/ freeme.biantao, 20180126. AE & ANR dialog.
        public static final String FREEME_CAN_SHOW_ERROR_DIALOGS = "freeme_can_show_error_dialogs";
        //*/

        //*/ freeme.yangzhengguang. 20180523. Applock
        public static final String FREEME_APPLOCK_ENABLED = "app_lock_enabled";
        public static final String FREEME_APPLOCK_LOCKED_APPS_CLASSES = "applock_locked_apps_classes";
        public static final String FREEME_APPLOCK_LOCKED_APPS_PACKAGES = "applock_locked_apps_packages";
        public static final String FREEME_APPLOCK_LOCK_TYPE = "applock_lock_type";
        //*/
    }

    public static final class Global {
        //*/freeme.shanjibing, 20160531. Power connect & full sound
        public static final String FREEME_SOUND_POWER_CONNECT = "freeme_power_connect_sound";
        public static final String FREEME_SOUND_POWER_FULL    = "freeme_power_full_sound";
        //*/

        //*/ freeme.biantao, 20180904. DeviceIdle-Whitelist.
        public static final String FREEME_DEVICE_IDLE_WHITELIST = "freeme_deviceidle_whitelist";
        //*/
    }
}
