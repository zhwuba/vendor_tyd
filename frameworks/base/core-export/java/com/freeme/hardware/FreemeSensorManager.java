package com.freeme.hardware;

/** @hide */
public class FreemeSensorManager {

    // * Sensor Hall
    static native int nativeGetSensorHallState();

    public static class SensorHall {
        public static final int HALL_CLOSE = 0;
        public static final int HALL_FAR   = 1;

        public static boolean isClose () {
            return nativeGetSensorHallState() == HALL_CLOSE;
        }

        private SensorHall() {
            // Do not initialise.
        }
    }

    private FreemeSensorManager() {
        // Do not initialise.
    }
}