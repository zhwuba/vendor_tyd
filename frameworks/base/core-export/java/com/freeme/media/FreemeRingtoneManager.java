package com.freeme.media;

import android.Manifest;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;

import com.android.internal.database.SortCursor;

import java.util.List;

import com.freeme.util.FreemeOption;
import com.freeme.provider.FreemeSettings;

/** @hide */
public class FreemeRingtoneManager {
    private static final String TAG = "FreemeRingtoneManager";

    // Make sure these are in sync with attrs.xml:
    // <attr name="ringtoneType">

    /**
     * Type that refers to sounds that are used for the message phone ringer.
     */
    public static final int TYPE_MESSAGE = 32;

    /**
     * Type that refers to sounds that are used for the sim2 phone ringer.
     */
    public static final int TYPE_RINGTONE_SIM2 = 64;

    /**
     * Type that refers to sounds that are used for the sim2 message phone ringer.
     */
    public static final int TYPE_MESSAGE_SIM2 = 128;

    /**
     * All ext types of sounds.
     */
    public static final int TYPE_ALL = TYPE_MESSAGE | TYPE_RINGTONE_SIM2 | TYPE_MESSAGE_SIM2;

    /**
     * The key used to store the default ringtone sound.
     */
    public static final String KEY_DEFAULT_RINGTONE = "freeme_default_ringtone";

    /**
     * The key used to store the default alarm sound.
     */
    public static final String KEY_DEFAULT_ALARM = "freeme_default_alarm";

    /**
     * The key used to store the default notification sound.
     */
    public static final String KEY_DEFAULT_NOTIFICATION = "freeme_default_notification";

    /**
     * The key used to store the default ringtone_sim2 sound.
     */
    public static final String KEY_DEFAULT_RINGTONE_SIM2 = "freeme_default_ringtone_sim2";

    /**
     * The key used to store the default message sound.
     */
    public static final String KEY_DEFAULT_MESSAGE = "freeme_default_message";

    /**
     * The key used to store the default message_sim2 sound.
     */
    public static final String KEY_DEFAULT_MESSAGE_SIM2 = "freeme_default_message_sim2";


    /**
     * used for RingtonePickerActivity.java
     */
    public static final String EXTRA_RINGTONE_CUSTOM = "extra_ringtone_custom";

    public static int inferStreamType(int type) {
        switch (type) {
            case TYPE_RINGTONE_SIM2:
                return AudioManager.STREAM_RING;
            case TYPE_MESSAGE:
            case TYPE_MESSAGE_SIM2:
                return AudioManager.STREAM_NOTIFICATION;
            default:
                return -1;
        }
    }

    public static String getSettingForType(int type) {
        if ((type & TYPE_MESSAGE) != 0) {
            return FreemeSettings.System.FREEME_RINGTONE_MESSAGE;
        } else if ((type & TYPE_RINGTONE_SIM2) != 0) {
            return FreemeSettings.System.FREEME_RINGTONE_PHONE_SIM2;
        } else if ((type & TYPE_MESSAGE_SIM2) != 0) {
            return FreemeSettings.System.FREEME_RINGTONE_MESSAGE_SIM2;
        } else {
            return null;
        }
    }

    public static Uri getCacheForType(int type, int userId) {
        if ((type & TYPE_MESSAGE) != 0) {
            return ContentProvider.maybeAddUserId(
                    FreemeSettings.System.FREEME_DEFAULT_RINGTONE_MESSAGE_CACHE_URI, userId);
        } else if ((type & TYPE_RINGTONE_SIM2) != 0) {
            return ContentProvider.maybeAddUserId(
                    FreemeSettings.System.FREEME_DEFAULT_RINGTONE_PHONE_SIM2_CACHE_URI, userId);
        } else if ((type & TYPE_MESSAGE_SIM2) != 0) {
            return ContentProvider.maybeAddUserId(
                    FreemeSettings.System.FREEME_DEFAULT_RINGTONE_MESSAGE_SIM2_CACHE_URI, userId);
        } else {
            return null;
        }
    }

    public static int getDefaultType(Uri defaultRingtoneUri) {
        if (defaultRingtoneUri == null) {
            return -1;
        } else if (defaultRingtoneUri.equals(FreemeSettings.System.FREEME_DEFAULT_RINGTONE_MESSAGE_URI)) {
            return TYPE_MESSAGE;
        } else if (defaultRingtoneUri.equals(FreemeSettings.System.FREEME_DEFAULT_RINGTONE_PHONE_SIM2_URI)) {
            return TYPE_RINGTONE_SIM2;
        } else if (defaultRingtoneUri.equals(FreemeSettings.System.FREEME_DEFAULT_RINGTONE_MESSAGE_SIM2_URI)) {
            return TYPE_MESSAGE_SIM2;
        } else {
            return -1;
        }
    }

    public static String getExternalDirectoryForType(int type) {
        switch (type) {
            case TYPE_MESSAGE:
            case TYPE_MESSAGE_SIM2:
                return Environment.DIRECTORY_NOTIFICATIONS;
            case TYPE_RINGTONE_SIM2:
                return Environment.DIRECTORY_RINGTONES;
            default:
                return null;
        }
    }


    public static int getDefaultType(Context context, Uri defaultRingtoneUri) {
        ContentResolver resolver = context.getContentResolver();
        if (defaultRingtoneUri == null) {
            return -1;
        } else if (defaultRingtoneUri.equals(
                RingtoneManager.getActualDefaultRingtoneUri(context, TYPE_MESSAGE))) {
            return TYPE_MESSAGE;
        } else if (defaultRingtoneUri.equals(
                RingtoneManager.getActualDefaultRingtoneUri(context, TYPE_RINGTONE_SIM2))) {
            return TYPE_RINGTONE_SIM2;
        } else if (defaultRingtoneUri.equals(
                RingtoneManager.getActualDefaultRingtoneUri(context, TYPE_MESSAGE_SIM2))) {
            return TYPE_MESSAGE_SIM2;
        } else if (defaultRingtoneUri.equals(
                RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE))) {
            return RingtoneManager.TYPE_RINGTONE;
        } else if (defaultRingtoneUri.equals(
                RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_NOTIFICATION))) {
            return RingtoneManager.TYPE_NOTIFICATION;
        } else if (defaultRingtoneUri.equals(
                RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_ALARM))) {
            return RingtoneManager.TYPE_ALARM;
        } else {
            return -1;
        }
    }

    public static Uri getDefaultUri(int type) {
        if ((type & TYPE_MESSAGE) != 0) {
            return FreemeSettings.System.FREEME_DEFAULT_RINGTONE_MESSAGE_URI;
        } else if ((type & TYPE_RINGTONE_SIM2) != 0) {
            return FreemeSettings.System.FREEME_DEFAULT_RINGTONE_PHONE_SIM2_URI;
        } else if ((type & TYPE_MESSAGE_SIM2) != 0) {
            return FreemeSettings.System.FREEME_DEFAULT_RINGTONE_MESSAGE_SIM2_URI;
        } else {
            return null;
        }
    }

    public static List<String> setFilterColumnsList(List<String> columns, int type) {
        if ((type & TYPE_RINGTONE_SIM2) != 0) {
            columns.add(MediaStore.Audio.AudioColumns.IS_RINGTONE);
        }

        if ((type & TYPE_MESSAGE) != 0
                || (type & TYPE_MESSAGE_SIM2) != 0) {
            columns.add(MediaStore.Audio.AudioColumns.IS_NOTIFICATION);
        }

        return columns;
    }

    public static Uri getDefaultRingtoneUri(Context context, int type) {
        String uriString = null;
        ContentResolver resolver = context.getContentResolver();
        switch (type) {
            case RingtoneManager.TYPE_RINGTONE:
                uriString = Settings.System.getString(resolver, KEY_DEFAULT_RINGTONE);
                break;

            case RingtoneManager.TYPE_NOTIFICATION:
                uriString = Settings.System.getString(resolver, KEY_DEFAULT_NOTIFICATION);
                break;

            case RingtoneManager.TYPE_ALARM:
                uriString = Settings.System.getString(resolver, KEY_DEFAULT_ALARM);
                break;

            case TYPE_MESSAGE:
                uriString = Settings.System.getString(resolver, KEY_DEFAULT_MESSAGE);
                break;

            case TYPE_MESSAGE_SIM2:
                uriString = Settings.System.getString(resolver, KEY_DEFAULT_MESSAGE_SIM2);
                break;

            case TYPE_RINGTONE_SIM2:
                uriString = Settings.System.getString(resolver, KEY_DEFAULT_RINGTONE_SIM2);
                break;

            default:
                break;
         }
         return uriString != null ? Uri.parse(uriString) : null;
    }

    public static String getCacheStringForType(int type) {
        if ((type & TYPE_MESSAGE) != 0) {
            return FreemeSettings.System.FREEME_RINGTONE_MESSAGE_CACHE;
        } else if ((type & TYPE_RINGTONE_SIM2) != 0) {
            return FreemeSettings.System.FREEME_RINGTONE_PHONE_SIM2_CACHE;
        } else if ((type & TYPE_MESSAGE_SIM2) != 0) {
            return FreemeSettings.System.FREEME_RINGTONE_MESSAGE_SIM2_CACHE;
        } else {
            return null;
        }
    }

    public static int getTypeForCache(Uri uri) {
        if (FreemeSettings.System.FREEME_DEFAULT_RINGTONE_MESSAGE_CACHE_URI.equals(uri)) {
            return TYPE_MESSAGE;
        } else if (FreemeSettings.System.FREEME_DEFAULT_RINGTONE_PHONE_SIM2_CACHE_URI.equals(uri)) {
            return TYPE_RINGTONE_SIM2;
        } else if (FreemeSettings.System.FREEME_DEFAULT_RINGTONE_MESSAGE_SIM2_CACHE_URI.equals(uri)) {
            return TYPE_MESSAGE_SIM2;
        } else {
            return -1;
        }
    }

    public static String getCacheForString(String defName, String name) {
        if (defName == null) {
            if (FreemeSettings.System.FREEME_RINGTONE_MESSAGE.equals(name)) {
                return FreemeSettings.System.FREEME_RINGTONE_MESSAGE_CACHE;
            } else if (FreemeSettings.System.FREEME_RINGTONE_PHONE_SIM2.equals(name)) {
                return FreemeSettings.System.FREEME_RINGTONE_PHONE_SIM2_CACHE;
            } else if (FreemeSettings.System.FREEME_RINGTONE_MESSAGE_SIM2.equals(name)) {
                return FreemeSettings.System.FREEME_RINGTONE_MESSAGE_SIM2_CACHE;
            }
        }

        return defName;
    }

    /* Restore Default Ringtone if deleted by user*/
    public static Uri getRingtone(final Context context, Uri ringtoneUri, int streamType) {
        int ringtoneType = -1;
        if (ringtoneUri == null) {
            return null;
        }

        ringtoneType = RingtoneManager.isDefault(ringtoneUri) ?
                RingtoneManager.getDefaultType(ringtoneUri) :
                FreemeRingtoneManager.getDefaultType(context, ringtoneUri);
        if (ringtoneType != -1) {
            Uri defUri = RingtoneManager.getActualDefaultRingtoneUri(context, ringtoneType);
            // check rongtone existed
            if (validRingtoneUri(context, defUri, ringtoneType) == -1 /* internel ringtone*/) {
                defUri = FreemeRingtoneManager.getDefaultRingtoneUri(context, ringtoneType);
                // restore ringtone
                RingtoneManager.setActualDefaultRingtoneUri(context, ringtoneType, defUri);
                return defUri;
            }
        }

        // return origin Uri
        return ringtoneUri;
    }

    private static int validRingtoneUri(Context context, Uri ringtoneUri, int type) {
        if (!isRingtoneExist(context, ringtoneUri)) {
            return 0; // no permission
        }

        final RingtoneManager rm = new RingtoneManager(context);
        rm.setType(type);
        return rm.getRingtonePosition(ringtoneUri);
    }

    /**
     * Just check permission to check external ringtone exist
     * {@link android.Manifest.permission#READ_EXTERNAL_STORAGE}.
     */
    private static boolean isRingtoneExist(Context context, Uri uri) {
        if (uri == null) {
            return false;
        }

        if (uri.toString().contains("external")
            && context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        return true;
    }
}
