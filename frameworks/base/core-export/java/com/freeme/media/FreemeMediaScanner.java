package com.freeme.media;


import android.media.RingtoneManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.os.SystemProperties;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;

import com.freeme.util.FreemeOption;
import com.freeme.provider.FreemeSettings;
import com.freeme.media.FreemeRingtoneManager;

/** @hide */
public class FreemeMediaScanner {

    private static final String TAG = "FreemeMediaScanner";

    private static final boolean DEBUG = false;

    private static final boolean SUPPORT_RINGTONE_DUAL_SIM = FreemeOption.FREEME_DUAL_SIM_RINGTONE_SUPPORT;
    private static final boolean SUPPORT_RINGTONE_MESSAGE = FreemeOption.FREEME_MESSAGE_RINGTONE_SUPPORT;

    private static final String DEFAULT_RINGTONE_PROPERTY_PREFIX = "ro.config.";

    private static final String RINGTONE_SIM2_SET = "freeme_ringtone2_set";
    private static final String MESSAGE_SET       = "freeme_message_set";
    private static final String MESSAGE_SIM2_SET  = "freeme_message2_set";

    private static FreemeMediaScanner sFreemeMediaScanner = new FreemeMediaScanner();

    private boolean mDefaultRingtoneSet2;
    private boolean mDefaultMessageSet;
    private boolean mDefaultMessageSet2;

    private String mDefaultRingtoneFilename2;
    private String mDefaultMessageFilename;
    private String mDefaultMessageFilename2;

    FreemeMediaScanner() {
        // do nothing
    }

    public static FreemeMediaScanner getInstance() {
        if (sFreemeMediaScanner == null) {
            sFreemeMediaScanner = new FreemeMediaScanner();
        }

        return sFreemeMediaScanner;
    }

    public boolean needSetSettings(boolean ringtones, boolean notifications, String path, Context context) {

        Log.v(TAG, "check needSetSettings: " + path);
        if (SUPPORT_RINGTONE_DUAL_SIM) {
            if (ringtones && (!mDefaultRingtoneSet2 || doesSettingEmpty(RINGTONE_SIM2_SET, context))) {
                if (TextUtils.isEmpty(mDefaultRingtoneFilename2)
                    || doesPathHaveFilename(path, mDefaultRingtoneFilename2)) {
                    if (DEBUG) {
                        Log.v(TAG, "endFile: mDefaultRingtoneSet2 needToSetRingtone=true.");
                    }
                    mDefaultRingtoneSet2 = true;
                }
            }
        }

        if (SUPPORT_RINGTONE_MESSAGE) {
            if (notifications && (!mDefaultMessageSet || doesSettingEmpty(MESSAGE_SET, context))) {
                if (TextUtils.isEmpty(mDefaultMessageFilename)
                    || doesPathHaveFilename(path, mDefaultMessageFilename)) {
                    if (DEBUG) {
                        Log.v(TAG, "endFile: mDefaultMessageSet needToSetRingtone=true.");
                    }
                    mDefaultMessageSet = true;
                }
            }

            if (notifications && (!mDefaultMessageSet2 || doesSettingEmpty(MESSAGE_SIM2_SET, context))) {
                if (TextUtils.isEmpty(mDefaultMessageFilename2)
                    || doesPathHaveFilename(path, mDefaultMessageFilename2)) {
                    if (DEBUG) {
                        Log.v(TAG, "endFile: mDefaultMessageSet2 needToSetRingtone=true.");

                    }
                    mDefaultMessageSet2 = true;
                }
            }
        }

        if (DEBUG) {
            Log.v(TAG, "mDefaultRingtoneSet2 = " + mDefaultRingtoneSet2
                    + ", mDefaultMessageSet = " + mDefaultMessageSet
                    + ", mDefaultMessageSet2 = " + mDefaultMessageSet2);
        }

        return mDefaultRingtoneSet2 || mDefaultMessageSet || mDefaultMessageSet2;
    }

    private boolean doesPathHaveFilename(String path, String filename) {
        int pathFilenameStart = path.lastIndexOf(File.separatorChar) + 1;
        int filenameLength = filename.length();
        return path.regionMatches(pathFilenameStart, filename, 0, filenameLength) &&
                pathFilenameStart + filenameLength == path.length();
    }

    public void setDefaultRingtoneFileNames() {
        if (!(SUPPORT_RINGTONE_DUAL_SIM || SUPPORT_RINGTONE_MESSAGE)) {
            return;
        }

        mDefaultRingtoneFilename2 = SystemProperties.get(DEFAULT_RINGTONE_PROPERTY_PREFIX
                + FreemeSettings.System.FREEME_RINGTONE_PHONE_SIM2);
        mDefaultMessageFilename = SystemProperties.get(DEFAULT_RINGTONE_PROPERTY_PREFIX
                + FreemeSettings.System.FREEME_RINGTONE_MESSAGE);
        mDefaultMessageFilename2 = SystemProperties.get(DEFAULT_RINGTONE_PROPERTY_PREFIX
                + FreemeSettings.System.FREEME_RINGTONE_MESSAGE_SIM2);

        if (DEBUG) {
            Log.v(TAG, "mDefaultRingtoneFilename2: ringtone = " + mDefaultRingtoneFilename2
                    + ",mDefaultMessageFilename = " + mDefaultMessageFilename
                    + ",mDefaultMessageFilename2 = " + mDefaultMessageFilename2);
        }
    }

    public void setRingtoneSettings(boolean needToSetSystemSettings,
                                    boolean needToSetSettings,
                                    boolean ringtones,
                                    boolean notifications,
                                    boolean alarms,
                                    Uri tableUri,
                                    long rowId,
                                    Context context) {

        if (needToSetSystemSettings) {
            setBackupSystemSettings(ringtones, notifications, alarms, tableUri, rowId, context);
        }

        if (!(SUPPORT_RINGTONE_DUAL_SIM || SUPPORT_RINGTONE_MESSAGE)) {
            return;
        }

        if (DEBUG) {
            Log.v(TAG, "setRingtoneSettings : needToSetSettings " + needToSetSettings + "ringtones = " + ringtones
                    + "doesSettingEmpty(RINGTONE_SIM2_SET, context)" + doesSettingEmpty(RINGTONE_SIM2_SET, context)
                    + "doesSettingEmpty(MESSAGE_SET, context)" + doesSettingEmpty(MESSAGE_SET, context)
                    + "doesSettingEmpty(MESSAGE_SIM2_SET, context)" + doesSettingEmpty(MESSAGE_SIM2_SET, context));
        }

        if(needToSetSettings) {
            if (ringtones) {
                if (SUPPORT_RINGTONE_DUAL_SIM) {
                    if (doesSettingEmpty(RINGTONE_SIM2_SET, context)) {
                        if (mDefaultRingtoneSet2) {
                            if (DEBUG) {
                                Log.v(TAG, "mDefaultRingtoneSet2 setRingtoneSettings : " + tableUri);
                            }
                            setSettingIfNotSet(FreemeSettings.System.FREEME_RINGTONE_PHONE_SIM2, tableUri, rowId, context);
                            setBackupSettings(FreemeRingtoneManager.TYPE_RINGTONE_SIM2, tableUri, rowId, context);
                            setSettingFlag(RINGTONE_SIM2_SET, context);
                            mDefaultRingtoneSet2 = false;
                        }
                    }
                }
            } else if (notifications) {
                if (SUPPORT_RINGTONE_MESSAGE) {
                    if (doesSettingEmpty(MESSAGE_SET, context)) {
                        if (mDefaultMessageSet) {
                            if (DEBUG) {
                                Log.v(TAG, "mDefaultMessageSet setRingtoneSettings : " + tableUri);
                            }
                            setSettingIfNotSet(FreemeSettings.System.FREEME_RINGTONE_MESSAGE, tableUri, rowId, context);
                            setBackupSettings(FreemeRingtoneManager.TYPE_MESSAGE, tableUri, rowId, context);
                            setSettingFlag(MESSAGE_SET, context);
                            mDefaultMessageSet = false;
                        }
                    }

                    if (doesSettingEmpty(MESSAGE_SIM2_SET, context)) {
                        if (mDefaultMessageSet2) {
                            if (DEBUG) {
                                Log.v(TAG, "mDefaultMessageSet2 setRingtoneSettings : " + tableUri);
                            }
                            setSettingIfNotSet(FreemeSettings.System.FREEME_RINGTONE_MESSAGE_SIM2, tableUri, rowId, context);
                            setBackupSettings(FreemeRingtoneManager.TYPE_MESSAGE_SIM2, tableUri, rowId, context);
                            setSettingFlag(MESSAGE_SIM2_SET, context);
                            mDefaultMessageSet2 = false;
                        }
                    }
                }
            }
        }
    }

    private void setSettingIfNotSet(String settingName, Uri uri, long rowId, Context context) {
        Log.e(TAG, "setSettingIfNotSet: name=" + settingName
                + " with value=" + uri);
        ContentResolver cr = context.getContentResolver();
        String existingSettingValue = Settings.System.getString(cr, settingName);
        if (TextUtils.isEmpty(existingSettingValue)) {
            final Uri settingUri = Settings.System.getUriFor(settingName);
            final Uri ringtoneUri = ContentUris.withAppendedId(uri, rowId);
            RingtoneManager.setActualDefaultRingtoneUri(context,
                    RingtoneManager.getDefaultType(settingUri), ringtoneUri);
            if (DEBUG) {
                Log.v(TAG, "is empty !!!!!!!!!!!!!!!setSettingIfNotSet:+ "
                        + " name=" + settingName
                        + ",value=" + rowId
                        + ",Type = " + RingtoneManager.getDefaultType(settingUri));
            }
        } else {
            Log.e(TAG, "setSettingIfNotSet: name=" + settingName
                    + " with value=" + existingSettingValue);
        }

    }

    private boolean doesSettingEmpty(String settingName, Context context) {
        return TextUtils.isEmpty(Settings.System.getString(context.getContentResolver(), settingName));
    }

    private void setSettingFlag(String settingName, Context context) {
        final String VALUE = "yes";
        if (DEBUG) {
            Log.d(TAG, "setSettingFlag set:" + settingName);
        }
        Settings.System.putString(context.getContentResolver(), settingName, VALUE);
    }

    public static void setBackupSettings(int type, Uri uri, long rowId, Context context) {
        switch (type) {
            case FreemeRingtoneManager.TYPE_MESSAGE: {
                setBackupSettingIfNotSet(FreemeRingtoneManager.KEY_DEFAULT_MESSAGE, uri, rowId, context);
            } break;
            case FreemeRingtoneManager.TYPE_MESSAGE_SIM2: {
                setBackupSettingIfNotSet(FreemeRingtoneManager.KEY_DEFAULT_MESSAGE_SIM2, uri, rowId, context);
            } break;
            case FreemeRingtoneManager.TYPE_RINGTONE_SIM2: {
                setBackupSettingIfNotSet(FreemeRingtoneManager.KEY_DEFAULT_RINGTONE_SIM2, uri, rowId, context);
            } break;
            case RingtoneManager.TYPE_RINGTONE: {
                setBackupSettingIfNotSet(FreemeRingtoneManager.KEY_DEFAULT_RINGTONE, uri, rowId, context);
            } break;
            case RingtoneManager.TYPE_NOTIFICATION: {
                setBackupSettingIfNotSet(FreemeRingtoneManager.KEY_DEFAULT_NOTIFICATION, uri, rowId, context);
            } break;
            case RingtoneManager.TYPE_ALARM: {
                setBackupSettingIfNotSet(FreemeRingtoneManager.KEY_DEFAULT_ALARM, uri, rowId, context);
            } break;
            default:
        }
    }

    public static void setBackupSystemSettings(boolean ringtones, boolean notifications, boolean alarms,
                                               Uri uri, long rowId, Context context) {
        if (ringtones) {
            setBackupSettings(RingtoneManager.TYPE_RINGTONE, uri, rowId, context);
        } else if (notifications) {
            setBackupSettings(RingtoneManager.TYPE_NOTIFICATION, uri, rowId, context);
        } else if (alarms) {
            setBackupSettings(RingtoneManager.TYPE_ALARM, uri, rowId, context);
        }
    }

    private static void setBackupSettingIfNotSet(String settingName, Uri uri, long rowId, Context context) {
        String existingSettingValue = Settings.System.getString(context.getContentResolver(),
                settingName);

        if (TextUtils.isEmpty(existingSettingValue)) {
            // Set the setting to the given URI
            Settings.System.putString(context.getContentResolver(), settingName,
                    ContentUris.withAppendedId(uri, rowId).toString());
            Log.v(TAG, "setBackupSettingIfNotSet: name=" + settingName + ",value=" + rowId);
        } else {
            Log.v(TAG, "setBackupSettingIfNotSet: name=" + settingName + " value=" + existingSettingValue);
        }
    }
}
