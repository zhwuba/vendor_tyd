package com.freeme.content;

public class FreemeIntent {
    /**
     * Broadcast Action: Three pointer take screen shot.
     */
    public static final String ACTION_GESTURE_TOUCH_THREE_POINT = "com.freeme.intent.action.GESTURE_TOUCH_THREE_POINT";

    /**
     * Broadcast extra: Three pointer style(0: take screen shot, 1: split screen).
     */
    public static final String EXTRA_THREE_POINT_STYLE = "com.freeme.intent.extra.EXTRA_THREE_POINT_STYLE";

    public static boolean isProtectedBroadcastRelaxed(String action) {
        if (action == null)
            return false;

        switch (action) {
            case ACTION_GESTURE_TOUCH_THREE_POINT:
                return true;
            default:
                return false;
        }
    }
}
