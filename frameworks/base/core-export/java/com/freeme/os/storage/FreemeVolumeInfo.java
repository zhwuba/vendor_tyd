package com.freeme.os.storage;

import android.os.storage.DiskInfo;
import android.os.storage.VolumeInfo;
import android.util.Log;

public class FreemeVolumeInfo {
    private static final String TAG = "FreemeVolumeInfo";

    public static boolean isUsbOtg(VolumeInfo volumeInfo) {
        DiskInfo diskInfo = volumeInfo.getDisk();
        if (diskInfo == null) {
            return false;
        }

        String diskID = diskInfo.getId();
        if (diskID != null) {
            // for usb otg, the disk id same as disk:8:x
            String[] idSplit = diskID.split(":");
            if (idSplit != null && idSplit.length == 2) {
                if (idSplit[1].startsWith("8,")) {
                    Log.d(TAG, "this is a usb otg");
                    return true;
                }
            }
        }
        return false;
    }


    private FreemeVolumeInfo() {
        // Do not initialize.
    }
}