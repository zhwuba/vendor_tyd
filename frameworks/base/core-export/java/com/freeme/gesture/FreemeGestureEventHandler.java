package com.freeme.gesture;

import android.content.Context;
import android.content.Intent;
import android.view.MotionEvent;

import com.freeme.content.FreemeIntent;

/**
 * @hide
 */
public class FreemeGestureEventHandler {
    private final static String TAG = "FreemeGestureInterceptHandler";

    public final static int GESTURE_TYPE_SPUERSHOT  = 0x01;

    protected final static int TOUCH_STATE_REST      = 0;
    protected final static int TOUCH_STATE_SCROLLING = 1;

    protected int mTouchState = TOUCH_STATE_REST;
    protected boolean mIntercepted;

    protected int mTouchSlop;
    protected float mDensity;

    private int mCurrentControl;
    private int mCurrentGestureType;

    protected Context mContext;

    public FreemeGestureEventHandler(Context context, int gestureType, float density, int touchSlop) {
        mContext = context;
        mDensity = density;
        mTouchSlop = touchSlop;
        mCurrentGestureType = gestureType;
    }

    /**
     * Check condition to handle event
     * @param event The user touch event
     * @return true show event has be handle, otherwise return false
     */
    public boolean onInterceptTouchEvent(MotionEvent event, int control) {
        setGestureTouchControl(control);

        if ((mCurrentGestureType & mCurrentControl) != 0) {
            int action = event.getAction();
            int pointerCount = event.getPointerCount();
            if (getPointCount() == pointerCount) {
                switch (action & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_POINTER_DOWN: {
                        if (getPointCount() == pointerCount){
                            initGestureDown(event);
                        }
                    }
                    break;
                    case MotionEvent.ACTION_MOVE:
                        boolean intercepted = false;
                        if (getPointCount() == pointerCount) {
                            intercepted = mIntercepted || mTouchState != TOUCH_STATE_SCROLLING ?
                                    determineInterceptStart(event) : handleGestureChange(event);
                        }
                        return intercepted;
                    case MotionEvent.ACTION_UP: {
                        resetTouchState();
                    }
                }
            }
        }

        return false;
    }

    /**
     * Save first down event
     * @param event The user touch event
     */
    protected void initGestureDown(MotionEvent event){

    }

    /**
     * Check condition to intercept
     * @param event The user touch event
     * @return true show event has be intercept, otherwise return false
     */
    protected boolean determineInterceptStart(MotionEvent event){
        return false;
    }

    /**
     * Do handle event
     * @param event The user touch event
     * @return true show event has be handle, otherwise return false
     */
    protected boolean handleGestureChange(MotionEvent event){
        return false;
    }

    protected int getPointCount() {
        return 0;
    }

    protected void resetTouchState() {
        mTouchState = TOUCH_STATE_REST;
    }

    private void setGestureTouchControl(int control) {
        mCurrentControl = control;
    }

    /// ----------------------------Three finger snapshot ------------------------------------------

    public static class ThreePointEventHandler extends FreemeGestureEventHandler {
        private final static int SLIDE_DISTANCE_THRESHOLD    = 100;

        private final static int POINTER_COUNT = 3;

        private final static int THREE_POINT_STYLE_SCREEN_SHOT  = 0;
        private final static int THREE_POINT_STYLE_SPLIT_SCREEN = 1;

        private int[] mPyDown     = new int[POINTER_COUNT];
        private int[] mPyLastDown = new int[POINTER_COUNT];

        public ThreePointEventHandler(Context context, float density, int touchSlop){
             super(context, GESTURE_TYPE_SPUERSHOT, density, touchSlop);
        }

        @Override
        protected int getPointCount() {
            return POINTER_COUNT;
        }

        @Override
        public void initGestureDown(MotionEvent event) {
            mIntercepted = false;
            for (int i = 0; i < POINTER_COUNT; i++) {
                mPyDown[i] = mPyLastDown[i] = (int) event.getY(i);
            }
        }

        @Override
        public boolean determineInterceptStart(MotionEvent event) {
            boolean res = true;
            for (int i = 0; i < POINTER_COUNT; i++) {
                res &= Math.abs((event.getY(i) - mPyLastDown[i])) > mTouchSlop;
                mPyLastDown[i] = (int) event.getY(i);
            }

            if (res) {
                mTouchState = TOUCH_STATE_SCROLLING;
                return true;
            }

            return false;
        }

        @Override
        public boolean handleGestureChange(MotionEvent event) {
            final float distance = SLIDE_DISTANCE_THRESHOLD * mDensity;
            boolean resUp = true;
            boolean resDown = true;
            for (int i = 0; i < POINTER_COUNT; i++) {
                resUp &= event.getY(i) - mPyDown[i] > distance;
                resDown &= mPyDown[i] - event.getY(i) > distance;
            }

            if (resUp || resDown) {
                Intent intent = new Intent();
                intent.setAction(FreemeIntent.ACTION_GESTURE_TOUCH_THREE_POINT);
                intent.putExtra(FreemeIntent.EXTRA_THREE_POINT_STYLE,
                        resUp ? THREE_POINT_STYLE_SCREEN_SHOT : THREE_POINT_STYLE_SPLIT_SCREEN);
                mContext.sendBroadcast(intent);

                mTouchState = TOUCH_STATE_REST;
                mIntercepted = true;
                return true;
            }

            return false;
        }

        @Override
        protected void resetTouchState() {
            super.resetTouchState();

            for(int i = 0; i < POINTER_COUNT; i++) {
               mPyDown[i] = mPyLastDown[i] = 0;
            }
        }
    }
}
