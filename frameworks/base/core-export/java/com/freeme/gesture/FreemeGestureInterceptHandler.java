package com.freeme.gesture;

import android.os.RemoteException;
import android.content.Context;
import android.view.IWindowManager;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

import com.freeme.util.FreemeOption;

import java.util.ArrayList;
import java.util.List;

import com.freeme.gesture.FreemeGestureEventHandler;

/**
 * @hide
 */
public class FreemeGestureInterceptHandler {
    public final static boolean GESTRUE_INTRERCEPT_ENBALE =
            FreemeOption.FREEME_GESTURE_TOUCH_THREE_POINT;

    /// TODO: Add order is important, because process priority check by point count
    private List<FreemeGestureEventHandler> mEventHandlerList= new ArrayList<FreemeGestureEventHandler>();

    private IWindowManager mWindowManager;

    public FreemeGestureInterceptHandler(Context context, IWindowManager windowManager) {
        float density = context.getResources().getDisplayMetrics().density;
        int touchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        mWindowManager = windowManager;

        if (FreemeOption.FREEME_GESTURE_TOUCH_THREE_POINT) {
            mEventHandlerList.add(new FreemeGestureEventHandler.ThreePointEventHandler(context, density, touchSlop));
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        for (FreemeGestureEventHandler handler : mEventHandlerList) {
            if(handler.onInterceptTouchEvent(event, getGestureTouchControl())) {
                return true;
            }
        }

        return false;
    }

    private int getGestureTouchControl() {
        try {
            return mWindowManager.getGestureTouch();
        } catch (RemoteException ex) {
            return 0;
        }
    }
}
