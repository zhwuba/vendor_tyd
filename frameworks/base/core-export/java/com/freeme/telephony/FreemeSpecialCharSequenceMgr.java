package com.freeme.telephony;

import java.util.Map;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;

import com.android.internal.util.ArrayUtils;
import com.freeme.util.FreemeFeature;

/** Secret Code Template:
 *
 *  Define Code:   String TEST_SECRET_CODE = "*#<num>#"
 *  Define Action: String ACTION_TEST_SECRET_CODE = "com.*.*.*.*"
 *
 *  Caution:
 *     1.Don't use package + acivity to startActivity, except: mediatek engineermode
 */

public class FreemeSpecialCharSequenceMgr {
    private static final String TAG = "FreemeSecretCode";

    private static final class SecretCode  {
        static SecretCode newInstance() {
            return new SecretCode();
        }

        interface Callback {
            boolean allowed();
        }

        private static final class Action {
            String action;
            Callback callback;
        }

        private final Map<String, Action> mData;
        SecretCode() {
            mData = new ArrayMap<>(32);
        }

        SecretCode build(String code, String action, String codeAltered, Callback callback) {
            Action a = new Action();
            a.action = action;
            a.callback = callback;

            mData.put(code, a);
            if (!TextUtils.isEmpty(codeAltered) && !codeAltered.equals(code)) {
                mData.put(codeAltered, a);
            }

            return this;
        }

        SecretCode build(String code, String action) {
            return build(code, action, null, null);
        }

        SecretCode build(String code, String action, Callback callback) {
            return build(code, action, null, callback);
        }

        SecretCode build(String code, String action, String codeAltered) {
            return build(code, action, codeAltered, null);
        }

        SecretCode buildKey(String code, String action, String codeAlteredKey, Callback callback) {
            return build(code, action,
                    FreemeFeature.get("secret-code", codeAlteredKey),
                    callback);
        }

        SecretCode buildKey(String code, String action, String codeAlteredKey) {
            return buildKey(code, action, codeAlteredKey, null);
        }

        SecretCode build(String[] codeArr, String[] actionArr) {
            if (ArrayUtils.isEmpty(codeArr) || ArrayUtils.isEmpty(actionArr)) {
                return this;
            }
            final int N = Math.min(codeArr.length, actionArr.length);
            for (int i = 0; i < N; i++) {
                String code = codeArr[i];
                String action = actionArr[i];
                if (TextUtils.isEmpty(code) || TextUtils.isEmpty(action)) {
                    continue;
                }
                build(code, action);
            }
            return this;
        }

        SecretCode buildExtraKey(String codeKey, String actionKey) {
            String codeSequence = FreemeFeature.get("secret-code", codeKey);
            String actionSequence = FreemeFeature.get("secret-code", actionKey);
            if (TextUtils.isEmpty(codeSequence) || TextUtils.isEmpty(actionSequence)) {
                return this;
            }
            return build(codeSequence.split("\\|"), actionSequence.split("\\|"));
        }

        Intent launch(String code) {
            Action a = mData.get(code);
            if (a != null && (a.callback == null || a.callback.allowed())) {
                ComponentName cn = ComponentName.unflattenFromString(a.action);
                if (cn != null) {
                    return new Intent(Intent.ACTION_MAIN).setComponent(cn);
                } else {
                    return new Intent(a.action);
                }
            }
            return null;
        }
    }

    private static final int SECRETCODE_MIN_LENGTH = "*#06#".length();

    private static final SecretCode kSecretCode;
    static {
        kSecretCode = SecretCode.newInstance()
            .build("*#6801#", "android.settings.DEVICE_INFO_SETTINGS", "*#6802#")  // Software info
            .buildKey("*#6803#", "com.mediatek.engineermode/com.mediatek.engineermode.EngineerMode", "alternative.engineer_mode") // Engineering test
            .buildKey("*#6804#", "com.freeme.intent.action.HARDWARE_TEST", "alternative.factory_test") // Factory test
            .build("*#6807#", "com.freeme.intent.action.SET_FAKE_MODEL") // Fake product mode
            .build("*#6808#", "com.freeme.intent.action.SET_FAKE_DATA") // Fake rom and ram
            .build("*#6810#", "com.freeme.intent.action.SOFTWARE_INFO") // Software info
            .build("*#6812#", "com.freeme.intent.action.HARDWARE_INFO") // Hardware info
            .build("*#6813#", "com.freeme.intent.action.IMEI_SETTING", "alternative.imei_setting") // Imei setting
            .buildKey("*#6814#", "com.freeme.intent.action.TEE_INFO", "alternative.tee_info") //TEE info
            .build("*#6815#", "com.tyd.intent.action.MEMORY_TEST") // Memory test
            .build("*#6821#", "com.freeme.intent.action.AGING_TEST") // Aging test
            .build("*#6878#", "com.freeme.intent.action.UPDATESELF_LABEL") // Platform UpdateSelf info
            .buildExtraKey("alternative.extra.code", "alternative.extra.action") // Extra
            ;
    }

    public static boolean handleSecretCode(Context context, String input) {
        if (input.length() < SECRETCODE_MIN_LENGTH) {
            return false;
        }

        Intent intent = kSecretCode.launch(input);
        if (intent != null) {
            startSecretCodeActivity(context, intent);
            return true;
        }
        return false;
    }

    private static void startSecretCodeActivity(Context context, Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            if ("eng".equals(Build.TYPE) || "userdebug".equals(Build.TYPE)) {
                throw e;
            } else {
                Log.w(TAG, "SecretCode Activity Not Found", e);
            }
        }
    }
}
