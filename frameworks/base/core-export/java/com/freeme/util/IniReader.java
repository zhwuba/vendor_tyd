package com.freeme.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Loads, edits and saves INI-style configuration files. While loading from and 
 * saving to streams and files, <code>IniReader</code> preserves comments and
 * blank lines as well as the order of sections and lines in general.
 * <p>
 * <code>IniReader</code> assumes configuration files to be split in sections.
 * A section starts out with a header, which consists of the section name
 * enclosed in brackets (<code>'['</code> and <code>']'</code>). Everything
 * before the first section header is ignored when loading from a stream or
 * file. The <code>{@link IniReader.Section}</code> class can be used to load
 * configuration files without sections (ie Java-style properties).
 * <p>
 * A "common section" may be named. All sections inherit the options of this
 * section but can overwrite them.
 * <p>
 * <code>IniReader</code> represents an INI file (or rather, its sections) line
 * by line, as comment, blank and option lines. A comment is a line which has a
 * comment delimiter as its first non-white space character. The default comment
 * delimiters, which may be overwritten, are <code>'#'</code> and
 * <code>';'</code>.
 * <p>
 * A blank line is any line that consists only of white space.
 * <p>
 * Everything else is an option line. Option names and values are separated by
 * option delimiters <code>'='</code>, <code>':'</code> or white space (spaces
 * and tabs).
 * <p>
 * Here's a minimal example. Suppose, we have this in a file called
 * <code>users.ini</code>:
 * <pre>
 *   [root]
 *   role = administrator
 *   last_login = 2003-05-04
 *
 *   [joe]
 *   role = author
 *   last_login = 2003-05-13
 * </pre>
 * Let's load that file:
 * <pre>
 *   IniReader users = new IniReader();
 *   users.load("users.ini");
 * </pre>
 * <p>
 * IniReader provides services simliar to the standard Java API class
 * <code>java.util.Properties</code>. It uses its own parser, though, which
 * differs in these respects from that of <code>Properties</code>:
 * <ul>
 * <li>Line continuations (backslashes at the end of an option line) are not
 * supported.</li>
 * <li>No kind of character escaping is performed or recognized. Characters are
 * read and written in in the default character encoding. If you want to use a
 * different character encoding, use the {@link #load(InputStreamReader)}
 * method with a reader tuned to the desired character encoding.</li>
 * <li>As a consequence, option names may not contain option/value separators
 * (normally <code>'='</code>, <code>':'</code> and white space).</li>
 * </ul>
 *
 * @hide
 */
public class IniReader {

    private static boolean DEFAULT_CASE_SENSITIVITY = false;

    private Map<String, Section> sections;
    private List<String> sectionOrder;
    private String commonName;
    private char[] commentDelims;
    private boolean isCaseSensitive;

    /**
     * Constructs new bare IniReader instance.
     */
    public IniReader() {
        this(null, null);
    }

    /**
     * Constructs new bare IniReader instance specifying case-sensitivity.
     *
     * @param isCaseSensitive section and option names are case-sensitive if
     *    this is true
     */
    public IniReader(boolean isCaseSensitive) {
        this(null, null, isCaseSensitive);
    }

    /**
     * Constructs new IniReader instance with a common section. Options in the
     * common section are used as defaults for all other sections.
     *
     * @param commonName name of the common section
     */
    public IniReader(String commonName) {
        this(commonName, null);
    }

    /**
     * Constructs new IniReader instance with a common section. Options in the
     * common section are used as defaults for all other sections.
     *
     * @param commonName name of the common section
     * @param isCaseSensitive section and option names are case-sensitive if
     *    this is true
     */
    public IniReader(String commonName, boolean isCaseSensitive) {
        this(commonName, null, isCaseSensitive);
    }

    /**
     * Constructs new IniReader defining comment delimiters.
     *
     * @param delims an array of characters to be recognized as starters of
     *    comment lines; the first of them will be used for newly created
     *    comments
     */
    public IniReader(char[] delims) {
        this(null, delims);
    }

    /**
     * Constructs new IniReader defining comment delimiters.
     *
     * @param delims an array of characters to be recognized as starters of
     *    comment lines; the first of them will be used for newly created
     *    comments
     * @param isCaseSensitive section and option names are case-sensitive if
     *    this is true
     */
    public IniReader(char[] delims, boolean isCaseSensitive) {
        this(null, delims, isCaseSensitive);
    }

    /**
     * Constructs new IniReader instance with a common section, defining
     * comment delimiters. Options in the common section are used as defaults
     * for all other sections.
     *
     * @param commonName name of the common section
     * @param delims an array of characters to be recognized as starters of
     *    comment lines; the first of them will be used for newly created
     *    comments
     */
    public IniReader(String commonName, char[] delims) {
        this(commonName, delims, DEFAULT_CASE_SENSITIVITY);
    }

    /**
     * Constructs new IniReader instance with a common section, defining
     * comment delimiters. Options in the common section are used as defaults
     * for all other sections.
     *
     * @param commonName name of the common section
     * @param delims an array of characters to be recognized as starters of
     *    comment lines; the first of them will be used for newly created
     *    comments
     */
    public IniReader(String commonName, char[] delims, boolean isCaseSensitive) {
        this.sections = new HashMap<String, Section>();
        this.sectionOrder = new LinkedList<String>();
        this.isCaseSensitive = isCaseSensitive;
        if (commonName != null) {
            this.commonName = commonName;
            addSection(this.commonName);
        }
        this.commentDelims = delims;
    }

    /**
     * Returns the value of a given option in a given section or null if either
     * the section or the option don't exist. If a common section was defined
     * options are also looked up there if they're not present in the specific
     * section.
     *
     * @param section the section's name
     * @param option the option's name
     * @return the option's value
     * @throws NullPointerException any of the arguments is <code>null</code>
     */
    public String get(String section, String option) {
        if (hasSection(section)) {
            Section sect = getSection(section);
            if (sect.hasOption(option)) {
                return sect.get(option);
            }
            if (this.commonName != null) {
                return getSection(this.commonName).get(option);
            }
        }
        return null;
    }

    /**
     * Checks whether an option exists in a given section. Options in the
     * common section are assumed to not exist in particular sections,
     * unless they're overwritten.
     *
     * @param section the section's name
     * @param option the option's name
     * @return true if the given section has the option
     */
    public boolean hasOption(String section, String option) {
        return hasSection(section) && getSection(section).hasOption(option);
    }

    /**
     * Checks whether a section with a particular name exists in this instance.
     *
     * @param name the name of the section
     * @return true if the section exists
     */
    public boolean hasSection(String name) {
        return this.sections.containsKey(normSection(name));
    }

    /**
     * Adds a section if it doesn't exist yet.
     *
     * @param name the name of the section
     * @return <code>true</code> if the section didn't already exist
     * @throws IllegalArgumentException the name is illegal, ie contains one
     *      of the characters '[' and ']' or consists only of white space
     */
    public boolean addSection(String name) {
        String normName = normSection(name);
        if (!hasSection(normName)) {
            // Section constructor might throw IllegalArgumentException
            Section section = new Section(normName, this.commentDelims, this.isCaseSensitive);
            this.sections.put(normName, section);
            this.sectionOrder.add(normName);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns all section names in this instance minus the common section if
     * one was defined.
     *
     * @return list of the section names in original/insertion order
     */
    public List<String> sectionNames() {
        List<String> sectList = new ArrayList<String>(this.sectionOrder);
        if (this.commonName != null) {
            sectList.remove(this.commonName);
        }
        return sectList;
    }

    /**
     * Returns all option names of a section, not including options from the
     * common section.
     *
     * @param section the section's name
     * @return list of option names or empty list
     */
    public List<String> optionNames(String section) {
        if (hasSection(section)) {
            return getSection(section).optionNames();
        }
        return Collections.EMPTY_LIST;
    }

    /**
     * Loads INI formatted input from a file into this instance, using the
     * default character encoding. Everything in the file before the first
     * section header is ignored.
     *
     * @param filename file to read from
     * @throws IOException at an I/O problem
     */
    public void load(String filename) throws IOException {
        load(new File(filename));
    }

    /**
     * Loads INI formatted input from a file into this instance, using the
     * default character encoding. Everything in the file before the first
     * section header is ignored.
     *
     * @param file file to read from
     * @throws IOException at an I/O problem
     */
    public void load(File file) throws IOException {
        InputStream in = new FileInputStream(file);
        load(in);
        in.close();
    }

    /**
     * Loads INI formatted input from a stream into this instance, using the
     * default character encoding. This method takes an <code>InputStream</code>
     * for maximum flexibility, internally it does use a reader (using the
     * default character encoding) for character based input. Everything in the
     * stream before the first section header is ignored.
     *
     * @param stream where to read from
     * @throws IOException at an I/O problem
     */
    public void load(InputStream stream) throws IOException {
        load(new InputStreamReader(stream));
    }

    /**
     * Loads INI formatted input from a stream reader into this instance.
     * Everything in the stream before the first section header is ignored.
     *
     * @param streamReader where to read from
     * @throws IOException at an I/O problem
     */
    public void load(InputStreamReader streamReader) throws IOException {
        BufferedReader reader = new BufferedReader(streamReader);
        String curSection = null;
        String line = null;

        while ((line != null) || ((line = reader.readLine()) != null)) {
            line = line.trim();
            if (line.length() > 0 && line.charAt(0) == Section.HEADER_START) {
                int endIndex = line.indexOf(Section.HEADER_END);
                if (endIndex >= 0) {
                    curSection = line.substring(1, endIndex);
                    addSection(curSection);
                }
            }
            line = null; // Clear for next section line if possible.
            if (curSection != null) {
                Section sect = getSection(curSection);
                line = sect.load(reader);
            }
        }
    }

    /**
     * Returns a section by name or <code>null</code> if not found.
     *
     * @param name the section's name
     * @return the section
     */
    private Section getSection(String name) {
        return sections.get(normSection(name));
    }

    /**
     * Normalizes an arbitrary string for use as a section name. Currently
     * only makes the string lower-case (provided this instance isn't case-
     * sensitive) and trims leading and trailing white space. Note that
     * normalization isn't enforced by the Section class.
     *
     * @param name the string to be used as section name
     * @return a normalized section name
     */
    private String normSection(String name) {
        if (!this.isCaseSensitive) {
            name = name.toLowerCase();
        }
        return name.trim();
    }

    /**
     * Loads, edits and saves a section of an INI-style configuration file. This
     * class does actually belong to the internals of {@link IniReader} and
     * should rarely ever be used directly. It's exposed because it can be
     * useful for plain, section-less configuration files (Java-style
     * properties, for example).
     */
    public static class Section {

        private String name;
        private Map<String, Option> options;
        private char[] optionDelims;
        private char[] optionDelimsSorted;
        private char[] commentDelims;
        private char[] commentDelimsSorted;
        private boolean isCaseSensitive;

        private static final char[] DEFAULT_OPTION_DELIMS = new char[] { '=', ':' };
        private static final char[] DEFAULT_COMMENT_DELIMS = new char[] { '#', ';' };
        private static final char[] OPTION_DELIMS_WHITESPACE = new char[] { ' ', '\t' };
        private static final boolean DEFAULT_CASE_SENSITIVITY = false;

        public static final char HEADER_START = '[';
        public static final char HEADER_END = ']';
        private static final char[] INVALID_NAME_CHARS = { HEADER_START, HEADER_END };

        /**
         * Constructs a new section.
         *
         * @param name the section's name
         * @throws IllegalArgumentException the section's name is illegal
         */
        public Section(String name) {
            this(name, null);
        }

        /**
         * Constructs a new section, specifying case-sensitivity.
         *
         * @param name the section's name
         * @param isCaseSensitive option names are case-sensitive if this is true
         * @throws IllegalArgumentException the section's name is illegal
         */
        public Section(String name, boolean isCaseSensitive) {
            this(name, null, isCaseSensitive);
        }

        /**
         * Constructs a new section, defining comment delimiters.
         *
         * @param name the section's name
         * @param delims an array of characters to be recognized as starters of
         *    comment lines; the first of them will be used for newly created
         *    comments
         * @throws IllegalArgumentException the section's name is illegal
         */
        public Section(String name, char[] delims) {
            this(name, delims, DEFAULT_CASE_SENSITIVITY);
        }

        /**
         * Constructs a new section, defining comment delimiters.
         *
         * @param name the section's name
         * @param delims an array of characters to be recognized as starters of
         *    comment lines; the first of them will be used for newly created
         *    comments
         * @param isCaseSensitive option names are case-sensitive if this is true
         * @throws IllegalArgumentException the section's name is illegal
         */
        public Section(String name, char[] delims, boolean isCaseSensitive) {
            if (!validName(name)) {
                throw new IllegalArgumentException("Illegal section name:" + name);
            }
            this.name = name;
            this.isCaseSensitive = isCaseSensitive;
            this.options = new HashMap<String, Option>();
            this.optionDelims = DEFAULT_OPTION_DELIMS;
            this.commentDelims = (delims == null ? DEFAULT_COMMENT_DELIMS : delims);
            // sorting so we can later use binary search
            this.optionDelimsSorted = new char[this.optionDelims.length];
            System.arraycopy(this.optionDelims, 0, this.optionDelimsSorted, 0, this.optionDelims.length);
            this.commentDelimsSorted = new char[this.commentDelims.length];
            System.arraycopy(this.commentDelims, 0, this.commentDelimsSorted, 0, this.commentDelims.length);
            Arrays.sort(this.optionDelimsSorted);
            Arrays.sort(this.commentDelimsSorted);
        }

        /**
         * Returns the names of all options in this section.
         *
         * @return list of names of this section's options in
         *      original/insertion order
         */
        public List<String> optionNames() {
            return new ArrayList<String>(this.options.keySet());
        }

        /**
         * Checks whether a given option exists in this section.
         *
         * @param name the name of the option to test for
         * @return true if the option exists in this section
         */
        public boolean hasOption(String name) {
            return this.options.containsKey(normOption(name));
        }

        /**
         * Returns an option's value.
         *
         * @param option the name of the option
         * @return the requested option's value or <code>null</code> if no
         *      option with the specified name exists
         */
        public String get(String option) {
            String normed = normOption(option);
            if (hasOption(normed)) {
                return getOption(normed).value();
            }
            return null;
        }

        /**
         * Sets an option's value and creates the option if it doesn't exist.
         *
         * @param option the option's name
         * @param value the option's value
         * @throws IllegalArgumentException the option name is illegal,
         *      ie contains a '=' character or consists only of white space
         */
        public void set(String option, String value) {
            set(option, value, this.optionDelims[0]);
        }

        /**
         * Sets an option's value and creates the option if it doesn't exist.
         *
         * @param option the option's name
         * @param value the option's value
         * @param delim the delimiter between name and value for this option
         * @throws IllegalArgumentException the option name is illegal,
         *      ie contains a '=' character or consists only of white space
         */
        public void set(String option, String value, char delim) {
            String normed = normOption(option);
            if (hasOption(normed)) {
                getOption(normed).set(value);
            } else {
                // Option constructor might throw IllegalArgumentException
                Option opt = new Option(normed, value, delim);
                this.options.put(normed, opt);
            }
        }

        /**
         * Loads options from a reader into this instance. Will read from the
         * stream until it hits a section header, ie a '[' character, and resets
         * the reader to point to this character.
         *
         * @param reader where to read from
         * @throws IOException at an I/O problem
         */
        public String load(BufferedReader reader) throws IOException {
            String line;
            while ((line = reader.readLine()) != null) {
                line = line.trim();

                // Check for section header
                if (line.length() > 0 && line.charAt(0) == HEADER_START) {
                    return line;
                }

                int delimIndex = -1;
                // blank line
                if (line.equals("")) {
                    // ignore blank line
                }
                // comment line
                else if ((delimIndex = Arrays.binarySearch(this.commentDelimsSorted, line.charAt(0))) >= 0) {
                    // ignore comment line
                }
                // option line
                else {
                    delimIndex = -1;
                    int delimNum = -1;
                    int lastSpaceIndex = -1;
                    for (int i = 0, l = line.length(); i < l && delimIndex < 0; i++) {
                        delimNum = Arrays.binarySearch(this.optionDelimsSorted, line.charAt(i));
                        if (delimNum >= 0) {
                            delimIndex = i;
                        } else {
                            boolean isSpace = Arrays.binarySearch(Section.OPTION_DELIMS_WHITESPACE, line.charAt(i)) >= 0;
                            if (!isSpace && lastSpaceIndex >= 0) {
                                break;
                            } else if (isSpace) {
                                lastSpaceIndex = i;
                            }
                        }
                    }
                    // delimiter at start of line
                    if (delimIndex == 0) {
                        // XXX what's a man got to do?
                    }
                    // no delimiter found
                    else if (delimIndex < 0) {
                        if (lastSpaceIndex < 0) {
                            this.set(line, "");
                        } else {
                            this.set(line.substring(0, lastSpaceIndex), line.substring(lastSpaceIndex + 1));
                        }
                    }
                    // delimiter found
                    else {
                        this.set(line.substring(0, delimIndex), line.substring(delimIndex + 1), line.charAt(delimIndex));
                    }
                }
            }
            return null;
        }

        /**
         * Returns an actual Option instance.
         *
         * @param name the name of the option, assumed to be normed already (!)
         * @return the requested Option instance
         * @throws NullPointerException if no option with the specified name exists
         */
        private Option getOption(String name) {
            return this.options.get(name);
        }

        /**
         * Checks a string for validity as a section name. It can't contain the
         * characters '[' and ']'. An empty string or one consisting only of
         * white space isn't allowed either.
         *
         * @param name the name to validate
         * @return true if the name validates as a section name
         */
        private static boolean validName(String name) {
            if (name.trim().equals("")) {
                return false;
            }
            for (int i = 0; i < INVALID_NAME_CHARS.length; i++) {
                if (name.indexOf(INVALID_NAME_CHARS[i]) >= 0) {
                    return false;
                }
            }
            return true;
        }

        /**
         * Normalizes an arbitrary string for use as an option name, ie makes
         * it lower-case (provided this section isn't case-sensitive) and trims
         * leading and trailing white space.
         *
         * @param name the string to be used as option name
         * @return a normalized option name
         */
        private String normOption(String name) {
            if (!this.isCaseSensitive) {
                name = name.toLowerCase();
            }
            return name.trim();
        }
    }

    private static class Option {

        private String name;
        private String value;
        private char separator;

        private static final String ILLEGAL_VALUE_CHARS = "\n\r";

        public Option(String name, String value, char separator) {
            if (!validName(name, separator)) {
                throw new IllegalArgumentException("Illegal option name:" + name);
            }
            this.name = name;
            this.separator = separator;
            set(value);
        }

        public String name() {
            return this.name;
        }

        public String value() {
            return this.value;
        }

        public void set(String value) {
            if (value == null) {
                this.value = value;
            } else {
                StringTokenizer st = new StringTokenizer(value.trim(), ILLEGAL_VALUE_CHARS);
                StringBuffer sb = new StringBuffer();
                // XXX this might not be particularly efficient
                while (st.hasMoreTokens()) {
                    sb.append(st.nextToken());
                }
                this.value = sb.toString();
            }
        }

        @Override
        public String toString() {
            return "%s %s %s".format(this.name, this.separator, this.value);
        }

        private static boolean validName(String name, char separator) {
            if (name.trim().equals("")) {
                return false;
            }
            if (name.indexOf(separator) >= 0) {
                return false;
            }
            return true;
        }
    }
}
