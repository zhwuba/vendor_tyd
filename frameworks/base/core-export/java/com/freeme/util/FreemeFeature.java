package com.freeme.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import android.app.ActivityThread;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;

public final class FreemeFeature {
    private static final String TAG = "FreemeFeature";

    private static final String DEFAULT_CONFIG_PROPERTY =
            "persist.freeme.feature_cfgfile";
    private static final String DEFAULT_CONFIG_FILENAME =
            "/system/etc/freeme/freeme_default.ini";

    private static final String  DEFAULT_COMMON_SECTION = "system";
    private static final boolean DEFAULT_CASE_SENSITIVITY = false;

    private static String sConfigFilename;
    private static IniReader sIniReader;

    static {
        Log.v(TAG, "Init Start...");
        try {
            init();
        } catch (IOException e) {
            Log.e(TAG, "Init failed.", e);
        }
        Log.v(TAG, "Init completely.");
    }

    private static void init() throws IOException {
        String filename = SystemProperties.get(DEFAULT_CONFIG_PROPERTY);
        if (!TextUtils.isEmpty(filename) && new File(filename).exists()) {
            sConfigFilename = filename;
        } else if (new File(DEFAULT_CONFIG_FILENAME).exists()) {
            sConfigFilename = DEFAULT_CONFIG_FILENAME;
        }

        if (!TextUtils.isEmpty(sConfigFilename)) {
            IniReader reader = new IniReader(DEFAULT_COMMON_SECTION, DEFAULT_CASE_SENSITIVITY);
            reader.load(sConfigFilename);
            sIniReader = reader;
            return;
        }

        throw new IOException("Cannot find feature configuration file.");
    }

    /**
     * Get the value for the given section and key.
     * @return if the section or key isn't found, return def if it isn't null,
     *      or an empty string otherwise
     * @throws NullPointerException if the section or key null
     */
    public static String get(String section, String key, String def) {
        if (sIniReader != null) {
            String value = sIniReader.get(section, key);
            if (!TextUtils.isEmpty(value)) {
                return value;
            }
        }
        return def != null ? def : "";
    }

    /**
     * Get the value for the given section and key.
     * @return an empty string if the section or key isn't found
     * @throws NullPointerException if the section or key null
     */
    public static String get(String section, String key) {
        return get(section, key, null);
    }

    /**
     * Get the value for the given section and key, and return as an integer.
     * @return the key parsed as an integer, or def if the key isn't found or
     *         cannot be parsed
     * @throws NullPointerException if the section or key null
     */
    public static int getInt(String section, String key, int def) {
        if (sIniReader != null) {
            String value = sIniReader.get(section, key);
            if (!TextUtils.isEmpty(value)) {
                return Integer.parseInt(value);
            }
        }
        return def;
    }

    /**
     * Get the value for the given section and key, and return as a long.
     * @return the key parsed as a long, or def if the key isn't found or
     *         cannot be parsed
     * @throws NullPointerException if the section or key null
     */
    public static long getLong(String section, String key, long def) {
        if (sIniReader != null) {
            String value = sIniReader.get(section, key);
            if (!TextUtils.isEmpty(value)) {
                return Long.parseLong(value);
            }
        }
        return def;
    }

    /**
     * Get the value for the given section and key, returned as a boolean.
     * Values 'n', 'no', '0', 'false' or 'off' are considered false.
     * Values 'y', 'yes', '1', 'true' or 'on' are considered true.
     * (case sensitive).
     * If the key does not exist, or has any other value, then the default
     * result is returned.
     * @return the key parsed as a boolean, or def if the key isn't found or is
     *         not able to be parsed as a boolean.
     * @throws NullPointerException if the section or key null
     */
    public static boolean getBoolean(String section, String key, boolean def) {
        if (sIniReader != null) {
            String value = sIniReader.get(section, key);
            if (!TextUtils.isEmpty(value)) {
                switch (value) {
                    case "0":
                    case "n":
                    case "no":
                    case "false":
                    case "off":
                        return false;
                    case "1":
                    case "y":
                    case "yes":
                    case "true":
                    case "on":
                        return true;
                    default:
                        // using default
                        break;
                }
            }
        }
        return def;
    }

    public static boolean isSupported(String section, String key) {
        return getBoolean(section, key, false);
    }

    public static boolean has(String section, String key) {
        return sIniReader != null && sIniReader.hasOption(section, key);
    }

    public static void dump(PrintWriter pw) {
        pw.println("DUMP FEATURE CONFIGURATION FILE");
        pw.println("========================================");
        pw.print("File: "); pw.println(sConfigFilename);
        pw.print("Package: "); pw.println(ActivityThread.currentPackageName());
        pw.println("========================================");
        if (sIniReader != null) {
            IniReader reader = sIniReader;

            List<String> sectionNames = reader.sectionNames();
            if (!sectionNames.contains(DEFAULT_COMMON_SECTION)) {
                sectionNames.add(0, DEFAULT_COMMON_SECTION);
            }

            for (String section : sectionNames) {
                pw.println(String.format("[%s]", section));
                List<String> optionNames = reader.optionNames(section);
                for (String option : optionNames) {
                    String value = reader.get(section, option);
                    pw.println(String.format("%s = %s", option, value));
                }
            }
        } else {
            pw.println("Invalid.");
        }
    }

    // -------------------------------------------------------------------------

    public static String getLocal(String key, String def) {
        return get(ActivityThread.currentPackageName(), key, def);
    }

    public static String getLocal(String key) {
        return getLocal(key, null);
    }

    public static int getLocalInt(String key, int def) {
        return getInt(ActivityThread.currentPackageName(), key, def);
    }

    public static long getLocalLong(String key, long def) {
        return getLong(ActivityThread.currentPackageName(), key, def);
    }

    public static boolean getLocalBoolean(String key, boolean def) {
        return getBoolean(ActivityThread.currentPackageName(), key, def);
    }

    public static boolean isLocalSupported(String key) {
        return getLocalBoolean(key, false);
    }

    // -------------------------------------------------------------------------

    private static final String SYSTEM_SECTION = DEFAULT_COMMON_SECTION;

    public static String getSystem(String key, String def) {
        return get(SYSTEM_SECTION, key, def);
    }

    public static String getSystem(String key) {
        return getSystem(key, null);
    }

    public static int getSystemInt(String key, int def) {
        return getInt(SYSTEM_SECTION, key, def);
    }

    public static long getSystemLong(String key, long def) {
        return getLong(SYSTEM_SECTION, key, def);
    }

    public static boolean getSystemBoolean(String key, boolean def) {
        return getBoolean(SYSTEM_SECTION, key, def);
    }

    public static boolean isSystemSupported(String key) {
        return getSystemBoolean(key, false);
    }

    // -------------------------------------------------------------------------

    private FreemeFeature() {
        // Do not initialise.
    }
}
