package com.freeme.util;

import android.annotation.NonNull;
import android.annotation.Nullable;
import android.os.SystemProperties;

public final class FreemeOption {
    private static final int FREEME_NAVIGATION = getInt("ro.vendor.freeme.navigation");
    public static final class Navigation {
        public static final int FREEME_NAVIGATION_COLLAPSABLE   = 1 << 0; // 1
        public static final int FREEME_NAVIGATION_FINGERPRINT   = 1 << 1; // 2
        public static final int FREEME_NAVIGATION_SWIPEUP       = 1 << 2; // 4

        public static boolean supports(int op) {
            return (FREEME_NAVIGATION & op) != 0;
        }
    }

    public static final int FREEME_SCREEN_GESTURE_WAKEUP_SUPPORT = getInt("ro.vendor.freeme.screen_gesture_wakeup");
    public static final class SmartWake {
        public static boolean supports(int op) {
            switch (op) {
                case -1:
                    return FREEME_SCREEN_GESTURE_WAKEUP_SUPPORT != 0;
                default:
                    return (FREEME_SCREEN_GESTURE_WAKEUP_SUPPORT & op) != 0;
            }
        }
    }
    public static final boolean FREEME_SCREEN_DOUBLETAP_WAKEUP_SUPPORT = getBoolean("ro.vendor.freeme.screendoubletapwakeup");
    public static final boolean FREEME_HOME_DOUBLETAP_WAKEUP_SUPPORT = getBoolean("ro.vendor.freeme.home_doubletap_wakeup");

    public static final boolean FREEME_ALTER_ROM_RAM = getBoolean("ro.vendor.freeme.fake_rom_ram");

    public static final boolean FREEME_ALTER_PRODUCT_BRAND = getBoolean("ro.vendor.freeme.alter.product_brand");
    public static final boolean FREEME_ALTER_PRODUCT_MODEL = getBoolean("ro.vendor.freeme.alter.product_model");

    public static final String FREEME_PRODUCT_FOR = get("ro.vendor.freeme.build.production");
    public static final boolean FREEME_PRODUCT_FOR_CTS = "cts".equals(FREEME_PRODUCT_FOR);
    public static final boolean FREEME_PRODUCT_FOR_CTA = "cta".equals(FREEME_PRODUCT_FOR);

    public static final boolean FREEME_FINGERPRINT_FRONT_SUPPORT = getBoolean("ro.vendor.freeme.fingerprint_front");
    public static final int FREEME_FINGERPRINT_CUSTOM_SUPPORT = getInt("ro.vendor.freeme.fingerprint_custom");
    public static class FingerprintCustom {
        public static boolean supports(int op) {
            switch (op) {
                case -1: return FREEME_FINGERPRINT_CUSTOM_SUPPORT != 0;
                default: return (FREEME_FINGERPRINT_CUSTOM_SUPPORT & (1<<op)) != 0;
            }
        }
    }
    public static final boolean FREEME_FINGERPRINT_CUSTOM2_SUPPORT = getBoolean("ro.vendor.freeme.fingerprint_custom2");
    public static final boolean FREEME_DUAL_SIM_RINGTONE_SUPPORT = getBoolean("ro.vendor.freeme.ringtone_dual_sim");
    public static final boolean FREEME_MESSAGE_RINGTONE_SUPPORT  = getBoolean("ro.vendor.freeme.ringtone_message");
    public static final boolean FREEME_NOTIFICATION_RINGTONE_SUPPORT  = getBoolean("ro.vendor.freeme.ringtone_notification");
    public static final boolean FREEME_CUSTOM_RINGTONE_SUPPORT   = getBoolean("ro.vendor.freeme.ringtone_custom");

    public static final boolean FREEME_ONEHAND_SUPPORT = getBoolean("ro.vendor.freeme.onehand_support");
    public static final boolean FREEME_ONESTEP_SUPPORT = getBoolean("ro.vendor.freeme.onestep_support");

    public static final boolean FREEME_SUPER_SHOT = getBoolean("ro.vendor.freeme.super_shot");

    public static final boolean FREEME_GESTURE_TOUCH_THREE_POINT = getBoolean("ro.vendor.freeme.gesture_touch_three_p");
    public static final boolean FREEME_GESTURE_TOUCH_FIVE_POINTE = getBoolean("ro.vendor.freeme.gesture_touch_five_p");
    public static final boolean FREEME_GESTURE_TOUCH_TWO_POINT_LONGPRESS = getBoolean("ro.vendor.freeme.gesture_touch_two_lp");

    public static final boolean FREEME_NIGHTMARE_SUPPORT = getBoolean("ro.vendor.freeme.nightmare");

    public static final boolean FREEME_TOUCH_PROTECT = getBoolean("ro.vendor.freeme.touch_protect");

    public static final boolean FREEME_REVERSE_SILENT = getBoolean("ro.vendor.freeme.reverse_silent");

    public static final boolean FREEME_BOOT_AUDIO_SUPPORT = getBoolean("ro.vendor.freeme.boot_audio_support");
    public static final boolean FREEME_SHUT_AUDIO_SUPPORT = getBoolean("ro.vendor.freeme.shut_audio_support");

    public static final boolean FREEME_HW_SENSOR_PROXIMITY = getBoolean("ro.vendor.freeme.hw_sensor_proximity");

    //*/ freeme.zhaozehong, 20180127. for freemeOS, smart dial or answer incoming call
    public static final boolean FREEME_SMART_DIAL_ANSWER = getBoolean("ro.vendor.freeme.smart_dial_answer");
    //*/

    public static final int FREEME_NON_TOUCH_OPERATION_SUPPORT = getInt("ro.vendor.freeme.non_touch_operation");
    public static final class NonTouch {
        public static boolean supports(int op) {
            return (FREEME_NON_TOUCH_OPERATION_SUPPORT & op) != 0;
        }
    }

    public static final boolean FREEME_PERMISSION_PREAUTH = getBoolean("ro.vendor.freeme.permision_pre_auth");

    public static final boolean FREEME_FAKE_STARTING_WINDOW_SUPPORT = getBoolean("ro.vendor.freeme.fake_staring_window", true);

    public static final boolean FREEME_APPLOCK_SUPPORT = getBoolean("ro.vendor.freeme.applock_support");

    //*/ freeme.zhaozehong, 20180911. GameMode
    public static final boolean FREEME_GAMEMODE_SUPPORT = getBoolean("ro.vendor.freeme.gamemode_support");
    public static final boolean FREEME_GAMEMODE_TOOL_SUPPORT = getBoolean("ro.vendor.freeme.gamemode_tool");
    //*/

    public static final boolean FREEME_MULTIAPP_SUPPORT = getBoolean("ro.vendor.freeme.multiapp_support");

    //*/ ZY.weiyunsheng, 20190225. glove mode.
    public static final boolean FREEME_GLOVE_MODE_SUPPORT = getBoolean("ro.vendor.freeme.glove_mode");
    //*/

    //*/ DZ.zhangzhao, 20190428. volume wakeup
    public static final boolean VOLUME_WAKEUP_SUPPORT = getBoolean("ro.vendor.freeme.volume_wakeup");
    //*/

    //*/ DZ.zhangzhao, 20190507. Split-Screen.
    public static final boolean SPLIT_SCREEN_SUPPORT = getBoolean("ro.vendor.freeme.split_screen");
    //*/

    //*/ DZ.zhangling, 20190517. Record-Screen.
    public static final boolean RECORD_SCREEN_SUPPORT = getBoolean("ro.vendor.freeme.record_screen");
    //*/

    // =====================================================
    @NonNull
    public static String get(@NonNull String key) {
        return SystemProperties.get(key);
    }

    @NonNull
    public static String get(@NonNull String key, @Nullable String def) {
        return SystemProperties.get(key, def);
    }

    public static int getInt(String key) {
        return SystemProperties.getInt(key, 0);
    }

    public static int getInt(@NonNull String key, int def) {
        return SystemProperties.getInt(key, def);
    }

    public static long getLong(@NonNull String key, long def) {
        return SystemProperties.getLong(key, def);
    }

    /**
     * Get boolean value of key.
     * @param key length must less than 31, or will have JE
     */
    public static boolean getBoolean(String key) {
        return SystemProperties.getBoolean(key, false);
    }

    public static boolean getBoolean(@NonNull String key, boolean def) {
        return SystemProperties.getBoolean(key, def);
    }
}
