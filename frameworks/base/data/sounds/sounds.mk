LOCAL_PATH := vendor/tyd/frameworks/base/data/sounds

TYD_SOUNDS_PROJECT ?= $(TYD_PRODUCT_CUSTOM_HOME)/sounds
TYD_SOUNDS_DEVICE ?= device/tyd/$(TYD_PRODUCT_DEVICE)/sounds
TYD_SOUNDS_BOARD ?= device/tyd/$(TYD_PRODUCT_BOARD)/sounds
TYD_SOUNDS_PLATFORM ?= device/tyd/$(TYD_PRODUCT_PLATFORM)/sounds
TYD_SOUNDS_COMMON ?= device/tyd/common/sounds
TYD_SOUNDS_BASE ?= $(LOCAL_PATH)

define sounds-copyable-uniq
$(if $1,$(firstword $1) $(call sounds-copyable-uniq,$(filter-out %/$(notdir $(firstword $1)),$1)))
endef

# $(1): search path list
# $(2): search child path list
define sounds-path-search-for
$(strip \
    $(foreach c,$(2), \
        $(eval _sounds=$(foreach p,$(1),$(wildcard $(p)/$(c)/*.*))) \
        $(eval _sounds=$(call sounds-copyable-uniq,$(_sounds))) \
        $(foreach f,$(_sounds), \
            $(f):system/media/audio/$(c)/$(notdir $(f)) \
         ) \
     ) \
)
endef

# $(1): search path list
define sounds-makefile-search-for
    $(foreach f,$(1),$(call inherit-product-if-exists,$(f)/sounds.mk))
endef


ifeq ($(strip $(TYD_SOUNDS_EXTERNAL)),yes)

PRODUCT_COPY_FILES += \
    $(call sounds-path-search-for, \
        $(TYD_SOUNDS_PROJECT) \
        $(TYD_SOUNDS_DEVICE) \
        $(TYD_SOUNDS_BOARD) \
        $(TYD_SOUNDS_PLATFORM) \
        $(TYD_SOUNDS_COMMON) \
        $(TYD_SOUNDS_BASE), \
        alarms notifications ringtones ui)

$(call sounds-makefile-search-for, \
    $(TYD_SOUNDS_PROJECT) \
    $(TYD_SOUNDS_DEVICE) \
    $(TYD_SOUNDS_BOARD) \
    $(TYD_SOUNDS_PLATFORM) \
    $(TYD_SOUNDS_COMMON) \
    )

endif
