LOCAL_PATH := vendor/tyd/frameworks/base/data/bootanimation

TYD_BOOTANIMATION_PROJECT ?= $(TYD_PRODUCT_CUSTOM_HOME)/bootanimation
TYD_BOOTANIMATION_DEVICE ?= device/tyd/$(TYD_PRODUCT_DEVICE)/bootanimation
TYD_BOOTANIMATION_BOARD ?= device/tyd/$(TYD_PRODUCT_BOARD)/bootanimation
TYD_BOOTANIMATION_PLATFORM ?= device/tyd/$(TYD_PRODUCT_PLATFORM)/bootanimation
TYD_BOOTANIMATION_COMMON ?= device/tyd/common/bootanimation
TYD_BOOTANIMATION_BASE ?= $(LOCAL_PATH)

define bootanimations-copyable-uniq
$(if $1,$(firstword $1) $(call bootanimations-copyable-uniq,$(filter-out %/$(notdir $(firstword $1)),$1)))
endef

# $(1): file name
# $(2): search path list
define bootanimations-path-search-for
$(strip \
    $(eval _fileName := $(1)) \
    $(eval _bootanimations := $(foreach p,$(2),$(wildcard $(p)/$(_fileName)))) \
    $(eval _bootanimations := $(strip $(call bootanimations-copyable-uniq,$(_bootanimations)))) \
    $(if $(_bootanimations),$(_bootanimations):system/media/$(_fileName)) \
)
endef

# $(1): file name
# $(2): if non-empty, search with resolution
define bootanimations-file-search-for
$(strip \
    $(eval _appendResolution := $(strip $(2))) \
    $(call bootanimations-path-search-for, $(1), \
        $(TYD_BOOTANIMATION_PROJECT) \
        $(TYD_BOOTANIMATION_DEVICE) \
        $(TYD_BOOTANIMATION_BOARD)$(if $(_appendResolution),/$(TYD_LCD_RESOLUTION)) \
        $(TYD_BOOTANIMATION_PLATFORM)$(if $(_appendResolution),/$(TYD_LCD_RESOLUTION)) \
        $(TYD_BOOTANIMATION_COMMON)$(if $(_appendResolution),/$(TYD_LCD_RESOLUTION)) \
        $(TYD_BOOTANIMATION_BASE) \
     ) \
)
endef

################################################################################

ifeq ($(strip $(TYD_BOOT_ANIMATION_CUSTOMIZED)),yes)
  PRODUCT_COPY_FILES += \
    $(call bootanimations-file-search-for,bootanimation.zip,1)
endif

ifneq ($(filter yes yes_no,$(TYD_BOOT_AUDIO_SUPPORT)),)
  PRODUCT_COPY_FILES += \
    $(call bootanimations-file-search-for,$(if $(filter mtk,$(TYD_PRODUCT_CHIPVENDOR)),bootaudio.mp3,bootsound.mp3),)
  PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.boot_audio_support=1
  ifeq ($(strip $(TYD_BOOT_AUDIO_SUPPORT)),yes_no)
    PRODUCT_PROPERTY_OVERRIDES += persist.vendor.tyd.boot_audio_off=1
  endif
endif

ifeq ($(strip $(TYD_SHUT_ANIMATION_SUPPORT)),yes)
  PRODUCT_COPY_FILES += \
    $(call bootanimations-file-search-for,shutdownanimation.zip,1)
  PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.shut_anim_support=1
endif

ifneq ($(filter yes yes_no,$(TYD_SHUT_AUDIO_SUPPORT)),)
  PRODUCT_COPY_FILES += \
    $(call bootanimations-file-search-for,$(if $(filter mtk,$(TYD_PRODUCT_CHIPVENDOR)),shutdownaudio.mp3,shutdownsound.mp3),)
  PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.shut_audio_support=1
  ifeq ($(strip $(TYD_SHUT_AUDIO_SUPPORT)),yes_no)
    PRODUCT_PROPERTY_OVERRIDES += persist.vendor.tyd.shut_audio_off=1
  endif
endif

################################################################################
# Alternative bootanimation.
ifeq ($(strip $(TYD_BOOT_ANIMATION_ALTER)),yes)

  TYD_BOOTANIMATION_ALTERNATIVE_PROJECT ?= $(TYD_BOOTANIMATION_PROJECT)/alternative
  TYD_BOOTANIMATION_ALTERNATIVE_PRODUCT ?= system/media/alternative

  PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.boot_anim_alter=1
  PRODUCT_COPY_FILES += \
    $(foreach f,$(wildcard $(TYD_BOOTANIMATION_ALTERNATIVE_PROJECT)/*),$(f):$(TYD_BOOTANIMATION_ALTERNATIVE_PRODUCT)/$(notdir $(f))) \
    $(foreach f,$(wildcard $(TYD_BOOTANIMATION_ALTERNATIVE_PROJECT)/preview/*.png),$(f):$(TYD_BOOTANIMATION_ALTERNATIVE_PRODUCT)/preview/$(notdir $(f)))

endif
