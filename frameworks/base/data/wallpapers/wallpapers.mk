LOCAL_PATH := vendor/tyd/frameworks/base/data/wallpapers

TYD_WALLPAPERS_PROJECT ?= $(TYD_PRODUCT_CUSTOM_HOME)/wallpapers
TYD_WALLPAPERS_DEVICE ?= device/tyd/$(TYD_PRODUCT_DEVICE)/wallpapers
TYD_WALLPAPERS_BOARD ?= device/tyd/$(TYD_PRODUCT_BOARD)/wallpapers/$(TYD_LCD_RESOLUTION)
TYD_WALLPAPERS_PLATFORM ?= device/tyd/$(TYD_PRODUCT_PLATFORM)/wallpapers/$(TYD_LCD_RESOLUTION)
TYD_WALLPAPERS_COMMON ?= device/tyd/common/wallpapers/$(TYD_LCD_RESOLUTION)

define wallpapers-copyable-uniq
$(if $1,$(firstword $1) $(call wallpapers-copyable-uniq,$(filter-out %/$(notdir $(firstword $1)),$1)))
endef

define wallpapers-copyable-filter
$(filter-out $(addprefix %/,$(2)),$1)
endef

# $(1): search path list
# $(2): search child path list
# $(3): filter out list
define wallpapers-files-search-for
$(strip \
    $(eval _wallpapers=$(foreach p,$(1), \
        $(if $(strip $(2)), \
            $(foreach c,$(2), \
                $(foreach f,$(wildcard $(strip $(p))/$(strip $(c))/*.jpg), \
                    $(f):system/media/wallpaper/$(c)/$(notdir $(f)) \
                 ) \
             ), \
            $(foreach f,$(wildcard $(strip $(p))/*.jpg), \
                $(f):system/media/wallpaper/$(notdir $(f)) \
             ) \
         ) \
     )) \
    $(eval _wallpapers=$(call wallpapers-copyable-uniq,$(_wallpapers))) \
    $(if $(strip $(3)),$(call wallpapers-copyable-filter,$(_wallpapers),$(strip $(3))),$(_wallpapers)) \
)
endef

ifeq ($(strip $(TYD_WALLPAPERS_CUSTOMIZED)),yes)
#  PRODUCT_PROPERTY_OVERRIDES += \
#    ro.config.wallpaper=/system/media/wallpaper/default_wallpaper.jpg \
#    ro.config.lock_wallpaper=/system/media/wallpaper/default_lock_wallpaper.jpg

  PRODUCT_COPY_FILES += \
    $(call wallpapers-files-search-for, \
        $(TYD_WALLPAPERS_PROJECT) $(TYD_WALLPAPERS_DEVICE) $(TYD_WALLPAPERS_BOARD) $(TYD_WALLPAPERS_PLATFORM) $(TYD_WALLPAPERS_COMMON), \
        , \
        $(TYD_WALLPAPERS_FILTER_OUT) \
     )
endif
