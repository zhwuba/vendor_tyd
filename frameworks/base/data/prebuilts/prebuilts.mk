LOCAL_PATH := vendor/tyd/frameworks/base/data/prebuilts

TYD_PREBUILTS_PROJECT ?= $(TYD_PRODUCT_CUSTOM_HOME)/prebuilts
TYD_PREBUILTS_DEVICE ?= device/droi/$(TYD_PRODUCT_DEVICE)/prebuilts

# $(1): search path
# $(2): file name
define prebuilts-module-path-list-for
$(strip \
    $(wildcard $(1)/*/$(2)) \
)
endef

################################################################################

# $(1): search path
define prebuilts-android-module-name-list-for
$(strip \
    $(eval _path := $(strip $(1))) \
    $(eval _moduleMks := $(call prebuilts-module-path-list-for,$(_path),Android.mk)) \
    $(patsubst $(_path)/%/Android.mk,%,$(_moduleMks)) \
)
endef

# app prebuilts
PRODUCT_PACKAGES += \
    $(call prebuilts-android-module-name-list-for,$(TYD_PREBUILTS_PROJECT)) \
    $(call prebuilts-android-module-name-list-for,$(TYD_PREBUILTS_DEVICE))

#-------------------------------------------------------------------------------

# $(1): search path
define prebuilts-copyable-module-path-list-for
$(strip \
    $(eval _path := $(strip $(1))) \
    $(call prebuilts-module-path-list-for,$(_path),Prebuilt.mk) \
)
endef

# other prebuilts
$(foreach f,$(call prebuilts-copyable-module-path-list-for,$(TYD_PREBUILTS_PROJECT)),$(call inherit-product,$(f)))
$(foreach f,$(call prebuilts-copyable-module-path-list-for,$(TYD_PREBUILTS_DEVICE)),$(call inherit-product,$(f)))
