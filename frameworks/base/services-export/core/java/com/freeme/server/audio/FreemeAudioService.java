package com.freeme.server.audio;

import android.content.Context;
import android.media.AudioSystem;
import android.os.SystemProperties;
import android.provider.Settings;

public class FreemeAudioService {

    /**
     * Initialize default stream volume
     * @param context
     */
    public static void initDefaultStreamVolume(Context context) {
        // init defVolume by [sys.audio.config.*] in freeme_default.ini
        final int N = Settings.System.VOLUME_SETTINGS.length;
        for (int streamIndex = 0; streamIndex < N; streamIndex++) {
            int defVolume = com.freeme.util.FreemeFeature.getSystemInt(
                    "sys.audio.config." + Settings.System.VOLUME_SETTINGS[streamIndex],
                    AudioSystem.DEFAULT_STREAM_VOLUME[streamIndex]);
            if (defVolume != AudioSystem.DEFAULT_STREAM_VOLUME[streamIndex]) {
                AudioSystem.DEFAULT_STREAM_VOLUME[streamIndex] = defVolume;

                // re-backup to SettingsProvider db
                Settings.System.putInt(context.getContentResolver(),
                        Settings.System.VOLUME_SETTINGS[streamIndex],
                        defVolume);
            }
        }
    }

    /**
     * Persist ringermode for boot audio
     * @return True is not re-init music volume
     */
    public static boolean ignoreReinitMusicStream() {
        return true;
    }

    /**
     * Persist ringermode for boot audio
     * @param ringerMode
     */
    public static void persistRingerMode(int ringerMode) {
        SystemProperties.set("persist.sys.mute.state", Integer.toString(ringerMode));
    }
}
