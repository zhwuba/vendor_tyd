package com.freeme.server.am;

import android.content.ComponentName;
import android.content.Context;

import com.android.server.am.ActivityRecord;
import com.android.server.am.ActivityStack;

public final class FreemeActivityManagerService extends ActivityManagerServiceAlias {
    private static final String TAG = "FreemeActivityManagerService";

    public FreemeActivityManagerService(Context context) {
        super(context);
    }

    public String topAppName() {
         ActivityStack focusedStack;
         synchronized (this) {
             focusedStack = getFocusedStack();
         }
         ActivityRecord r = focusedStack.topRunningActivityLocked();
         if (r != null) {
             return r.shortComponentName;
         }
        return null;
    }

    public boolean serviceIsRunning(ComponentName serviceCmpName, int curUser) {
        synchronized (this) {
            return mServices.getServicesLocked(curUser).get(serviceCmpName) != null;
        }
    }
}
