package com.freeme.server;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.UserHandle;
import android.database.ContentObserver;
import android.provider.Settings;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;

import com.freeme.provider.FreemeSettings;

/**
 * @hide
 */
public class FreemeScreenOnProximityLock {
    private static final boolean DEBUG = true;
    private static final int EVENT_FAR_AWAY = 2;
    private static final int EVENT_NO_USER_ACTIVITY = 4;
    private static final int EVENT_RELEASE = 3;
    private static final int EVENT_TOO_CLOSE = 1;
    private static final int FIRST_CHANGE_TIMEOUT = 1000;
    private static final String LOG_TAG = "FreemeScreenOnProximityLock";

    private static final String DROI_SCREEN_ON_PROXIMITY_SENSOR = FreemeSettings.System.FREEME_TOUCH_PROTECT_KEY;
    private static final float PROXIMITY_THRESHOLD = 4.0F;
    private static final int RELEASE_DELAY = 300;
    private static int sValidChangeDelay = 10;
    private Context mContext;
    protected Dialog mDialog;
    private SparseArray<Boolean> mDownRecieved = new SparseArray();
    private Handler mHandler;
    private boolean mHeld;
    private boolean mIsFirstChange;
    private Sensor mSensor;
    private MySensorEventListener mSensorEventListener;
    private SensorManager mSensorManager;
    private boolean mTouchProtect = false;
    private boolean mDisableProximitor = false;
    private SettingsObserver mSettingsObserver;
    private boolean mSystemReady;
    private boolean mVolumeDownPressed;
    private boolean mVolumeUpKeyTriggered;

    public FreemeScreenOnProximityLock(Context paramContext) {
        this.mContext = paramContext;
        mSensorEventListener = new MySensorEventListener();

        this.mSensorManager = ((SensorManager) this.mContext
                .getSystemService("sensor"));
        this.mSensor = this.mSensorManager
                .getDefaultSensor(Sensor.TYPE_PROXIMITY);

        this.mHandler = new Handler(this.mContext.getMainLooper()) {
            public void handleMessage(Message paramMessage) {
                synchronized (FreemeScreenOnProximityLock.this) {
                    switch (paramMessage.what) {
                    case EVENT_TOO_CLOSE:
                        if (!FreemeScreenOnProximityLock.this.isHeld())
                            break;
                        Log.d("FreemeScreenOnProximityLock",
                                "too close screen, show hint...");
                        if (FreemeScreenOnProximityLock.this.mDialog != null)
                            break;
                        FreemeScreenOnProximityLock.this.prepareHintDialog();
                        FreemeScreenOnProximityLock.this.mDialog.show();
                        FreemeScreenOnProximityLock.this
                                .enableUserActivity(false);
                        break;
                    case EVENT_FAR_AWAY:
                        Log.d("FreemeScreenOnProximityLock",
                                "far from the screen, hide hint...");
                        if (FreemeScreenOnProximityLock.this.mDialog != null) {
                            FreemeScreenOnProximityLock.this.mDialog
                                    .dismiss();
                            FreemeScreenOnProximityLock.this.mDialog = null;
                        }
                        break;
                    case EVENT_RELEASE:
                        Log.d("FreemeScreenOnProximityLock",
                                "far from the screen for a certain time, release proximity sensor...");
                        FreemeScreenOnProximityLock.this.release();
                        break;
                    case EVENT_NO_USER_ACTIVITY:
                        FreemeScreenOnProximityLock.this
                                .enableUserActivity(true);
                        break;
                    default:
                        return;
                    }
                }
            }
        };
        mSettingsObserver = new SettingsObserver(mHandler);
        registerContentObserver(mContext);
    }

    private void registerContentObserver(Context context){
        context.getContentResolver().registerContentObserver(Settings.System.getUriFor(DROI_SCREEN_ON_PROXIMITY_SENSOR),
                false,
                mSettingsObserver,
                UserHandle.USER_ALL);
    }

    class SettingsObserver extends ContentObserver{
        public SettingsObserver(Handler handler){
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            initState();
        }
    }

    private void initState(){
        if(mSystemReady) {
            mTouchProtect = Settings.System.getIntForUser(mContext.getContentResolver(), DROI_SCREEN_ON_PROXIMITY_SENSOR, 0, UserHandle.USER_CURRENT) > 0;
        }
    }

    public void systemReady(boolean ready){
        mSystemReady = ready;
        initState();
    }

    public boolean releaseScreenOnProximitySensor()	{
        if(mDisableProximitor == false){
            return false;
        }
        if (!this.mSystemReady) {
            return false;
        }
        this.mDisableProximitor = false;
        return release();
    }

    public void interceptKeyBeforeQueueing(KeyEvent event, int policyFlags, boolean interactive){
        final boolean down = event.getAction() == KeyEvent.ACTION_DOWN;
        final int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_DOWN:{
                if (down) {
                    if (interactive && (event.getFlags() & KeyEvent.FLAG_FALLBACK) == 0) {
                        if(!mVolumeDownPressed){
                            mVolumeDownPressed = true;
                        }
                    }
                } else {
                    if (mVolumeDownPressed){
                        mVolumeDownPressed = false;
                    }
                }
                break;
            }
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_VOLUME_MUTE: {

                if (keyCode == KeyEvent.KEYCODE_VOLUME_UP){
                    if (down) {
                        mVolumeUpKeyTriggered = true;
                    }else{
                        mVolumeUpKeyTriggered = false;
                    }
                }
                break;
            }
        }

        if (down && (this.mSystemReady) && mDisableProximitor && (this.mVolumeDownPressed) && (this.mVolumeUpKeyTriggered)){
            releaseScreenOnProximitySensor();
        }
    }

    public void setVolumeUpKeyTriggered(boolean triggered){
        mVolumeUpKeyTriggered = triggered;
    }

    private void prepareHintDialog() {
        this.mDialog = new Dialog(this.mContext);
        this.mDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        this.mDialog.setContentView(View.inflate(this.mDialog.getContext(),
                com.android.internal.R.layout.screen_on_proximity_sensor_guide,
                null), new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
        WindowManager.LayoutParams localLayoutParams = this.mDialog.getWindow()
                .getAttributes();
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG;// 2016;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR
                | WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        // this.mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        this.mDialog.getWindow().setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        localLayoutParams.format = PixelFormat.TRANSLUCENT;
        localLayoutParams.gravity = Gravity.CENTER;
        this.mDialog.getWindow().setAttributes(localLayoutParams);
        this.mDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.argb(100, 0, 0, 0)));
        this.mDialog.setCancelable(false);

    }

    public void aquire() {
        if(mTouchProtect == false){
            return;
        }

    /*        if(SensorManager.isHallSensorOn()){
            return;
        }*/
        try {
            Log.d("FreemeScreenOnProximityLock", "try to aquire");
            if (!this.mHeld) {
                Log.d("FreemeScreenOnProximityLock", "aquire");
                this.mHeld = true;
                this.mIsFirstChange = true;
                this.mHandler.removeMessages(3);
                this.mHandler.sendEmptyMessageDelayed(3, 10000L);
                this.mSensorManager.registerListener(this.mSensorEventListener,
                        this.mSensor, SensorManager.SENSOR_DELAY_NORMAL);
            }
        } catch(Exception e){
            Log.d("FreemeScreenOnProximityLock", "rigister sensor exception");
            e.printStackTrace();
        }
        finally {
            //this.mSensorEventListener.handleChanges();
        }
        mDisableProximitor = true;
    }

    protected void enableUserActivity(boolean paramBoolean) {

    }

    public boolean isHeld() {
        return this.mHeld;
    }

    private boolean release() {
        Log.d("FreemeScreenOnProximityLock", "try to release");
        try {
            if (this.mHeld) {
                Log.d("FreemeScreenOnProximityLock", "release");
                this.mHeld = false;
                this.mIsFirstChange = false;
                this.mDownRecieved.clear();
                this.mSensorManager
                        .unregisterListener(this.mSensorEventListener);
                this.mHandler.removeMessages(3);
                this.mHandler.removeMessages(2);
                this.mHandler.sendEmptyMessage(2);

                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /*public boolean shouldBeBlocked(KeyEvent paramKeyEvent) {
        boolean bool = true;
        if ((paramKeyEvent == null) || (!this.mHeld))
            bool = false;
        return bool;

         * boolean bool = true; if ((paramKeyEvent == null) || (!this.mHeld))
         * bool = false; do { int i; do { return bool; switch
         * (paramKeyEvent.getKeyCode()) { default: i =
         * paramKeyEvent.getKeyCode(); if ((paramKeyEvent.getRepeatCount() != 0)
         * || (paramKeyEvent.getAction() != 0)) continue;
         * this.mDownRecieved.put(i, Boolean.valueOf(bool)); case 24: case 25:
         * case 79: case 85: case 86: case 87: case 126: case 127: } } while
         * (this.mDownRecieved.get(i) != null); return false; } while
         * (!((AudioManager) this.mContext.getSystemService("audio"))
         * .isMusicActive()); return false; return false;

    }*/

    private class MySensorEventListener implements SensorEventListener {
        boolean mIsTooClose;

        private MySensorEventListener() {
        }

        public void handleChanges() {
            StringBuilder localStringBuilder = new StringBuilder()
                    .append("handle change = ");
            Log.d("FreemeScreenOnProximityLock",
                    localStringBuilder.toString() + ",mIsTooClose = "
                            + mIsTooClose);
            if (this.mIsTooClose) {
                localStringBuilder.append("too close");
                Log.d("FreemeScreenOnProximityLock",
                        localStringBuilder.toString());
                if (FreemeScreenOnProximityLock.this.mHandler.hasMessages(3))
                    FreemeScreenOnProximityLock.this.mHandler
                            .removeMessages(3);
                if (FreemeScreenOnProximityLock.this.mIsFirstChange) {
                    FreemeScreenOnProximityLock.this.mHandler
                            .removeMessages(1);
                    FreemeScreenOnProximityLock.this.mHandler
                            .removeMessages(4);
                    FreemeScreenOnProximityLock.this.mHandler
                            .sendEmptyMessageDelayed(
                                    1,
                                    FreemeScreenOnProximityLock.sValidChangeDelay);
                }

            } else {
                localStringBuilder.append("far away");
                Log.d("FreemeScreenOnProximityLock",
                        localStringBuilder.toString());
                if (FreemeScreenOnProximityLock.this.mIsFirstChange) {
                    FreemeScreenOnProximityLock.this.mHandler
                            .removeMessages(2);
                    FreemeScreenOnProximityLock.this.mHandler
                            .removeMessages(3);
                    FreemeScreenOnProximityLock.this.mHandler
                            .sendEmptyMessageDelayed(3, 300L);
                }
            }

        }

        public void onAccuracyChanged(Sensor paramSensor, int paramInt) {
        }

        public void onSensorChanged(SensorEvent paramSensorEvent) {
            float f = paramSensorEvent.values[0];
            Log.d("FreemeScreenOnProximityLock", "onSensorChanged f = " + f);
            boolean bool2 = f >= 0.0f && f < 0.5f;
            /*
             * boolean bool1 = f < 0.0D; boolean bool2 = false; if (!bool1) {
             * boolean bool3 = f < 5.0F; bool2 = false; if (bool3) { boolean
             * bool4 = f <
             * FreemeScreenOnProximityLock.this.mSensor.getMaximumRange();
             * bool2 = false; if (bool4) bool2 = true; } }
             */
            this.mIsTooClose = bool2;
            handleChanges();
        }
    }
}
