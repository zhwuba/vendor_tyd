package com.freeme.server.policy;

import android.util.Log;
//import android.view.WindowManagerPolicy;
import com.android.server.policy.WindowManagerPolicy;
import com.android.server.policy.PhoneWindowManager;

public class FreemePolicyFactory {
    private static final String TAG = "FreemePolicyFactory";
    private static final Object mLock = new Object();
    private static volatile Factory mObj = null;

    public interface Factory {
        WindowManagerPolicy getFreemePhoneWindowManager();
    }

    public static WindowManagerPolicy getFreemePhoneWindowManager() {
        Factory obj = getImplObject();
        if (obj != null) {
            return obj.getFreemePhoneWindowManager();
        }
        return new PhoneWindowManager();
    }

    private static Factory getImplObject() {
        synchronized (mLock) {
            if (mObj != null) {
                return mObj;
            }
            try {
                mObj = (Factory) Class.forName("com.freeme.server.policy.FreemePolicyFactoryImpl").newInstance();
            } catch (Exception e) {
                Log.e(TAG, ": reflection exception is " + e.getMessage());
            }
            if (mObj != null) {
                Log.v(TAG, ": success to get AllImpl object and return....");
                return mObj;
            }
            Log.e(TAG, ": fail to get AllImpl object");
            return null;
        }
    }
}
