package com.freeme.server.wm;

import android.content.Context;
import com.android.server.policy.WindowManagerPolicy;
import com.android.server.input.InputManagerService;
import com.android.server.wm.WindowManagerService;
import com.freeme.server.policy.FreemePhoneWindowManager;

public class FreemeWindowManagerService extends WindowManagerService {
    public FreemeWindowManagerService(Context context, InputManagerService inputManager, boolean haveInputMethods, boolean showBootMsgs, boolean onlyCore, WindowManagerPolicy policy) {
        super(context, inputManager, haveInputMethods, showBootMsgs, onlyCore, policy);
    }

    @Override
    public void setCurrentUser(int newUserId, int[] currentProfileIds) {
        super.setCurrentUser(newUserId, currentProfileIds);
        synchronized (mWindowMap) {
            if (mPolicy instanceof FreemePhoneWindowManager) {
                ((FreemePhoneWindowManager) mPolicy).setCurrentUser(newUserId, currentProfileIds);
            }
        }
    }
}
