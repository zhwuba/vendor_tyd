package com.freeme.server.policy;

import com.android.server.policy.WindowManagerPolicy;

public class FreemePolicyFactoryImpl implements FreemePolicyFactory.Factory {
    private static final String TAG = "FreemePolicyFactoryImpl";

    public WindowManagerPolicy getFreemePhoneWindowManager() {
        return new FreemePhoneWindowManager();
    }
}
