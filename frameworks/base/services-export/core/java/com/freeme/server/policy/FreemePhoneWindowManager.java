package com.freeme.server.policy;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.RemoteException;
import android.os.UserHandle;
import android.util.Log;
import android.provider.Settings;
import android.view.IWindowManager;
import com.android.server.policy.PhoneWindowManager;

import com.android.server.statusbar.StatusBarManagerInternal;
import com.freeme.provider.FreemeSettings;
import com.freeme.util.FreemeOption;
import com.freeme.content.FreemeIntent;
import com.freeme.gesture.FreemeGestureEventHandler;


public class FreemePhoneWindowManager extends PhoneWindowManager {
    private static final String TAG = "FreemeWindowManager";
    private static final boolean DEBUG = false;

    private final static int THREE_POINT_STYLE_SCREEN_SHOT  = 0;
    private final static int THREE_POINT_STYLE_SPLIT_SCREEN = 1;

    private SettingsObserver mSettingsObserver;

    class SettingsObserver extends ContentObserver {
        SettingsObserver(Handler handler) {
            super(handler);
            registerContentObserver(UserHandle.myUserId());
        }
        public void registerContentObserver(int userId) {
            ContentResolver resolver = mContext.getContentResolver();

            if (FreemeOption.FREEME_GESTURE_TOUCH_THREE_POINT) {
                resolver.registerContentObserver(Settings.System.getUriFor(
                        FreemeSettings.System.FREEME_GESTURE_TOUCH_THREE_POINT),
                        false, this, userId);
            }

            updateSettings();
        }

        @Override
        public void onChange(boolean selfChange) {
            updateSettings();
        }
    }

    @Override
    public void setCurrentUserLw(int newUserId) {
        super.setCurrentUserLw(newUserId);
        mSettingsObserver.registerContentObserver(newUserId);
        mSettingsObserver.onChange(true);
    }

    public void setCurrentUser(int userId, int[] currentProfileIds) { }

    @Override
    public void updateSettings() {
        if (FreemeOption.FREEME_GESTURE_TOUCH_THREE_POINT) {
            setGestureTouch(FreemeSettings.System.FREEME_GESTURE_TOUCH_THREE_POINT,
                    FreemeGestureEventHandler.GESTURE_TYPE_SPUERSHOT, 1);
        }

        super.updateSettings();
    }

    @Override
    public void init(Context context, IWindowManager windowManager,
                     WindowManagerFuncs windowManagerFuncs) {
        super.init(context, windowManager, windowManagerFuncs);

        FreemeInit();
    }

    @Override
    public void systemReady() {
        super.systemReady();

        mSettingsObserver = new SettingsObserver(mHandler);
    }

    private void FreemeInit() {
        if (DEBUG) {
            Log.d(TAG, "FreemeInit");
        }

        if (FreemeOption.FREEME_GESTURE_TOUCH_THREE_POINT) {
            IntentFilter threePointerFilter = new IntentFilter();
            threePointerFilter.addAction(FreemeIntent.ACTION_GESTURE_TOUCH_THREE_POINT);
            mContext.registerReceiverAsUser(mFreemeThreePointerReceiver, UserHandle.ALL, threePointerFilter, null, null);
        }
    }

    private void setGestureTouch(String feature, int handlerType, int def) {
        boolean enable = Settings.System.getIntForUser(
                mContext.getContentResolver(), feature, def, UserHandle.USER_CURRENT) == 1;
        try {
            int nowControl = mWindowManager.getGestureTouch();
            mWindowManager.setGestureTouch(
                    enable ? (handlerType | nowControl) : (~handlerType & nowControl));
        } catch (RemoteException ex) {
            // Can't happen in system process.
            ex.printStackTrace();
        }
    }

    private BroadcastReceiver mFreemeThreePointerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() == null) {
                return;
            }
            final int style = intent.getExtras().getInt(FreemeIntent.EXTRA_THREE_POINT_STYLE);
            switch (style) {
                case THREE_POINT_STYLE_SCREEN_SHOT:
                    mHandler.post(mScreenshotRunnable);
                    break;
                case THREE_POINT_STYLE_SPLIT_SCREEN:
                    StatusBarManagerInternal statusbar = getStatusBarManagerInternal();
                    if (statusbar != null && FreemeOption.SPLIT_SCREEN_SUPPORT) {
                        statusbar.toggleSplitScreen();
                    }
                    break;
            }
        }
    };
}
