#!/bin/bash

echo "ro.build.tyd.display.id=$TYD_PRODUCT_INFO_SW_VERNO_INTERNAL"
echo "ro.build.tyd.version=$TYD_SYS_VERSION"
echo "ro.build.tyd.label=$TYD_SYS_LABEL"
echo "ro.build.tyd.channel=$TYD_SYS_CHANNEL"
echo "ro.build.tyd.customer=$TYD_SYS_CUSTOMER"
echo "ro.build.tyd.customer.branch=$TYD_SYS_CUSTOMER_BRANCH"

if [ -n "$TYD_SYS_TIMEZONE" ] ; then
  echo "persist.sys.timezone=$TYD_SYS_TIMEZONE"
fi

echo "ro.build.tyd.bt.name=$TYD_SYS_BLUETOOTH_NAME"
echo "ro.build.tyd.wlan.name=$TYD_SYS_WLAN_NAME"

if [ -n "$TYD_SYS_MTP_PTP_NAME" ] ; then
  echo "ro.build.tyd.mtp_ptp.name=$TYD_SYS_MTP_PTP_NAME"
fi
if [ -n "$TYD_SYS_EXIF_NAME" ] ; then
  echo "ro.build.tyd.exif.name=$TYD_SYS_EXIF_NAME"
fi

# @{ Deprecated, Remove soon
echo "ro.build.version.tydos=$TYD_SYS_VERSION"
echo "ro.build.tydos_label=$TYD_SYS_LABEL"
echo "ro.build.tydos_channel_no=$TYD_SYS_CHANNEL"
echo "ro.build.tydos_customer_no=$TYD_SYS_CUSTOMER"
echo "ro.build.tydos_customer_br=$TYD_SYS_CUSTOMER_BRANCH"
# @}
