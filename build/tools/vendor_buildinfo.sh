#!/bin/bash

echo "ro.build.tyd.hardware=$TYD_PRODUCT_INFO_HW_VERNO"
if [ -n "$TYD_PRODUCT_INFO_BASEBAND_VERNO" ] ; then
  echo "ro.build.tyd.baseband=$TYD_PRODUCT_INFO_BASEBAND_VERNO"
fi
if [ -n "$TYD_PRODUCT_INFO_KERNEL_VERNO" ] ; then
  echo "ro.build.tyd.kernel=$TYD_PRODUCT_INFO_KERNEL_VERNO"
fi
