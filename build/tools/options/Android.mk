LOCAL_PATH := $(call my-dir)

# Freeme Product Options
FREEME_PRODUCT_OPTIONS ?= $(LOCAL_PATH)/default.ini
FREEME_PRODUCT_OPTIONS_OVERLAY ?=

include $(CLEAR_VARS)
LOCAL_MODULE := tyd_default.ini
LOCAL_SRC_FILES := $(LOCAL_MODULE)
LOCAL_PREBUILT_MODULE_FILE := $(PRODUCT_OUT)/obj/FAKE/tyd/$(LOCAL_SRC_FILES)
LOCAL_MODULE_CLASS := FAKE
LOCAL_MODULE_PATH := $(TARGET_OUT_ETC)/tyd

$(LOCAL_PREBUILT_MODULE_FILE): $(TYD_PRODUCT_OPTIONS) $(TYD_PRODUCT_OPTIONS_OVERLAY)
	@echo "Generate tyd default options"
	@echo "TYD_PRODUCT_OPTIONS $^"
	@mkdir -p $(dir $@)
	@python vendor/tyd/build/tools/options/conf_generator.py $@ $^

include $(BUILD_PREBUILT)

ALL_DEFAULT_INSTALLED_MODULES += $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE)

# We need this so that the installed files could be picked up based on the
# local module name
ALL_MODULES.$(LOCAL_MODULE).INSTALLED := \
    $(ALL_MODULES.$(LOCAL_MODULE).INSTALLED) $(LOCAL_MODULE_PATH)/$(LOCAL_MODULE)
