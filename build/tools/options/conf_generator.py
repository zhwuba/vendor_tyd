#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Usage: conf_generator.py <[OUT, IN...]>
"""
import os
import sys
from ConfigParser import ConfigParser


class FeatureMerger(ConfigParser):
    pass


def main(argv):
    fn_out = argv[0]
    fn_in = argv[1:]
    fn_in = [f for f in fn_in if os.path.exists(f)]

    fm = FeatureMerger()
    fm.read(fn_in)
    fm.write(open(fn_out, 'w'))


if __name__ == '__main__':
    main(sys.argv[1:])

