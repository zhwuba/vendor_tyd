# $1
rootpath=${1}
old_device=${2}
old_project=${3}
new_project=${4}
project_old_device_dir=${rootpath}/device/${old_device}
project_new_device_dir=${1}/device/tyd/${new_project}

echo $project_old_device_dir

cp -r ${project_old_device_dir} ${project_new_device_dir}

sed -i "s/${old_project}/${new_project}/g" ${project_new_device_dir}/*.sh ${project_new_device_dir}/*.mk

mv ${project_new_device_dir}/${old_project}.mk ${project_new_device_dir}/${new_project}.mk


#clone kernel-config
defconfig_path=${rootpath}/kernel/msm-4.9/arch/arm64/configs

cp ${defconfig_path}/${old_project}_defconfig ${defconfig_path}/${new_project}_defconfig

cp ${defconfig_path}/${old_project}-perf_defconfig ${defconfig_path}/${new_project}-perf_defconfig
