#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import argparse
import glob
import os
import re
import sys
import xml
import xml.sax

""" Product Utils
 * Merge ProjectConfig
"""

class ProjectConfigMerger:
    def __init__(self):
        self.CONFIG_CONTENT_PATTERN = (
            re.compile(r"^([^#=\s]+)\s*([:?+]?=)\s*(.*)$"),
            re.compile(r"^-?(include)\s+(.*)\s*$"),
        )

    def merge(self, argv, configs = None):
        entrance = False
        if configs is None:
            entrance = True
            configs = {}

        config_overlay = False
        for fn in argv:
            if fn == '--config-overlay' or config_overlay:
                if not config_overlay:
                    config_overlay = True
                elif fn.startswith('-'):
                    pass
                else:
                    self.parse(configs, fn)
            elif fn.startswith('-'):
                pass
            elif os.path.exists(fn):
                with open(fn) as f:
                    for line in f.readlines():
                        self.parse(configs, line)

        if entrance:
            for key in sorted(configs.keys()):
                print("%s = %s" % (key, configs[key]))

    def parse(self, configs, content):
        matcher = (filter(None, [x.search(content) for x in self.CONFIG_CONTENT_PATTERN]) or [None])[0]
        if matcher and len(matcher.groups()) > 0:
            key = matcher.group(1)
            if not key:
                pass
            elif key == 'include':
                value = matcher.group(2)
                self.merge(glob.glob(value), configs)
            else:
                symbol = matcher.group(2)
                value = matcher.group(3)
                if symbol == '?=':
                    if key not in configs or not configs[key]:
                        configs[key] = value.strip()
                elif symbol == '+=':
                    if key not in configs or not configs[key]:
                        configs[key] = value.strip()
                    elif len(value.strip()) == 0:
                        pass
                    else:
                        configs[key] = '%s %s' % (configs[key], value.strip())
                else:
                    configs[key] = value.strip()

def merge_project_config(argv):
    merger = ProjectConfigMerger()
    merger.merge(argv)


""" Module Utils
 * Aidl Parser
 * Resource Helper
"""

""" * Aidl Parser """
class AidlFile:
    def __init__(self, filename):
        self.filename = os.path.exists(filename) and filename or None

    def content(self):
        if self.filename:
            with open(self.filename) as f:
                return f.read()
        return ''

    def strip_comment(self, source):
        source = re.sub(r'([^/])/\*.*?\*/', r'\1', source, flags=re.DOTALL)
        source = re.sub(r'^/\*.*?\*/', '', source, flags=re.DOTALL)
        source = re.sub(r'//[^\n\r]*([\n\r]*)', r'\1', source)
        return source

    def functions(self):
        source = self.content()
        source = self.strip_comment(source)
        strip_blank = lambda x:re.sub(r'\s+', ' ', x)
        return [ "%s" % (strip_blank(f)) for f in re.findall(r'\s*([^{};]+\([^(){}]*\))\s*;', source) ]

    def funcs(self):
        return [ "%s" % (re.match(r'^.+\s+(\w+)\s*\(.*\).*$', f).group(1)) for f in self.functions() ]

    def transaction(self, func):
        return self.funcs().index(func)

def calculate_aidl_transaction(argv):
    aidl = AidlFile(argv[0])
    try:
        print(aidl.transaction(argv[1]))
    except BaseException:
        print(0)


""" * Resource Helper """
class ResourceSymbolsHelper:
    class Parser(xml.sax.ContentHandler):
        def __init__(self, symbols={}):
            self.symbols = symbols

        def startElement(self, tag, attributes):
            if tag in ('resources', ):
                return
            t = attributes.get('type')
            n = attributes.get('name')
            if t and n:
                try:
                    ts = self.symbols[t]
                except KeyError:
                    ts = set()
                    self.symbols[t] = ts
                ts.add(n)

    def __init__(self, module_name):
        self.symbols_path_list = {
            'sample': (
                'sample',
            ),
            'framework-res': (
                'out/target/common/obj/APPS/framework-res_intermediates',
                'frameworks/base/core/res/res/values',
            ),
            'freeme-framework-res': (
                'out/target/common/obj/APPS/freeme-framework-res_intermediates',
                'vendor/freeme/frameworks/base/core/res/res/values',
            ),
        }[module_name]
        self.symbols_all = {}
        self.symbols_internal = {}
        self.symbols_public = {}

    def enumerate(self):
        symbols = [['public_resources.xml'], ['public.xml'], ['symbols*.xml']]
        for symbol in symbols:
            name = symbol[0]
            del symbol[:]
            for path in self.symbols_path_list:
                filename = os.path.join(path, name)
                symbol.extend(glob.glob(filename))
        return symbols

    def parse(self):
        full, public, internal = self.enumerate()

        parser = xml.sax.make_parser()
        parser.setFeature(xml.sax.handler.feature_namespaces, 0)

        parser.setContentHandler(self.Parser(self.symbols_all))
        for f in full:
            parser.parse(f)

        parser.setContentHandler(self.Parser(self.symbols_public))
        for f in public:
            parser.parse(f)

        parser.setContentHandler(self.Parser(self.symbols_internal))
        for f in internal:
            parser.parse(f)

        return self

    def output_undeclared(self):
        rst = self.dict_sets_operation('-', self.symbols_all, self.symbols_public)
        rst = self.dict_sets_operation('-', rst, self.symbols_internal)
        for t in sorted(rst):
            for i in sorted(rst[t]):
                print('<java-symbol type=\"%s\" name=\"%s\" />' % (t, i))
        return self

    @staticmethod
    def dict_sets_operation(optr, lhs, rhs):
        if optr == '&':
            if not lhs or not len(lhs) or not rhs or not len(rhs):
                return {}
            rst = {}
            for t in lhs:
                if t in rhs:
                    tr = lhs[t] & rhs[t]
                    if len(tr):
                        rst[t] = tr
            return rst
        elif optr == '|':
            if not lhs or not len(lhs):
                return rhs and rhs or {}
            elif not rhs or not len(rhs):
                return lhs
            rst = dict(lhs)
            for t in rhs:
                if t in rst:
                    rst[t] = rst[t] | rhs[t]
                else:
                    rst[t] = set(rhs[t])
            return rst
        elif optr == '-':
            if not lhs or not len(lhs):
                return {}
            elif not rhs or not len(rhs):
                return lhs
            rst = dict(lhs)
            for t in rhs:
                if t in rst:
                    tr = rst[t] - rhs[t]
                    if len(tr):
                        rst[t] = tr
                    else:
                        del rst[t]
            return rst
        else:
            raise NotImplementedError('Operator %s' % optr)

def resolve_resource_symbol(argv):
    parser = argparse.ArgumentParser('resource_symbol')
    parser.add_argument('--placeholder', action='store_true')
    parser.add_argument('--module', '-m', default='freeme-framework-res')
    parser.add_argument('--output', '-o', default='undeclared')
    args = parser.parse_args(argv)

    helper = ResourceSymbolsHelper(args.module)
    helper.parse()
    if args.output == 'undeclared':
        helper.output_undeclared()
    else:
        raise NotImplementedError('Argument %s' & args.output)


""" Common
"""

FUNCTION_TABLE = {
    'merge-config' : merge_project_config,
    'aidl-transaction' : calculate_aidl_transaction,
    'resource-symbol' : resolve_resource_symbol,
}


def main(argv):
    if len(argv) == 1:
        FUNCTION_TABLE[argv[0]]()
    elif len(argv) > 1:
        FUNCTION_TABLE[argv[0]](argv[1:])


if __name__ == '__main__':
    main(sys.argv[1:])
