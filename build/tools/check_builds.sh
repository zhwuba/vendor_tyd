#!/usr/bin/env bash

# bootanimation.zip path
function check_bootanimtion() {
    local bootanimation_zip=$1
    if [ -f ${bootanimation_zip} ] ; then
        local lastchar=$(unzip -p ${bootanimation_zip} desc.txt | hexdump -v -e '/1 "%02X\n"' | tail -1)
        local compressratio=$(echo $(zipinfo -t ${bootanimation_zip} | cut -d':' -f2))
        if [[ "$lastchar" != "0A" || "$compressratio" != "0.0%" ]] ; then
            return 1
        fi
    fi
    return 0
}
function check_bootanimtion_makefile() {
    check_bootanimtion $@ && echo 1 || echo 0
}

if [ $# -gt 1 ] ; then
    func=$1
    shift
    eval ${func} $@
    ret=$?
    [[ ${func} == *_makefile ]] || exit $ret
fi