#!/bin/bash
export TYD_BUILD_HOME='vendor/tyd/build'
export TYD_BUILD_CHIPVENDOR='qcom'

# imports
# TYD_PRODUCT_BOARD
# TYD_PRODUCT_BASED
# TYD_PRODUCT_FOR

function dry() {
    echo "$*"
}

# $(1: from) $(2: to) $(3: file[s])
function file_text_replace() {
    local from=$1; shift
    local to=$1; shift
    sed -i "s#$from#$to#g" $*
}

# $(1: from) $(2: to) $(3: file[s])
function file_text_whole_replace() {
    local from=$1; shift
    local to=$1; shift
    sed -i "s#\<$from\>#$to#g" $*
}

# $(1: file)
function file_extension() {
    echo ${1##*.}
}

# $(1:option file) $(2:section) $(3:key)
function get_option_val() {
    echo "$(sed -n "/\[$2\]/,/\[.*\]/{
  /^\[.*\]/d
  /^[ ]*$/d
  s/;.*$//
  s/^[ |        ]*$3[ | ]*=[ |  ]*\(.*\)[ |     ]*/\1/p
  }" $1)"
}

# $(1:config file) $(2:key)
function get_config_val() {
    echo "$(sed -n "
  /^\[.*\]/d
  /^[ ]*$/d
  s/;.*$//
  s/^[ |        ]*$2[ | ]*=[ |  ]*\(.*\)[ |     ]*/\1/p
  " $1)"
}

# Replace or add config pair(key-value)
# $(1:config file) $(2:key) $(3:value)
function place_config() {
    local cfg=$1
    local key=$2
    local val=$3
    [ "$(get_config_val ${cfg} ${key})" = "${val}" ] || {
        if [ "$(grep -nE "^\s*${key}\s*=" ${cfg})" ] ; then
            sed -i "s/^\s*${key}\s*=\s*.*/${key} = ${val}/g" ${cfg}
        else
            echo "${key} = ${val}" >> ${cfg}
        fi
    }
}

# Wrap of get_option_val and project.ini
function get_project_info() {
    get_option_val 'device/tyd/common/project.ini' $@
}

# $(1:func) $(2:project)
# $2 Sample: [full_][PROJECT[MODEL[BOARD]]]-[VARIENT]
#   full_v9h62_gb-userdebug
#   full_v9h62_gb
#   v9h62_gb
#   v9h62_gb_gmo
#   v9h62
#   v9h
# $(__config) xxx/ProjectConfig.mk
function get_project_section() {
    case $1 in
        BOARD)
            local model=$(get_project_section 'MODEL' $2)
            local board=$(get_project_info 'BOARD' ${model})
            echo ${board:-${model}}
            ;;
        MODEL)
            local project=$(get_project_section 'PROJECT' $2)
            echo ${project%%_*}
            ;;
        PROJECT)
            echo $(echo -n $2 | sed -e "s/-.*$//")
            ;;
        PRODUCT)
            echo "$2"
            ;;
        VARIANT)
            echo $2 | sed -e "s/^[^\-]*-//"
            ;;
        BASED)
            # example: v9h62_gb=>v9h=>*droi6750_65_n*
            local config
            if [ -n "${__config}" ] ; then
                config=${__config}
            elif [ -f "device/tyd/$(get_project_section 'PROJECT' $2)/ProjectConfig.mk" ] ; then
                config="device/tyd/$(get_project_section 'PROJECT' $2)/ProjectConfig.mk"
            else
                config="device/tyd/$(get_project_section 'BOARD' $2)/ProjectConfig.mk"
            fi
            get_config_val ${config} 'TYD_PRODUCT_BASED'
            ;;
        PLATFORM)
            # example: v9h62_gb=>v9h=>(*droi6750_65_n*=>)*mt6750*
            local project_based=$(get_project_section 'BASED' $2)
            get_project_info 'PLATFORM' ${project_based}
            ;;
        ARCH)
            local project_based=$(get_project_section 'BASED' $2)
            get_project_info 'ARCH' ${project_based}
            ;;
        VALUE_OF_KEY)
            get_config_val $2 $3
            ;;
        *)
            echo $2
            ;;
    esac
}

# @var: $(1:source file) [commit-id or branch]
# @return: the last commit SHA
function get_file_last_revision() {
    local srcfile=$1
    local commit_id=${2:-"HEAD"}

    cd $(dirname $srcfile)
    file=$(basename $srcfile)
    echo $(git log --pretty=format:"%H" -1 $commit_id $file)
    cd -
}

# @var: $(1:source file) [commit-id or branch]
# @return: the time of last commit, seconds since 1970.1.1, like "1481511190"
function get_file_last_revision_time() {
    local srcfile=$1
    local commit_id=${2:-"HEAD"}

    local HERE=$(pwd)
    cd $(dirname $srcfile)
    local file=$(basename $srcfile)
    local last_commit=$(git log --pretty=format:"%H" -1 $commit_id $file)
    echo $(git log -1 --format="@%ct" $last_commit $file | cut -c 2-)
    cd $HERE
}

function get_merge_tool() {
    local t=$(git config merge.tool)
    local mt=${t:-"bcompare"}
    echo $mt
}

# @var: $(1:base file) $(2:override file)
function check_need_compare() {
    local base_file=$1
    local override_file=$2

    local filtertype="png jpg gif jpeg bmp apk ogg"
    local extension="${override_file##*.}"

    [[ "$filtertype" =~ $extension ]] && return 0
    [[ ! -f "$base_file" ]] && return 0

    local base_datetime=$(get_file_last_revision_time $base_file)
    local override_datetime=$(get_file_last_revision_time $override_file)
    if [ $override_datetime -lt ${base_datetime:-0} ] ; then
        return 1
    else
        return 0
    fi
}

# $(1:src) $(2:dst)
function copyfile_force() {
    local src=$1
    local dst=$2
    [ -d $(dirname ${dst}) ] || mkdir -p $(dirname ${dst})
    cp -af ${src} ${dst} 2>/dev/null
}

# $(1:source file) $(2:target file) [--force-override|-f]
function merge_override_file() {
    local source_file=$1
    local target_file=$2
    shift
    shift

    local force_override
    for arg in $@ ; do
        case $arg in
            -f|--force-override)
                force_override=yes
                ;;
        esac
    done
    unset arg

    if [ "${force_override}" ] ; then
        copyfile_force "${source_file}" "${target_file}"
    else
        check_need_compare ${target_file} ${source_file}
        if [ "$?" = "1" ]; then
            local mt=$(get_merge_tool)
            local mt_list="meld bc bc3 bcompare"
            if [[ ! "$mt_list" =~ "$mt" ]] ; then
                local color=$'\E'"[0;33m" color_reset=$'\E'"[00m"
                echo "WARNING: $0:$LINENO ${color}$mt is not supported, only support [$mt_list] ${color_reset}"
            fi
            case $mt in
                bc|bc3)
                    bcompare ${source_file} ${target_file}
                    ;;
                *)
                    $mt ${source_file} ${target_file}
                    ;;
            esac
        else
            copyfile_force "${source_file}" "${target_file}"
        fi
    fi
}

# $(1:project) $(2:project override path) [--force-override|-f]
function merge_override() {
    local project=$1
    local override_dir=$2
    shift
    shift

    [ -d "${override_dir}" ] || return 0

    # check whether $override_dir is end with '/'
    local suffix=${override_dir: -1:1}
    if [ "$suffix" != "/" ]; then
        override_dir=$override_dir/
    fi

    for F in $(find ${override_dir} -type f 2>/dev/null); do
        local TF=${F##$override_dir}
        [ "$TF" = 'Android.mk' ] && continue
        merge_override_file "$F" "$TF" $@
    done
    unset F

    return 0
}

# $(1:project) $(2:config-custom) [--force-override|-f]
function merge_override_cts_ifneeded() {
    local project=$1
    local config=$2
    shift
    shift

    # * config for cts
    local config_factory_common
    if [ "$(get_config_val ${config} 'TYD_PRODUCT_FOR')" = "cts" ] ; then
        merge_override ${project} 'device/tyd/common/cts/override' $@
    fi
}

# Lifetime hook: called before action:breakfast
#  $project
#  $variant
function pre_breakfast() {
    return 0;
}

# Lifetime hook: called before action:new
#  $project
#  $variant
function pre_new() {
    [ -d 'out' ] && make clobber
}

# $(1:project) [--force-override|-f]
# $1: Sample: [full_]v9h62_gb[-[userdebug|user|eng]]
function breakfast() {
    return 0
}

alias bib=breakfast

# $(1:project) [[--force-override|-f]|[--breakfast|-b]|[config-overlay]]
# $1: Sample: [full_]v9h62_gb-<userdebug|user|eng>
function brunch() {
    local breakfast
    for arg in $@ ; do
        case $arg in
            -b|--breakfast)
                breakfast=yes
                ;;
        esac
    done
    unset arg

    local project
    if [ -z "$1" ] ; then
        echo "Which product would you like? [v9h62_gb-userdebug] "
        return 1
    else
        project=$1
    fi
    local product=$(get_project_section 'PRODUCT' ${project})

    # run breakfast again
    if [ -z "${breakfast}" ] ; then
        lunch ${product}
        if [ $? -eq 0 ] ; then
            return 0
        fi
    fi

    breakfast $@
    if [ $? -ne 0 ]; then
        echo "Failed. Try 'breakfast'"
        return 1
    fi

    lunch ${product}
    return $?
}

function cout() {
    if [ "$OUT" ] ; then
        cd $OUT
    else
        echo "Couldn't locate out directory. Try setting OUT."
    fi
}

function reposync() {
    repo sync -j 4 "$@"
}

function repodiff() {
    if [ -z "$*" ]; then
        echo "Usage: repodiff <ref-from> [[ref-to] [--numstat]]"
        return
    fi
    color=$'\E'"[0;33m" color_reset=$'\E'"[00m" \
    diffopts=$* repo forall -c \
      'echo "${color}$REPO_PATH ($REPO_REMOTE)${color_reset}"; git diff ${diffopts} 2>/dev/null ;'
}

function repostatus() {
    color=$'\E'"[0;33m" color_reset=$'\E'"[00m" \
    statusopts=$* repo forall -c \
      'echo "${color}$REPO_PATH ($REPO_REMOTE)${color_reset}"; git status -s ${statusopts} 2>/dev/null ;'
}

function repoupgrade() {
    local T=$(gettop)
    local dir=$T/.repo/repo
    local HERE=$(pwd)
    if [ ! -d ${dir} ]; then
        echo "Couldn't locate '.repo/repo'. Is this a valid repo source tree?" >&2
        return 1
    fi

    cd ${dir}; git pull; cd $HERE
}

function repomanifestsync() {
    local T=$(gettop)
    local dir=$T/.repo/manifests
    local HERE=$(pwd)
    if [ ! -d ${dir} ] ; then
        echo "Couldn't locate '.repo/manifests'. Is this a valid repo source tree?" >&2
        return 1
    fi

    cd ${dir}; git pull; cd $HERE
}

function repolastsync() {
    local RLSPATH="$(gettop)/.repo/.repo_fetchtimes.json"
    local RLSLOCAL=$(date -d "$(stat -c %z $RLSPATH)" +"%Y-%m-%d, %T %Z")
    local RLSUTC=$(date -d "$(stat -c %z $RLSPATH)" -u +"%Y-%m-%d, %T %Z")
    echo "Last repo sync: $RLSLOCAL / $RLSUTC"
}

# [-a]
function repoclean() {
    color=$'\E'"[0;33m" color_reset=$'\E'"[00m" \
    repo forall -c \
      'echo "${color}$REPO_PATH ($REPO_REMOTE)${color_reset}"; git clean -fd ; git reset --hard ;'

    [[ $# -gt 0 && "$1" = '-a' ]] && {
        T=$(gettop)
        [ -d $T/out ] && rm -rf $T/out
        shift
    }
}

function repogc() {
    color=$'\E'"[0;33m" color_reset=$'\E'"[00m" \
    repo forall -c \
      'echo "${color}$REPO_PATH ($REPO_REMOTE)${color_reset}"; git gc ;'
}

# [branch]
function gitpush() {
    local branch=$(git rev-parse --abbrev-ref HEAD)
    local remote=$(git config branch.${branch}.remote)
    local merge=$(git config branch.${branch}.merge)
    git push ${remote} HEAD:refs/for/${1:-${merge##*/}}
    return $?
}

# [$(1:left branch)] [$(2:right branch)]
function gitdiffcommits() {
    [ "$1" = "--help" -o "$1" = "-h" ] && {
        echo "gitdiffcommits [<left beanch>|-] [<right branch>]"
        return 1
    }
    local lbr=$1 ; [ "$lbr" = "-" ] && lbr=
    local rbr=$2 ; [ "$rbr" = "-" ] && rbr=
    local lcf=$(mktemp)
    local rcf=$(mktemp)

    local difftool
    for difftool in bcompare meld diff ; do
        [ -n `which ${difftool}` ] && {
            case $difftool in
                diff)
                    difftool='diff -u'
                    ;;
            esac
            break
        }
    done
    git log --abbrev-commit --pretty=format:'%h - %s%d  (%an - %ad)' --date=format:'%Y-%m-%d %H:%M:%S' ${lbr} > ${lcf} ; \
    git log --abbrev-commit --pretty=format:'%h - %s%d  (%an - %ad)' --date=format:'%Y-%m-%d %H:%M:%S' ${rbr} > ${rcf} ; \
    ${difftool} ${lcf} ${rcf}
}

# [$(1:target path)]
function gms_filter() {
    local T=${1:-'.'}
    # remove `local' to make compatible with zsh
    filter_list=(x86_64 x86 xxxhdpi)
    for P in ${filter_list[@]} ; do
        grep -n -E -l -w $P -r --include="Android.mk" $T | \
            xargs sed -i "s/\b$P\b//g"
        find $T -name "*_$P*.apk" -type f -delete -o -name "$P" -type d -exec rm -rf {} \;
    done
    unset filter_list[@]
    unset P
}

function wallpaper_thumb() {
    local T=$(gettop)
    python $T/${TYD_BUILD_HOME}/tools/wallpaper_utils.py $@
}

function resource_symbol() {
    local T=$(gettop)
    cd $T && python ${TYD_BUILD_HOME}/tools/build_utils.py 'resource-symbol' '--placeholder' $@
}

# $(1:target path) $(2:source file/dir)
function overlay() {
    if [ $# -lt 2 ] ; then
        echo "usage: overlay <path/to/place> <target/to/overlay>..."
        return 1
    fi

    local target=$1
    shift
    if [[ -e "$target" && -f "$target" ]] ; then
        echo "error: \"$target\" should be a directory."
        return 1
    fi

    local source="$@"
    for S in $source ; do
        if [ -d "$S" ] ; then
            local TD="$target/$S"
            [ -d "$TD" ] || mkdir -p $TD
            echo $TD
        elif [ -f "$S" ] ; then
            local TF="$target/$S"
            local TD=$(dirname $TF)
            [ -d "$TD" ] || mkdir -p $TD
            echo $TF
        fi
    done
    unset S
}

function publication() {
    local T=$(gettop)
    for F in $(find $T \( -path "$T/.repo" \) -prune -o -type d -name '.git' -print) \
             "$T/.repo" \
             $(eval echo $(cat $T/$TYD_BUILD_HOME/tools/publication.ini))
    do
        echo "Removing $F ..."
        rm -rf $F 2>/dev/null
    done
}

function check_bash_version() {
    # Keep us from trying to run in something that isn't bash.
    if [ -z "${BASH_VERSION}" ]; then
        return 1
    fi

    # Keep us from trying to run in bash that's too old.
    if [ "${BASH_VERSINFO[0]}" -lt 4 ] ; then
        return 2
    fi

    return 0
}

# Execute the contents of any envsetup.*.sh files we can find.
for f in `test -d 'vendor/tyd/build' && find -L 'vendor/tyd/build' -maxdepth 1 -name 'envsetup.*.sh' 2> /dev/null | sort` \
         `test -d 'device/tyd/common/build' && find -L 'device/tyd/common/build' -maxdepth 1 -name 'envsetup.*.sh' 2> /dev/null | sort`
do
    echo "including $f"
    . $f
done
for f in `test -d 'vendor/tyd/build' && find -L 'vendor/tyd/build' -maxdepth 1 -name "envsetup-${TYD_BUILD_CHIPVENDOR}*.sh" 2> /dev/null | sort` \
         `test -d 'device/tyd/common/build' && find -L 'device/tyd/common/build' -maxdepth 1 -name "envsetup-${TYD_BUILD_CHIPVENDOR}*.sh" 2> /dev/null | sort`
do
    echo "including $f"
    . $f
done
unset f

# Add completions
check_bash_version && {
    dirs="vendor/tyd/build/bash_completion"
    for dir in $dirs; do
        if [ -d ${dir} ]; then
            for f in `/bin/ls ${dir}/[a-z]*.bash 2> /dev/null`; do
                echo "including $f"
                . $f
            done
        fi
    done
}
