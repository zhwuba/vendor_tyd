ifeq ($(strip $(TYD_BOOT_ANIMATION_CUSTOMIZED)),yes)

target := $(TARGET_OUT)/media/bootanimation.zip
script := vendor/tyd/build/tools/check_builds.sh

$(call dist-for-goals,check-bootanimation, $(target))

.PHONY: check-bootanimation
check-bootanimation: PRIVATE_SCRIPT := $(script)
check-bootanimation: $(script)
check-bootanimation: PRIVATE_TARGET := $(target)
check-bootanimation: $(target)
	@echo "Check bootanimaton package for $(PRIVATE_TARGET) $(wildcard $(PRIVATE_TARGET))"
	$(hide) r=`bash $(PRIVATE_SCRIPT) check_bootanimtion_makefile $(PRIVATE_TARGET)`; \
	    if [ "$$r" = "0" ] ; then \
	        echo "error: the bootanimation.zip dropped the last line-break."; \
	        exit 1; \
	    fi

droidcore : check-bootanimation

endif
