#
# TYD specific macros
#

# Returns true if $(1) and $(2) are equal.  Returns
# the empty string if they are not equal.
define strequal
$(strip $(if $(strip $(1)),\
  $(if $(strip $(2)),\
    $(if $(filter-out __,_$(subst $(strip $(1)),,$(strip $(2)))$(subst $(strip $(2)),,$(strip $(1)))_),,true), \
    ),\
  $(if $(strip $(2)),\
    ,\
    true)\
 ))
endef

# $(1): condition prefix
# $(2): package list
define install-modules-vars-for
$(strip \
    $(eval _cond_pre := $(strip $(1))) \
    $(foreach p,$(2), \
        $(eval _pkg := $(p)) \
        $(eval _cond := $(_cond_pre)$(_pkg)) \
        $(if $(call strequal,yes,$($(_cond))),$(_pkg)) \
     ) \
)
endef

# $(1): condition prefix
# $(2): argument key
# $(3): argument value
# $(4): module list
define install-modules-vars
$(strip \
    $(eval _arg_key := $(strip $(2))) \
    $(eval _arg_val := $(strip $(3))) \
    $(if $(_arg_key), \
        $(if $(call strequal,$($(_arg_key)),$(_arg_val)), \
            $(call install-modules-vars-for,$(1),$(4)), \
            $(if $(filter !%,$(firstword $(_arg_val))), \
                $(if $(call strequal,$($(_arg_key)),$(patsubst !%,%,$(_arg_val))),,$(call install-modules-vars-for,$(1),$(4))) \
             ) \
         ), \
        $(call install-modules-vars-for,$(1),$(4)) \
     ) \
)
endef

define uniq
$(if $1,$(firstword $1) $(call uniq,$(filter-out $(firstword $1),$1)))
endef

# $(1): mappings
# $(2): expression
define calculate-sum-flag
$(strip \
  $(eval _out := 0) \
  $(foreach _exp,$(1),$(eval $(_exp))) \
  $(foreach _exp,$(2),$(eval _out := $(shell expr $(_out) + $($(_exp))))) \
  $(_out) \
)
endef

# $(1): target aidl file
# $(2): target function name
define calculate-transaction
$(strip \
    $(shell python $(TOP)/vendor/tyd/build/tools/build_utils.py aidl-transaction $(1) $(2)) \
)
endef

# $(1): target list
# $(2): filter-out list
define filter-copyable
$(filter-out $(addprefix %/,$(2)),$1)
endef

# $(1): source relpath
# $(2): source list
# $(3): baseline source list
define java-package-source-uniq
$(strip \
    $(3) \
    $(foreach f,$(2),$(if $(filter %/$(f:$(strip $1)/%=%),$(3)),,$(f))) \
)
endef
