export FREEME_TOOL_OTADIFF_EXARG='-s vendor/mediatek/proprietary/scripts/releasetools/mt_ota_from_target_files --block'

# $(1:project) $(2:source project)
function setup_project_dependency() {
    local project=$1
    local project_source=$2

    local config="device/tyd/${project}/ProjectConfig.mk"
    local config_source="${3}/ProjectConfig.mk"
    if [ -z "$(get_project_section 'BASED' ${project})" ] ; then
        (cat << EOF) >> ${config}
#
# The followings are generated while "clone project"
# Do not modify
#
TYD_PRODUCT_BOARD = ${project}
TYD_PRODUCT_BASED = ${project_source}

EOF
    else
        local board=$(get_project_section 'BOARD' ${project})
        [[ "${board}" == "${project}" && "$(get_config_val ${config} 'TYD_PRODUCT_BOARD')" != "${project}" ]] && {
            sed -i "s/^TYD_PRODUCT_BOARD\s*=\s*.*/TYD_PRODUCT_BOARD = ${project}/g" ${config}
        }
    fi
}

# $(1:project-source) $(2:project)
# @PLATFORM
function clone_project() {
    local project_source=$1
    local project=$2

    local project_source_dir="device/tyd/${project_source}"
    if [ -n "$(echo ${project_source} | grep /)" ] ; then
        project_source_dir=${project_source}
        project_source=$(basename ${project_source_dir})
    fi
    [ -d "${project_source_dir}" ] || return 1
    echo -n "#### Clone Project ${project_source} to ${project} "

    local project_source_dir_ref_device=${project_source_dir/#device\/}
    echo "-------$PWD--------"
    sh vendor/tyd/build/tools/clone_project.sh $PWD ${project_source_dir_ref_device} ${project_source} ${project}
    setup_project_dependency ${project} ${project_source} ${project_source_dir}

    return 0
}

# $(1:project)
# $1: Sample: v9h v9h62_gb
# @PLATFORM
function remove_project() {
    local project=$1

    [ -n "$project" ] || return 1
    echo -n "#### Remove Project $project "

    local kernel=kernel/msm-4.9/
    local arch=arm64
    local platform

    # device/droi
    local project_dir=device/tyd/${project}
    if [ -d "$project_dir" ] ; then
        #kernel=$(get_config_val "${project_dir}/ProjectConfig.mk" 'LINUX_KERNEL_VERSION')
        #arch=$(get_project_section 'ARCH' ${project})
        #platform=$(get_project_section 'PLATFORM' ${project})

        rm -rf $project_dir
        echo -n '.'
    fi

    # kernel*
    if [ -d "${kernel}" ] ; then
        local kernel_files="
            ${kernel}/arch/${arch}/configs/${project}-perf_defconfig
            ${kernel}/arch/${arch}/configs/${project}_defconfig
        "
        for F in $kernel_files ; do
            [ -e $F ] && rm -rf $F
            echo -n '.'
        done
        unset F
    fi

    echo
    return 0
}

# $(1:project) $(2:project custom bootanimation path) $(3:resolution)
# $2: Sample: droi/v9h/v9h62/v9h62_gb/bootanimation
function merge_bootanimation() {
    local resolution=$3

    local bootanimation_custom=$2
    #[ -d "${bootanimation_custom}" ] || return 0

    local logo_gen="device/qcom/common/display/logo/logo_gen.py"
    if [ -f "${bootanimation_custom}/logo.bmp" ] ; then
        python $logo_gen  ${bootanimation_custom}/logo.bmp
    fi
    if [ -f "${bootanimation_custom}/logo.png" ] ; then
        python $logo_gen ${bootanimation_custom}/logo.png
    fi

    if [ -f "splash.img" ] ; then
        echo "has custom logo"
    else
        cp device/tyd/common/bootanimation/splash.img splash.img
    fi
}


# $(1:project) $(2:config-final) $(3:config-custom) [config-overlay]
function merge_project_config() {
    local project=$1; shift
    local config_final=$1; shift
    local board=$(get_project_section 'BOARD' ${project})
    local platform=$(get_project_section 'PLATFORM' ${project})

    # * basic configs
    local config_custom=$1; shift
    local config_board="device/tyd/${board}/ProjectConfig.mk"
    local config_common="device/tyd/common/ProjectConfig.mk"
    local config_platform="device/tyd/${platform}/ProjectConfig.mk"

    # * alternative configs
    local config_project_inherit
    local config_factory_common
    if [ -f "${config_custom}" ] ; then
        # upstream project config
        local project_inherit=$(get_config_val ${config_board} 'TYD_PRODUCT_BASED')
	echo "project_inherit=$project_inherit"
        config_project_inherit=device/tyd/${platform}/ProjectConfig.${project_inherit}.mk
        echo "config_project_inherit=$config_project_inherit"

        # factory common config
        if [ "$(get_config_val ${config_custom} 'FREEME_PRODUCT_FOR')" = "factory" ] ; then
            config_factory_common="device/droi/common/ProjectFactoryConfig.mk"
        fi
    else
        # Assert cannot reach here
        return -1
    fi

    # * config finally
    python ${TYD_BUILD_HOME}/tools/build_utils.py merge-config \
            ${config_common} \
            ${config_platform} \
            ${config_project_inherit} \
            ${config_board} \
            ${config_custom} \
            ${config_factory_common} \
            --config-overlay $@ \
          > ${config_final}
}

# $(1:project) $(2:config-final)
function merge_board_partition_config() {
    local project=$1; shift
    local config_final=$1; shift

    [ -f 'device/tyd/common/BoardConfig.partition.generator' ] && {
        FREEME_BOARD_CFG_PT_FOR=$(get_config_val ${config_final} 'FREEME_BOARD_CFG_PT_FOR') \
        FREEME_BOARD_CFG_PT_CACHE_SIZE_KB=$(get_config_val ${config_final} 'FREEME_BOARD_CFG_PT_CACHE_SIZE_KB') \
        FREEME_BOARD_CFG_PT_SYSTEM_SIZE_KB=$(get_config_val ${config_final} 'FREEME_BOARD_CFG_PT_SYSTEM_SIZE_KB') \
        FREEME_BOARD_CFG_PT_VENDOR_SIZE_KB=$(get_config_val ${config_final} 'FREEME_BOARD_CFG_PT_VENDOR_SIZE_KB') \
        bash device/droi/common/BoardConfig.partition.generator | tee -a device/droi/$project/BoardConfig.mk
    }
}

# $(1:project) $(2:project custom path) [--force-override|-f]
# @PLATFORM
function merge_preloader_and_kernel_config() {
    local project=$1
    local custom=$2
    shift
    shift

    # lk
#    local lk_target_file="vendor/mediatek/proprietary/bootable/bootloader/lk/project/${project}.mk"
#    if [ -f "$lk_target_file" ] ; then
#        local lk_source_file="$custom/lk.mk"
#        if [ -f "$lk_source_file" ] ; then
#            cp -af $lk_source_file $lk_target_file
#        fi
#    fi

    # kernel
    #local arch=$(get_project_section 'ARCH' ${project})
    local arch=arm64
    #local kernel=$(get_config_val "device/tyd/${project}/ProjectConfig.mk" 'LINUX_KERNEL_VERSION')
    local kernel=kernel/msm-4.9
    local kernel_target_dir="${kernel}/arch/${arch}/configs"
    if [ -d "$kernel_target_dir" ] ; then
        local kernel_target_file_prefix="${kernel_target_dir}/${project}"
        local kernel_source_files="${custom}/perf_defconfig"
        echo "kernel_target_file_prefix=$kernel_target_file_prefix"
        if [ -f "$kernel_source_files" ] ; then
		local kernel_target_file="${kernel_target_file_prefix}-$(basename ${kernel_source_files})"
		echo "kernel_target_file=$kernel_target_file"
	 	cp -af ${kernel_source_files} ${kernel_target_file}
        fi
	kernel_source_files="${custom}/defconfig"
	if [ -f "$kernel_source_files" ] ; then
		local kernel_target_file="${kernel_target_file_prefix}_$(basename ${kernel_source_files})"
	 	cp -af ${kernel_source_files} ${kernel_target_file}
        fi
    fi
}

# $(1:project) $(2:project bsp path) [--force-override|-f]
# @PLATFORM
function merge_bsp() {
    local project=$1
    local bsp_custom=$2
    shift
    shift

    #echo "I'm a stub ^_^"
}

# $(1:project) $(2:project custom config) [--force-override|-f]
# @PLATFORM
function merge_stub_ifneeded() {
    local project=$1
    local config=$2
    shift
    shift

    #echo "I'm a stub ^_^"
}

# $(1:project) [--force-override|-f]
# $1: Sample: [full_]v9h62_gb[-[userdebug|user|eng]]
function breakfast() {
    local project
    if [ "$1" ] ; then
        project=$(get_project_section 'PROJECT' $1)
        shift
    else
        echo "Which product would you like? [v9h62_gb] "
        return 1
    fi
    local model=$(get_project_section 'MODEL' ${project})
    local board=$(get_project_section 'BOARD' ${project})

    local config_custom="tyd/${board}/${model}/${project}/ProjectConfig.mk"
    if [ ! -f "${config_custom}" ] ; then
        echo "Where is the 'ProjectConfig.mk($(dirname ${config_custom}))'"
        return 1
    fi

    # * prepare project
    echo ${project} ${board}
    if [ "${project}" != "${board}" ] ; then
        remove_project ${project}
        clone_project ${board} ${project}
        if [ $? -ne 0 ]; then
            echo "Generate project '${project}' failed."
            return 1
        fi
    fi

    # * merge project config
    local config="device/tyd/${project}/ProjectConfig.mk"
    merge_project_config ${project} ${config} ${config_custom} $@

    # * merge project board config
    #merge_board_partition_config ${project} ${config}

    # * merge bootanimation
    local project_custom_dir=$(dirname ${config_custom})
    local resolution=$(get_config_val ${config} 'BOOT_LOGO')
    merge_bootanimation ${project} "${project_custom_dir}/bootanimation" ${resolution}

    # * merge/override bsp sources
    merge_bsp ${project} ${project_custom_dir} $@
    [ $? -ne 0 ] && return 1

    # * merge/override sources
    merge_override ${project} "${project_custom_dir}/override" $@
    merge_preloader_and_kernel_config ${project} ${project_custom_dir} $@
    merge_override_cts_ifneeded ${project} ${config_custom} $@
    merge_stub_ifneeded ${project} ${config_custom} $@

    return 0
}
