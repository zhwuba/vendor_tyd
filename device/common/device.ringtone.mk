# Ringtone & Audio

# Support Ringtone Audible
FREEME_RINGTONE_AUDIBLE_SUPPORT ?= yes
# Support Dual Sim Ringtone
FREEME_RINGTONE_DUAL_SIM_SUPPORT ?= yes
# Support Message Ringtone
FREEME_RINGTONE_MESSAGE_SUPPORT ?= no
# Support notification Ringtone
FREEME_RINGTONE_NOTIFICATION_SUPPORT ?= yes
# Support Custom Ringtone
FREEME_RINGTONE_CUSTOM_SUPPORT ?= yes

# Config Ringtone
FREEME_CONFIG_AUDIBLE_NOTIFICATION ?= OnTheHunt.ogg
FREEME_CONFIG_AUDIBLE_ALARM ?= Cesium.ogg
FREEME_CONFIG_AUDIBLE_RINGTONE ?= Ring_Synth_04.ogg
FREEME_CONFIG_AUDIBLE_RINGTONE2 ?= Ring_Synth_04.ogg
FREEME_CONFIG_AUDIBLE_MESSAGE ?= pixiedust.ogg
FREEME_CONFIG_AUDIBLE_MESSAGE2 ?= pixiedust.ogg


# -- Configurations
ifeq ($(strip $(FREEME_RINGTONE_AUDIBLE_SUPPORT)),yes)
  ifeq ($(strip $(FREEME_RINGTONE_DUAL_SIM_SUPPORT)),yes)
    FREEME_PRODUCT_PROPERTY_OVERRIDES += ro.vendor.freeme.ringtone_dual_sim=1
    ifneq ($(strip $(FREEME_CONFIG_AUDIBLE_RINGTONE2)),)
      FREEME_PRODUCT_PROPERTY_OVERRIDES += ro.config.ringtone2=$(FREEME_CONFIG_AUDIBLE_RINGTONE2)
    endif
  endif

  ifeq ($(strip $(FREEME_RINGTONE_MESSAGE_SUPPORT)), yes)
    FREEME_PRODUCT_PROPERTY_OVERRIDES += ro.vendor.freeme.ringtone_message=1
    ifneq ($(strip $(FREEME_CONFIG_AUDIBLE_MESSAGE)),)
      FREEME_PRODUCT_PROPERTY_OVERRIDES += ro.config.message=$(FREEME_CONFIG_AUDIBLE_MESSAGE)
    endif
    ifneq ($(strip $(FREEME_CONFIG_AUDIBLE_MESSAGE2)),)
      FREEME_PRODUCT_PROPERTY_OVERRIDES += ro.config.message2=$(FREEME_CONFIG_AUDIBLE_MESSAGE2)
    endif
  endif

  ifeq ($(strip $(FREEME_RINGTONE_NOTIFICATION_SUPPORT)), yes)
    FREEME_PRODUCT_PROPERTY_OVERRIDES += ro.vendor.freeme.ringtone_notification=1
  endif

  ifeq ($(strip $(FREEME_RINGTONE_CUSTOM_SUPPORT)), yes)
    FREEME_PRODUCT_PROPERTY_OVERRIDES += ro.vendor.freeme.ringtone_custom=1
  endif

  FREEME_PRODUCT_PROPERTY_OVERRIDES += \
      ro.config.notification_sound=$(FREEME_CONFIG_AUDIBLE_NOTIFICATION) \
      ro.config.alarm_alert=$(FREEME_CONFIG_AUDIBLE_ALARM) \
      ro.config.ringtone=$(FREEME_CONFIG_AUDIBLE_RINGTONE)
endif
