# fingerprint

ifneq ($(strip $(TYD_FINGERPRINT_SUPPORT)),)
  PRODUCT_PACKAGES += \
    android.hardware.biometrics.fingerprint@2.1-service

  PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.fingerprint.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.fingerprint.xml

  PRODUCT_PROPERTY_OVERRIDES += ro.hardware.fingerprint=$(TYD_FINGERPRINT_SUPPORT)

  ifeq ($(strip $(TYD_FINGERPRINT_TEST_PASSWORD)),yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.tyd.fingerprint_test_pwd=1
  endif

  ifneq ($(wildcard $(TYD_PRODUCT_CUSTOM_HOME)/fingerprint/$(TYD_FINGERPRINT_SUPPORT)),)
    TYD_BOARD_SEPOLICY_DIRS += $(wildcard $(TYD_PRODUCT_CUSTOM_HOME)/fingerprint/$(TYD_FINGERPRINT_SUPPORT)/sepolicy)
    $(call inherit-product-if-exists, $(TYD_PRODUCT_CUSTOM_HOME)/fingerprint/$(TYD_FINGERPRINT_SUPPORT)/fingerprint.mk)
  else ifneq ($(wildcard device/tyd/$(TYD_PRODUCT_DEVICE)/fingerprint/$(TYD_FINGERPRINT_SUPPORT)),)
    TYD_BOARD_SEPOLICY_DIRS += $(wildcard device/tyd/$(TYD_PRODUCT_DEVICE)/fingerprint/$(TYD_FINGERPRINT_SUPPORT)/sepolicy)
    $(call inherit-product-if-exists, device/tyd/$(TYD_PRODUCT_DEVICE)/fingerprint/$(TYD_FINGERPRINT_SUPPORT)/fingerprint.mk)
  else ifneq ($(wildcard device/tyd/$(TYD_PRODUCT_PLATFORM)/fingerprint/$(TYD_FINGERPRINT_SUPPORT)),)
    TYD_BOARD_SEPOLICY_DIRS += $(wildcard device/tyd/$(TYD_PRODUCT_PLATFORM)/fingerprint/$(TYD_FINGERPRINT_SUPPORT)/sepolicy)
    $(call inherit-product-if-exists, device/tyd/$(TYD_PRODUCT_PLATFORM)/fingerprint/$(TYD_FINGERPRINT_SUPPORT)/fingerprint.mk)
  else
    TYD_BOARD_SEPOLICY_DIRS += $(wildcard device/tyd/common/fingerprint/$(TYD_FINGERPRINT_SUPPORT)/sepolicy)
    $(call inherit-product-if-exists, device/tyd/common/fingerprint/$(TYD_FINGERPRINT_SUPPORT)/fingerprint.mk)
  endif
endif
