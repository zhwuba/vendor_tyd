# FactoryTest

# -- Defaults
TYD_FACTORY_TEST_SUPPORT ?= yes

# BCamera [no|yes|yes_yes]
TYD_FACTORY_TEST_CAMERA_BACK ?= yes
TYD_FACTORY_TEST_CAMERA_FRONT ?= yes
TYD_FACTORY_TEST_CAMERA_BACK_VICE ?= no
TYD_FACTORY_TEST_SENSOR_ACCE ?= yes
TYD_FACTORY_TEST_SENSOR_LIGHT ?= yes
TYD_FACTORY_TEST_SENSOR_PROXIMITY ?= yes
TYD_FACTORY_TEST_SENSOR_MAGNETIC ?= yes
TYD_FACTORY_TEST_SENSOR_GYROSCOPE ?= yes
TYD_FACTORY_TEST_SENSOR_HALL ?= no
TYD_FACTORY_TEST_WIFI ?= yes
TYD_FACTORY_TEST_MHL ?= no
TYD_FACTORY_TEST_GPS ?= yes
TYD_FACTORY_TEST_INFRARED ?= no
TYD_FACTORY_TEST_LIGHT_KEYBOARD ?= no
TYD_FACTORY_TEST_LIGHT_ATTENTION ?= yes
TYD_FACTORY_TEST_AUDIO_HIFI ?= no
TYD_FACTORY_TEST_AUDIO_DUALMIC ?= yes
TYD_FACTORY_TEST_AUDIO_DUALSPEAKER ?= no
TYD_FACTORY_TEST_STROBE_BACK ?= yes
TYD_FACTORY_TEST_STROBE_FRONT ?= no
TYD_FACTORY_TEST_FM ?= yes
TYD_FACTORY_TEST_BT ?= yes
TYD_FACTORY_TEST_NFC ?= yes
TYD_FACTORY_TEST_REBOOT ?= no
TYD_FACTORY_TEST_HEADSET ?= yes
TYD_FACTORY_TEST_SHAKE ?= yes
# [home_menu_back]
TYD_FACTORY_TEST_KEYBOARD ?= home_menu_back
TYD_FACTORY_TEST_USB_OTG ?= yes
TYD_FACTORY_TEST_STORAGE_SDCARD ?= yes
TYD_FACTORY_TEST_PRESSURE_SENSOR ?= yes


# -- Configurations
ifeq ($(strip $(TYD_FACTORY_TEST_SUPPORT)), yes)
  PRODUCT_PACKAGES += TYDFactoryTest
  ifeq ($(strip $(TYD_FACTORY_TEST_CAMERA_BACK)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_camera_back=1
  else ifeq ($(strip $(TYD_FACTORY_TEST_CAMERA_BACK)), yes_yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_camera_back=2
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_CAMERA_BACK_VICE)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_camera_back_vice=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_CAMERA_FRONT)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_camera_front=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_SENSOR_MAGNETIC)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_sensor_magnetic=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_SENSOR_ACCE)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_sensor_acce=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_SENSOR_LIGHT)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_sensor_light=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_SENSOR_GYROSCOPE)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_sensor_gyroscope=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_SENSOR_PROXIMITY)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_sensor_proximity=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_SENSOR_HALL)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_sensor_hall=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_MHL)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_mhl=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_GPS)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_gps=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_WIFI)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_wifi=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_INFRARED)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_infrared=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_LIGHT_ATTENTION)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_light_attention=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_LIGHT_KEYBOARD)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro..vendor.tyd.hw.light_keyboard=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_AUDIO_HIFI)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_audio_hifi=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_AUDIO_DUALMIC)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_audio_dualmic=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_AUDIO_DUALSPEAKER)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_audio_dualspeaker=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_STROBE_BACK)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_strobe_back=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_STROBE_FRONT)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_strobe_front=1
  endif
  ifneq ($(strip $(TYD_FACTORY_TEST_KEYBOARD)),)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_keyboard=$(TYD_FACTORY_TEST_KEYBOARD)
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_USB_OTG)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_usb_otg=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_STORAGE_SDCARD)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_storage_sdcard=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_FM)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_fm=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_BT)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_bt=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_NFC)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_nfc=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_REBOOT)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.aging_test_reboot=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_HEADSET)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_headset=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_SHAKE)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_shake=1
  endif
  ifeq ($(strip $(TYD_FACTORY_TEST_PRESSURE_SENSOR)), yes)
    PRODUCT_PROPERTY_OVERRIDES += ro.vendor.tyd.hw_pressure_sensor=1
  endif
  TYD_FACTORY_TEST_JNI_SPEAKER_EXMK =
endif
