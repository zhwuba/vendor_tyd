# Build misc

# User build dexpreopt:
# Enable dex-preoptimization to speed up first boot sequence
ifeq ($(filter yes yesud,$(TYD_BUILD_USER_DEXPREOPT)),)
  WITH_DEXPREOPT := false
  WITH_DEXPREOPT_BOOT_IMG_AND_SYSTEM_SERVER_ONLY := true
else ifeq (yes,$(strip $(TYDBUILD_USER_DEXPREOPT)))
  ifeq (user,$(strip $(TARGET_BUILD_VARIANT)))
    WITH_DEXPREOPT := true
    DONT_DEXPREOPT_PREBUILTS := false
  else
    WITH_DEXPREOPT := false
    WITH_DEXPREOPT_BOOT_IMG_AND_SYSTEM_SERVER_ONLY := true
  endif
else ifeq (yesud,$(strip $(TYD_BUILD_USER_DEXPREOPT)))
  ifneq ($(filter user userdebug,$(TARGET_BUILD_VARIANT)),)
    WITH_DEXPREOPT := true
    DONT_DEXPREOPT_PREBUILTS := false
  else
    WITH_DEXPREOPT := false
    WITH_DEXPREOPT_BOOT_IMG_AND_SYSTEM_SERVER_ONLY := true
  endif
endif

# Product mode
ifeq (cts,$(strip $(TYD_PRODUCT_FOR)))
  PRODUCT_PACKAGE_OVERLAYS := $(wildcard device/tyd/common/cts/overlay) \
    $(PRODUCT_PACKAGE_OVERLAYS)
  PRODUCT_PROPERTY_OVERRIDES += ro.tyd.build.production=cts
else ifeq (factory,$(strip $(TYD_PRODUCT_FOR)))
  PRODUCT_PROPERTY_OVERRIDES += ro.tyd.build.production=factory
endif
