# tyd memory test

TYD_MEMORY_TEST_SUPPORT ?= yes

# -- Configurations
ifeq ($(strip $(TYD_MEMORY_TEST_SUPPORT)), yes)
    PRODUCT_PACKAGES += TydMemoryTester
endif
